import NN
import base64
import platform
systemName = platform.system()
print(systemName, 'HELLOW THIS IS THE CURRENT SYSTEM')
if systemName != 'Linux':
    import matplotlib.pyplot as plt
from datetime import datetime

from NN import DoShit, os, p_geometry, Data_Weight, CheckSum, keyboard
from NN import Metatron

ds = DoShit()
dw = Data_Weight()
pgeo = p_geometry()

class file_filter():
    '''Takes a non TXT document like a video or exe file and turns it into a class object.'''
    def __init__(self,url):
        self.url=url
        self.raw=self._ensure_video(self.url)
        self.contents=self.get_contents()

    def get_contents(self):
        return self.contents 
    def _ensure_video(self,url):
        '''Read the files content.'''
        f=ds.readfrom(url,readmode='rb',cont=1)
        f=' '.join(f)
        if f:
            contents=base64.b64encode(f)
            if contents:
                self.raw=contents
                self.contents=contents
                return contents
            else:   return None
        else:
            return None

    def raw_contents(self):
        '''Return the base 64 encoded content of the file if possible.'''
        if not self.raw and self.url:
            self.raw=self._ensure_video(self.url)
            return self.raw
        else:
            return self.raw

    def _build_video(self,new_filename,b64encoding):
        '''Create a new file from base64 encoded string. Decodes the base64 into bytes on writing.'''
        ds.writeto(new_filename,base64.b64decode(b64encoding),mode='wb',m=1)

    def build_video(self,new_filename):
        '''Create a new file from base64 encoded string. Decodes the base64 into bytes on writing.'''
        self._build_video(new_filename,self.raw)

    def save_encoding_to_file(self,filename):
        '''Save the base64 string to a file. Doesnt decode base64.'''
        ds.writeto(filename,self.raw,mode='wb',m=1)

class file_object():
    '''This is a mess and shoud be integrated with filesystem class'''
    def __init__(self,url):
        self.name,self.permissions=url,None
        self.size,self.last_access = None,None
        self.last_modified,self.type=None,None
        self.content,self.timestamp=None,datetime.now()
        self.get_file_info(url)

    def get_file_info(self, mydir):
        '''Returns information on a file or folder'''
        g=os.stat(mydir)
        if DoShit.is_file(mydir):
            url=ds.break_fileurl(mydir)
            file_type=url[-1]
            content = file_filter(self.name).raw
        elif DoShit.is_folder(mydir):
            file_type,content='Folder',''
        self.name, self.permissions,self.size= mydir,g.st_mode,g.st_size
        self.last_access,self.last_modified=g.st_atime,g.st_mtime
        self.type,self.content=file_type,content

def files_to_block(filelist):
    '''Gather all the file information for a list of files.'''
    return [file_object(file).__dict__ for file in filelist]

def find_metadata(filelist):
    '''Gather all the file information for a list of files.'''
    if type(filelist) == list:
        return [file_object(file).__dict__ for file in filelist]
    elif type(filelist) == str:
        return file_object(file).__dict__



class FileSystem:
    '''Class for statistical analysis of directories and files.'''
    def get_all_filenames(self, mypath):
        '''Returns all files only fonud within a directory.'''
        if DoShit.file_exist(mypath):
            onlyfiles = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
            return onlyfiles
        else:
            print('No file found!')
            return None

    def get_all_dirs(self, directory):
        '''Retuns all folders only found within a directory'''
        g = [x[0] for x in os.walk(directory)]
        print('done')
        return g

    def get_current_dir(self):
        '''Return the current working directory'''
        g = os.getcwd()
        return g

    def get_dir_contents(self, mydir):
        '''Return all files and folders within a directory.'''
        g = os.listdir(str(mydir))
        f = []
        for x in g:
            f.append(ds.multi_replace(mydir+x,('\\'),'/'))
        return f

    def get_size(start_path = '.'):
        '''Get the size of a specified folder'''
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(start_path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                # skip if it is symbolic link
                if not os.path.islink(fp):
                    total_size += os.path.getsize(fp)
            return total_size

    def get_file_info(self, mydir):
        return file_object(mydir)

    def convert_date(self, timestamp):
        '''Convert a unix timestamp to human readable date and time.'''
        d = datetime.fromtimestamp(timestamp)
        formated_date = d.strftime('%d %b %Y')
        return formated_date

    def get_files(self, mydir):
        '''Get all the files in a directory.'''
        dir_entries = os.scandir(mydir)
        file_description = {}
        directory = [] 
        for entry in dir_entries:
            if entry.is_file():
                info = entry.stat()
             #   print(f'{entry.name}\t Last Modified: {self.convert_date(info.st_mtime)}')
                try:
                    if entry.name:
                        file_description['FILENAME'] = entry.name
                    if info.path:
                        file_description['PATH'] = info.path
                except AttributeError:
                    pass
                if info.st_mtime:
                    file_description['LAST MODIFIED'] = self.convert_date(info.st_mtime)
                if info.st_size:
                    file_description['FILESIZE'] = info.st_size
                try:
                    if info.st_birthtime:
                        file_description['FILE CREATED'] = info.st_birthtime
                except AttributeError:
                    pass
                try:
                    if info.st_type:
                        file_description['FILETYPE'] = info.st_type
                except AttributeError:
                    pass
                try:
                    if info.st_creator:
                        file_description['FILE AUTHOR'] = info.st_creator
                except AttributeError:
                    pass
                directory.append(file_description)
            file_description = {}
        return directory

    def _delete_dir(self, directory):
        '''Removes an entire folder from directory'''
        if DoShit.file_exist(directory):
            os.rmdir(directory)
            print('Directory Deleted')
        else:
            pass

    def _delete_file(self, myfile):
        '''Removes a single folder from a directory.'''
        if DoShit.file_exist(myfile):
            os.remove(myfile)
            print('File Deleted')
        else:
            pass

    def convert_to_usable_link(self, plain_string):
        pass

    def get_folder_size(self, dir):
        '''Get the size of a file or folder'''
        if DoShit.file_exist(dir):
            total = 0
            try:
                for entry in os.scandir(dir):
                    if entry.is_file():
                        total += entry.stat().st_size
                    elif entry.is_dir():
                        total += self.get_folder_size(entry.path)
                return total
            except NotADirectoryError:
                return os.path.getsize(dir)
            except PermissionError:
                return 0
            return total
        else:
            print('File not found or file does not exist.')
            return 0
    
    def format_size(self, b, factor=1024, suffix='B'):
        '''Formats size from bit value to specified data measurement'''
        if b:
            for unit in ['', 'K','M','G','T','P','E','Z']:
                if b < factor:
                    return f'{b:.2f}{unit}{suffix}'
                b /= factor
            return f'{b:.2f}Y{suffix}'
        else:
            return 0


class SystemButler(FileSystem):
        def __init__(self, directory=None):
            '''Class that allows for even simpler file system traversal with simple commands such as show_files(), you can view all the files and folders within a current direcotry'''
            FileSystem.__init__(self)
            if not directory:
                name = self.get_current_dir()
            elif directory:
                name = directory
            if name:
                self.current_dir = name
                self.dir         = self.current_dir
                self.directory   = self.current_dir
                self.dir_files   = self.get_dir_contents(self.current_dir)
            self.focus_file = None

        def show_files(self):
            for x in self.dir_files:
                print(str('\"'+x+'\"'))
            print(self.current_dir+' is the current working directory.')

        def file_focus(self, file_to_focus):
            self.focus_file = file_to_focus
            print(self.focus_file+', has been selected.')
            
        def get_file_index(self, file_to_find):
            p = self.dir_files.index(str(file_to_find))
            print(p)
            return p

        def change_current_dir_to(self, directory):
            self.current_dir = directory
            self.dir_files = self.get_dir_contents(self.current_dir)
            print('Current directory updated to, ', directory)

        def pack(self,dest_filename):
            for x in self.dir_files:
            #    print('originnal:',x)
                MK = str(x).replace('\\','/')
            #    pyt.authorize(MK)
                ds.writeto(str(dest_filename), MK, 'a',m=1)
            print('{} files have been copied from \"{}\" to \"{}\"'.format(str(len(self.dir_files)), str(self.current_dir), str(dest_filename)))
        
        def get_clean_files(self):
            return ds.clean_files(self.dir_files)



        
def random_color(stratify=0):
    wheel = list(range(0,256))
    r,g,b,a = pgeo.random_selection(wheel),pgeo.random_selection(wheel),pgeo.random_selection(wheel),pgeo.random_selection(wheel)
    if stratify:
        r/=255
        g/=255
        b/=255
        a/=255
    print(r,g,b,a)
    return (r,g,b,a)

def plot_pie(sizes, names, show=1, save_image=None):
    if systemName != 'Linux':
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
        colors=[random_color(1) for r in range(len(names))]
        patches = plt.pie(sizes, labels=names,colors=colors, startangle=90, autopct=lambda pct: f"{pct:.2f}%")

        plt.legend(names, loc=0, bbox_to_anchor=(0.11,0.95))
        #porcent = 100.*sizes/sum(sizes)
        #ax.legend(sizes,ds.clean_files(names), loc='center left', bbox_to_anchor=(-0.1, 1.), fontsize=8)
       # ax.title('Sizes')
        if save_image:
            plt.savefig(fname=str(save_image),format='png',dpi=300, bbox_inches='tight') 
        if show:
            plt.show()
    else:
        for x, y in zip(sizes, names):
            print(x,y)

def directory_pieChart(directory=None, plot=1, logto=None,saveplot=None,delete_empty=False):
    sb = SystemButler()                    #interntal filesystem explorer
    if directory == None:                   #if no home dircetory given 
        io = input('Enter a directory:')    #then ask the user for a direcotry
        folder_path = io                    
    else:                                   #otherwise use directory given
        folder_path = str(directory)
    directory_sizes,names,empty = [],[],[]
    empties = 0
    if DoShit.file_exist(folder_path):
        for directory in os.listdir(folder_path):       #for each item in directory
            directory = os.path.join(folder_path, directory)    #append new file/folder to current directory
            directory_size = sb.get_folder_size(directory)      #get the size of the file or folder
            if directory_size == 0:           #if the file or folder is empty then skip to next file
                if delete_empty:
                    empty.append(directory)
                    if os.path.isfile(directory):
                        os.remove(directory)
                        empties+=1
                    elif os.path.isdir(directory):
                        try:
                            os.rmdir(directory)
                        except:
                            pass
                continue
            directory_sizes.append(directory_size)      ##add file/folder size to sizes list if not empty
            names.append(directory)#+": "+sb.format_size(directory_size)))   #
            print('{} - {}'.format(directory, sb.format_size(directory_size)))
        print("Total directory size: {}".format(sb.format_size(sum(directory_sizes))))
#    except FileNotFoundError:
    else:
        print('File or Directory Not Found') 
    if plot == 1 or saveplot:
        plot_pie(directory_sizes, names,plot,saveplot)
    if logto:
        for fn in names:
            ds.writeto(logto,fn,'a',m=1)
    return names, directory_sizes, empty
    #return names

def get_file_names_with_strings(str_list):
    full_list = NN.os.listdir('/home/tberry/Documents/')
    final_list = [nm for ps in str_list for nm in full_list if ps in nm]
    return final_list

import glob
def search(directory=None, search_term='*'):
    '''directory=Directory in which to search for files,
        search_Term=If searching for files by filetype use *.filetype otherwise
        use "*" to fill in the missing part of your query so that partial strings are possible. Implements iglob.'''
    #directory = '/home/tberry/Documents/PythonScripts/'
    if directory:
        #print(directory+search_term)
        i = list(glob.iglob('{}/{}'.format(directory,search_term)))
        for x in i:
            print(x)
        return i

def pack(file_list, destination):
    '''Pack a list of strings to a file.'''
    for x in file_list:
        ds.writeto(destination, x, 'a', 1)
    print('complete')

def find_pack(directories,searchterm:str='*',destination:str='.txt',only_use_filenames=False,overwrite=False):
    if overwrite:
        DoShit().writeto(destination,'','w',1,mute=True)
    if type(directories) in (tuple, list):
        for d in directories:
            files=search(d,searchterm)
            if only_use_filenames:
                f=[]
                for file in files:
                    bfn=DoShit().break_fileurl(file)
                    f.append(''.join([bfn[1],bfn[-1]]))
                files=f
            pack(files,destination,1)
        return
    elif type(directories) == str:
        files=search(directories, searchterm)
        pack(files,destination,1)
        return
    else:
        print('Directories must be of type str, tuple, or list. Type {} was used'.format(type(directories)))
        return
def search2(directory=None, search_term=''):
    '''Searches a directory explicitly withouth any other modules. Sloppy.'''
    i = directory_pieChart(directory)
    li=[]
    for x in i:
        if str(search_term) in str(x):
            a = x.split(':')
            li.append(str(directory)+str(a[0]))
    return li

class proto_slider():
    def __init__(self, start=0, target_min=0, target_max=10):
        self.target_min=int(target_min)
        self.target_max=int(target_max)
        self.base=int(start)
        self.median = (self.target_max+self.target_min)/2

    def increase(self, increment=1):
        if self.base >= self.target_min and self.base < self.target_max:
            self.base+=increment
        if self.base>=self.target_max:
            self.base=self.target_max
            return self.base
        else:
            return self.base

    def decrease(self, increment=1):
        if self.base >= self.target_min and self.base < self.target_max:
            self.base-=increment
        if self.base<self.target_min:
            self.base=self.target_min
        return self.base

#ps = proto_slider(target_max=50)
#for x in range(20):
#    if ps.increase(2):
#        print(ps.base)

from random import random

layers=[]
def layer0(size=20):
    '''Fill a dictionary. {float:list[empty]}'''
    A={}
    for x in range(size):
        li = round(pgeo.random_float(), 9)
        if li not in A:
            A[li]=[]
    return A

def layerX(layer):
    B,c={},0
    for x in layer:
        c+=x
        if c not in B:
            B[c]=[]
    return B

def _extend_layer_(layers,amount):
    for x in range(int(amount)):
        B = layerX(layers[-1])
        layers.append(B)
    return layers

def _collapse_layers_(layers):
    for x in layers:
        layers[0].update(x)
    return layers[0]

def _get_layer_range(layers,low, high):
    layers = _collapse_layers_(layers)
    for x in layers:
        if x > low and x < high:
            print(x)

def find_all(filetype, path='C:\\'):
    '''Scans entire directory recursively for a specified filetype.'''
    dir_path = path#os.path.dirname(os.path.realpath(__file__)) 
    L=[]
    tpl=len(filetype)
    COUNT=0
    for root, dirs, files in os.walk(dir_path): 
        for file in files:  
            # change the extension from '.mp3' to  
            # the one of your choice.
            if type(filetype) == str:
                if file.endswith(str(filetype)) or str(filetype) in file: 
                    L.append(root+'/'+str(file))
                    print('{} - {}/{}'.format(COUNT,root,file))
                    COUNT+=1
            if type(filetype) in (list,tuple):
                if tpl==2 or tpl == 3:
                    if file.endswith(str(filetype[0])): 
                        L.append(root+'/'+str(file))
                        print('{} - {}/{}'.format(COUNT,root,file))
                        COUNT+=1
                    if file.endswith(str(filetype[1])): 
                        L.append(root+'/'+str(file))
                        print('{} - {}/{}'.format(COUNT,root,file))
                        COUNT+=1
                if tpl == 3:
                    if file.endswith(str(filetype[2])): 
                        L.append(root+'/'+str(file))
                        print('{} - {}/{}'.format(COUNT,root,file))
                        COUNT+=1
    print('{} files with extension {} found within {}'.format(COUNT, filetype, path))
    return L


class Head:
    pass

class Scribe:
    '''Subclass for designing, generating , and packing dialogue trees and preprogrammed conversations.'''
    def __init__(self):
        self.characters=[]
        self.script_title = None
        self.all_social_interations = {'friendly':['Give item', 'Tell joke', 'Compliment', 'Small Talk', 'Be flirty'],
                                        'rude':['Insult', 'Push', 'Tease', 'Punch'],
                                        'demanding':['Forced Hscene', 'Carry person', 'Strip person naked', 'Rob item', 'Forced administration'],
                                        'sneaky':['Pickpocket', 'Use Weapon'],
                                        'neutral':['Talk about family', 'talk about friends', 'talk about likes', 'talk about dislikes', 'Smell person', 'Sell item', 'Ask to follow', 'Ask about sexuality', 'Ask about health']}
                                #if fighting skill then add stealth takedowns
        self.mock_inventory = ['cigarettes', 'crack']
        self.jokes = [{'joke':'Why did the chicken cross the road?',
                       'punchline':'To get to the other side!'},
                       {'joke':'What did the tomato say to the other tomato during a race?',
                        'punchline':'Ketchup!'},
                       {'joke':'What kind of cheese isn\'t, yours?',
                        'punchline':'Nacho cheese'},
                       {'joke':'Why are frogs so happy?',
                        'punchline':'Because they eat whatever bugs them'}]
    
    def commands(self):
        e = input()
        add_dialogue = ('add dialogue', 'dialogue')

    def debug_controller(self):
        '''Console dialogue selections'''
        print('Press a button to open dialogue tree: ')
        print('A - FRIENDLY INTERACTIONS S - RUDE/MEAN INTERACTIONS D - DEMANDING INTERACTIONS F - SNEAKY INTERACTION  G - NEUTRAL INTERACTIONS')
        b = True
        while b:
            a  = input()
            if a in ('A', 'a'):
                print('FRIENDLY INTERACTIONS')
                input('Give them')
                print()
            if a in ('S','s'):
                print('RUDE/MEAN INTERACTIONS')
                print()
            if a in ('D','d'):
                print('DEMANDING INTERACTIONS')
                print()
            if a in ('F','f'):
                print('SNEAKY INTERACTIONS')
                print()
            if a in ('G','g'):
                print('NEUTRAL INTERACTIONS')
                print()
            if keyboard.is_pressed('esc'):
                b = False
    #dialogue tree manager
    #enter dialogue option then program responses
    #able to pick a dialoge option to start a conversation
    #CONVERSATION TITLE
    #CONVERSATION ENTRIES
    #CONVERSATION RESPONSES
        


#img = round_rectangle((720,480), (200,200,130,130), None)
#img.show()

#p=draw_polygon(points)
#p.show()
#img= Images.blur(img)
#for x in img:
#    x.show()

#im=draw_arc((0,0,175,200),angleB=45)
#im.show()

#img.show()
def Transpose(base_img,):
    pass

#image = Images.d_load_image('E:/VIDEO/1608428868363.jpg')
#image[0].paste(p,(0,0),mask=p)
#image[0].show()
#im = Images.blur(image[0])
#im = Images.blur(im[0])
#im=Images.sobel_edge_detection(image[0])
#im =Images.image_from_array(im)
#im.show()
#for x in im:
#    x.show()

#im = Image.open('/home/tberry/Downloads/society-simulator-master/core/usethese/A/adult/must_be_refined/hair1.png')
#im.show()

#new_im.save('test.jpg')
    
#Images.convert_image(im[0], 'ari_head')
#print()
#print(f)
#o = Images.generate_static_from_image_colors(Assets.ari_head)
#h = Images.generate_shader_from_static(i)
#g = Images._np_array(o)
#s = len(g)
#print(s)
#o = Images.image_from_array(g)

#NUMBERS TO COLOR

def mat_add(code_one: int, code_two: int):
    matrix = []
    code_one = str(code_one)
    code_two = str(code_two)
    for x in code_one:
        for y in code_two:
            matrix.append(int(x)+int(y))
    print(matrix)
    return matrix

def mat_sub(code_one: int, code_two: int):
    matrix = []
    code_one = str(code_one)
    code_two = str(code_two)
    for x in code_one:
        for y in code_two:
            matrix.append(int(x)-int(y))
    print(matrix)
    return matrix

def mat_mult(code_one: int, code_two: int):
    matrix = []
    code_one = str(code_one)
    code_two = str(code_two)
    for x in code_one:
        for y in code_two:
            matrix.append(int(x)*int(y))
    print(matrix)
    return matrix

def mat_div(code_one: int, code_two: int):
    matrix = []
    code_one = str(code_one)
    code_two = str(code_two)
    for x in code_one:
        for y in code_two:
            try:
                matrix.append(int(int(x)/int(y)))
            except ZeroDivisionError:
                matrix.append(0)
    print(matrix)
    return matrix

class Host_File:
    def __init__(self, w_directory=None, filename=None, ext='.txt'):
        self.directory = w_directory
        self.filename = filename + ext
        self.ext = ext
        self.fdir = self.directory + '\\' +self.filename

class Register:
    def __init__(self, title=None, parent_filename=None, directory=None, host_file=None, host_xml=None, file_extension='.txt', overwrite_host=0):
        meta = Metatron()
        self.ds = NN.DoShit()
        self.sb = SystemButler()
        self.type = 0
        if parent_filename == None and title == None:
            self.parent_file = str(0)
            t = meta.hash256(self.parent_file)
            #if title == None:
            self.title = t
        elif title and parent_filename == None:
            self.title = title
            self.parent_file = str(0)
        elif title == None and parent_filename:
            self.title = meta.hash256(parent_filename)
            self.parent_file = parent_filename
        elif title and parent_filename:
            self.title = title 
            self.parent_file = parent_filename
        
        if directory == None:
            self.directory = self.sb.current_dir
        elif directory != None:
            self.directory = directory
        self.host_file = Host_File(self.directory,self.title, file_extension)
        if NN.DoShit.file_exist(self.host_file.fdir) == False:
            self.ds.writeto(self.host_file.fdir, '', m=1)
            self.using = 'fresh'
        elif NN.DoShit.file_exist(self.host_file.fdir):
            self._overwrite_host(mode=overwrite_host)
            self.using = None
                
        self.filename = self.host_file.filename
        self.fdir = self._current_file()
        self.host_xml = None
        self.parent_file = None
        self.child_file = None
        self.contents = self.get_contents()
        
    def _host_file_exists(self):
        #self.directory
        pass

    def get_contents(self, m=1):
        contents = self.ds.readfrom(self.host_file.fdir, mode=m)
        if len(contents) > 0:
            cont = []
            for x in contents:
                if '\n' in x:
                    i = contents.index(x)
                    n = x.replace('\n', '')
                    contents[i] = n
            if contents[0] == '':
                contents[0]=str(0)
            return contents
        elif len(contents) == 0:
            return contents
     
    def reinitiate(self, title):
        pass

    def _overwrite_host(self, mode=0):
        '''Mode 0: ASK THE USER IF THEY WISH TO REINITAIATE THE FILE WHEN IT IS CALLED,
           MODE 1: KEEP THE OLD FILE,
           MODE 2: OVERWRITE THE FILE,
           MODE 3: IF THE FILE EXISTS THEN KEEP OLD, IF NOT THE CREATE A NEW FILE'''
        if mode == 0:
            print('File '+self.title+' already exists within directory "'+self.host_file.fdir+'"')
            print('Press Y if you would like to start a new registry and overwite the old?')
            print('Press N to keep the old registry')
            helpme = input()
            while helpme not in ('Y','y','N','n'):
                helpme = input()
                if helpme in ('Y','y'):
                    self.ds.writeto(self.host_file.fdir, '', m=1)
                    self.using = 'fresh'
                elif helpme in ('N','n'):
                    self.using = 'old'
            else:
                if helpme in ('Y', 'y'):
                    self.ds.writeto(self.host_file.fdir, '', m=1)
                    self.using = 'fresh'
                elif helpme in ('N', 'n'):
                    self.using = 'old'
        elif mode == 1:
            self.using = 'old'
        elif mode == 2:
            self.ds.writeto(self.host_file.fdir, '', m=1)
            self.using = 'fresh'
        elif mode == 3:
            if NN.DoShit.file_exist(self.host_file.fdir):
                self.using = 'old'
            elif NN.DoShit.file_exist(self.host_file.fdir) == False:
                self.using = 'fresh'
                self.ds.writeto(self.host_file.fdir, '', m=1)


    def append(self, content, mute=False):
        self.ds.writeto(self.host_file.fdir, content, mode='a+', m=1, mute=mute)
        if mute == False:
            print(content,', has been added to file ,',self.host_file.fdir)

    def append_column(self, content):
        contents = self.get_contents()
        #for x in contents:
        pass

    def jappend(self, content_one=None, content_two=None, content_three=None, mute=False):
        l = []
        contain_dict = False
        if content_one:
            if type(content_one) == dict:
                contain_dict = True
                l.append(content_one)
            elif type(content_one) == list:
                for x in content_one:
                    if type(x) == dict:
                        contain_dict =True
                l.append(content_one)
        if content_two:
            if type(content_two) == dict:
                contain_dict = True
                l.append(content_two)
            elif type(content_two) == list:
                for x in content_two:
                    if type(x) == dict:
                        contain_dict =True
                l.append(content_two)
        if content_three:
            if type(content_three) == dict:
                contain_dict = True
                l.append(content_three)
            elif type(content_three) == list:
                for x in content_three:
                    if type(x) == dict:
                        contain_dict =True
                l.append(content_three)

        if contain_dict == False and len(l) > 0:  
            F = NN.json.dump(l) 
            self.append(F, mute)
        if contain_dict == True and len(l) > 0:
            F = NN.json.dumps(l)
            self.append(F, mute)
      
    def _current_file(self):
        f = self.host_file.fdir
        return f
    #name
    #geolocation
    #skills

class register_manager():
    '''Used to manage registers. Create/Import registers,
     keep track of register file urls and contents'''
    def __init__(self, urls = []):
        self.urls = urls
        self.register_list = []
        self.selected_i = 0
        self.selected_url = None
        self.contents = {}
        self.selected_contents = []
        if len(self.urls) > 0:
            for x in self.urls:
                reg = self._load_register(x)
                self.register_list.append(reg)
            self.selected_i = 0
            self.selected_url = self.urls[self.selected_i]
            self.contents = self.dict_contents()
            if self.selected_url:
                self.selected_contents = self.find_and_load_contents(self.selected_url)

    def _unload_register(self, url):
        if url in self.urls:
            print('url',url,'url_list',self.urls)
            self.urls.remove(url)
            print('url',url,'url_list',self.urls)
            if url in self.contents:
                print(self.contents)
                del self.contents[url]
                print(self.contents)

            return self.urls

    def _load_register(self, url):
        e = ds.break_fileurl(url)
        if e:
            reg = Register(e[1], directory=e[0], file_extension=e[2], overwrite_host=3)
            return reg

    def load_register(self, url):
        reg = self._load_register(url)
        self.urls.append(url)
        self.register_list.append(reg)
        content = self.read_contents()
        if content:
            self.selected_contents = content[url]
    #    print('contents',self.selected_contents)
    
    def get_loaded_contents(self, url):
        if url in self.contents:
            content = self.contents[url]
            self.find_selection(url)
            self.selected_contents = content
            return content
        elif url not in self.contents:
            return []
            
    def dict_contents(self):
        ss = []
        if len(self.register_list) > 0:
            for x in self.register_list:
                #print('filename',x.filename)
                contents = x.get_contents()
                if len(contents) > 0:
                    ss.append(contents)
                elif len(contents) == 0:
                    ss.append([])
                #    print('ss',ss)
            if len(ss) == len(self.urls):
                M = NN.DoShit.get_dict(self.urls, ss)
                if M:
                    return M
        elif len(self.register_list) == 0:
            return {}

    def read_contents(self):
        if len(self.urls) == len(self.register_list):
            M = None
            while M == None: 
                M = self.dict_contents()
            if M:
                self.contents = M
                return M
        
    def find_selection(self, url):
        if url in self.urls:
            i = self.urls.index(url)
            self.selected_i = i
            self.selected_url = url
            return int(i)

    def find_and_load_contents(self, url):
        if url:
            self.read_contents()
            i = self.find_selection(url)
            if i and self.contents:
                if int(i) and len(self.contents) >= int(i):
                        contents = self.contents[str(url)]
                        return contents

    def load_from_selection(self):
        pass
    
    def new_register(self, title=None, parent_file=None, directory=None, host_file=None, host_xml=None, file_extension='.json', w_mode=3):
        if directory == None:
            directory = ds.current_dir()
        register = Register(title=title,parent_filename=parent_file,directory=directory,host_file=host_file,host_xml=host_xml,file_extension=file_extension,overwrite_host=w_mode)
        if register:
            self.urls.append(str(register.fdir))
            self.register_list.append(register)
            m = self.read_contents()
            contents = self.get_loaded_contents(str(register.fdir))
            contents = self.selected_contents
            #print('contents within new registe',contents)
            if len(contents) >= 0:
                return {'reg':register, 'cont':contents, 'url':str(register.fdir),'url_li':self.urls, 'reg_li':self.register_list}


#ds = NN.DoShit()
#ff = ds.readfrom('C:\\Users\\Caelum\\Desktop\\structure\\arrays.txt', mode=1)

class Wesker:
    '''Wesker is a special type of register that houses information to be used for thought retention'''
    def __init__(self, directory=None, title='0', ext='.txt', parent_file=None):
        self.register = Register(directory=directory, title=title, parent_filename=parent_file, )
        self.basic_string = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        #self.register.append(self.basic_string)
        initiated = False
        if initiated == False:
            self._initiate()
            initiated = True
        t = {'to':None,
            'from':None,
            'signature':None}
        #c = self.register.get_contents()
        #print(c)
    def _initiate(self):
        r = self.register.get_contents()
        if len(r) == 0 or r == [''] or r[0] != '':
            for x in self.basic_string:
                self.register.ds.writeto(self.register.host_file.fdir, x, mode='a+', m=1, mute=True)
            r = self.register.get_contents()
            if len(r) == len(self.basic_string):
                print('Registry Successfully Initiated And Ready For I/O')
            else:
                print('Registry Failed to Intiate Properly -- FileLength Error')
                #self.register.ds.writeto(self.register.host_file.fdir, '', m=1)
                #for x in self.basic_string:
                #    self.register.ds.writeto(self.register.host_file.fdir, x, mode='a+', m=1, mute=True)
                #print('Registry Failed to Intiate Properly -- FileLength Error')
        elif len(r)>1:
            print('Registry is must be empty')
            print('Running initiation sequence...')
            self.register._overwrite_host()

    def _rewrite(self):
        self.register.ds.writeto(self.host_file.fdir, '', m=1)

    def get_contents(self):
        pass

class Solution:
    def trap(heights:list):
        n = len(heights)
        l, r = [0] * (n + 1), [0] * (n + 1)
        ans = 0
        for i in range(1, len(heights) + 1):
            l[i] = max(l[i - 1], heights[i - 1])
        for i in range(len(heights) - 1, 0, -1):
            r[i] = max(r[i + 1], heights[i])
        for i in range(len(heights)):
            ans += max(0, min(l[i + 1], r[i]) - heights[i])
        return ans

    def longestConsecutive(nums):
        longest_streak = 0
        for num in nums:
            current_num = num
            current_streak = 1
            while current_num + 1 in nums:
                current_num += 1
                current_streak += 1
            longest_streak = max(longest_streak, current_streak)
        return longest_streak

    def isInterleave(s1=str(0), s2=str(0), s3=str(0)):
        r, c , l =len(s1), len(s2), len(s3)
        if r + c != l:
            return False
        
        def dfs(x, y, visited):
            if x + y == l:
                return True
            if (x, y) not in visited:
                visited.add((x, y))
                valid = (x < r and s1[x] == s3[x+y] and (x+1, y) not in visited and dfs(x+1, y, visited) 
							or y < c and s2[y] == s3[x+y] and (x, y+1) not in visited and dfs(x, y+1, visited))
                return valid
        return dfs(0,0, set())


class user_card():
    '''Started creation of a user database'''
    def __init__(self, first_name='', last_name='', birthday=None, workplace_history=[], education_history=[], current_location='',hometown='', locations_visited=[],favorite_locations=[],phone_number='', card=None, photos=[], videos=[], clips=[], email='', gender='',orientation='',relationship='',sponsored=[],sponsoring=[]):
        self.card = card
        if self.card == None and first_name and last_name and birthday:  
            self.card = {'first name':first_name, 'last name':last_name,      
                        'birthday':birthday,
                        'workplace':workplace_history, 'education':education_history,            
                        'current Location':current_location,'hometown':hometown,              
                        'locations visited':locations_visited,'favorite locations':favorite_locations,     
                        'phone number':phone_number,          
                        'photos':photos,'videos':videos,'clips':clips,   
                        'email':email,             
                        'gender':gender,'orientation':orientation,'relationship Status':relationship,
                        'sponsored By':sponsored,'sponsoring':sponsoring,
                        'likes':{'audio':[],'visuals':[],       
                              'sources':[],'food':[],'games':[]},         
                        'linked Accounts':[],'wallets':[],  
                        'rating':[],                             
                        'activities':[],'skills':[],'collectibles':[],
                        'log':[],
                        'timestamp':'',
                        'public_key':''}
        elif card:
            self.card = card
    
    def hello():
        pass

    
#class user_register():
#st = 'hello'
#t = 'bitch'
#s = Solution.isInterleave(st, t, st+t)
#print(s)
#print()
#print(f)
#o = Images.generate_static_from_image_colors(Assets.ari_head)
#h = Images.generate_shader_from_static(i)
#g = Images._np_array(o)
#s = len(g)
#print(s)
#o = Images.image_from_array(g)

#NUMBERS TO COLOR
#mat_div(15616702652762509511591602609612313816112701621313, 5000)
#ds = DoShit()
#                       number O loop

#L = ds.readfrom(Assets.game_directory+'ari_percent.txt')
#L = NN.json.loads(L)
#k = len(L)
#print(k)
#print(L[0])
#color_mid = (128.0, 128.0, 128.0)

#r=color_mid[0]
#g=color_mid[1]
#b=color_mid[2]
#lighter = []
#darker = []
#mid_tones = []
#for x in L:
#    red = x['color'][0]
#    green = x['color'][1]
#    blue = x['color'][2]
#    if red >= r and green >= g and blue >= b:
#        lighter.append(tuple(x['color']))
#        continue
#    elif red <= r and green <= g and blue <= b:
#        darker.append(tuple(x['color']))
#        continue
#    else:
#        mid_tones.append(tuple(x['color']))
    
#s = len(darker)
#l = len(mid_tones)
#k = len(lighter)
#print(s+l+k)
#p = (darker, mid_tones, lighter)  

#print(p[0])
#print(tuple(color_mid))
#rint(L)
#it = 0

#for x in p[2]:
#    if x['percent'] <= 0.01:
#        e = Images.calculate_neighbors(x['color'], 25)
        #for y in e:
#    i = Images.make_transparent(i, x)
#    print(str(it)+'/'+str(k))
#    it+=1

#L = Images.calculate_percentages(i)
#for x in L:
#    if x['percent'] <= 1:
#        print(x)
#        e = Images.calculate_neighbors(x['color'], 25)
#        for y in e:
#            i = Images.make_transparent(i, y)

#perhaps we could somhow measure the distances between dark(0,0,0,255) and light(255, 255, 255, 255) and group them based on how far they are apart and also measure the percentage of the of pixels
#i.show()

#i = o[0]
#k = 1
#for x in range(0,15):
#    try:
#        for y in p[k]:
#            print(y['color'])
#            i = Images.make_transparent(i, y['color'])
#        k +=1
#        print(k)
#    except IndexError:
#        pass
#i.show()
#g = np.array(o[0])

#g = Images._replace_pixels(o[0], (230,229,227), (0,0,0,0))
#g = Images._replace_pixels(g, (232,228,225), (0,0,0,0))
#print(g)
#for x in g:
#    print(x)
#i = Images.image_from_array(g)
#g.show()

#@jit(nopython=True)
#def monte_carlo_pi(nsamples):
#    acc = 0
#    for i in range(nsamples):
#        x = random.random()
#        y = random.random()
#        if (x ** 2 + y ** 2) < 1.0:
#            acc += 1
#    return 4.0 * acc / nsamples

#d = monte_carlo_pi(100)
#print(d)

#@jit(nopython=True)

class denc():
    '''Outdated encoder/decoder, use d_enc.py for lates version of denc class'''
    def strtobin(string, is_list=False):
        if type(string) == str:
            e = ' '.join(format(ord(x), 'b') for x in string)
            return e
        elif type(string) in (int, float):
            string = str(int(string))
            e = ' '.join(format(ord(x), 'b') for x in string)
            return e
        elif type(string) in (list, tuple):
            ds = DoShit()
            try:
                string = str(sum(string))
            except TypeError:
                string = str(string)
            string = ds.multi_replace(string, remove_items=[',','[',']','(',')', '\'', ' '])
            print(string)
            e = ' '.join(format(ord(x), 'b') for x in string)
            return e

    def bit_filter(binary_array):
        jj = []                                                                    
        for x in binary_array:                                                 #we need to remove the -0b and 0b trash from the array so we filter
            if '-' in x:                                                         #using the for loop here
                jj.append(x[3:])
            else:
                jj.append(x[2:])
        gg = ' '.join(jj)
        return gg

    def inv_bin(string):
        string = str(string)
        new_string = ''
        for x in string:
            if x == 1 or x == '1':
                new_string = new_string+'0'
            elif x == 0 or x == '0':
                new_string = new_string+'1'
            elif x == ' ':
                new_string = new_string+x
        return new_string

    def bin_sub(bin1, bin2):
        f = bin1.split(' ')
        g = bin2.split(' ')
        new = []
        for x, y in zip(f, g):
            d = int(x, 2) - int(y, 2)
            d = bin(d)
            new.append(d)
        b = denc.bit_filter(new)
        return b

    def bin_add(bin1, bin2):
        f = bin1.split(' ')
        g = bin2.split(' ')
        new = []
        for x, y in zip(f, g):
            d = int(x, 2) + int(y, 2)
            d = bin(d)
            new.append(d)
        b = denc.bit_filter(new)
        return b

    def bintostr(binary):
        n = []
        if type(binary) in (list, tuple):
            for x in binary:
                try:
                    c = chr(int(x,2))
                    n.append(c)
                except ValueError or TypeError:
                    pass
            new = ' '.join(str(x) for x in n)
            return new
        elif type(binary) in (str, int):
            binary = binary.split(' ')
            m = []
            for x in binary:
                try:
                    c = chr(int(x,2))
                    n.append(c)
                except ValueError or TypeError:
                    pass
            #new = ''.join(str(u) for u in n)
            return n

    def _encode(string, s=True):
        if len(string) < 64:
            similar_strings = dw.like_strings(string, sample_size=2, s=s, mute=True)                   #take the original string and locate a similar string based on weight of binary digits
            o_string_binary = denc.strtobin(string)                                      #convert the original string into binary format
            similar_string = pgeo.random_selection(similar_strings)                    #choose a random string from the list of similar strings on step 1
            similar_string_binary = denc.strtobin(similar_string)                        #convert the similar string to binary
            bin_diff_array = denc.bin_add(o_string_binary, similar_string_binary)           #add the simailar string to the original string
            hud = {'original string': string,                       #input from user
                   'similar string': similar_string,                #output to user
                   'original binary':o_string_binary,               
                   's string binary':similar_string_binary,
                   'bin sum':bin_diff_array}                 #write to file as binary
            e = denc.bin_sub(bin_diff_array, similar_string_binary)              #decodes the message by subtracting the key(similar string) from the encoded string(bin_diff)
            H = denc.bintostr(e)                                                 #converts the difference between the binary strings to a string array
            H = ''.join(H)                                                  #joins the string array to a char string
            n = denc.bintostr(bin_diff_array)
            n = ''.join(n)
#           print('input:',hud['original string'])
#           print('key:',hud['similar string'])
#           print('to be written:',hud['bin sum'])
#           print('encoded as:', n)
#           print('decoded as:', H)
            z = {'input':hud['original string'], 'key':hud['similar string'], 'enc_bin':hud['bin sum'], 'enc_char':n, 'output':H}
            return z

    def d_encode(string):
        a = len(str(string))
#       print(a)
        if a > 63:
            print('Blank spaces may cause system to hang. Please use minimum amount of spaces or remove them completely')
            f = list(ds._chunking(string, int(a/64)))
#           print(f)
            enc_bin_temp=[] 
            H={'input':string, 'key':'', 'enc_bin':'',
               'enc_char':'', 'output':''}
            #while H['output'] != string:
            for chunk in f:
                d = denc._encode(chunk)
                H['key'] = H['key']+d['key']
                H['enc_bin'] = H['enc_bin']+d['enc_bin']+' '
                H['enc_char'] = H['enc_char']+d['enc_char']
                H['output'] = H['output']+d['output']  
                #print(H)
            H['enc_bin'] = H['enc_bin'][:-1]
            #print(H)
            #print(H)
            return H
        elif a < 64:
            d = denc._encode(string)
            return d

    def decode(input_string, key):
        input_string = denc.strtobin(input_string)
        key = denc.strtobin(key)
        e = denc.bin_sub(input_string, key)              #decodes the message by subtracting the key(similar string) from the encoded string(bin_diff)
        H = denc.bintostr(e)                                                 #converts the difference between the binary strings to a string array
        H = ''.join(H)                                                  #joins the string array to a char string
        return H

    def encode(string):
        i = str(string)
        f = {'output':None}
        while f['output'] != i:
            f = denc.d_encode(i)
        #if f:
            #gg={'input':f['input'], 
#                'key':f['key'],
#                'enc_char':f['enc_char'],
#                'output':f['output']}
        print(f)
        return f

    def decoder():
        i = input('Encoded String:')
        f = input('Key:')
        m = denc.decode(i,f)
        print('Message: ',m)

    def binary_decode():
        pass

    def enc_write(string, key_file, message_file):
        e = denc.encode(string)
        if e:
            ds.writeto(key_file, e['enc_bin'], 'a+')
            ds.writeto(message_file, e['key'], 'a+')
            print('complete')

    def enc_read(key_file, message_file):
        key_list = ds.readfrom(key_file, 1, True)
        messages = ds.readfrom(message_file, 1, True)
        l = []
        for x, y in zip(key_list, messages):
            #print(y)
            H = denc.bintostr(y)
            H = ''.join(H)
        #print(H)
            mm = denc.decode(H, x)
            print(mm)
            l.append(mm)
        return l

class classifier():
    def __init__(self, inp=[]):
        self.input = None
        if type(inp) in (str, list,tuple):
            self.input = list(inp)
        elif type(inp) == int:
            self.input = [int(a) for a in str(inp)]
        elif type(inp) == float:
            i = str(inp).split('.')
            z = [int(a) for a in str(i[0])]
            w = [int(b) for b in str(i[1])]
            w.insert(0,'.')
            z.extend(w)
            self.input = z
        elif callable(inp):
            met = Metatron()
            g = met.hash76(32, 2)
            self.MAP = {str(g[0])+str(g[1]):inp}
        #    print(self.MAP)
        elif type(inp) == dict:
            met = Metatron()
            g = met.hash76(32, 2)
            self.MAP = {str(g[0])+str(g[1]):inp}
        #    print(self.MAP)
            #self.input = 
        else:
            met = Metatron()
            g = met.hash76(32, 2)
            self.MAP = {str(g[0])+str(g[1]):inp}
        #    print(self.MAP)
        if self.input:
            self.MAP = self._GETMAP()
        #    print(self.MAP)

    def _GETMAP(self):
        L = len(self.input)
        F = {}
        u = 0
        for x in range(2, L+2):
            if x ==2:
                x = 0
            num = pgeo.fib(x)
            F[num] = self.input[u]
            u +=1
            #print(num)
        #print(F)
    #    for a,b in F.items():
    #        print(a,b)
        #get all keys and values place in inventory
    #    FF = DoShit.get_dict(F.values(), F.keys())
    #    FF.update(F)
        #for a,b in FF.items():
        #    print(a,b)
        return F
        #print(FF)

class Transporter():
    '''Transports a load uisng value(destination distance), cost(amount of vectors between each step)'''
    def __init__(self, load=None, value=100, cost=1):
        self.ini = 0
        self.cost = cost
        if self.ini == 0:
            self.value = value+1
            self.ini = 1 
        if self.value >= self.cost:                    
            self.value = self._update()# value - cost
            self.load = load
            self.ETA = int(value / cost)
        else:
            pass

    def _update(self):
        if self.value >= self.cost:
            self.value = self.value - self.cost
            return self.value
        

class XY_Transporter(Transporter):
    def __init__(self, load=None, TX=(100,1),TY=(100,1)):
        '''Transport a load a certain distance using vector references
            TX = (value1, cost1), TY = (value2. cost2)'''
        self.xT = Transporter(load=load,value=TX[0],cost=TX[1])
        self.yT = Transporter(load=load, value=TY[0], cost=TY[1])
    def _movex(self):
        a = []
        for x in range(self.xT.ETA):
            F = self.xT._update()
            a.append(F)
        return a

    def _movey(self):
        a = []
        for y in range(self.yT.ETA):
            F = self.yT._update()
            a.append(F)
        return a
    
    def _tmovex(self):
        F = self.xT._update()
        return F

    def _tmovey(self):
        F = sef.yT._update()
        return F

    def move_xandy(self):
        A = self._movex()
        B = self._movey()
        s = []
        for x in A:
            for y in B:
                P = (x, y)
                s.append(P)
        return s
    
    def move(self):
        A = self._tmovex()
        B = self._tmovey()
        return (A,B)


def _HASH_PACK(types:dict):
    cl = {}
    for x in range(0, len(types)):
        #print(x)
        GO = types[x]
        X = classifier(GO)
        Y = classifier(X)
        cl.update(Y.MAP)
    print(cl)
    return cl

#_INV = Inventory(32)
#A = _HASH_PACK(types)
#B = _HASH_PACK(types)
#C = _HASH_PACK(types)
#for x in A.values():
#    _INV.put(x)
#print(_INV.inventory)

#for a in cl:
#    for b, p in a.items():
#       print(p.MAP)

#    Z = classifier(types[x])

num = {'0':'white',
       '1':'purple',
       '2':'blue',
       '3':'blue/green',
       '4':'green',
       '5':'yellow',
       '6':'orange',
       '7':'red',
       '8':'brown',
       '9':'black'}

#measure_man()
        #Images._save_image(e, Assets.game_directory+'usethese\\full_body\\'+mm+'.png')
        #print(e)
#Images._save_image(e[0], Assets.game_directory+'pep.png')
#print(sk)
#e = Images.d_load_image('E:\\Essentials\\myfuckinggame\\usethese\\chest_torso_arms.png')
#r = Images.d_load_image('E:\\Essentials\\myfuckinggame\\usethese\\heads.png')#
#dirs = 'E:\\Essentials\\myfuckinggame\\usethese\\'
#f  = [dirs+'adult\\HGSS_009.png']
#g = Images.d_image_group(f)
#print(g)
#for x in g[0]:
#    m = Images._super_crop(x,g[1])
#    for g in m:
#        g.show()
    #x.show()

#_files = []
#for x in range(0, 53):
#    dfile = dirs+str(x)+'.png'
#   # print(dfile)
#    if NN.DoShit.file_exist(str(dfile)):
#        _files.append(dfile)

#ee = Images.d_image_group(_files)
#n = 0
#for files in ee[0]:
#    ee[0][n] = files.convert('P')
#    n+=1

#ee[0][0].show()
#t = Images.create_gif(ee[0])
#_palettes = []
#shift = 1
#ff = []
#n = []
#want = (0, 255, 0)

#    print(m)
#    p = m.getpalette()
##    _palettes.append(p)
#    m = (np.array(m) +shift) % 256
#    m = Image.fromarray(m).convert('P')
#    m.putpalette(p[-3*shift:]+p[:-3*shift])
#    ff.append(m)

#img = Image.new('RGBA', (100, 100), (0,0,0,0))
#print(ee[0][1])

#ee[0][1]=ee[0][1].convert('P')

#print(ee[0][1])
#t = ee[0][1].info['transparency']
#print(t)

#max_person_height = (7, 10)
#meters = p.ft_in_to_meters(max_person_height)
#feet = p.meters_to_ft(meters)
#inches = p.ft_to_inches(feet)
#print(meters)
#print(feet)

#cm = meters* 100
#dpi = ppu(cm, 720)
#print(round(dpi),'pixels per unit')

#for x in range(0, 720, round(dpi)):
#    print(x)

#e = ppu()

#t.show()
#e[0].paste(r[0], (0,0), r[0])
#e[0].show()

#f = Images._super_crop(e[0], e[1])
#for x in f:
#e = (e[0], (720, 720))
#    Images._display_image(x)
#r[0].load()
#r,g,b,a = r[0].split()
#r[0].show()
#new = Image.new('RGB', (720, 720))
#new = new.paste([0],(0,0, 720, 720))

#new.show()

#print(TypeError(bool))
#s = Scribe()
#s.debug_controller()
