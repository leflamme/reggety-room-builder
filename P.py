from NN import p_geometry, DoShit
import arcade
from math import sin, cos, tan, atan2, atan, sqrt, pi
import numpy as np
#import pymunk
p = p_geometry()
"""
PAGE FOR ADDING SIMPLE PHYSICS/REACTIONS/INTERACTIONS
AS WELL AS THE UI FOR THE GAME
"""

class Assets():
    '''Game assets for society sim'''
    #diro = DoShit().current_dir()
    #diro = diro.split('/')
    #diro.pop(-1)
    #diro = '\\'.join(diro)
    #print(diro,'in assets')
    game_directory='E:\\CODE\\PYTHON\\PythonScripts\\CustomPrereleases\\'
    ari_head = game_directory+'usethese\\ari.png'
    json_hex_skin_colors_dark = game_directory+'versions\\african_indian_darkarab_pallete.json'
    json_drugs =game_directory+'versions\\drugs.json'
    json_female_names = game_directory+'versions\\female_first_names.json'
    json_full_body_template = game_directory+'versions\\full_template.json'
    json_rgb_hair_colors_gui = game_directory+'versions\\haircolorpallette.json'
    json_health_issues = game_directory+'versions\\health_issues.json'
    json_hex_hair_colors_basic=game_directory+'versions\\hex_hair_colors.json'
    json_male_names = game_directory+'versions\\male_first_names.json'
    json_races = game_directory+'versions\\races.json'
    json_surnames_sorted_race = game_directory+'versions\\surnames_by_race.json'
    json_RAW_surnames_subraces = game_directory+'versions\\surnames_detail.json'
    json_body_nose_template = game_directory+'versions\\templates\\nose_template.json'
    json_body_organs_template = game_directory+'versions\\templates\\organs_template.json'
    json_body_face_template = game_directory+'versions\\templates\\face_template.json'
    sprites_dir = game_directory+'usethese\\'
    sprites_body_head_front = game_directory+'usethese\\heads.png'
    sprites_body_head_back = game_directory+'usethese\\heads.png'
    sprites_body_head_side = game_directory+'usethese\\heads.png'
    sprites_body_upper_front = game_directory+'usethese\\upper_body.png'
    sprites_body_upper_back = game_directory+'usethese\\upper_body.png'
    sprites_body_upper_side = game_directory+'usethese\\upper_body.png'
    sprites_body_lower_front = game_directory+'usethese\\theigh_shin_foot.png'
    sprites_body_lower_back = game_directory+'usethese\\theigh_shin_foot.png'
    sprites_body_lower_side = game_directory+'usethese\\theigh_shin_foot.png'
    super_sprite_magic_chonk_64 = game_directory+'usethese\\chonk.png'
    super_sprite_male_char_nude_64 = game_directory+'usethese\\main_char\\asian\\raw_main_char.png'
    super_sprite_hair_64 = game_directory+'usethese\\main_char\\asian\\raw_hair.png'
    super_sprite_clothes_64 = game_directory+'usethese\\main_char\\asian\\raw_clothes.png'
    n = []
    for x in range(0, 53):
        n.append(game_directory+'usethese\\UI\\fill\\'+str(x)+'.png')  
    sprites_fill = n
    sprites_confidence= game_directory+'usethese\\UI\\icon_confidence.png'
    sprites_energy= game_directory+'usethese\\UI\\icon_energy.png'
    sprites_hand_open = game_directory+'usethese\\UI\\icon_hand_open.png'
    sprites_hand_point = game_directory+'usethese\\UI\\icon_pointer.png'
    sprites_sexual =  game_directory+'usethese\\UI\\icon_sexual.png'
    sprites_shop =  game_directory+'usethese\\UI\\icon_shop.png'
    sprites_social = game_directory+'usethese\\UI\\icon_social.png'
    sprites_trade =  game_directory+'usethese\\UI\\icon_trade.png'
    sprites_hunger = game_directory+'usethese\\UI\\icon_hunger.png'
    sprites_hygiene = game_directory+'usethese\\UI\\icon_hygiene.png'
    sprites_bladder = game_directory+'usethese\\UI\\icon_toilet.png'
    sprites_moodlet_antisocial =  game_directory+'usethese\\UI\\icon_antisocial.png'
    sprites_moodlet_broken_heart = game_directory+'usethese\\UI\\icon_broken_heart.png'
    sprites_moodlet_contagious = game_directory+'usethese\\UI\\icon_contagious.png'
    sprites_moodlet_cringe = game_directory+'usethese\\UI\\icon_cringe.png'
    sprites_moodlet_dead_wow =game_directory+'usethese\\UI\\icon_dead_xo.png'
    sprites_moodlet_disappointed = game_directory+'usethese\\UI\\icon_disappointment.png'
    sprites_moodlet_excited = game_directory+'usethese\\UI\\icon_excited.png'
    sprites_moodlet_amused = game_directory+'usethese\\UI\\icon_giggling.png'
    sprites_moodlet_grumpy = game_directory+'usethese\\UI\\icon_grumpy.png'
    sprites_moodlet_happy = game_directory+'usethese\\UI\\icon_happy.png'
    sprites_moodlet_happy_epic = game_directory+'usethese\\UI\\icon_happy_epic.png'
    sprites_moodlet_happy_approval = game_directory+'usethese\\UI\\icon_happy_thumb_up.png'
    sprites_moodlet_laughing = game_directory+'usethese\\UI\\icon_laughing.png'
    sprites_moodlet_adoration = game_directory+'usethese\\UI\\icon_love_eyes.png'
    sprites_moodlet_oh = game_directory+'usethese\\UI\\icon_oohh.png'
    sprites_poop_icon = game_directory+'usethese\\UI\\icon_poopsy.png'
    sprites_moodlet_sad = game_directory+'usethese\\UI\\icon_sad.png'
    sprites_moodlet_tear_drop=game_directory+'usethese\\UI\\icon_sad_crying.png'
    sprites_moodlet_whimpering = game_directory+'usethese\\UI\\icon_sad_wimpering.png'
    sprites_moodlet_sick = game_directory+'usethese\\UI\\icon_sick.png'
    sprites_moodlet_suprised = game_directory+'usethese\\UI\\icon_suprised.png'
    sprites_moodlet_uneasy = game_directory+'usethese\\UI\\icon_uneasy.png'
    sprites_moodlet_angry = game_directory+'usethese\\UI\\icon_upset.png'
    sprites_moodlet_angryb = game_directory+'usethese\\UI\\icon_upset2.png'
    sprites_moodlet_worried = game_directory+'usethese\\UI\\icon_upset0.png'
    body_parts_dir = game_directory+'usethese\\full_body\\'
    body_template_torso = body_parts_dir+'torso_template.png'
    body_template_arm = body_parts_dir+'arm_template.png'
    body_template_abs = body_parts_dir+'ab_template.png'
    body_template_boob = body_parts_dir+'boob_template.png'
    body_template_groin = body_parts_dir+'groin_template.png'
    body_template_leg = body_parts_dir+'leg_template.png'
    body_roll_call = sprites_dir+'measurements_background.png'
    fonts_pokemon_classic = game_directory+'pokemon_classic\\Pokemon Classic.ttf'
    color_need_fill = (255, 255,255)
    color_default_hair_outline = (0, 0, 0)
    color_default_hair_shadow = (90, 90, 90)
    color_default_hair_mean = (135, 135, 135)
    color_default_hair_highlights = (180, 180, 180)
    clothes = []
    body = []
    hair = []
    for x in range(0, 16):
        a = 'E:\\Essentials\\myfuckinggame\\usethese\\main\\clothes_'+str(x)+'.png'
        clothes.append(a)
        b = 'E:\\Essentials\\myfuckinggame\\usethese\\main\\main_'+str(x)+'.png'
        body.append(b)
        c = 'E:\\Essentials\\myfuckinggame\\usethese\\main\\hair_'+str(x)+'.png'
        hair.append(c)

    def objectify(x, y, height, width, sprite):
        '''Takes an the dimensions of an object and turns it into a dictionary that can be passed
        between internal functions.'''
        if x and y and height and width and sprite:
            obj = {'origin':(x, y), 'width':width, 'height':height, 'sprite':sprite}
            return obj

    def _fill_calc(level):
        '''Calcualate fill index from fill percentage'''
        if level >= 0:
            out = int((level/100)*52)
            return out
        else: 
            return 0

    def get_sprite(url= 'E:\\Essentials\\myfuckinggame\\char.png', x=300, y=300, height=140, width=140, color=None):
        '''Returns a sprite from a url'''
        player_sprite = arcade.Sprite(url)
        #player_sprite.width = 600
        #player_sprite.height = 600
        player_sprite.center_x = x
        player_sprite.center_y = y
        if color:
            player_sprite.color=color
        player_sprite.width = width
        player_sprite.height = height
        return player_sprite

#class poly_filled(arcade.Shape):
#    def __init__(self, x, y, width, height, angle, delta_x, delta_y, delta_angle, color):
#        super().__init__(x, y, width, height, angle, delta_x, delta_y, delta_angle, color)
#        shape = arcade.draw_polygon_filled()
#        arcade.crea

    #       MALE---NULL---FEMALE
    #BABY
    #TODDLER
    #KINDER
    #TEEN
    #ADULT
    #OLDY

class Forces:
    pass

class Collider:
    '''Use to define a collider object. Probably should move this to NNs Block Builder'''
    def __init__(self, origin=(0, 0), width=100, height=100):
        self.origin= origin
        self.width = width
        self.height = height
        self.object_base = {'origin':self.origin, 'width':self.width, 'height':self.height}

    def collider_object(self):
        return self.object_base
    pass

class Physics_Object(Collider):
    '''Describes the possible physics variables and forces of an object.'''
    def __init__(self, origin=(0, 0), width=100, height=100):
        Collider.__init__(self, origin, width, height)
        self.sprite=arcade.SpriteList()
        try:
            self.height=self.object_base['height']
            self.width=self.object_base['width']
            self.center=self.object_base['origin']
        except AttributeError:
            self.height = 0
            self.width = 0
            self.center = (0, 0)
        self.density = 0
        self.acc = (0, 0)
        self.velocity = (0, 0)
        self.speed = (0, 0)
        self.weight = 0
        self.volume = 0
        self.mass = 1000
        self.melting_point = 0
        self.vapor_point = 0
        self.freezing_point = 0
        self.last_center=()
        self.name=None
        self.collider= None#{'shape':'square', 'dimensions':{}},
        self.creation_time=None
        self.last_interaction=None
        self.interactions={}
        self.properties={}
        self.child_sprite=[]
        self.object_base = {'origin':self.center, 'width':self.width, 'height':self.height}
        self.forces = []
        self.current_state = None
        self.previous_state = None
        self.viscocity = None  
        self.force_applied = []
        
    def add_force(self, force=(0, 0)):
        '''Adds a force to the queue of forces to be applied.'''
        self.forces.append(tuple(force))
        return self.forces
    
    def _get_force(self, mass=None, acc=[]):
        '''Calculates the force applied using acceleration(int,int) and mass(int)'''
        if mass:
            self.mass = mass
        if len(acc) > 0:
            self.acc = acc
        self.force_applied = np.multiply(self.acc, (self.mass))
        return self.force_applied

    def apply_force(self, force=()):
        '''Applies a force or queued forces to an object'''
        if len(force)>0:
            self.forces.append(tuple(force))
        frc = len(self.forces)
        e = self.forces.copy()
        if frc > 0:
            speed = e[0]#.pop(0)
            self.speed = tuple(speed)
            f = (0)
           # for n in range(0, fr):
            vel = np.divide(speed, self.mass, dtype=float)
            print(speed)
            print('velocity:',vel)
            self.velocity = vel
            print(self.forces)
            #for x in range(0, fr):
            self.acc = np.multiply(speed, vel, dtype=float)#self.forces[x])
            self.forces = []
        elif frc == 0:
            speed = (0,0)
            self.acc = (0,0)
            self.forces = []
      
      #  self.avv = np.divide()
     #   self.acc = np.divide(f, )

    def get_speed(self):
        '''Calculate the speed of an object using speed plus the velocity'''
        self.speed = tuple(np.multiply(self.acc, self.velocity, dtype=float))
        return self.speed
    
    def move(self, x=None, y=None):
        '''Move objects coordinates to a new coordinate based on its speed'''
        if (x, y) != (None, None):
            self.speed = (x, y)
            self.center = tuple(np.add(self.center, self.speed))
            #self.sprite.center = self.center
            return self.center
        else:
            print(self.center)
            print('SPEED:',self.speed)
            if len(self.forces) > 0:
                move = self.forces.pop(0)
                self.center = tuple(np.multiply(move, self.speed))
                print(self.center)
            elif len(self.forces) == 0:
                self.center = self.center
            return self.center

    def update(self):
       # #print(self.center)
        #print('Velocity:'+str(self.velocity)+' Acceleration:'+str(self.acc)+' Speed:'+str(self.speed)+' Origin:'+str(self.center))
        self.speed = self.get_speed()
        self.center = self.move()
        self.sprite.center_x = self.center[0]
        self.sprite.center_y = self.center[1]
        self.force_applied = self._get_force(self.mass, self.acc)
        self.acc = np.multiply(self.acc, self.velocity)


    def draw(self):
        self.sprite.center_x = self.center[0]
        self.sprite.center_y = self.center[1]
        #print('SPRITE CENTER:',self.center)
            
        self.sprite.draw()
        #print('Velocity:'+str(self.velocity)+' Acceleration:'+str(self.acc)+' Speed:'+str(self.speed)+' Origin:'+str(self.center))

class Physics(Physics_Object):
    '''Simple physics collisions to be used in multiple ways.'''
    def __init__(self, origin=(0, 0), width=100, height=100):
        Physics_Object.__init__(self, origin, width, height)

    def box_collider(self, x1origin, x1width, x1height, x2origin, x2width, x2height, function=None):
        if x1origin and x1width and x1height and x2origin and x2width and x2height:
            if x1origin[0] < x2origin[0] + x2width and x1origin[0] + x1width > x2origin[0] and x1origin[1] < x2origin[1]+x2height and x1origin[1] + x1height > x2origin[1]:
               # #print('shit')
                return True

    def sprite_box_collider(self, sprite1, sprite2):
        o1 = (sprite1.left, sprite1.bottom)        
        o2 = (sprite2.left, sprite2.bottom)
        t1 = sprite1.height
        t2 = sprite2.height
        r1 = sprite1.width
        r2 = sprite2.width
        T = self.box_collider(o1, r1, t1, o2, r2, t2)
        return T

    def sbox_collider(self, object1, object2, function=None):
        x1origin = object1['origin']
        x1width  = int(object1['width'])
        x1height = int(object1['height'])
        x2origin = object2['origin']
        x2width  = int(object2['width'])
        x2height = int(object2['height'])
        if x1origin and x1width and x1height and x2origin and x2width and x2height:
            if x1origin[0] < x2origin[0] + x2width and x1origin[0] + x1width > x2origin[0] and x1origin[1] < x2origin[1]+x2height and x1origin[1] + x1height > x2origin[1]:
                ##print('holllaa')
                return True
    
    def sbox_collider_push(self, object1, object2, function=None):
        x1origin = object1['origin']
        x1width  = object1['width']
        x1height = object1['height']
        x2origin = object2['origin']
        x2width  = object2['width']
        x2height = object2['height']
        if x1origin and x1width and x1height and x2origin and x2width and x2height:
        # if x1origin[0] < x2origin[0] + x2width and x1origin[0] + x1width > x2origin[0] and x1origin[1] < x2origin[1]+x2height and x1origin[1] + x1height > x2origin[1]:
           #     #print('holllaa')
            object_top = int(x1origin[1]+x1height)
            object_bottom = int(x1origin[1])
            object_right = int(x1origin[0]+x1width)
            object_left = int(x1origin[0])
            mouse_x = int(x2origin[0]+x2width)
            mouse_xb = int(x2origin[0])
            mouse_y = int(x2origin[1]+x2height)
            mouse_yb = int(x2origin[1])
            lr_mid = int(object_right/2)
            tb_mid = int(object_top/2)
            #move up bottom left and bottom right quadrants 
            #q1 = mousey >= object_bottom-1 & mousey < tb_mid & mousex < lr_mid & mousex < object_right  
            #move down top left and top right qandrants 
            #q2 = mousey < object_top+1 & mouse < tb_mid & mousex < lr_mid & mousex < object_right
            #move left bottom and top left quadrants
            #q3 = mousex < object_left-1 & mousex < lr_mid & mousey > tb_mid & mousey < tb_mid
            #move right bottom and top left quadrants
            #q4 = mousex < object_right+1 & mousex > lr_mid & mousey > tb_mid & mousey < tb_mid
            a = list(range(object_bottom+35, object_top-1))
            b = list(range(object_left+35, object_right-35))
            c = list(range(object_bottom-1, object_bottom+35))
            d = list(range(object_left+35, object_right-35))
            e = list(range(object_bottom, object_top))
            f = list(range(object_left-1, object_left+35))
            g = list(range(object_bottom, object_top))
            h = list(range(object_right-35, object_right+1))
            if mouse_y in a and mouse_x in b: # and mouse_x < object_right/2 and mouse_y < object_top and mouse_y > object_bottom:              
                #print('top')
                #MOVES OBJECT DOWN
           #     f = 10
           #     #while f > 0:
                    #e = p.fib(f)
                object1['sprite'].move(0, -5)
                object1['origin']=(object1['sprite'][0].center_x, object1['sprite'][0].center_y)
                return True
                ##print(object1['origin'])
            #if x1origin[0] + x1width == x2origin[0]:
            if mouse_y in c and mouse_x in d: #and mouse_y < object_top  and mouse_y > object_bottom and mouse_x <= object_right+1:
                #print('bottom')
                object1['sprite'].move(0, 5)
                object1['origin']=(object1['sprite'][0].center_x, object1['sprite'][0].center_y)
                return True  
                        
            if mouse_y in e and mouse_x in f: #and mouse_y < object_top  and mouse_y > object_bottom and mouse_x <= object_right+1:
                #print('left')      
     #       if mouse_x > object_left and mouse_x < object_right and mouse_y < object_top and mouse_y >= object_bottom+1:              
                object1['sprite'].move(5, 0)
                object1['origin']=(object1['sprite'][0].center_x, object1['sprite'][0].center_y)
                return True 
            if mouse_y in g and mouse_x in h: #and mouse_y < object_top  and mouse_y > object_bottom and mouse_x <= object_right+1:
#            if mouse_y <= object_bottom-1 and mouse_y < tb_mid and mouse_x < object_right:
                object1['sprite'].move(-5, 0)
                object1['origin']=(object1['sprite'][0].center_x, object1['sprite'][0].center_y)
                return True
# 
# if x1origin[1] == x2origin[1]+x2height:
 #               #print('bottom')
  #              object1['sprite'].move(0, 1)
   #             object1['origin']=(object1['sprite'][0].center_x, object1['sprite'][0].center_y)
#
 #           if x1origin[1] + x1height == x2origin[1]:
  #              #print('top')
   #             object1['sprite'].move(0, -1)
    #            object1['origin']=(object1['sprite'][0].center_x, object1['sprite'][0].center_y)
    def sbox_collider_hit(self, object1, object2, object1_velocity, object2_velocity):
        x1origin = object1['origin']
        x1width  = object1['width']
        x1height = object1['height']
        x2origin = object2['origin']
        x2width  = object2['width']
        x2height = object2['height']

        if x1origin and x1width and x1height and x2origin and x2width and x2height:
        # if x1origin[0] < x2origin[0] + x2width and x1origin[0] + x1width > x2origin[0] and x1origin[1] < x2origin[1]+x2height and x1origin[1] + x1height > x2origin[1]:
           #     #print('holllaa')
            object_top = int(x1origin[1]+x1height)
            object_bottom = int(x1origin[1])
            object_right = int(x1origin[0]+x1width)
            object_left = int(x1origin[0])
            mouse_x = int(x2origin[0]+x2width)
            mouse_xb = int(x2origin[0])
            mouse_y = int(x2origin[1]+x2height)
            mouse_yb = int(x2origin[1])
            lr_mid = int(object_right/2)
            tb_mid = int(object_top/2)
            #move up bottom left and bottom right quadrants 
            #q1 = mousey >= object_bottom-1 & mousey < tb_mid & mousex < lr_mid & mousex < object_right  
            #move down top left and top right qandrants 
            #q2 = mousey < object_top+1 & mouse < tb_mid & mousex < lr_mid & mousex < object_right
            #move left bottom and top left quadrants
            #q3 = mousex < object_left-1 & mousex < lr_mid & mousey > tb_mid & mousey < tb_mid
            #move right bottom and top left quadrants
            #q4 = mousex < object_right+1 & mousex > lr_mid & mousey > tb_mid & mousey < tb_mid
            a = list(range(object_bottom+65, object_top-1))
            b = list(range(object_left+35, object_right-35))
            c = list(range(object_bottom-1, object_bottom+35))
            d = list(range(object_left+35, object_right-35))
            e = list(range(object_bottom, object_top))
            f = list(range(object_left-1, object_left+35))
            g = list(range(object_bottom, object_top))
            h = list(range(object_right-35, object_right+1))
            if mouse_y in a and mouse_x in b: # and mouse_x < object_right/2 and mouse_y < object_top and mouse_y > object_bottom:              
                #print('top')
                #MOVES OBJECT DOWN
                object1['sprite'].move(0, -abs(object2_velocity))
                object1['origin']=(object1['sprite'][0].center_x, object1['sprite'][0].center_y)
                return True
                ##print(object1['origin'])
            #if x1origin[0] + x1width == x2origin[0]:
            if mouse_y in c and mouse_x in d: #and mouse_y < object_top  and mouse_y > object_bottom and mouse_x <= object_right+1:
                #print('bottom')
                object1['sprite'].move(0, object2_velocity)
                object1['origin']=(object1['sprite'][0].center_x, object1['sprite'][0].center_y)
                return True
            if mouse_y in e and mouse_x in f: #and mouse_y < object_top  and mouse_y > object_bottom and mouse_x <= object_right+1:
                #print('left')      
     #       if mouse_x > object_left and mouse_x < object_right and mouse_y < object_top and mouse_y >= object_bottom+1:              
                object1['sprite'].move(object2_velocity, 0)
                object1['origin']=(object1['sprite'][0].center_x, object1['sprite'][0].center_y)
                return True 
            if mouse_y in g and mouse_x in h: #and mouse_y < object_top  and mouse_y > object_bottom and mouse_x <= object_right+1:
#            if mouse_y <= object_bottom-1 and mouse_y < tb_mid and mouse_x < object_right:
                object1['sprite'].move(-abs(object2_velocity), 0)
                object1['origin']=(object1['sprite'][0].center_x, object1['sprite'][0].center_y)
                return True
#            
    def _portal(self):
        pass

    def _multiportal(self):
        pass

    def _stop_collider(self):
        pass

    def _gate_collider(self):
        pass
    
    def pendulum(self, center_origin=(200, 300), swing_origin=(200, 300), arm_length=100, start_angle=0):
        center = center_origin #center may have some dimensions
        swinger = swing_origin #swinger probably has some dimensions
        arm = arm_length   #arm could have dimensions but not necessary
        angle = start_angle
        velocity = 0.1
        sprite = arcade.SpriteList()
        angle += velocity
        x = center[0] + arm * sin(angle)
        y = center[1] + arm * cos(angle)
        sp = arcade.Sprite(Assets.sprites_fill[52], scale=0.15, center_x=x, center_y=y)
        sprite.append(sp)
        return sprite

    def distance_between_points(x1, x2, y1, y2):
        d = sqrt((pow((x2 - x1), 2) + pow((y2 - y1), 2)))
        return d

class Orf():
    def __init__(self, center_origin=(0,0), height=0, width=0):
        self.base_center = center_origin
        self.center = center_origin
        self.sprite = arcade.SpriteList()
        self.base_height = height
        self.base_width = width
        self.height = height
        self.width = width
        self.more_wide = False
        self.less_wide = False
        self.inside = False
        self.s = arcade.Sprite(Assets.sprites_fill[51] , center_x=self.center[0], center_y=self.center[1])#arcade.create_ellipse_outline(self.center[0], self.center[1], width=self.width, height=self.height, color=arcade.color.WHITE, border_width=2)
        self.s.width = self.width
        self.s.height= self.height
        self.sprite.append(self.s)
        self.p_object = Assets.objectify(self.center[0], self.center[1], self.height, self.width, self.sprite)
     #   self.s = []

    def update(self, center_update=None):
        if type(center_update) in (tuple, list):
            self.center = center_update
            self.sprite = arcade.SpriteList()
            self.s = arcade.Sprite(Assets.sprites_fill[51], center_x=self.center[0], center_y=self.center[1])#arcade.create_ellipse_outline(self.center[0], self.center[1], width=self.width, height=self.height, color=arcade.color.WHITE, border_width=2)
            self.s.width = self.width
            self.s.height = self.height
            if self.s:
                self.sprite.append(self.s)

            if self.inside == True:
                self.s.width += 4
                self.s.height += 5

            if self.inside == False and self.s.width > self.base_width:
                self.s.width -= 8
            if self.inside == False and self.s.height > self.base_height:
                self.s.height -= 8
                self.center = self.base_center
        #if self.width == 0:
        #    self.s.width = self.base_width
        ##print('list_length', len(self.sprite))
            self.p_object = Assets.objectify(self.center[0], self.center[1], self.s.height, self.s.width, self.sprite)
            self._update_center(self.base_center)

            return self.p_object
        else:
        #for x in range(3):
        #    #print(3)
            if self.s:
                self.sprite.append(self.s)

                if self.inside == True:
                    self.s.width += 4
                    self.s.height += 5

                if self.inside == False and self.s.width > self.base_width:
                    self.s.width -= 8
                if self.inside == False and self.s.height > self.base_height:
                    self.s.height -= 8
        #if self.width == 0:
        #    self.s.width = self.base_width

        ##print('list_length', len(self.sprite))
                self.p_object = Assets.objectify(self.center[0], self.center[1], self.s.height, self.s.width, self.sprite)
                self._update_center(self.base_center)
                return self.p_object
    
    def draw(self):
      #  arcade.start_render()
        if len(self.sprite) > 0:
            self.sprite.draw()

    def controller(self, keycode, key, up=False):
        if key == keycode and up == False:
            self.inside = True
        else:
            self.width = self.width
            self.inside = False

    def _update_center(self, coords=None):
        if coords:
            self.center = coords
        else:
            pass

class Orf2():
    def __init__(self):
        self.z = np.zeros(shape=(11,11), dtype=int)
       # self.z.shape = (30, 30)
        self.n = np.ones(shape=(10,1), dtype=int)
        ##print(self.n)
        #self.z()
       # #print(self.z)
        self.z[5][0] = 1
        self.pen_layer_one=[[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0],
                            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0],
                            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1,1,0],
                            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1,1,0],
                            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0],
                            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0]
                            ]
        self.pen_layer_two=[[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0],
                            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0],
                            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0],
                            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0],
                            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0],
                            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0]
                            ]
        s = np.rot90(self.pen_layer_two)
     #   #print(s)
        uprite = [[0,0,0,1,1,1,0,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,1,1,1,1,1,1,1,0],
                  [1,1,1,1,1,1,1,1,1],
                  [1,1,1,1,1,1,1,1,1],
                  [1,1,1,1,1,1,1,1,1],
                  [0,1,0,1,1,1,0,1,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0],
                  [0,0,1,1,1,1,1,0,0]]
      #  #print(self.z)
        #self.x = np.add(self.n, self.z[1])
        ##print(self.x)
#c = Orf2()

class Pendulum():
    def __init__(self, center_origin=(300, 300), swing_origin=(300, 100), arm_length=100, start_angle=pi, sprite=None):
        self.center = center_origin #center may have some dimensions
        self.swinger = swing_origin #swinger probably has some dimensions
        self.arm = arm_length   #arm could have dimensions but not necessary
        self.rest_length = arm_length 
        self.angle = start_angle
        self.k = 0.01
        self.sprite = arcade.SpriteList()
        self.velocity = 0
        self.acc = (0.8 / self.arm) * sin(self.angle)
        self.velocity += self.acc
        self.x = self.center[0] + self.arm * sin(self.angle)
        self.y = self.center[1] + self.arm * cos(self.angle)        
        self.sp = arcade.Sprite(Assets.game_directory+'usethese\\full_body\\boob_template.png', scale=0.15, center_x=self.x, center_y=self.y)
        self.sprite.append(self.sp)
        self.sprite.draw()
        self.force = Physics.distance_between_points(self.center[0], self.x, self.center[1], self.y)
        self.d = self.rest_length - self.arm
        self.force = (-1 * self.k * self.d)
        self.momentum = 0
    
    def update_angle(self, swinger_weight=1000, center=None, anker_momentum=(0,0)):
        if center == None:
            center = self.center
        self.sprite = arcade.SpriteList()
        #anker_momentumx = ((self.x) * tan(anker_momentum[0]))
      #  anker_momentumy = ((self.y) * tan(anker_momentum[1]))/3
      #  if center > self.mouse_object
        self.momentum = anker_momentum[0]#(sum(anker_momentum) /2) * 0.50
     #   #print(self.momentum)
      #  #print('anker momentumx', anker_momentumx, 'anker momentum y', anker_momentumy)
    #    #print('combined', sum((anker_momentumx, anker_momentumy)))
        try:
            self.acc = ((0.8 / swinger_weight) * sin(self.angle))+self.momentum
        except ZeroDivisionError:
            pass
        self.velocity += self.acc
        #self.angle = self.velocity
      #  self.velocity += self.acc
#        self.momentum = swinger_weight * self.velocity + self.momentum
        self.angle += self.velocity
       # self.arm += self.force
        if self.arm < self.rest_length:
            self.force = abs(self.force)
            self.arm += self.force
            self.velocity *= 0.99
        elif self.arm > self.rest_length:
            self.force = abs(self.force)
            self.arm += -0.8
            self.velocity *= 0.995
        self.x = center[0] + self.arm * sin(self.angle)
        self.y = center[1] + self.arm * cos(self.angle)        
        self.sp = arcade.Sprite(Assets.game_directory+'usethese\\full_body\\boob_template.png', scale=0.34, center_x=self.x, center_y=self.y)
        self.sp2 = arcade.Sprite(Assets.game_directory+'usethese\\full_body\\boob_template.png', scale=0.34, center_x=self.x, center_y=self.y)
        self.sprite.append(self.sp2)
        self.sprite.append(self.sp)
        self.sprite.draw()
#        #print('velocity', self.acc)
        self.velocity *= 0.99
      #  self.momentum *= 0.99 #list(np.multiply(self.momentum, (0.99)))
       # obj = self.summon_object_dictionary()
        #return obj

    def draw(self):
        self.sprite.draw()
        return self.sprite

    def move_with_mouse(self, mouse_object):
        try: 
            x = mouse_object['origin'][0]
            y = mouse_object['origin'][1]
          #  if ph.sbox_collider(self.obj, mouse_object):
            d = Physics.distance_between_points(self.x, x, self.y, y)
          #  d = sqrt((pow((x - self.x), 2) + pow((y - self.y), 2)))
          # #print('distance',d)
            self.arm = d
            self.x = x
            self.y = y
            self.x = (x - self.center[0])
            self.y = (y - self.center[1])
            t = atan2(self.x, self.y)
            x - self.x 
        #    #print(t)
           # self.arm = 
            self.angle = t
        except ZeroDivisionError:
            pass

    def summon_object_dictionary(self):
        self.obj = Assets.objectify(self.x, self.y, 10, 10, self.sprite)
        return self.obj
        
#class Spring(Pen):
class Button():
    #size proportionate to screen size
    #fucntion passed as argument when clicked
    #hover tool tips
    #shadowed box areas
    #more intuitive show hide
    def __init__(self, screen_width, screen_height):
         #   UI.__init__(self, screen_width, screen_height)
        self.screen_height = screen_height
        self.screen_width = screen_width
        self.player_object = None
        ui_layout = []
        #self.on_mouse_press()

    def efficient_floor_plan(self,origin=(0,0), width=None, height=None, tile_height=16, tile_width=16, plan_type=None):
        ox = list(range(origin[0], origin[0]+width, tile_width))
        oy = list(range(origin[1], origin[1]+height, tile_height))
        matrix = []
              #for x, y in zip(ox, oy):
              #       matrix.append((x, y))
        for x in ox:
                for y in oy:
                    matrix.append((x, y))
        return {'type':plan_type, 'floorplan':matrix, 'dimensions':(height, width), 'origin':origin, 'tile_size':(tile_height, tile_width)}

    def button_sprite(self, x=40, y=40,pwidth=720, pheight=720, spread=10, height=16, width=16, tile_sprite=None, background_color=tuple(arcade.color.ORCHID), button_type=None):
           #   found = []
           #   matches = len(found)
           #   while matches == 0:
        matrix = self.efficient_floor_plan(origin=(x, y), width=pwidth, height=pheight, tile_height=height, tile_width=width, plan_type=button_type)
        if button_type !=None:
            for layout in self.ui_layout:
                if str(button_type) == layout:
                    self.ui_layout[layout].append(matrix)
                else:
                    self.ui_layout[layout]=[]
                    self.ui_layout[layout].append(matrix)
        matches = True
             # while matches == False:
             #        matches = self.check_floorplan_overlap(matrix['floorplan'])
             #        if matches == False:
                   #         matrix = self.efficient_floor_plan(origin=(x, y), width=pwidth, height=pheight, tile_height=height, tile_width=width)
              #       #print(matches)
              
        if matches == True:
            sprite_list = arcade.SpriteList()
            text = arcade.draw_text('MENU', 40, 40, arcade.color.WHITE, 60 , 600)

            for coords in matrix['floorplan']:
                if tile_sprite ==  None:
                             #  point = arcade.draw_ellipse_filled(color=background_color)
                    point = arcade.SpriteSolidColor(height, width, background_color)
                elif tile_sprite != None: 
                    point = arcade.Sprite(tile_sprite)
                point.center_x = coords[0]
                point.center_y = coords[1]
                         #   point._set_height(height)
                         #   point._set_width(width)
                     # point.draw()
                            
                sprite_list.append(point)
            sprite_list.append(text)
              #sprite_list.draw()
            return sprite_list

    def nonstupid_button(self):
        sprite_list = arcade.SpriteList()
        #text = arcade.draw_text('MENU', 40, 40, arcade.color.WHITE, 60 , 600)
        point = arcade.SpriteSolidColor(int(self.screen_width/3), int(self.screen_height/3), arcade.color.HARLEQUIN)
        point.center_x = self.screen_width/2
        point.center_y = self.screen_height/2
        sprite_list.append(point)
        #sprite_list.append(text)
        return sprite_list


class UI_BASE():
    '''Handles overall UI framework. Contains methods for creating UI items from scratch
    Moodlet_Handler(), _INVENTORY_UI_BASE(), UI_CONTROL() are based on this class and extend it'''
    #NEED TO MAKE TRAY AND BACKGROUND UPDATE SEPARATELY AND ONLY WHEN SCREEN CHANGES
    def __init__(self, screen_width, screen_height, mouse_object):
        self.screen_width = screen_width
        self.screen_height= screen_height
        self.screen_size = (self.screen_width, self.screen_height)
        self.mouse_object = mouse_object
        self.tray = None
        self._bladder_need = 100
        self._hunger_need = 100
        self._conf_need = 100
        self._hygiene_need = 100
        self._energy_need = 100
        self._social_need = 100
        self._sexual_need = 100
        self.origin = [0,0]
        self.offset = [0,0]
        self.color = self._add_alpha(arcade.color.GRAY, 80)
        self.color2 = self._add_alpha(arcade.color.BLACK, 15)
        self.NEEDS_BACKGROUND = arcade.SpriteList()

    def background(self, x=100, y=100, width=100, height=100, color=arcade.color.ASH_GREY, tooltip='Cool tooltip bro'):
        '''Creates a simple 2D square sprite'''
        sprite_list = arcade.SpriteList()
        #text = arcade.draw_text('MENU', 40, 40, arcade.color.WHITE, 60 , 600)
        point = arcade.SpriteSolidColor(int(width), int(height), color)
        point.center_x = int(x)#self.screen_width/2
        point.center_y = int(y)#self.screen_height/2
        sprite_list.append(point)
        #sprite_list.append(text)
        f = {'origin':(point.center_x, point.center_y), 'height':height, 'width':width, 'color':color, 'sprite':sprite_list, 'tooltip':tooltip}
        return f

    def _add_alpha(self, original_rgb_tuple, alpha):
        '''Adds an alpha channel and value to a non RGBA color tuple'''
        rgb_list = list(original_rgb_tuple).copy()
        alpha = int(2.55*alpha)
        rgb_list.append(alpha)
        return tuple(rgb_list)

    def _main_bar(self, x=800, y=800, width=None, height=None, background_color=arcade.color.ASH_GREY, opacity=87):
        '''Draws the main UI background for needs and other UI,
            Very Fast. Returns internal sprite dictionary'''
        sprite_list = arcade.SpriteList()
        if width == None:
            width = self.screen_width/2
        if height == None:
            height = 16*5#int(self.screen_height/15)*2
        if background_color:
            bc = list(background_color).copy()
            bc.append(int(2.55 * opacity))
            background_color = tuple(bc)
      #      background_color = list(background_color).append(50)
      #      background_color = tuple(background_color)
        e = self.background(self.origin[0]+(width/2)+self.offset[0], self.origin[1]+(height/2)+self.offset[1], width, height, background_color)
        sprite_list.append(e['sprite'][0])
        #self._update_viewport()
        f = {'origin':e['origin'], 'height':e['height'], 'width':e['width'], 'color':e['color'], 'sprite':sprite_list}
        return f

    def _fill_calc(level):
        '''Calculates the fill value to percentage compatiable with available fill sprites'''
        out = int((level/100)*52)
        return out

    def get_background(self):
        '''Returns the main needs UI bar sprite and contains it within the global UI class'''
        sprites = arcade.SpriteList()
        MAINBAR = self._main_bar()
        if len(MAINBAR)>0:
            INX = int(MAINBAR['origin'][0])
            INY = MAINBAR['origin'][1]
            INMAIN = self.background(INX, INY, MAINBAR['width']-16, MAINBAR['height']-16, self.color) 
            if INMAIN:
                self.tray = self.background(INX, INY, height=INMAIN['height']-16, width=INMAIN['width']-16, color=self.color2)
            #UI BACKGROUND
                print(self.tray)
                self.NEEDS_BACKGROUND.append(MAINBAR['sprite'][0])
                self.NEEDS_BACKGROUND.append(INMAIN['sprite'][0])
                self.NEEDS_BACKGROUND.append(self.tray['sprite'][0])
                return self.NEEDS_BACKGROUND
        
#need to add full body ui and inventiry ui
class Need:
    '''A universal needs object, create any type of need using a symbol sprite. Updating and drawing classes
    are located within Moodlet_Handler() class.'''
    def __init__(self,name='',level=100,sprite_symbol=arcade.Sprite()):
        self.symbol = self._is_symbol_sprite_or_url(sprite_symbol)
        self.name = name
        self.level = level
        self.transplace = self._fill_calc(level)
        self.origin = (self.symbol.center_x,self.symbol.center_y)
        self.width = self.symbol.width
        self.height = self.symbol.height
        print('wid',self.width)
        self.fill_sprite = Assets.get_sprite(Assets.sprites_fill[self.transplace], x=self.origin[0], y=self.origin[1], height=self.height, width=self.width)
        self.sprite = arcade.SpriteList()
        self.sprite.append(self.fill_sprite)
        self.sprite.append(self.symbol)
        till = self._load_fill_sprites()
        self.till = till
    def _load_fill_sprites(self):
        '''Loads the fill sprites from assets so that the computer doesnt
        have to load them from disk each time.'''
        fill_list = []
        for x in Assets.sprites_fill:
            sprite = Assets.get_sprite(x, 0,0,self.height, self.width)
            fill_list.append(sprite)
        return fill_list

    def get_fill_sprite(self, i, x, y, height, width):
        '''Returns the fills sprite of a certain level(i), 
        to the origin(x, y)'''
        if len(self.till) > 0:
            fill = self.till[i]
            fill.center_x = x
            fill.center_y = y
            fill.height = height
            fill.width = width
            return fill

    def _is_symbol_sprite_or_url(self,sprite):
        '''Determines if the sprite loaded into the class is a sprite or a url and
        returns a sprite.'''
        if type(sprite) == arcade.Sprite:
            symbol = sprite
        elif type(sprite) == str:
            symbol = arcade.Sprite(sprite)
        return symbol

    def _fill_calc(self, level):
        '''Calculates the fill value to percentage compatiable with available fill sprites'''
        if level >= 0:
            out = int((level/100)*52)
            return out
        else:
            return 0
            
    def draw(self):
        '''Draw the need sprite.'''
        self.sprite[0].draw()
        self.sprite[1].draw()

    def update_value(self, level):
        '''Update the need level and location of the need level.'''
        if int(level) > 0:
            self.level = level
            self.transplace = self._fill_calc(self.level)
            self.fill_sprite = self.get_fill_sprite(self.transplace, self.origin[0]+1, self.origin[1]+1, 40, 40)  #Assets.get_sprite(self.till[self.transplace], x=self.origin[0], y=self.origin[1], height=40, width=40)
            self.sprite = arcade.SpriteList()
            self.sprite.append(self.fill_sprite)
            self.sprite.append(self.symbol)
        elif int(level) <= 0:
            self.sprite[0].center_x = self.origin[0]
            self.sprite[0].center_y = self.origin[1]
            self.sprite[1].center_x = self.origin[0]
            self.sprite[1].center_y = self.origin[1]
            
    def update(self):
        '''Update the sprite.'''
        self.sprite.update()


class Moodlet_Handler(UI_BASE):
    '''Declare and manage ui elements'''
    def __init__(self, screen_width, screen_height, mouse_object):
        UI_BASE.__init__(self, screen_width, screen_height, mouse_object)
        self.bladder_need = Need('bladder',self._bladder_need, Assets.get_sprite(Assets.sprites_bladder, x=40, y=40, height=21, width=21))
        self.hunger_need = Need('hunger', self._hunger_need, Assets.get_sprite(Assets.sprites_hunger,x=42*2, y=40,height=32,width=32))
        self.conf_need = Need('confidence', self._conf_need, Assets.get_sprite(Assets.sprites_confidence,x=42*3, y=40,width=32,height=32))
        self.hygiene_need = Need('hygiene', self._hygiene_need, Assets.get_sprite(Assets.sprites_hygiene, x=42*4, y=40,width=21,height=21))
        self.energy_need = Need('energy', self._energy_need, Assets.get_sprite(Assets.sprites_energy, x=42*5, y=40,width=32,height=32))
        self.social_need = Need('social', self._social_need, Assets.get_sprite(Assets.sprites_social, x=42*6, y=40,width=28,height=28))
        self.sexual_need = Need('sexual', self._sexual_need, Assets.get_sprite(Assets.sprites_sexual, x=42*7, y=40,width=16,height=32))
        self.TF = [0,0,0,0,0,0,0]           #BOOL ARRAY FOR SCHEDULING
        self.sprites = arcade.SpriteList()
        self.back = None
        self.UPDATE_TIMER = 0
    def _conf(self, level=100, origin=None):
        '''Updates the confidence need'''
        if origin and type(origin) == tuple:
            self.conf_need.origin = origin
            self.conf_need.symbol.center_x = self.conf_need.origin[0]
            self.conf_need.symbol.center_y = self.conf_need.origin[1]
        self.conf_need.update_value(level)
        
    def _hunger(self, level=100, origin=None):
        '''Updates the hunger need'''
        if origin and type(origin) == tuple:
            self.hunger_need.origin = origin
            self.hunger_need.symbol.center_x = self.hunger_need.origin[0]
            self.hunger_need.symbol.center_y = self.hunger_need.origin[1]
        self.hunger_need.update_value(level)

    def _energy(self, level=100, origin=None):
        '''Updates the energy need'''
        if origin and type(origin) == tuple:
            self.energy_need.origin = origin
            self.energy_need.symbol.center_x = self.energy_need.origin[0]
            self.energy_need.symbol.center_y = self.energy_need.origin[1]
        self.energy_need.update_value(level)

    def _social(self, level=100, origin=None):
        '''Updates the social need'''
        if origin and type(origin) == tuple:
            self.social_need.origin = origin
            self.social_need.symbol.center_x = self.social_need.origin[0]
            self.social_need.symbol.center_y = self.social_need.origin[1]
        self.social_need.update_value(level)

    def _sexual(self, level=100, origin=None):
        '''Updates the sexual need'''
        if origin and type(origin) == tuple:
            self.sexual_need.origin = origin
            self.sexual_need.symbol.center_x = self.sexual_need.origin[0]
            self.sexual_need.symbol.center_y = self.sexual_need.origin[1]
        self.sexual_need.update_value(level)

    def _hygiene(self, level=100, origin=None):
        '''Updates the hygiene need'''
        if origin and type(origin) == tuple:
            self.hygiene_need.origin = origin
            self.hygiene_need.symbol.center_x = self.hygiene_need.origin[0]
            self.hygiene_need.symbol.center_y = self.hygiene_need.origin[1]
        self.hygiene_need.update_value(level)

    def _bladder(self, level=100, origin=None):
        '''Updates the bladder need'''
        if origin and type(origin) == tuple:
            self.bladder_need.origin = origin
            self.bladder_need.symbol.center_x = self.bladder_need.origin[0]
            self.bladder_need.symbol.center_y = self.bladder_need.origin[1]
        self.bladder_need.update_value(level)
    
    def _UPDATE_MAIN_UI_BAR_(self):
        '''Update the main UI bar background location to the updated location of the viewport,
        mainly used internally and updated with the need sprites.'''
        if len(self.NEEDS_BACKGROUND) > 0:
            #for x in self.NEEDS_BACKGROUND:
            self.NEEDS_BACKGROUND[0].center_x = self.origin[0]+200
            self.NEEDS_BACKGROUND[0].center_y = self.origin[1]+40
            self.NEEDS_BACKGROUND[1].center_x = self.origin[0]+200
            self.NEEDS_BACKGROUND[1].center_y = self.origin[1]+40
            self.NEEDS_BACKGROUND[2].center_x = self.origin[0]+200
            self.NEEDS_BACKGROUND[2].center_y = self.origin[1]+40

    def update_all_need_levels(self):
        '''Updates the all of the need levels at once.'''
        self._conf(self.conf_need.level)
        self._hunger(self.hunger_need.level)
        self._energy(self.energy_need.level)
        self._social(self.social_need.level)
        self._sexual(self.sexual_need.level)
        self._hygiene(self.hygiene_need.level)
        self._bladder(self.bladder_need.level)

    def get_needs_sprites(self):
        '''Returns the need sprites ,updates the need levels and also returns main bar sprite'''
        sprites = arcade.SpriteList()
        if self.tray == None:
            T = self.get_background()
            sprites.append(T[0])
            sprites.append(T[1])
            sprites.append(T[2])
    
        self.update_all_need_levels()

        sprites.append(self.hunger_need.sprite[0])
        sprites.append(self.hunger_need.sprite[1])

        sprites.append(self.conf_need.sprite[0])
        sprites.append(self.conf_need.sprite[1])
        
        sprites.append(self.energy_need.sprite[0])
        sprites.append(self.energy_need.sprite[1])
    
        sprites.append(self.social_need.sprite[0])
        sprites.append(self.social_need.sprite[1])
        
        sprites.append(self.sexual_need.sprite[0])
        sprites.append(self.sexual_need.sprite[1])

        sprites.append(self.hygiene_need.sprite[0])
        sprites.append(self.hygiene_need.sprite[1])
        
        sprites.append(self.bladder_need.sprite[0])
        sprites.append(self.bladder_need.sprite[1])
        self.SPRITES = sprites
        return self.SPRITES

    def _decay_needs(self, decay=0.05):
        '''Decay the need levels by certain variable each time this is called.
        COULD IMPLEMENT INDIVIDUAL MODIFIERS.'''
        if self.bladder_need.level > 0:
            self.bladder_need.level -= decay
            self._bladder(self.bladder_need.level, (self.origin[0]+42, self.origin[1]+40))    
        else:
            self._bladder(0, (self.origin[0]+42, self.origin[1]+40))    
            
        if self.hunger_need.level > 0:
            self.hunger_need.level -= decay
            self._hunger(self.hunger_need.level, (self.origin[0]+(42*2), self.origin[1]+40))
        else:
            self._hunger(0, (self.origin[0]+(42*2), self.origin[1]+40))
        
        if self.conf_need.level > 0:
            self.conf_need.level -= decay
            self._conf(self.conf_need.level, (self.origin[0]+(42*3), self.origin[1]+40))
        else:
            self._conf(self.conf_need.level, (self.origin[0]+(42*3), self.origin[1]+40))
        
        if self.hygiene_need.level > 0:
            self.hygiene_need.level -= decay
            self._hygiene(self.hygiene_need.level, (self.origin[0]+(42*4), self.origin[1]+40))    
        else:
            self._hygiene(self.hygiene_need.level, (self.origin[0]+(42*4), self.origin[1]+40))    
        
        if self.energy_need.level > 0:
            self.energy_need.level -= decay
            self._energy(self.energy_need.level, (self.origin[0]+(42*5), self.origin[1]+40))
        else:
            self._energy(self.energy_need.level, (self.origin[0]+(42*5), self.origin[1]+40))
        
        if self.social_need.level > 0:
            self.social_need.level -= decay
            self._social(self.social_need.level, (self.origin[0]+(42*6), self.origin[1]+40))
        else:
            self._social(self.social_need.level, (self.origin[0]+(42*6), self.origin[1]+40))
        
        if self.sexual_need.level > 0:
            self.sexual_need.level -= decay
            self._sexual(self.sexual_need.level, (self.origin[0]+(42*7), self.origin[1]+40))
        else:
            self._sexual(self.sexual_need.level, (self.origin[0]+(42*7), self.origin[1]+40))
    

    def _update_needs(self):
        '''Update the need levels.'''
        #SIMULATE NEED DECAY
        #if self.UPDATE_TIMER <= 1:
        self._decay_needs()
        #UPDATE THE BACKGROUND
        self._UPDATE_MAIN_UI_BAR_()
        #RESET THE TIMER
        if self.UPDATE_TIMER > 1.3:
            self.UPDATE_TIMER = 0
        self.UPDATE_TIMER +=0.01
        

    def draw_needs(self):
        '''Draw the need sprites.'''
        self.NEEDS_BACKGROUND[0].draw()
        self.NEEDS_BACKGROUND[1].draw()
        self.NEEDS_BACKGROUND[2].draw()
           
        self.sexual_need.sprite[0].draw()
        self.sexual_need.sprite[1].draw()
        #self.sexual_need.draw()

        self.social_need.sprite[0].draw()
        self.social_need.sprite[1].draw()

        self.hygiene_need.sprite[0].draw()
        self.hygiene_need.sprite[1].draw()

        self.conf_need.sprite[0].draw()
        self.conf_need.sprite[1].draw()
                
        self.bladder_need.sprite[0].draw()
        self.bladder_need.sprite[1].draw()
                
        self.hunger_need.sprite[0].draw()
        self.hunger_need.sprite[1].draw()

        self.energy_need.sprite[0].draw()
        self.energy_need.sprite[1].draw()

#inv_slot = {'sprite', 'item type', 'item name', 'item description', slot_origin,}

class _INVENTORY_UI_BASE(UI_BASE):
    '''Declare basic requirements for the inventory'''
    def __init__(self, inventory, screen_width, screen_height, mouse_object, x, y):
        UI_BASE.__init__(self, screen_width, screen_height, mouse_object)
        from inventory import Inventory
        self.i = Inventory()
        bg_Color = self._add_alpha(arcade.color.DARK_GRAY, 76)
        inner_Color = self._add_alpha(arcade.color.BLACK, 16)
        self.bg = self.background(x=x,y=y,width=64*8,height=64*5, color=bg_Color)
        self._INNER_ = self.background(x=x,y=y,width=(64*8)*0.95,height=(64*5)*0.825, color=inner_Color)
        self.title = arcade.draw_text('Inventory',font_size=20,start_x=x,start_y=y,color=(60,60,60))
        self.title_shadow = arcade.draw_text('Inventory',font_size=20,start_x=x,start_y=y,color=(0,0,0))
        self.origin = (x,y)

        self.SLOTS = []
        for SP in range(0, 33):
            S = self.background(x=x,y=y,width=32,height=32, color=inner_Color)
            self.SLOTS.append(S)
        self.Test = Assets.get_sprite(Assets.sprites_moodlet_angry, x=x,y=y,height=32,width=32) #self.background(x=x,y=y,width=32,height=32, color=arcade.color.GREEN)
        
    #    print(self.i.inventory)

        self.imported_inventory = inventory
        self.sprite = arcade.SpriteList()
        self._INVENTORY_SPRITES = arcade.SpriteList()
        self.move_inventory_sprites(self.origin, self.screen_size)
    #    self.positions = self.get_positons()
    #    f = {0:{'sprite':self.Test,
    #            'origin':self.positions[0],
    #            'id':None,
    #            'name':None}
    #            }
    #    g = 
    #    for PS in range(0, 33):   

    def move_inventory_sprites(self, origin, screen_size):
        '''CALLING THIS FUNCTIONS MOVES ALL THE DECLARED SPRITES TO THE POSTIONS OF THE VIEWPORT'''
        self.origin = origin
        self.screen_width = screen_size[0]
        self.screen_height = screen_size[1]

        self.bg['sprite'][0].center_x = self.origin[0]+self.screen_width/2#-(center)#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.bg['sprite'][0].center_y = self.origin[1]+self.screen_height/2#-(center-5)#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        #print(self.bg['sprite'][0].left)
            #MOVE THE TITLE
        self.title.center_x = self.bg['sprite'][0].left+64#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.title.center_y = self.bg['sprite'][0].top-16#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.title_shadow.center_x = self.bg['sprite'][0].left+64.2#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.title_shadow.center_y = self.bg['sprite'][0].top-16#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        #MOVE THE INNER INDENT BACKGROUND
        self._INNER_['sprite'][0].center_x = self.origin[0]+self.screen_width/2#-(center)#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self._INNER_['sprite'][0].center_y = self.origin[1]+self.screen_height/2#-(center)#self.origin[1]+16#self.INTERACTION_MENU[0].center_y

        #MOVE SLOT A
        self.SLOTS[0]['sprite'][0].center_x = (self.bg['sprite'][0].left+52)#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[0]['sprite'][0].center_y = self.bg['sprite'][0].top-64#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[1]['sprite'][0].center_x = self.SLOTS[0]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[1]['sprite'][0].center_y = self.SLOTS[0]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[2]['sprite'][0].center_x = self.SLOTS[1]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[2]['sprite'][0].center_y = self.SLOTS[1]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[3]['sprite'][0].center_x = self.SLOTS[2]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[3]['sprite'][0].center_y = self.SLOTS[2]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[4]['sprite'][0].center_x = self.SLOTS[3]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[4]['sprite'][0].center_y = self.SLOTS[3]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[5]['sprite'][0].center_x = self.SLOTS[4]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[5]['sprite'][0].center_y = self.SLOTS[4]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[6]['sprite'][0].center_x = self.SLOTS[5]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[6]['sprite'][0].center_y = self.SLOTS[5]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[7]['sprite'][0].center_x = self.SLOTS[6]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[7]['sprite'][0].center_y = self.SLOTS[6]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[8]['sprite'][0].center_x = self.SLOTS[7]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[8]['sprite'][0].center_y = self.SLOTS[7]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[9]['sprite'][0].center_x = self.SLOTS[8]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[9]['sprite'][0].center_y = self.SLOTS[8]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[10]['sprite'][0].center_x = self.SLOTS[9]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[10]['sprite'][0].center_y = self.SLOTS[9]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[11]['sprite'][0].center_x = self.SLOTS[0]['sprite'][0].center_x#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[11]['sprite'][0].center_y = self.SLOTS[0]['sprite'][0].center_y-40#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[12]['sprite'][0].center_x = self.SLOTS[11]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[12]['sprite'][0].center_y = self.SLOTS[11]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[13]['sprite'][0].center_x = self.SLOTS[12]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[13]['sprite'][0].center_y = self.SLOTS[12]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
    
        self.SLOTS[14]['sprite'][0].center_x = self.SLOTS[13]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[14]['sprite'][0].center_y = self.SLOTS[13]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[15]['sprite'][0].center_x = self.SLOTS[14]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[15]['sprite'][0].center_y = self.SLOTS[14]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[16]['sprite'][0].center_x = self.SLOTS[15]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[16]['sprite'][0].center_y = self.SLOTS[15]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[17]['sprite'][0].center_x = self.SLOTS[16]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[17]['sprite'][0].center_y = self.SLOTS[16]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[18]['sprite'][0].center_x = self.SLOTS[17]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[18]['sprite'][0].center_y = self.SLOTS[17]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[19]['sprite'][0].center_x = self.SLOTS[18]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[19]['sprite'][0].center_y = self.SLOTS[18]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[20]['sprite'][0].center_x = self.SLOTS[19]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[20]['sprite'][0].center_y = self.SLOTS[19]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[21]['sprite'][0].center_x = self.SLOTS[20]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[21]['sprite'][0].center_y = self.SLOTS[20]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[22]['sprite'][0].center_x = self.SLOTS[11]['sprite'][0].center_x#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[22]['sprite'][0].center_y = self.SLOTS[11]['sprite'][0].center_y-40#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[23]['sprite'][0].center_x = self.SLOTS[22]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[23]['sprite'][0].center_y = self.SLOTS[22]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[24]['sprite'][0].center_x = self.SLOTS[23]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[24]['sprite'][0].center_y = self.SLOTS[23]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[25]['sprite'][0].center_x = self.SLOTS[24]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[25]['sprite'][0].center_y = self.SLOTS[24]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[26]['sprite'][0].center_x = self.SLOTS[25]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[26]['sprite'][0].center_y = self.SLOTS[25]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[27]['sprite'][0].center_x = self.SLOTS[26]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[27]['sprite'][0].center_y = self.SLOTS[26]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[28]['sprite'][0].center_x = self.SLOTS[27]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[28]['sprite'][0].center_y = self.SLOTS[27]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[29]['sprite'][0].center_x = self.SLOTS[28]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[29]['sprite'][0].center_y = self.SLOTS[28]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[30]['sprite'][0].center_x = self.SLOTS[29]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[30]['sprite'][0].center_y = self.SLOTS[29]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.SLOTS[31]['sprite'][0].center_x = self.SLOTS[30]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[31]['sprite'][0].center_y = self.SLOTS[30]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.SLOTS[32]['sprite'][0].center_x = self.SLOTS[31]['sprite'][0].center_x+40#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.SLOTS[32]['sprite'][0].center_y = self.SLOTS[31]['sprite'][0].center_y#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
                 
    def draw_inventory(self):
        '''CALL THIS FUNCTION TO DRAW THE INVENTORY SPRITES'''
        self.bg['sprite'].draw()
        self._INNER_['sprite'].draw()
        for x in self.SLOTS:
            x['sprite'].draw()
        self.title_shadow.draw()
        self.title.draw()

    def get_positions(self):
        '''GET THE VECTOR POSTIONS OF THE INVENTORY SLOTS'''
        coord_list = []
        for x in range(0, 33):
            E = (self.SLOTS[x]['sprite'][0].center_x, self.SLOTS[x]['sprite'][0].center_y) 
            coord_list.append(E)
        return coord_list

    def place(self):
        pass

class INVENTORY_UI(_INVENTORY_UI_BASE):
    def __init__(self, inventory, screen_width, screen_height, mouse_object, x, y):
        _INVENTORY_UI_BASE.__init__(self, inventory, screen_width, screen_height, mouse_object, x, y)
        self.positions = self.get_positions()
        n = 0
        f = {}
        #for x in self.positions

class INTERACTION_UI_BASE(UI_BASE):
    '''Declare basic requirements for INTERACTIONS'''
    def __init__(self, screen_width, screen_height, mouse_object, x, y):
        UI_BASE.__init__(self, screen_width, screen_height, mouse_object)

        self.origin = (x, y)
        bg_Color = self._add_alpha(arcade.color.DARK_GRAY, 76)
        inner_Color = self._add_alpha(arcade.color.BLACK, 16)
        self.bg = self.background(x=x,y=y,width=64*5,height=64*5, color=bg_Color)
        self._INNER_ = self.background(x=x,y=y,width=(64*5)*0.95,height=(64*5)*0.825, color=inner_Color)
        self.title = arcade.draw_text('INTERACTIONS',font_size=20,start_x=x,start_y=y,color=(60,60,60))
        self.title_shadow = arcade.draw_text('INTERACTIONS',font_size=20,start_x=x,start_y=y,color=(0,0,0))
    
    def move_sprites(self, origin, screen_size):
        '''MOVE THE INTERACTIONS UI SPRITES WITH THE VEIWPORT'''
        self.screen_width = screen_size[0]
        self.screen_height = screen_size[1]
        self.origin = origin
        x = self.origin[0]+self.screen_width-(self.bg['sprite'][0].width/2)
        y = self.origin[1]+self.screen_height/2
        self.bg['sprite'][0].center_x = x #-(center)#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.bg['sprite'][0].center_y = y#-(center-5)#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.title.center_x = (self.origin[0]+self.bg['sprite'][0].width*2)#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.title.center_y = self.bg['sprite'][0].top-16#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        self.title_shadow.center_x = self.origin[0]+(self.bg['sprite'][0].width*2)#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.title_shadow.center_y = self.bg['sprite'][0].top-16#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        #MOVE THE INNER INDENT BACKGROUND
        self._INNER_['sprite'][0].center_x = x  #-(center)#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self._INNER_['sprite'][0].center_y = y-5#-(center)#self.origin[1]+16#self.INTERACTION_MENU[0].center_y

    def draw(self):
        self.bg['sprite'].draw()
        self._INNER_['sprite'].draw()    
        self.title_shadow.draw()
        self.title.draw()


class _ITEM():
    '''ATTEMPT TO DECLARE A GAME ITEM/OBJECT'''
    def __init__(self,NAME, URL, ORIGIN, HEIGHT, WIDTH):
        self.url = URL
        self.sprite = self._is_symbol_sprite_or_url(self.url)
        self.origin = ORIGIN
        self.height = HEIGHT
        self.width = WIDTH
        self.name = NAME
        #names:id
        #url
        #translate name or id to id
        #want to be able to add to inventory the id associated
        #each inventory slot is associated with a position on the screen
        #load the sprite at the position of of the inventory slot

    def _is_symbol_sprite_or_url(self,sprite):
        '''Determines if the sprite loaded into the class is a sprite or a url and
        returns a sprite.'''
        if type(sprite) == arcade.Sprite:
            symbol = sprite
        elif type(sprite) == str:
            symbol = arcade.Sprite(sprite)
        return symbol


class _BUTTON():
    def __init__(self, text=None, origin = (100,100), width=150, height=30, button_color=arcade.color.BLACK, text_color=arcade.color.WHITE, font= Assets.fonts_pokemon_classic):
        self.color = button_color
        self.text_color = text_color
        self.font = font
        self.text = text
        self.width = width
        self.height = height
        self.origin = origin 
        self.face = tuple(np.add(self.color, (100), dtype=int))
        print(self.face)
        self.OUTER_BUTTON = arcade.SpriteSolidColor(int(self.width),int(self.height), self.color)
        self.OUTER_BUTTON.center_x = self.origin[0]
        self.OUTER_BUTTON.center_y = self.origin[1]
        self.INNER_BUTTON = arcade.SpriteSolidColor(int(self.width*0.95),int(self.height*0.8), self.face)
        self.INNER_BUTTON.center_x = self.origin[0]
        self.INNER_BUTTON.center_y = self.origin[1]
        if self.text:
            self.button_text = arcade.draw_text(self.text, self.origin[0]*0.35, self.origin[1]*0.9, self.text_color, 10, font_name=self.font)
        self._button_event = False
        self.button_tooltip = arcade.SpriteList()
        
    def update(self, origin, mouse_object):
        self.OUTER_BUTTON.center_x = self.origin[0]+origin[0]
        self.OUTER_BUTTON.center_y = self.origin[1]+origin[1]
        self.INNER_BUTTON.center_x = self.origin[0]+origin[0]
        self.INNER_BUTTON.center_y = self.origin[1]+origin[1]
        if self.text:
            self.button_text.center_x = self.origin[0]+origin[0]
            self.button_text.center_y = self.origin[1]+origin[1]


        p = Physics()
        self.button_tooltip = arcade.SpriteList()
        #self.mouse_object = mouse_object
        
        try:
            if p.sprite_box_collider(sprite1=self.OUTER_BUTTON, sprite2=mouse_object['sprite'][0]):
                print('cool')
                u = arcade.draw_text('dix', mouse_object['origin'][0], mouse_object['origin'][1], arcade.color.BLACK, 10, font_name=Assets.fonts_pokemon_classic)
                self.button_tooltip.append(u)
                if mouse_object['left']:
                    print('SHITTY TOASTER')
                    self._button_event = True
            else:
                self.button_tooltip = arcade.SpriteList()
                self._button_event = False
        except TypeError:
            self.button_tooltip = arcade.SpriteList()
            self._button_event = False
    
    def draw(self):
        self.OUTER_BUTTON.draw()
        self.INNER_BUTTON.draw()
        if self.text:
            self.button_text.draw()

        if len(self.button_tooltip) > 0:
            self.button_tooltip.draw()

class _CHARACTER_UI_BASE(UI_BASE):
    '''Declare basic requirements for the CHARACHTER INFOMRATION UI'''
    def __init__(self, screen_width, screen_height, mouse_object, x, y):
        UI_BASE.__init__(self, screen_width, screen_height, mouse_object)
        bg_Color = self._add_alpha(arcade.color.DARK_GRAY, 76)
        inner_Color = self._add_alpha(arcade.color.BLACK, 16)
        self.bg = self.background(x=x,y=y,width=64*11,height=64*9, color=bg_Color)
        self._INNER_ = self.background(x=x,y=y,width=(64*11)*0.95,height=(64*10)*0.825, color=inner_Color)
        self.title = arcade.draw_text('Body',font_size=20,start_x=x,start_y=y,color=(60,60,60))
        self.title_shadow = arcade.draw_text('Body',font_size=20,start_x=x,start_y=y,color=(0,0,0))
        self.AA = ((50, 100), (50, 50), (100, 75))
        #self.tri = arcade.ShapeElementList()
        self.t = arcade.create_polygon(self.AA, arcade.color.GREEN) #draw_triangle_filled(50,50,50,70,60,60, arcade.color.GREEN)
        #self.tri.append(self.t)
        self.button = _BUTTON('Me hoy minoy', (200, 200), 200)
        self.origin = (x,y)
    
    def move_sprites(self, origin, screen_size, mouse_object):
        '''MOVE CHARACTER INFORMATION UI SPRITES'''
        self.origin = origin
        self.screen_width = screen_size[0]
        self.screen_height = screen_size[1]

        self.bg['sprite'][0].center_x = self.origin[0]+self.screen_width/2#-(center)#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.bg['sprite'][0].center_y = self.origin[1]+self.screen_height/2#-(center-5)#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        
        self.title.center_x = self.bg['sprite'][0].left+42#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.title.center_y = self.bg['sprite'][0].top-16#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        self.title_shadow.center_x = self.bg['sprite'][0].left+42#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self.title_shadow.center_y = self.bg['sprite'][0].top-16#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
            
        #MOVE THE INNER INDENT BACKGROUND
        self._INNER_['sprite'][0].center_x = self.origin[0]+self.screen_width/2#-(center)#self.origin[0]+16#self.INTERACTION_MENU[0].center_x
        self._INNER_['sprite'][0].center_y = (self.origin[1]+self.screen_height/2)-5#-(center)#self.origin[1]+16#self.INTERACTION_MENU[0].center_y
        #self.tri = arcade.draw_polygon_filled(self.AA, arcade.color.GREEN) #draw_triangle_filled(50,50,50,70,60,60, arcade.color.GREEN)
        self.button.update(origin, mouse_object)
        
    def draw_char_menu(self):
        '''DRAW THE CHARACTER INFORMATION SPRITES'''
        self.bg['sprite'].draw()
        self._INNER_['sprite'].draw()
        self.title_shadow.draw()
        self.title.draw()
        #arcade.draw_polygon_filled(self.AA, arcade.color.GREEN) #draw_triangle_filled(50,50,50,70,60,60, arcade.color.GREEN)
        self.t.draw()
        self.button.draw()

##############################################################################
################# MAIN UI CONTROLLER BELOW ###################################
##############################################################################

class UI_CONTROL(Moodlet_Handler):
    '''Controls the viewport, ui scaling, player needs and ui queing.'''
    def __init__(self, player_sprite=arcade.Sprite(),screen_size=(800, 600), mouse_object=None):
        Moodlet_Handler.__init__(self, screen_width=screen_size[0], screen_height=screen_size[1],mouse_object=mouse_object)
        self.changed = False
        self.view_left = 0
        self.view_bottom = 0
    #   print(player_sprite.player[0])
        self.player = player_sprite.sprite#.player[0]
        self.VIEWPORT_MARGIN = 64
        self.inventory_open = False
        self.INVENTORY_MENU = INVENTORY_UI(None,self.screen_width, self.screen_height, self.mouse_object, self.origin[0]+self.screen_width-(64*2),self.origin[1]+self.screen_height-(64*2))     #[X] - SHOW WHAT ITEMS ARE CONTAINED IN PLAYER INVENTORY          #_INVENTORY_UI_BASE(self.screen_width, self.screen_height, self.mouse_object, self.screen_width-64,self.screen_height-64)
        
        self.character_menu_open = False   
        self.CHARACTER_MENU = _CHARACTER_UI_BASE(self.screen_width, self.screen_height, self.mouse_object, self.origin[0]+self.screen_width-(64*2),self.origin[1]+self.screen_height-(64*2))        #[x] - SHOW THE CURRENT STATS OF PLAYER AND NPCS WITHIN CERTAIN DISTANCE OF PLAYER
        #CHAR MENU INCLUDE:     #RELATIONSHIPS BUTTON -> HOUSEHOLD -> CHAR CUSTOMIZIER
                                                                #  -> FAMILY TREE
                                                                #  -> TORTURE
                                                                #  -> CALL TO LOCATION 
                                                   #  -> SHOWS ALL RELATIONSHIPS
        
        self.interaction_open = False
        self.INTERACTION_MENU = INTERACTION_UI_BASE(self.screen_width, self.screen_height, self.mouse_object, 0, self.screen_height)    #[x] - INTERACT WITH OBJECTS OR NPCS USING JSON PRESETS

        self.relationships_open = False     #[] - NAVIFGATE TO USING A BUTTON FROM THE CHARACHTER MENU - SHOW RELATIONSHIPS AND FAMILY TREE
        self.RELATIONSHIPS = None
        
        self.char_customizer_open = False      
        self.CHARACTER_CUSTOMIZATION_MENU = None    #[] - CUSTOMIZE THE CURRENT PLAYER OR A PLAYER WITHIN THE HOUSEHOLD - CAN BE OPENED FROM THE CHARACHTER MENU
        
        self.house_customization_open = False
        self.HOUSE_CUSTOMIZATION = None         #[] - MOUSE PAINT THE HOUSE THE PLAYER CURRENTLY OWNS - CAN BE OPENED FROM A SPECIAL ITEM IN EACH PLAYER OWENDD HOUSE
        
        self.h_open = False
        self.H_MENU = None              #[] - OPEN HSCENE ANIMATIONS MINIGAME
        
        self.conversation_open = False
        self.CONVERSATION_UI = None     #[] - DISPLAY MULTIPLE CHOICE DIALOGUE

        self.household_open = False 
        self.HOUSEHOLD = None           #[] - DISPLAY A LSIT OF THE CURRENT HOUSEHOLD, CAN HAVE UNLIMITED AMOUNT - ADOPT OR HAVE AS MANY PEOPLE AS YOUR WANT

        self.tort_open = False
        self.TORT_UI = None             #[] - FULL BODY  GRAPHIC TORUTRU SIMULATION MINIGAME
        
        self.town_map_open = False
        self.TOWN_MAP = None            #[] - SHOW A MAP OF THE CURRENTLY GENERATED TOWN

        self.F_pressed = False
        self.nearby_objects = []
    
    def _interpret_keys(self, KEY):
        '''UI HOTKEYS/KEY ACTIONS GO HERE'''
        I = arcade.key.I    #OPEN/CLOSE INVENTORY
        C = arcade.key.C
        F = arcade.key.F
        G = arcade.key.G
        if KEY == G:
            self.hygiene_need = Need('meep', self._sexual_need, Assets.get_sprite(Assets.sprites_sexual, x=42*7, y=40,width=16,height=32))

    #    print(KEY)
        if KEY == I and self.inventory_open == False:
            self.inventory_open = True
            return True
        elif KEY == I and self.inventory_open:
            self.inventory_open = False
            return False

        if KEY == C and self.character_menu_open == False:
            self.character_menu_open = True
            return True
        elif KEY == C and self.character_menu_open:
            self.character_menu_open = False
            return False

        if KEY == F and self.interaction_open == False:
            self.interaction_open = True
            return True
        elif KEY == F and self.interaction_open:
            self.interaction_open = False
            return False

    #    if KEY == F and self.inventory_open == False:
    #        self.INTERACTION_MENU = _INVENTORY_UI_BASE(None,self.screen_width, self.screen_height, self.mouse_object, self.origin[0]+self.screen_width-(64*2),self.origin[1]+self.screen_height-(64*2))
    #        self.interaction_open = True
    #    elif KEY == F and self.inventory_open:
    #        self.interaction_open = False

    def drawInventory(self, mouse_object):
        self.mouse_object = mouse_object
        if self.mouse_object['origin']:
            for x in self.mouse_object['sprite']:
                x.center_x = self.mouse_object['origin'][0]-2
                x.center_y = self.mouse_object['origin'][1]-15
        
        '''DRAW THE UI SPRITES'''
        if self.inventory_open:  
            center = 64*2#self.bg['sprite'][0].left
            #MOVE THE BACKGROUND
            self.INVENTORY_MENU.move_inventory_sprites(self.origin, (self.screen_width,self.screen_height))
            self.INVENTORY_MENU.draw_inventory()
            MM = self.INVENTORY_MENU.get_positions()
           # print(len(MM))
            for x in MM:
                self.INVENTORY_MENU.Test.center_x=x[0]
                self.INVENTORY_MENU.Test.center_y=x[1]
                self.INVENTORY_MENU.Test.draw()

        if self.character_menu_open:
        #    self.CHARACTER_MENU.move_sprites(self.origin, (self.screen_width,self.screen_height))
            self.CHARACTER_MENU.move_sprites(self.origin, (self.screen_width,self.screen_height), self.mouse_object)
            self.CHARACTER_MENU.draw_char_menu() 

        if self.interaction_open and len(self.nearby_objects) > 0:
            for x in self.nearby_objects:
                print(x,'nearby object')
            self.INTERACTION_MENU.move_sprites(self.origin, (self.screen_width, self.screen_height))
            self.INTERACTION_MENU.draw()
     
        #if len(self.buttons) > 0:
        #    self.buttons.draw()


    def _buttons(self, mouse_object):
        self.mouse_object = mouse_object
        #self.TEST_BUTTON.update(self.origin, mouse_object)

    def calculateDistance(self, x1,y1,x2,y2): 
        dist = sqrt((x2 - x1)**2 + (y2 - y1)**2)  
        return dist  
        #print calculateDistance(x1, y1, x2, y2) 

    def sqrt(self, num):
        s = sqrt(num)
        return s
    
    def player_update_viewport(self, player=None, draw=True):
        '''Update the viewport based on location of the player.'''
        changed = False
        # Scroll left
        if player:
            self.player = player
            self.player.update()
            #print(self.player)
        #self.SPRITES.update()
        left_boundary = self.view_left + self.VIEWPORT_MARGIN
        right_boundary = self.view_left + self.screen_width - self.VIEWPORT_MARGIN
        top_boundary = self.view_bottom + self.screen_height - self.VIEWPORT_MARGIN
        bottom_boundary = self.view_bottom + self.VIEWPORT_MARGIN
        move_left = move_right = move_down = move_up = False
        #print(left_boundary,right_boundary,top_boundary,bottom_boundary)
        if self.player.left < left_boundary:
            self.view_left -= left_boundary - self.player.left
            move_left = True
        #    self.SPRITES.move(-7,0)
        #    self.SPRITES.update()
        # Scroll right
        if self.player.right > right_boundary:
            self.view_left += self.player.right - right_boundary
            move_right = True
        #    self.SPRITES.move(7,0)
        #    self.SPRITES.update()
        # Scroll up
        if self.player.top > top_boundary:
            self.view_bottom += self.player.top - top_boundary
            move_down = True
        #    self.SPRITES.move(0,7)
        #    self.SPRITES.update()
        # Scroll down
        if self.player.bottom < bottom_boundary:
            self.view_bottom -= bottom_boundary - self.player.bottom
            move_up = True
        #    self.SPRITES.move(0,-7)
            #self.SPRITES.update()
        #self.SPRITES.draw()
        #if self.UPDATE_TIMER < 1.294 and not changed:                         #DRAW THE SPRITES CONTAINED WITHIN GLOBAL SPRITE VARIABLE
        #    print(len(self.SPRITES))
        #    for x in self.SPRITES:
        #        x.draw()
            #self.SPRITES.draw()
        if move_left or move_right or move_down or move_up:
            changed = True
        if changed:
            self.view_left = int(self.view_left)
            self.view_bottom = int(self.view_bottom)
            A = self.view_left          #LEFT
            B = self.screen_width + self.view_left  #RIGHT   
            C = self.view_bottom                   #BOTTOM
            D = self.screen_height + self.view_bottom #BOTTOM
            self.screen_width = ((B-A)/(B-A))*(B-A)     #SCREEN WIDTH   NONSCROLL
            self.screen_height = ((D-C)/(D-C))*(D-C)    #SCREEN HEIGTH NON SCROLL
            self.origin = (A, C)                        #SCREEN ORIGIN AFTER SCROLL
            #arcade.set_viewport(A,B,C,D)
            #arcade.set_viewport(A,B,C,D)
            return (A,B,C,D, self.SPRITES)
        else:
            return
#        else:
#            self.SPRITES.update()
#            self.SPRITES.draw()

    def _tooltips(self, mouse_object):
        '''Draws and displays text when the mouse hovers over a need ui object'''
        p = Physics()
        tooltips = arcade.SpriteList()
        self.mouse_object = mouse_object
        try:
            if p.sbox_collider(object1=self.bladder_need.__dict__, object2=mouse_object):
                u = arcade.draw_text(self.bladder_need.name.capitalize(), mouse_object['origin'][0], mouse_object['origin'][1], arcade.color.WHITE, 10, font_name=Assets.fonts_pokemon_classic)
                tooltips.append(u)
                return tooltips

            elif p.sbox_collider(object1=self.conf_need.__dict__, object2=mouse_object):
                u = arcade.draw_text(self.conf_need.name.capitalize(), mouse_object['origin'][0], mouse_object['origin'][1], arcade.color.WHITE, 10, font_name=Assets.fonts_pokemon_classic)
                tooltips.append(u)
                return tooltips
        
            elif p.sbox_collider(object1=self.hygiene_need.__dict__, object2=mouse_object):
                u = arcade.draw_text(self.hygiene_need.name.capitalize(), mouse_object['origin'][0], mouse_object['origin'][1], arcade.color.WHITE, 10, font_name=Assets.fonts_pokemon_classic)
                tooltips.append(u)
                return tooltips
        
            elif p.sbox_collider(object1=self.hunger_need.__dict__, object2=mouse_object):
                u = arcade.draw_text(self.hunger_need.name.capitalize(), mouse_object['origin'][0], mouse_object['origin'][1], arcade.color.WHITE, 10, font_name=Assets.fonts_pokemon_classic)
                tooltips.append(u)
                return tooltips
            elif p.sbox_collider(object1=self.energy_need.__dict__, object2=mouse_object):
                u = arcade.draw_text(self.energy_need.name.capitalize(), mouse_object['origin'][0], mouse_object['origin'][1], arcade.color.WHITE, 10, font_name=Assets.fonts_pokemon_classic)
                tooltips.append(u)
                return tooltips
            elif p.sbox_collider(object1=self.sexual_need.__dict__, object2=mouse_object):
                u = arcade.draw_text(self.sexual_need.name.capitalize(), mouse_object['origin'][0], mouse_object['origin'][1], arcade.color.WHITE, 10, font_name=Assets.fonts_pokemon_classic)
                tooltips.append(u)
                return tooltips
            elif p.sbox_collider(object1=self.social_need.__dict__, object2=mouse_object):
                u = arcade.draw_text(self.social_need.name.capitalize(), mouse_object['origin'][0], mouse_object['origin'][1], arcade.color.WHITE, 10, font_name=Assets.fonts_pokemon_classic)
                tooltips.append(u)
                return tooltips
            else:
                tooltips = arcade.SpriteList()
                return tooltips
        except TypeError:
            tooltips = arcade.SpriteList()
            return tooltips


class Interaction_UI(UI_BASE):
    '''Manage UI for interacting with in game objects and items along with interactive UI'''
    pass

#u = UI_BASE(100, 100, 100)
#u._need_fill()

##print(world)