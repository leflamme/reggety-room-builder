from Time import dt
import random
import sys as sy
class p_geometry():
    '''Common measurements, types, conversions functions, lists, and label dictionaries.'''
    def __init__(self):
#            for colorname in pgeo.rgb_hair_colors:
#        print(colorname)
#        for rgb in pgeo.rgb_hair_colors[colorname]:
#            print(rgb)
#            print(pgeo.rgb_hair_colors[colorname][rgb])
#            color = pgeo.rgb_hair_colors[colorname][rgb]
       # self.half_seconds = 10
        self.seconds = 1
        self.minutes = self.seconds * 60
        self.hour = self.minutes * 60 
        self.day = self.hour *24
        self.week = self.day * 7
        self.month = self.week *4
        self.year = self.month *12
        self.fiveyears =self.year * 5
        self.decade = self. fiveyears * 2
        self.fiftyyears = self.decade * 50
        self.century = self.fiftyyears * 2

        bit=1
        kilobyte=1024*bit
        megabyte=1024*kilobyte
        gigabyte=1024*megabyte
        terabyte=1024*gigabyte
        petabyte=1024*terabyte
        
        self.eyeball_template = {'iris_color':None,
                    'pupil_type':None,
                    'pupil_size':None,
                    'pupil_color':None,
                    'whites_color':None,
                    'center':None,
                    'radius':None,
                    'x':None, 'y':None, 'z':None
                    }
        self.EYE_COLOR_NAMES = ['AMBER', 'LIGHTBROWN', 'BROWN','DARKBROWN','LIGHTBLUE','SAPPHIRE','EMERALD', 'HAZEL', 'GREY','BLUEGREY', 'AMBER', 'BLACK', 'WHITE']
        self.EYE_COLORS = {}
        self.CHEST_SIZE=['Pects','Flat','A','B','C','D','DD','DDD','E','F','G','H']
        self.HEIGHT_FEET = list(range(1,7))
        self.HEIGHT_INCHES = list(range(0, 11))
        self.HEIGHT_METERS = list(range(0, 3)) 
        self.WEIGHT_LBS = list(range(1, 800))
        self.WEIGHT_KG = list(range(0, 362))
        self.MUSCULARITY = list(range(0, 100))
        self.FATTINESS = list(range(0, 100))
      #  self.BMI = self.WEIGHT_KG/(self.HEIGHT_METERS*self.HEIGHT_METERS)
      #  self.OVERALL_FITNESS = (self.MUSCULARITY + self.FATTINESS)/2
      #  self.FITNESS = {"overall": self.OVERALL_FITNESS,
      #                  "muscles": self.MUSCULARITY, 
      #                  "fat":self.FATTINESS,
      #                  "bmi":self.BMI}
        self.FACIAL_HAIR = []
        self.SKILL_INTELLIGENCE=list(range(0, 250))
        self.ADDICTIONS = []
        self.NATURAL_ATTRACTIONS=[]
        self.PURPOSE = []
        self.PHILOSOPHY = [] #philosophy generator //develop a who what when where and why for existing
        self.SKILL_LANGUAGE = []
        self.FAMILY = {'mother':None, 'father':None, 'siblings':None}
        self.CHAR_TRAITS = []
        self.DISEASES = []
        self.SHOULDER_WIDTH = list(range(0,100))
        self.SHOULDER_DEPTH = list(range(0, 100))
        self.WAIST_WIDTH = list(range(0, 100))
        self.WAIST_DEPTH = list(range(0, 100))
        self.HIP_WIDTH = list(range(0, 100))
        self.HIP_DEPTH = list(range(0, 100))
        
        self.SEX = ['MALE', 'TRANSMALE','INTERSEX/FUTA/HERMAPHRODITE', 'TRANSFEMALE','FEMALE', 'NULL']
        self.GENDER_IDENTITY = ['MAN', 'BOY', 'TRANS', 'GIRL','WOMAN']
        self.GENDER_EXPRESSION = ['MASCULINE', 'ANDROGYNOUS', 'FEMININE']
        self.SEXUAL_ORIENTATION = ['HETEROSEXUAL', 'BISEXUAL','HOMOSEXUAL', 'ASEXUAL', 'NONSEXUAL']
        self.alive = [True]
        self.times = []
       # self.current = int(time.time())
       # self.subtime = int(time.time()) - self.century
        #self.valid_times = list(range(self.subtime, self.current, 500))
        #print(self.valid_times)
       # self.bd_get = self.random_selection(self.valid_times)
        #self.times.append(int(self.bd_get))
        self.time_m = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
        self.time_d = list(range(1, 32))
        self.time_y = list(range(1930, 2023))
        self.birthday=[str(self.random_selection(self.time_y))+'-'+ str(self.random_selection(self.time_m))+'-'+str(self.random_selection(self.time_d))]
        #self.birthday = 5 #[int(self.times[0])]
      #  self.age = self._is_born()
        self.male_first_names = ['Liam', 'Noah', 'William', 'James', 'Oliver', 'Benjamin', 'Elijah', 'Lucas', 'Mason', 'Logan', 'Alexander', 'Ethan', 'Jacob', 'Michael', 'Daniel', 'Henry', 'Jackson', 'Sebastian', 'Aiden', 'Matthew', 'Samuel', 'David', 'Joseph', 'Carter', 'Owen', 'Wyatt', 'John', 'Jack', 'Luke', 'Jayden', 'Dylan', 'Grayson', 'Levi', 'Isaac', 'Gabriel', 'Julian', 
        'Mateo', 'Anthony', 'Jaxon', 'Lincoln', 'Joshua', 'Christopher', 'Andrew', 'Theodore', 'Caleb', 'Ryan', 'Asher', 'Nathan', 'Thomas', 'Leo', 'Isaiah', 'Charles', 'Josiah', 'Hudson', 'Christian', 'Hunter', 'Connor', 'Eli', 'Ezra', 'Aaron', 'Landon', 'Adrian', 'Jonathan', 'Nolan', 'Jeremiah', 'Easton', 'Elias', 'Colton', 'Cameron', 'Carson', 'Robert', 
        'Angel', 'Maverick', 'Nicholas', 'Dominic', 'Jaxson', 'Greyson', 'Adam', 'Ian', 'Austin', 'Santiago', 'Jordan', 'Cooper', 'Brayden', 'Roman', 'Evan', 'Ezekiel', 'Xavier', 'Jose', 'Jace', 'Jameson', 'Leonardo', 'Bryson', 'Axel', 'Everett', 'Parker', 'Kayden', 'Miles', 'Sawyer', 'Jason', 'Declan', 'Weston', 'Micah', 'Ayden', 'Wesley', 'Luca', 'Vincent',
        'Damian', 'Zachary', 'Silas', 'Gavin', 'Chase', 'Kai', 'Emmett', 'Harrison', 'Nathaniel', 'Kingston', 'Cole', 'Tyler', 'Bennett', 'Bentley', 'Ryker', 'Tristan', 'Brandon', 'Kevin', 'Luis', 'George', 'Ashton', 'Rowan', 'Braxton', 'Ryder', 'Gael', 'Ivan', 'Diego', 'Maxwell', 'Max', 'Carlos', 'Kaiden', 'Juan', 'Maddox', 'Justin', 'Waylon', 'Calvin', 
        'Giovanni', 'Jonah', 'Abel', 'Jayce', 'Jesus', 'Amir', 'King', 'Beau', 'Camden', 'Alex', 'Jasper', 'Malachi', 'Brody', 'Jude', 'Blake', 'Emmanuel', 'Eric', 'Brooks', 'Elliot', 'Antonio', 'Abraham', 'Timothy', 'Finn', 'Rhett', 'Elliott', 'Edward', 'August', 'Xander', 'Alan', 'Dean', 'Lorenzo', 'Bryce', 'Karter', 'Victor', 'Milo', 'Miguel', 'Hayden', 
        'Graham', 'Grant', 'Zion', 'Tucker', 'Jesse', 'Zayden', 'Joel', 'Richard', 'Patrick', 'Emiliano', 'Avery', 'Nicolas', 'Brantley', 'Dawson', 'Myles', 'Matteo', 'River', 'Steven', 'Thiago', 'Zane', 'Matias', 'Judah', 'Messiah', 'Jeremy', 'Preston', 'Oscar', 'Kaleb', 'Alejandro', 'Marcus', 'Mark', 'Peter', 'Maximus', 'Barrett', 'Jax', 'Andres', 'Holden', 
        'Legend', 'Charlie', 'Knox', 'Kaden', 'Paxton', 'Kyrie', 'Kyle', 'Griffin', 'Josue', 'Kenneth', 'Beckett', 'Enzo', 'Adriel', 'Arthur', 'Felix', 'Bryan', 'Lukas', 'Paul', 'Brian', 'Colt', 'Caden', 'Leon', 'Archer', 'Omar', 'Israel', 'Aidan', 'Theo', 'Javier', 'Remington', 'Jaden', 'Bradley', 'Emilio', 'Colin', 'Riley', 'Cayden', 'Phoenix', 
        'Clayton', 'Simon', 'Ace', 'Nash', 'Derek', 'Rafael', 'Zander', 'Brady', 'Jorge', 'Jake', 'Louis', 'Damien', 'Karson', 'Walker', 'Maximiliano', 'Amari', 'Sean', 'Chance', 'Walter', 'Martin', 'Finley', 'Andre', 'Tobias', 'Cash', 'Corbin', 'Arlo', 'Iker', 'Erick', 'Emerson', 'Gunner', 'Cody', 'Stephen', 'Francisco', 'Killian', 'Dallas', 'Reid', 'Manuel',
        'Lane', 'Atlas', 'Rylan', 'Jensen', 'Ronan', 'Beckham', 'Daxton', 'Anderson', 'Kameron', 'Raymond', 'Orion', 'Cristian', 'Tanner', 'Kyler', 'Jett', 'Cohen', 'Ricardo', 'Spencer', 'Gideon', 'Ali', 'Fernando', 'Jaiden', 'Titus', 'Travis', 'Bodhi', 'Eduardo', 'Dante', 'Ellis', 'Prince', 'Kane', 'Luka', 'Kash', 'Hendrix', 'Desmond', 'Donovan', 'Mario', 
        'Atticus', 'Cruz', 'Garrett', 'Hector', 'Angelo', 'Jeffrey', 'Edwin', 'Cesar', 'Zayn', 'Devin', 'Conor', 'Warren', 'Odin', 'Jayceon', 'Romeo', 'Julius', 'Jaylen', 'Hayes', 'Kayson', 'Muhammad', 'Jaxton', 'Joaquin', 'Caiden', 'Dakota', 'Major', 'Keegan', 'Sergio', 'Marshall', 'Johnny', 'Kade', 'Edgar', 'Leonel', 'Ismael', 'Marco', 'Tyson', 'Wade', 
        'Collin', 'Troy', 'Nasir', 'Conner', 'Adonis', 'Jared', 'Rory', 'Andy', 'Jase', 'Lennox', 'Shane', 'Malik', 'Ari', 'Reed', 'Seth', 'Clark', 'Erik', 'Lawson', 'Trevor', 'Gage', 'Nico', 'Malakai', 'Quinn', 'Cade', 'Johnathan', 'Sullivan', 'Solomon', 'Cyrus', 'Fabian', 'Pedro', 'Frank', 'Shawn', 'Malcolm', 'Khalil', 'Nehemiah', 'Dalton', 'Mathias', 
        'Jay', 'Ibrahim', 'Peyton', 'Winston', 'Kason', 'Zayne', 'Noel', 'Princeton', 'Matthias', 'Gregory', 'Sterling', 'Dominick', 'Elian', 'Grady', 'Russell', 'Finnegan', 'Ruben', 'Gianni', 'Porter', 'Kendrick', 'Leland', 'Pablo', 'Allen', 'Hugo', 'Raiden', 'Kolton', 'Remy', 'Ezequiel', 'Damon', 'Emanuel', 'Zaiden', 'Otto', 'Bowen', 'Marcos', 'Abram', 
        'Kasen', 'Franklin', 'Royce', 'Jonas', 'Sage', 'Philip', 'Esteban', 'Drake', 'Kashton', 'Roberto', 'Harvey', 'Alexis', 'Kian', 'Jamison', 'Maximilian', 'Adan', 'Milan', 'Phillip', 'Albert', 'Dax', 'Mohamed', 'Ronin', 'Kamden', 'Hank', 'Memphis', 'Oakley', 'Augustus', 'Drew', 'Moises', 'Armani', 'Rhys', 'Benson', 'Jayson', 'Kyson', 'Braylen', 'Corey', 
        'Gunnar', 'Omari', 'Alonzo', 'Landen', 'Armando', 'Derrick', 'Dexter', 'Enrique', 'Bruce', 'Nikolai', 'Francis', 'Rocco', 'Kairo', 'Royal', 'Zachariah', 'Arjun', 'Deacon', 'Skyler', 'Eden', 'Alijah', 'Rowen', 'Pierce', 'Uriel', 'Ronald', 'Luciano', 'Tate', 'Frederick', 'Kieran', 'Lawrence', 'Moses', 'Rodrigo', 'Brycen', 'Leonidas', 'Nixon', 'Keith', 
        'Chandler', 'Case', 'Davis', 'Asa', 'Darius', 'Isaias', 'Aden', 'Jaime', 'Landyn', 'Raul', 'Niko', 'Trenton', 'Apollo', 'Cairo', 'Izaiah', 'Scott', 'Dorian', 'Julio', 'Wilder', 'Santino', 'Dustin', 'Donald', 'Raphael', 'Saul', 'Taylor', 'Ayaan', 'Duke', 'Ryland', 'Tatum', 'Ahmed', 'Moshe', 'Edison', 'Emmitt', 'Cannon', 'Alec', 'Danny', 'Keaton', 'Roy',
        'Conrad', 'Roland', 'Quentin', 'Lewis', 'Samson', 'Brock', 'Kylan', 'Cason', 'Ahmad', 'Jalen', 'Nikolas', 'Braylon', 'Kamari', 'Dennis', 'Callum', 'Justice', 'Soren', 'Rayan', 'Aarav', 'Gerardo', 'Ares', 'Brendan', 'Jamari', 'Kaison', 'Yusuf', 'Issac', 'Jasiah', 'Callen', 'Forrest', 'Makai', 'Crew', 'Kobe', 'Bo', 'Julien', 'Mathew', 'Braden', 'Johan', 
        'Marvin', 'Zaid', 'Stetson', 'Casey', 'Ty', 'Ariel', 'Tony', 'Zain', 'Callan', 'Cullen', 'Sincere', 'Uriah', 'Dillon', 'Kannon', 'Colby', 'Axton', 'Cassius', 'Quinton', 'Mekhi', 'Reece', 'Alessandro', 'Jerry', 'Mauricio', 'Sam', 'Trey', 'Mohammad', 'Alberto', 'Gustavo', 'Arturo', 'Fletcher', 'Marcelo', 'Abdiel', 'Hamza', 'Alfredo', 'Chris', 'Finnley', 
        'Curtis', 'Kellan', 'Quincy', 'Kase', 'Harry', 'Kyree', 'Wilson', 'Cayson', 'Hezekiah', 'Kohen', 'Neil', 'Mohammed', 'Raylan', 'Kaysen', 'Lucca', 'Sylas', 'Mack', 'Leonard', 'Lionel', 'Ford', 'Roger', 'Rex', 'Alden', 'Boston', 'Colson', 'Briggs', 'Zeke', 'Dariel', 'Kingsley', 'Valentino', 'Jamir', 'Salvador', 'Vihaan', 'Mitchell', 'Lance', 'Lucian', 
        'Darren', 'Jimmy', 'Alvin', 'Amos', 'Tripp', 'Zaire', 'Layton', 'Reese', 'Casen', 'Colten', 'Brennan', 'Korbin', 'Sonny', 'Bruno', 'Orlando', 'Devon', 'Huxley', 'Boone', 'Maurice', 'Nelson', 'Douglas', 'Randy', 'Gary', 'Lennon', 'Titan', 'Denver', 'Jaziel', 'Noe', 'Jefferson', 'Ricky', 'Lochlan', 'Rayden', 'Bryant', 'Langston', 'Lachlan', 'Clay', 
        'Abdullah', 'Lee', 'Baylor', 'Leandro', 'Ben', 'Kareem', 'Layne', 'Joe', 'Crosby', 'Deandre', 'Demetrius', 'Kellen', 'Carl', 'Jakob', 'Ridge', 'Bronson', 'Jedidiah', 'Rohan', 'Larry', 'Stanley', 'Tomas', 'Shiloh', 'Thaddeus', 'Watson', 'Baker', 'Vicente', 'Koda', 'Jagger', 'Nathanael', 'Carmelo', 'Shepherd', 'Graysen', 'Melvin', 'Ernesto', 'Jamie', 
        'Yosef', 'Clyde', 'Eddie', 'Tristen', 'Grey', 'Ray', 'Tommy', 'Samir', 'Ramon', 'Santana', 'Kristian', 'Marcel', 'Wells', 'Zyaire', 'Brecken', 'Byron', 'Otis', 'Reyansh', 'Axl', 'Joey', 'Trace', 'Morgan', 'Musa', 'Harlan', 'Enoch', 'Henrik', 'Kristopher', 'Talon', 'Rey', 'Guillermo', 'Houston', 'Jon', 'Vincenzo', 'Dane', 'Terry', 'Azariah', 'Castiel', 
        'Kye', 'Augustine', 'Zechariah', 'Joziah', 'Kamryn', 'Hassan', 'Jamal', 'Chaim', 'Bodie', 'Emery', 'Branson', 'Jaxtyn', 'Kole', 'Wayne', 'Aryan', 'Alonso', 'Brixton', 'Madden', 'Allan', 'Flynn', 'Jaxen', 'Harley', 'Magnus', 'Sutton', 'Dash', 'Anders', 'Westley', 'Brett', 'Emory', 'Felipe', 'Yousef', 'Jadiel', 'Mordechai', 'Dominik', 'Junior', 'Eliseo', 
        'Fisher', 'Harold', 'Jaxxon', 'Kamdyn', 'Maximo', 'Caspian', 'Kelvin', 'Damari', 'Fox', 'Trent', 'Hugh', 'Briar', 'Franco', 'Keanu', 'Terrance', 'Yahir', 'Ameer', 'Kaiser', 'Thatcher', 'Ishaan', 'Koa', 'Merrick', 'Coen', 'Rodney', 'Brayan', 'London', 'Rudy', 'Gordon', 'Bobby', 'Aron', 'Marc', 'Van', 'Anakin', 'Canaan', 'Dario', 'Reginald', 'Westin', 'Darian',
        'Ledger', 'Leighton', 'Maxton', 'Tadeo', 'Valentin', 'Aldo', 'Khalid', 'Nickolas', 'Toby', 'Dayton', 'Jacoby', 'Billy', 'Gatlin', 'Elisha', 'Jabari', 'Jermaine', 'Alvaro', 'Marlon', 'Mayson', 'Blaze', 'Jeffery', 'Kace', 'Braydon', 'Achilles', 'Brysen', 'Saint', 'Xzavier', 'Aydin', 'Eugene', 'Adrien', 'Cain', 'Kylo', 'Nova', 'Onyx', 'Arian', 'Bjorn', 'Jerome', 
        'Miller', 'Alfred', 'Kenzo', 'Kyng', 'Leroy', 'Maison', 'Jordy', 'Stefan', 'Wallace', 'Benicio', 'Kendall', 'Zayd', 'Blaine', 'Tristian', 'Anson', 'Gannon', 'Jeremias', 'Marley', 'Ronnie', 'Dangelo', 'Kody', 'Will', 'Bentlee', 'Gerald', 'Salvatore', 'Turner', 'Chad', 'Misael', 'Mustafa', 'Konnor', 'Maxim', 'Rogelio', 'Zakai', 'Cory', 'Judson', 'Brentley', 'Darwin', 
        'Louie', 'Ulises', 'Dakari', 'Rocky', 'Wesson', 'Alfonso', 'Payton', 'Dwayne', 'Juelz', 'Duncan', 'Keagan', 'Deshawn', 'Bode', 'Bridger', 'Skylar', 'Brodie', 'Landry', 'Avi', 'Keenan', 'Reuben', 'Jaxx', 'Rene', 'Yehuda', 'Imran', 'Yael', 'Alexzander', 'Willie', 'Cristiano', 'Heath', 'Lyric', 'Davion', 'Elon', 'Karsyn', 'Krew', 'Jairo', 'Maddux', 'Ephraim', 'Ignacio', 
        'Vivaan', 'Aries', 'Vance', 'Boden', 'Lyle', 'Ralph', 'Reign', 'Camilo', 'Draven', 'Terrence', 'Idris', 'Ira', 'Javion', 'Jericho', 'Khari', 'Marcellus', 'Creed', 'Shepard', 'Terrell', 'Ahmir', 'Camdyn', 'Cedric', 'Howard', 'Jad', 'Zahir', 'Harper', 'Justus', 'Forest', 'Gibson', 'Zev', 'Alaric', 'Decker', 'Ernest', 'Jesiah', 'Torin', 'Benedict', 'Bowie', 'Deangelo', 
        'Genesis', 'Harlem', 'Kalel', 'Kylen', 'Bishop', 'Immanuel', 'Lian', 'Zavier', 'Archie', 'Davian', 'Gus', 'Kabir', 'Korbyn', 'Randall', 'Benton', 'Coleman', 'Markus '] 
        
        self.female_first_names = ['Myriam', 'Laila', 'Habiba', 'Bushra', 'Nadia', 'Noura', 'Nona', 'Reenad', 'Alexandra', 'Ola', 'Mariam', 'Fatima', 'Hadeel', 'Alaa', 'Yasmine', 'Lamia', 'Alsama', 'Ouiam', 'Lulu', 'Ghayda', 'Salsabyl', 'Aleyna', 'Zeynep', 'Elif', 'Ecrin', 'Yagmur', 'Azra', 'Zehra', 'Nisanur', 'Ela', 'Belinay', 'Nehir', 'Fatma', 'Ayse', 'Emine', 'Hatice', 
        'Meryem', 'Serife', 'Sultan', 'Lian', 'Luan', 'Mai', 'Mei', 'Jun', 'Amy', 'Gina', 'Mary', 'Mimi', 'Christine', 'Jessica', 'Rosa', 'Andrea', 'Esther', 'Celina', 'Tiffany', 'Lucy', 'Meiling', 'Irene', 'Lauren', 'Grace', 'Ruby', 'Anna', 'Iris', 'Connie', 'Lisa', 'Melissa', 'Victoria', 'Alice', 'Christina', 'Karen', 'Yui', 'Yuna', 'Hina', 'Hinata', 'Mei', 'Mio', 'Saki', 
        'Akari', 'Momoka', 'Airi', 'Yuka', 'Ayane', 'Ayase', 'Misaki', 'Rina', 'Hana', 'Shiori', 'Fuka', 'Anna', 'Nana', 'Himari', 'Sakura', 'Yuuka', 'Nanami', 'Kana', 'Mai', 'Haruka', 'Tomoko', 'Fumiko', 'Hiroko', 'Kyouko', 'Chiyo', 'Fuuko', 'Miu', 'Chitose', 'Konata', 'Futaba', 'Kirino', 'Anko', 'Ritsu', 'Yayoi', 'Tomoe', 'Sayaka', 'Hitoha', 'Tsumugi', 'Abeba', 'Abrihet', 
        'Adanech', 'Adina', 'Afework', 'Alam', 'Alitash', 'Amhara', 'Ayana', 'Bathsheba', 'Berta', 'Cheren', 'Debtera', 'Desta', 'Fana', 'Falasha', 'Falashina', 'Fannah', 'Habesha', 'Kayla', 'Kelile', 'Lishan', 'Magdala', 'Makda', 'Makeda', 'Mandera', 'Melesse', 'Negasi', 'Nyala', 'Qwara', 'Retta', 'Seble', 'Senalat', 'Sheba', 'Shinasha', 'Teru', 'Tsage', 'Wagaye', 'Zala', 
        'Zeina', 'Zena', 'Zenia', 'Zere', 'Ciara', 'Oluwafunke', 'Ololade', 'Eraikhoba', 'Kasheina', 'Ibukunoluwa', 'Chioma', 'Afulenu', 'Adaobi', 'Abieyuwa', 'Plangnan', 'Odosa', 'Chidinma', 'Ohoma', 'Ogoma', 'Adejoke', 'Onari', 'Sherifat', 'Olayemi', 'Ngoma', 'Titilayo', 'Tamunodieprieye', 'Adaeze', 'Nadoca', 'Chiapali', 'Efemena', 'Loutoyopnica', 'Moraya', 'Omowunmi', 
        'Perosola', 'Feyisara', 'Oluwakemi', 'Oyindamola', 'Emma', 'Julia', 'Isa', 'Zoë', 'Anna', 'Eva', 'Sara', 'Lotte', 'Lisa', 'Fleur', 'Sarah', 'Lieke', 'Roos', 'Nora', 'Sofie', 'Maud', 'Sanne', 'Esmee', 'Noor', 'Olivia', 'Amy', 'Milou', 'Nina', 'Femke', 'Anne', 'Jasmijn', 'Emily', 'Vera', 'Eline', 'Floortje', 'Iris', 'Elise', 'Maria', 'Isabel', 'Evy', 'Lana', 'Tessa', 
        'Lara', 'Charlotte', 'Ella', 'Suus', 'Mirthe', 'Anouk', 'Ilse', 'Laura', 'Hanna', 'Merel', 'Mara', 'Chloe', 'Sophie', 'Emily', 'Emma', 'Sarah', 'Brianna', 'Ashley', 'Linda', 'Suzy', 'Jenny', 'Zoe', 'Hannah', 'Hailey', 'Alexis', 'Grace', 'Olivia', 'Samantha', 'Madison', 'Jessica', 'Lauren', 'Megan', 'Brittany', 'Amber', 'Allison', 'Heather', 'Amy', 'Monica', 'Danas', 
        'Amanda', 'Laura', 'Silvia', 'Lily', 'Taylor', 'Rachel', 'Katie', 'Lucy', 'Jade', 'Holly', 'Ashleigh', 'Lisa', 'Anna', 'Laura', 'Sarah', 'Lena', 'Leonie', 'Johanna', 'Sophie', 'Marie', 'Hannah', 'Lara', 'Alina', 'Jana', 'Sabrina', 'Anja', 'Melanie', 'Steffi', 'Svenja', 'Natascha', 'Tamara', 'Nele', 'Clara', 'Mia', 'Katja', 'Eva', 'Fiona', 'Hanna', 'Isabel', 'Tanja', 
        'Emily', 'Stella', 'Miriam', 'Stefanie', 'Karolina', 'Kasia', 'Marta', 'Monika', 'Magda', 'Paulina', 'Aleksandra', 'Natalia', 'Anna', 'Klaudia', 'Joanna', 'Julia', 'Kate', 'Sara', 'Ada', 'Ela', 'Maja', 'Emilia', 'Basia', 'Aneta', 'Anastasia', 'Ann', 'Alina', 'Marina', 'Nastya', 'Anna', 'Dasha', 'Julia', 'Victoria', 'Sasha', 'Maria', 'Yana', 'Kate', 'Olga', 'Tania', 
        'Ira', 'Anastasiya', 'Sophie', 'Sveta', 'Yulia', 'Irina', 'Natalia', 'Oksana', 'Olya', 'Alyona', 'Elena', 'Masha', 'Diana', 'Tatiana', 'Natasha', 'Vlada', 'Nastia', 'Laura', 'Natalya', 'Rita', 'Taya', 'Asya', 'Valia', 'Hanna', 'Katia', 'Anya', 'Ivanna', 'Inna', 'Yuliya', 'Valeria', 'Laura', 'Lorena', 'Maria', 'Ana-Maria', 'Andrea', 'Natalia', 'Paula', 'Alejandra', 
        'Tititorres', 'Angelica', 'Manuela', 'Valentina', 'Camila', 'Valeria', 'Daniela', 'Mariana', 'Sandra', 'Eliana', 'Luisa', 'Isabel', 'Lucia', 'Cristina', 'Gabriella', 'Maria', 'Carmen', 'Veronica', 'Alejandra', 'Gabriela', 'Patricia', 'Isabella', 'Marianna', 'Daniella', 'Victoria', 'Angelina', 'Francisca', 'Rosalina', 'Elizabeth', 'Sandra', 'Marta', 'Cristina', 
        'Monica', 'Susana', 'Andrea', 'Lola', 'Luna', 'Priyanka', 'Tanya', 'Aditi', 'Divya', 'Shreya', 'Anjali', 'Priya', 'Tanvi', 'Shivangi', 'Gayatri', 'Ananya', 'Juvina', 'Prachi', 'Pavithra', 'Mahima', 'Radhika', 'Riya', 'Ishita', 'Sakshi', 'Archita', 'Vaishnavi', 'Leah', 'Anisha', 'Akansha', 'Seema', 'Binita', 'Keerthi', 'Akshata', 'Manasi', 'Anoushka', 'Alisha', 
        'Kavya', 'Maria', 'Dimitra', 'Anastasia', 'Irene', 'Konstantina', 'Katerina', 'Mary', 'Anna', 'Joanna', 'Helen', 'Alexandra', 'Christina', 'Eva', 'Elena', 'Sophie', 'Sofia', 'Zoe', 'Marina', 'Evi', 'Alexia', 'Emma', 'Danae', 'Athena', 'Carmen', 'Maria', 'Veronica', 'Isabella', 'Daniella', 'Marianna', 'Angelina', 'Monica', 'Sofia', 'Sara', 'Emma', 'Alice', 'Francesca', 
        'Noemi', 'Viola', 'Elisa', 'Rebecca', 'Angelica', 'Martina', 'Julia', 'Laura', 'Lola', 'Silvia', 'Carmen', 'Maria', 'Veronica', 'Isabella', 'Daniella', 'Marianna', 'Angelina', 'Monica', 'Sofia', 'Sara', 'Emma', 'Alice', 'Francesca', 'Noemi', 'Viola', 'Elisa', 'Rebecca', 'Angelica', 'Martina', 'Julia', 'Laura', 'Lola', 'Silvia', 'Anna', 'Laura', 'Veera', 'Anni', 'Hanna', 
        'Jenni', 'Noora', 'Emma', 'Emilia', 'Sara', 'Sonja', 'Elina', 'Jenna', 'Julia', 'Sofia', 'Heidi', 'Annika', 'Ella', 'Krista', 'Jasmin', 'Emmi', 'Riikka', 'Viivi', 'Nina', 'Susanna', 'Silje', 'Julie', 'Thea', 'Anna', 'Vilde', 'Emma', 'Sara', 'Helene', 'Emilie', 'Stine', 'Maja', 'Nora', 'Hilde', 'Victoria', 'Marte', 'Oda', 'Elise', 'Amalie', 'Lotte', 'Katrine', 'Anne', 
        'Vivian', 'Melissa', 'Sandra', 'Charlotte', 'Emma', 'Amanda', 'Elin', 'Anna', 'Julia', 'Linnea', 'Sara', 'Hanna', 'Johanna', 'Emilia', 'Ida', 'Malin', 'Lisa', 'Matilda', 'Sofia', 'Linn', 'Lovisa', 'Sandra', 'Wilma', 'Felicia', 'Alice', 'Olivia', 'Elevina', 'Ellen', 'Molly', 'Linda', 'Alma', 'Sanna', 'Maja', 'Sofie', 'Erica', 'Ahulani', 'Akela', 'Amaui', 'Ani', 'Basha', 
        'Dara', 'Daba', 'Daya', 'Edria', 'Endora', 'Halia', 'Inoa', 'Kailani', 'Kalei', 'Kali', 'Kapua', 'kawena', 'Kunani', 'Lani', 'Lea', 'Laulani', 'Leilani', 'Lokalia', 'Makala', 'Mana', 'Mei', 'Nana', 'Nani', 'Noelani', 'Okalani', 'Olina', 'Pililani', 'Pua', 'Roselani', 'Wainani', 'Nicole', 'Kim', 'Marie', 'Angelica', 'Sarah', 'Kate', 'Anna', 'Bea', 'Sam', 'Jessica', 
        'Michelle', 'Patricia', 'Trisha', 'Anne', 'Bianca', 'Victoria', 'Sheila', 'Camille', 'Joyce', 'NIkki', 'Angela', 'Cheskka', 'Pauline', 'Erin', 'Alyssa', 'Yanna', 'Angel', 'Mio']

        self.skin = ["#87533d", "#87533e", "#87533f", "#875340", "#875341", "#875342", "#875343", "#875344", "#875345", "#875346", "#875347", "#875348", "#875349", "#87534a", "#87534b", "#87534c", "#87534d", "#87534e", "#87534f", "#875350", "#87543d", "#87543e", "#87543f", "#875440", "#875441", "#875442", "#875443", "#875444", "#875445", "#875446", "#875447", "#875448", "#875449", "#87544a", "#87544b", "#87544c", "#87544d", "#87544e", "#87544f", "#875450", "#87553d", "#87553e", "#87553f", "#875540", "#875541", "#875542", "#875543", "#875544", "#875545", "#875546", "#875547", "#875548", "#875549", "#87554a", "#87554b", "#87554c", "#87554d", "#87554e", "#87554f", "#875550", "#87563d", "#87563e", "#87563f", "#875640", "#875641", "#875642", "#875643", "#875644", "#875645", "#875646", "#875647", "#875648", "#875649", "#87564a", "#87564b", "#87564c", "#87564d", "#87564e", "#87564f", "#875650", "#87573d", "#87573e", "#87573f", "#875740", "#875741", "#875742", "#875743", "#875744", "#875745", "#875746", "#875747", "#875748", "#875749", "#87574a", "#87574b", "#87574c", "#87574d", "#87574e", "#87574f", "#875750", "#87583d", "#87583e", "#87583f", "#875840", "#875841", "#875842", "#875843", "#875844", "#875845", "#875846", "#875847", "#875848", "#875849", "#87584a", "#87584b", "#87584c", "#87584d", "#87584e", "#87584f", "#875850", "#87593d", "#87593e", "#87593f", "#875940", "#875941", "#875942", "#875943", "#875944", "#875945", "#875946", "#875947", "#875948", "#875949", "#87594a", "#87594b", "#87594c", "#87594d", "#87594e", "#87594f", "#875950", "#875a3d", "#875a3e", "#875a3f", "#875a40", "#875a41", "#875a42", "#875a43", "#875a44", "#875a45", "#875a46", "#875a47", "#875a48", "#875a49", "#875a4a", "#875a4b", "#875a4c", "#875a4d", "#875a4e", "#875a4f", "#875a50", "#875b3d", "#875b3e", "#875b3f", "#875b40", "#875b41", "#875b42", "#875b43", "#875b44", "#875b45", "#875b46", "#875b47", "#875b48", "#875b49", "#875b4a", "#875b4b", "#875b4c", "#875b4d", "#875b4e", "#875b4f", "#875b50", "#875c3d", "#875c3e", "#875c3f", "#875c40", "#875c41", "#875c42", "#875c43", "#875c44", "#875c45", "#875c46", "#875c47", "#875c48", "#875c49", "#875c4a", "#875c4b", "#875c4c", "#875c4d", "#875c4e", "#875c4f", "#875c50", "#875d3d", "#875d3e", "#875d3f", "#875d40", "#875d41", "#875d42", "#875d43", "#875d44", "#875d45", "#875d46", "#875d47", "#875d48", "#875d49", "#875d4a", "#875d4b", "#875d4c", "#875d4d", "#875d4e", "#875d4f", "#875d50", "#875e3d", "#875e3e", "#875e3f", "#875e40", "#875e41", "#875e42", "#875e43", "#875e44", "#875e45", "#875e46", "#875e47", "#875e48", "#875e49", "#875e4a", "#875e4b", "#875e4c", "#875e4d", "#875e4e", "#875e4f", "#875e50", "#875f3d", "#875f3e", "#875f3f", "#875f40", "#875f41", "#875f42", "#875f43", "#875f44", "#875f45", "#875f46", "#875f47", "#875f48", "#875f49", "#875f4a", "#875f4b", "#875f4c", "#875f4d", "#875f4e", "#875f4f", "#875f50", "#87603d", "#87603e", "#87603f", "#876040", "#876041", "#876042", "#876043", "#876044", "#876045", "#876046", "#876047", "#876048", "#876049", "#87604a", "#87604b", "#87604c", "#87604d", "#87604e", "#87604f", "#876050", "#87613d", "#87613e", "#87613f", "#876140", "#876141", "#876142", "#876143", "#876144", "#876145", "#876146", "#876147", "#876148", "#876149", "#87614a", "#87614b", "#87614c", "#87614d", "#87614e", "#87614f", "#876150", "#87623d", "#87623e", "#87623f", "#876240", "#876241", "#876242", "#876243", "#876244", "#876245", "#876246", "#876247", "#876248", "#876249", "#87624a", "#87624b", "#87624c", "#87624d", "#87624e", "#87624f", "#876250", "#87633d", "#87633e", "#87633f", "#876340", "#876341", "#876342", "#876343", "#876344", "#876345", "#876346", "#876347", "#876348", "#876349", "#87634a", "#87634b", "#87634c", "#87634d", "#87634e", "#87634f", "#876350", "#87643d", "#87643e", "#87643f", "#876440", "#876441", "#876442", "#876443", "#876444", "#876445", "#876446", "#876447", "#876448", "#876449", "#87644a", "#87644b", "#87644c", "#87644d", "#87644e", "#87644f", "#876450", "#87653d", "#87653e", "#87653f", "#876540", "#876541", "#876542", "#876543", "#876544", "#876545", "#876546", "#876547", "#876548", "#876549", "#87654a", "#87654b", "#87654c", "#87654d", "#87654e", "#87654f", "#876550", "#87663d", "#87663e", "#87663f", "#876640", "#876641", "#876642", "#876643", "#876644", "#876645", "#876646", "#876647", "#876648", "#876649", "#87664a", "#87664b", "#87664c", "#87664d", "#87664e", "#87664f", "#876650", "#88533d", "#88533e", "#88533f", "#885340", "#885341", "#885342", "#885343", "#885344", "#885345", "#885346", "#885347", "#885348", "#885349", "#88534a", "#88534b", "#88534c", "#88534d", "#88534e", "#88534f", "#885350", "#88543d", "#88543e", "#88543f", "#885440", "#885441", "#885442", "#885443", "#885444", "#885445", "#885446", "#885447", "#885448", "#885449", "#88544a", "#88544b", "#88544c", "#88544d", "#88544e", "#88544f", "#885450", "#88553d", "#88553e", "#88553f", "#885540", "#885541", "#885542", "#885543", "#885544", "#885545", "#885546", "#885547", "#885548", "#885549", "#88554a", "#88554b", "#88554c", "#88554d", "#88554e", "#88554f", "#885550", "#88563d", "#88563e", "#88563f", "#885640", "#885641", "#885642", "#885643", "#885644", "#885645", "#885646", "#885647", "#885648", "#885649", "#88564a", "#88564b", "#88564c", "#88564d", "#88564e", "#88564f", "#885650", "#88573d", "#88573e", "#88573f", "#885740", "#885741", "#885742", "#885743", "#885744", "#885745", "#885746", "#885747", "#885748", "#885749", "#88574a", "#88574b", "#88574c", "#88574d", "#88574e", "#88574f", "#885750", "#88583d", "#88583e", "#88583f", "#885840", "#885841", "#885842", "#885843", "#885844", "#885845", "#885846", "#885847", "#885848", "#885849", "#88584a", "#88584b", "#88584c", "#88584d", "#88584e", "#88584f", "#885850", "#88593d", "#88593e", "#88593f", "#885940", "#885941", "#885942", "#885943", "#885944", "#885945", "#885946", "#885947", "#885948", "#885949", "#88594a", "#88594b", "#88594c", "#88594d", "#88594e", "#88594f", "#885950", "#885a3d", "#885a3e", "#885a3f", "#885a40", "#885a41", "#885a42", "#885a43", "#885a44", "#885a45", "#885a46", "#885a47", "#885a48", "#885a49", "#885a4a", "#885a4b", "#885a4c", "#885a4d", "#885a4e", "#885a4f", "#885a50", "#885b3d", "#885b3e", "#885b3f", "#885b40", "#885b41", "#885b42", "#885b43", "#885b44", "#885b45", "#885b46", "#885b47", "#885b48", "#885b49", "#885b4a", "#885b4b", "#885b4c", "#885b4d", "#885b4e", "#885b4f", "#885b50", "#885c3d", "#885c3e", "#885c3f", "#885c40", "#885c41", "#885c42", "#885c43", "#885c44", "#885c45", "#885c46", "#885c47", "#885c48", "#885c49", "#885c4a", "#885c4b", "#885c4c", "#885c4d", "#885c4e", "#885c4f", "#885c50", "#885d3d", "#885d3e", "#885d3f", "#885d40", "#885d41", "#885d42", "#885d43", "#885d44", "#885d45", "#885d46", "#885d47", "#885d48", "#885d49", "#885d4a", "#885d4b", "#885d4c", "#885d4d", "#885d4e", "#885d4f", "#885d50", "#885e3d", "#885e3e", "#885e3f", "#885e40", "#885e41", "#885e42", "#885e43", "#885e44", "#885e45", "#885e46", "#885e47", "#885e48", "#885e49", "#885e4a", "#885e4b", "#885e4c", "#885e4d", "#885e4e", "#885e4f", "#885e50", "#885f3d", "#885f3e", "#885f3f", "#885f40", "#885f41", "#885f42", "#885f43", "#885f44", "#885f45", "#885f46", "#885f47", "#885f48", "#885f49", "#885f4a", "#885f4b", "#885f4c", "#885f4d", "#885f4e", "#885f4f", "#885f50", "#88603d", "#88603e", "#88603f", "#886040", "#886041", "#886042", "#886043", "#886044", "#886045", "#886046", "#886047", "#886048", "#886049", "#88604a", "#88604b", "#88604c", "#88604d", "#88604e", "#88604f", "#886050", "#88613d", "#88613e", "#88613f", "#886140", "#886141", "#886142", "#886143", "#886144", "#886145", "#886146", "#886147", "#886148", "#886149", "#88614a", "#88614b", "#88614c", "#88614d", "#88614e", "#88614f", "#886150", "#88623d", "#88623e", "#88623f", "#886240", "#886241", "#886242", "#886243", "#886244", "#886245", "#886246", "#886247", "#886248", "#886249", "#88624a", "#88624b", "#88624c", "#88624d", "#88624e", "#88624f", "#886250", "#88633d", "#88633e", "#88633f", "#886340", "#886341", "#886342", "#886343", "#886344", "#886345", "#886346", "#886347", "#886348", "#886349", "#88634a", "#88634b", "#88634c", "#88634d", "#88634e", "#88634f", "#886350", "#88643d", "#88643e", "#88643f", "#886440", "#886441", "#886442", "#886443", "#886444", "#886445", "#886446", "#886447", "#886448", "#886449", "#88644a", "#88644b", "#88644c", "#88644d", "#88644e", "#88644f", "#886450", "#88653d", "#88653e", "#88653f", "#886540", "#886541", "#886542", "#886543", "#886544", "#886545", "#886546", "#886547", "#886548", "#886549", "#88654a", "#88654b", "#88654c", "#88654d", "#88654e", "#88654f", "#886550", "#88663d", "#88663e", "#88663f", "#886640", "#886641", "#886642", "#886643", "#886644", "#886645", "#886646", "#886647", "#886648", "#886649", "#88664a", "#88664b", "#88664c", "#88664d", "#88664e", "#88664f", "#886650", "#89533d", "#89533e", "#89533f", "#895340", "#895341", "#895342", "#895343", "#895344", "#895345", "#895346", "#895347", "#895348", "#895349", "#89534a", "#89534b", "#89534c", "#89534d", "#89534e", "#89534f", "#895350", "#89543d", "#89543e", "#89543f", "#895440", "#895441", "#895442", "#895443", "#895444", "#895445", "#895446", "#895447", "#895448", "#895449", "#89544a", "#89544b", "#89544c", "#89544d", "#89544e", "#89544f", "#895450", "#89553d", "#89553e", "#89553f", "#895540", "#895541", "#895542", "#895543", "#895544", "#895545", "#895546", "#895547", "#895548", "#895549", "#89554a", "#89554b", "#89554c", "#89554d", "#89554e", "#89554f", "#895550", "#89563d", "#89563e", "#89563f", "#895640", "#895641", "#895642", "#895643", "#895644", "#895645", "#895646", "#895647", "#895648", "#895649", "#89564a", "#89564b", "#89564c", "#89564d", "#89564e", "#89564f", "#895650", "#89573d", "#89573e", "#89573f", "#895740", "#895741", "#895742", "#895743", "#895744", "#895745", "#895746", "#895747", "#895748", "#895749", "#89574a", "#89574b", "#89574c", "#89574d", "#89574e", "#89574f", "#895750", "#89583d", "#89583e", "#89583f", "#895840", "#895841", "#895842", "#895843", 
        "#895844", "#895845", "#895846", "#895847", "#895848", "#895849", "#89584a", "#89584b", "#89584c", "#89584d", "#89584e", "#89584f", "#895850", "#89593d", "#89593e", "#89593f", "#895940", "#895941", "#895942", "#895943", "#895944", "#895945", "#895946", "#895947", "#895948", "#895949", "#89594a", "#89594b", "#89594c", "#89594d", "#89594e", "#89594f", "#895950", "#895a3d", "#895a3e", "#895a3f", "#895a40", "#895a41", "#895a42", "#895a43", "#895a44", "#895a45", "#895a46", "#895a47", "#895a48", "#895a49", "#895a4a", "#895a4b", "#895a4c", "#895a4d", "#895a4e", "#895a4f", "#895a50", "#895b3d", "#895b3e", "#895b3f", "#895b40", "#895b41", "#895b42", "#895b43", "#895b44", "#895b45", "#895b46", "#895b47", "#895b48", "#895b49", "#895b4a", "#895b4b", "#895b4c", "#895b4d", "#895b4e", "#895b4f", "#895b50", "#895c3d", "#895c3e", "#895c3f", "#895c40", "#895c41", "#895c42", "#895c43", "#895c44", "#895c45", "#895c46", "#895c47", "#895c48", "#895c49", "#895c4a", "#895c4b", "#895c4c", "#895c4d", "#895c4e", "#895c4f", "#895c50", "#895d3d", "#895d3e", "#895d3f", "#895d40", "#895d41", "#895d42", "#895d43", "#895d44", "#895d45", "#895d46", "#895d47", "#895d48", "#895d49", "#895d4a", "#895d4b", "#895d4c", "#895d4d", "#895d4e", "#895d4f", "#895d50", "#895e3d", "#895e3e", "#895e3f", "#895e40", "#895e41", "#895e42", "#895e43", "#895e44", "#895e45", "#895e46", "#895e47", "#895e48", "#895e49", "#895e4a", "#895e4b", "#895e4c", "#895e4d", "#895e4e", "#895e4f", "#895e50", "#895f3d", "#895f3e", "#895f3f", "#895f40", "#895f41", "#895f42", "#895f43", "#895f44", "#895f45", "#895f46", "#895f47", "#895f48", "#895f49", "#895f4a", "#895f4b", "#895f4c", "#895f4d", "#895f4e", "#895f4f", "#895f50", "#89603d", "#89603e", "#89603f", "#896040", "#896041", "#896042", "#896043", "#896044", "#896045", "#896046", "#896047", "#896048", "#896049", "#89604a", "#89604b", "#89604c", "#89604d", "#89604e", "#89604f", "#896050", "#89613d", "#89613e", "#89613f", "#896140", "#896141", "#896142", "#896143", "#896144", "#896145", "#896146", "#896147", "#896148", "#896149", "#89614a", "#89614b", "#89614c", "#89614d", "#89614e", "#89614f", "#896150", "#89623d", "#89623e", "#89623f", "#896240", "#896241", "#896242", "#896243", "#896244", "#896245", "#896246", "#896247", "#896248", "#896249", "#89624a", "#89624b", "#89624c", "#89624d", "#89624e", "#89624f", "#896250", "#89633d", "#89633e", "#89633f", "#896340", "#896341", "#896342", "#896343", "#896344", "#896345", "#896346", "#896347", "#896348", "#896349", "#89634a", "#89634b", "#89634c", "#89634d", "#89634e", "#89634f", "#896350", "#89643d", "#89643e", "#89643f", "#896440", "#896441", "#896442", "#896443", "#896444", "#896445", "#896446", "#896447", "#896448", "#896449", "#89644a", "#89644b", "#89644c", "#89644d", "#89644e", "#89644f", "#896450", "#89653d", "#89653e", "#89653f", "#896540", "#896541", "#896542", "#896543", "#896544", "#896545", "#896546", "#896547", "#896548", "#896549", "#89654a", "#89654b", "#89654c", "#89654d", "#89654e", "#89654f", "#896550", "#89663d", "#89663e", "#89663f", "#896640", "#896641", "#896642", "#896643", "#896644", "#896645", "#896646", "#896647", "#896648", "#896649", "#89664a", "#89664b", "#89664c", "#89664d", "#89664e", "#89664f", "#896650", "#8a533d", "#8a533e", "#8a533f", "#8a5340", "#8a5341", "#8a5342", "#8a5343", "#8a5344", "#8a5345", "#8a5346", "#8a5347", "#8a5348", "#8a5349", "#8a534a", "#8a534b", "#8a534c", "#8a534d", "#8a534e", "#8a534f", "#8a5350", "#8a543d", "#8a543e", "#8a543f", "#8a5440", "#8a5441", "#8a5442", "#8a5443", "#8a5444", "#8a5445", "#8a5446", "#8a5447", "#8a5448", "#8a5449", "#8a544a", "#8a544b", "#8a544c", "#8a544d", "#8a544e", "#8a544f", "#8a5450", "#8a553d", "#8a553e", "#8a553f", "#8a5540", "#8a5541", "#8a5542", "#8a5543", "#8a5544", "#8a5545", "#8a5546", "#8a5547", "#8a5548", "#8a5549", "#8a554a", "#8a554b", "#8a554c", "#8a554d", "#8a554e", "#8a554f", "#8a5550", "#8a563d", "#8a563e", "#8a563f", "#8a5640", "#8a5641", "#8a5642", "#8a5643", "#8a5644", "#8a5645", "#8a5646", "#8a5647", "#8a5648", "#8a5649", "#8a564a", "#8a564b", "#8a564c", "#8a564d", "#8a564e", "#8a564f", "#8a5650", "#8a573d", "#8a573e", "#8a573f", "#8a5740", "#8a5741", "#8a5742", "#8a5743", "#8a5744", "#8a5745", "#8a5746", "#8a5747", "#8a5748", "#8a5749", "#8a574a", "#8a574b", "#8a574c", "#8a574d", "#8a574e", "#8a574f", "#8a5750", "#8a583d", "#8a583e", "#8a583f", "#8a5840", "#8a5841", "#8a5842", "#8a5843", "#8a5844", "#8a5845", "#8a5846", "#8a5847", "#8a5848", "#8a5849", "#8a584a", "#8a584b", "#8a584c", "#8a584d", "#8a584e", "#8a584f", "#8a5850", "#8a593d", "#8a593e", "#8a593f", "#8a5940", "#8a5941", "#8a5942", "#8a5943", "#8a5944", "#8a5945", "#8a5946", "#8a5947", "#8a5948", "#8a5949", "#8a594a", "#8a594b", "#8a594c", "#8a594d", "#8a594e", "#8a594f", "#8a5950", "#8a5a3d", "#8a5a3e", "#8a5a3f", "#8a5a40", "#8a5a41", "#8a5a42", "#8a5a43", "#8a5a44", "#8a5a45", "#8a5a46", "#8a5a47", "#8a5a48", "#8a5a49", "#8a5a4a", "#8a5a4b", "#8a5a4c", "#8a5a4d", "#8a5a4e", "#8a5a4f", "#8a5a50", "#8a5b3d", "#8a5b3e", "#8a5b3f", "#8a5b40", "#8a5b41", "#8a5b42", "#8a5b43", "#8a5b44", "#8a5b45", "#8a5b46", "#8a5b47", "#8a5b48", "#8a5b49", "#8a5b4a", "#8a5b4b", "#8a5b4c", "#8a5b4d", "#8a5b4e", "#8a5b4f", "#8a5b50", "#8a5c3d", "#8a5c3e", "#8a5c3f", "#8a5c40", "#8a5c41", "#8a5c42", "#8a5c43", "#8a5c44", "#8a5c45", "#8a5c46", "#8a5c47", "#8a5c48", "#8a5c49", "#8a5c4a", "#8a5c4b", "#8a5c4c", "#8a5c4d", "#8a5c4e", "#8a5c4f", "#8a5c50", "#8a5d3d", "#8a5d3e", "#8a5d3f", "#8a5d40", "#8a5d41", "#8a5d42", "#8a5d43", "#8a5d44", "#8a5d45", "#8a5d46", "#8a5d47", "#8a5d48", "#8a5d49", "#8a5d4a", "#8a5d4b", "#8a5d4c", "#8a5d4d", "#8a5d4e", "#8a5d4f", "#8a5d50", "#8a5e3d", "#8a5e3e", "#8a5e3f", "#8a5e40", "#8a5e41", "#8a5e42", "#8a5e43", "#8a5e44", "#8a5e45", "#8a5e46", "#8a5e47", "#8a5e48", "#8a5e49", "#8a5e4a", "#8a5e4b", "#8a5e4c", "#8a5e4d", "#8a5e4e", "#8a5e4f", "#8a5e50", "#8a5f3d", "#8a5f3e", "#8a5f3f", "#8a5f40", "#8a5f41", "#8a5f42", "#8a5f43", "#8a5f44", "#8a5f45", "#8a5f46", "#8a5f47", "#8a5f48", "#8a5f49", "#8a5f4a", "#8a5f4b", "#8a5f4c", "#8a5f4d", "#8a5f4e", "#8a5f4f", "#8a5f50", "#8a603d", "#8a603e", "#8a603f", "#8a6040", "#8a6041", "#8a6042", "#8a6043", "#8a6044", "#8a6045", "#8a6046", "#8a6047", "#8a6048", "#8a6049", "#8a604a", "#8a604b", "#8a604c", "#8a604d", "#8a604e", "#8a604f", "#8a6050", "#8a613d", "#8a613e", "#8a613f", "#8a6140", "#8a6141", "#8a6142", "#8a6143", "#8a6144", "#8a6145", "#8a6146", "#8a6147", "#8a6148", "#8a6149", "#8a614a", "#8a614b", "#8a614c", "#8a614d", "#8a614e", "#8a614f", "#8a6150", "#8a623d", "#8a623e", "#8a623f", "#8a6240", "#8a6241", "#8a6242", "#8a6243", "#8a6244", "#8a6245", "#8a6246", "#8a6247", "#8a6248", "#8a6249", "#8a624a", "#8a624b", "#8a624c", "#8a624d", "#8a624e", "#8a624f", "#8a6250", "#8a633d", "#8a633e", "#8a633f", "#8a6340", "#8a6341", "#8a6342", "#8a6343", "#8a6344", "#8a6345", "#8a6346", "#8a6347", "#8a6348", "#8a6349", "#8a634a", "#8a634b", "#8a634c", "#8a634d", "#8a634e", "#8a634f", "#8a6350", "#8a643d", "#8a643e", "#8a643f", "#8a6440", "#8a6441", "#8a6442", "#8a6443", "#8a6444", "#8a6445", "#8a6446", "#8a6447", "#8a6448", "#8a6449", "#8a644a", "#8a644b", "#8a644c", "#8a644d", "#8a644e", "#8a644f", "#8a6450", "#8a653d", "#8a653e", "#8a653f", "#8a6540", "#8a6541", "#8a6542", "#8a6543", "#8a6544", "#8a6545", "#8a6546", "#8a6547", "#8a6548", "#8a6549", "#8a654a", "#8a654b", "#8a654c", "#8a654d", "#8a654e", "#8a654f", "#8a6550", "#8a663d", "#8a663e", "#8a663f", "#8a6640", "#8a6641", "#8a6642", "#8a6643", "#8a6644", "#8a6645", "#8a6646", "#8a6647", "#8a6648", "#8a6649", "#8a664a", "#8a664b", "#8a664c", "#8a664d", "#8a664e", "#8a664f", "#8a6650", "#8b533d", "#8b533e", "#8b533f", "#8b5340", "#8b5341", "#8b5342", "#8b5343", "#8b5344", "#8b5345", "#8b5346", "#8b5347", "#8b5348", "#8b5349", "#8b534a", "#8b534b", "#8b534c", "#8b534d", "#8b534e", "#8b534f", "#8b5350", "#8b543d", "#8b543e", "#8b543f", "#8b5440", "#8b5441", "#8b5442", "#8b5443", "#8b5444", "#8b5445", "#8b5446", "#8b5447", "#8b5448", "#8b5449", "#8b544a", "#8b544b", "#8b544c", "#8b544d", "#8b544e", "#8b544f", "#8b5450", "#8b553d", "#8b553e", "#8b553f", "#8b5540", "#8b5541", "#8b5542", "#8b5543", "#8b5544", "#8b5545", "#8b5546", "#8b5547", "#8b5548", "#8b5549", "#8b554a", "#8b554b", "#8b554c", "#8b554d", "#8b554e", "#8b554f", "#8b5550", "#8b563d", "#8b563e", "#8b563f", "#8b5640", "#8b5641", "#8b5642", "#8b5643", "#8b5644", "#8b5645", "#8b5646", "#8b5647", "#8b5648", "#8b5649", "#8b564a", "#8b564b", "#8b564c", "#8b564d", "#8b564e", "#8b564f", "#8b5650", "#8b573d", "#8b573e", "#8b573f", "#8b5740", "#8b5741", "#8b5742", "#8b5743", "#8b5744", "#8b5745", "#8b5746", "#8b5747", "#8b5748", "#8b5749", "#8b574a", "#8b574b", "#8b574c", "#8b574d", "#8b574e", "#8b574f", "#8b5750", "#8b583d", "#8b583e", "#8b583f", "#8b5840", "#8b5841", "#8b5842", "#8b5843", "#8b5844", "#8b5845", "#8b5846", "#8b5847", "#8b5848", "#8b5849", "#8b584a", "#8b584b", "#8b584c", "#8b584d", "#8b584e", "#8b584f", "#8b5850", "#8b593d", "#8b593e", "#8b593f", "#8b5940", "#8b5941", "#8b5942", "#8b5943", "#8b5944", "#8b5945", "#8b5946", "#8b5947", "#8b5948", "#8b5949", "#8b594a", "#8b594b", "#8b594c", "#8b594d", "#8b594e", "#8b594f", "#8b5950", "#8b5a3d", "#8b5a3e", "#8b5a3f", "#8b5a40", "#8b5a41", "#8b5a42", "#8b5a43", "#8b5a44", "#8b5a45", "#8b5a46", "#8b5a47", "#8b5a48", "#8b5a49", "#8b5a4a", "#8b5a4b", "#8b5a4c", "#8b5a4d", "#8b5a4e", "#8b5a4f", "#8b5a50", "#8b5b3d", "#8b5b3e", "#8b5b3f", "#8b5b40", "#8b5b41", "#8b5b42", "#8b5b43", "#8b5b44", "#8b5b45", "#8b5b46", "#8b5b47", "#8b5b48", "#8b5b49", "#8b5b4a", "#8b5b4b", "#8b5b4c", "#8b5b4d", "#8b5b4e", "#8b5b4f", "#8b5b50", "#8b5c3d", "#8b5c3e", "#8b5c3f", "#8b5c40", "#8b5c41", "#8b5c42", "#8b5c43", "#8b5c44", "#8b5c45", "#8b5c46", "#8b5c47", "#8b5c48", "#8b5c49", "#8b5c4a", "#8b5c4b", "#8b5c4c", "#8b5c4d", "#8b5c4e", "#8b5c4f", "#8b5c50", "#8b5d3d", "#8b5d3e", "#8b5d3f", "#8b5d40", "#8b5d41", "#8b5d42", "#8b5d43", "#8b5d44", "#8b5d45", "#8b5d46", "#8b5d47", "#8b5d48", "#8b5d49", "#8b5d4a", "#8b5d4b", 
        "#8b5d4c", "#8b5d4d", "#8b5d4e", "#8b5d4f", "#8b5d50", "#8b5e3d", "#8b5e3e", "#8b5e3f", "#8b5e40", "#8b5e41", "#8b5e42", "#8b5e43", "#8b5e44", "#8b5e45", "#8b5e46", "#8b5e47", "#8b5e48", "#8b5e49", "#8b5e4a", "#8b5e4b", "#8b5e4c", "#8b5e4d", "#8b5e4e", "#8b5e4f", "#8b5e50", "#8b5f3d", "#8b5f3e", "#8b5f3f", "#8b5f40", "#8b5f41", "#8b5f42", "#8b5f43", "#8b5f44", "#8b5f45", "#8b5f46", "#8b5f47", "#8b5f48", "#8b5f49", "#8b5f4a", "#8b5f4b", "#8b5f4c", "#8b5f4d", "#8b5f4e", "#8b5f4f", "#8b5f50", "#8b603d", "#8b603e", "#8b603f", "#8b6040", "#8b6041", "#8b6042", "#8b6043", "#8b6044", "#8b6045", "#8b6046", "#8b6047", "#8b6048", "#8b6049", "#8b604a", "#8b604b", "#8b604c", "#8b604d", "#8b604e", "#8b604f", "#8b6050", "#8b613d", "#8b613e", "#8b613f", "#8b6140", "#8b6141", "#8b6142", "#8b6143", "#8b6144", "#8b6145", "#8b6146", "#8b6147", "#8b6148", "#8b6149", "#8b614a", "#8b614b", "#8b614c", "#8b614d", "#8b614e", "#8b614f", "#8b6150", "#8b623d", "#8b623e", "#8b623f", "#8b6240", "#8b6241", "#8b6242", "#8b6243", "#8b6244", "#8b6245", "#8b6246", "#8b6247", "#8b6248", "#8b6249", "#8b624a", "#8b624b", "#8b624c", "#8b624d", "#8b624e", "#8b624f", "#8b6250", "#8b633d", "#8b633e", "#8b633f", "#8b6340", "#8b6341", "#8b6342", "#8b6343", "#8b6344", "#8b6345", "#8b6346", "#8b6347", "#8b6348", "#8b6349", "#8b634a", "#8b634b", "#8b634c", "#8b634d", "#8b634e", "#8b634f", "#8b6350", "#8b643d", "#8b643e", "#8b643f", "#8b6440", "#8b6441", "#8b6442", "#8b6443", "#8b6444", "#8b6445", "#8b6446", "#8b6447", "#8b6448", "#8b6449", "#8b644a", "#8b644b", "#8b644c", "#8b644d", "#8b644e", "#8b644f", "#8b6450", "#8b653d", "#8b653e", "#8b653f", "#8b6540", "#8b6541", "#8b6542", "#8b6543", "#8b6544", "#8b6545", "#8b6546", "#8b6547", "#8b6548", "#8b6549", "#8b654a", "#8b654b", "#8b654c", "#8b654d", "#8b654e", "#8b654f", "#8b6550", "#8b663d", "#8b663e", "#8b663f", "#8b6640", "#8b6641", "#8b6642", "#8b6643", "#8b6644", "#8b6645", "#8b6646", "#8b6647", "#8b6648", "#8b6649", "#8b664a", "#8b664b", "#8b664c", "#8b664d", "#8b664e", "#8b664f", "#8b6650", "#8c533d", "#8c533e", "#8c533f", "#8c5340", "#8c5341", "#8c5342", "#8c5343", "#8c5344", "#8c5345", "#8c5346", "#8c5347", "#8c5348", "#8c5349", "#8c534a", "#8c534b", "#8c534c", "#8c534d", "#8c534e", "#8c534f", "#8c5350", "#8c543d", "#8c543e", "#8c543f", "#8c5440", "#8c5441", "#8c5442", "#8c5443", "#8c5444", "#8c5445", "#8c5446", "#8c5447", "#8c5448", "#8c5449", "#8c544a", "#8c544b", "#8c544c", "#8c544d", "#8c544e", "#8c544f", "#8c5450", "#8c553d", "#8c553e", "#8c553f", "#8c5540", "#8c5541", "#8c5542", "#8c5543", "#8c5544", "#8c5545", "#8c5546", "#8c5547", "#8c5548", "#8c5549", "#8c554a", "#8c554b", "#8c554c", "#8c554d", "#8c554e", "#8c554f", "#8c5550", "#8c563d", "#8c563e", "#8c563f", "#8c5640", "#8c5641", "#8c5642", "#8c5643", "#8c5644", "#8c5645", "#8c5646", "#8c5647", "#8c5648", "#8c5649", "#8c564a", "#8c564b", "#8c564c", "#8c564d", "#8c564e", "#8c564f", "#8c5650", "#8c573d", "#8c573e", "#8c573f", "#8c5740", "#8c5741", "#8c5742", "#8c5743", "#8c5744", "#8c5745", "#8c5746", "#8c5747", "#8c5748", "#8c5749", "#8c574a", "#8c574b", "#8c574c", "#8c574d", "#8c574e", "#8c574f", "#8c5750", "#8c583d", "#8c583e", "#8c583f", "#8c5840", "#8c5841", "#8c5842", "#8c5843", "#8c5844", "#8c5845", "#8c5846", "#8c5847", "#8c5848", "#8c5849", "#8c584a", "#8c584b", "#8c584c", "#8c584d", "#8c584e", "#8c584f", "#8c5850", "#8c593d", "#8c593e", "#8c593f", "#8c5940", "#8c5941", "#8c5942", "#8c5943", "#8c5944", "#8c5945", "#8c5946", "#8c5947", "#8c5948", "#8c5949", "#8c594a", "#8c594b", "#8c594c", "#8c594d", "#8c594e", "#8c594f", "#8c5950", "#8c5a3d", "#8c5a3e", "#8c5a3f", "#8c5a40", "#8c5a41", "#8c5a42", "#8c5a43", "#8c5a44", "#8c5a45", "#8c5a46", "#8c5a47", "#8c5a48", "#8c5a49", "#8c5a4a", "#8c5a4b", "#8c5a4c", "#8c5a4d", "#8c5a4e", "#8c5a4f", "#8c5a50", "#8c5b3d", "#8c5b3e", "#8c5b3f", "#8c5b40", "#8c5b41", "#8c5b42", "#8c5b43", "#8c5b44", "#8c5b45", "#8c5b46", "#8c5b47", "#8c5b48", "#8c5b49", "#8c5b4a", "#8c5b4b", "#8c5b4c", "#8c5b4d", "#8c5b4e", "#8c5b4f", "#8c5b50", "#8c5c3d", "#8c5c3e", "#8c5c3f", "#8c5c40", "#8c5c41", "#8c5c42", "#8c5c43", "#8c5c44", "#8c5c45", "#8c5c46", "#8c5c47", "#8c5c48", "#8c5c49", "#8c5c4a", "#8c5c4b", "#8c5c4c", "#8c5c4d", "#8c5c4e", "#8c5c4f", "#8c5c50", "#8c5d3d", "#8c5d3e", "#8c5d3f", "#8c5d40", "#8c5d41", "#8c5d42", "#8c5d43", "#8c5d44", "#8c5d45", "#8c5d46", "#8c5d47", "#8c5d48", "#8c5d49", "#8c5d4a", "#8c5d4b", "#8c5d4c", "#8c5d4d", "#8c5d4e", "#8c5d4f", "#8c5d50", "#8c5e3d", "#8c5e3e", "#8c5e3f", "#8c5e40", "#8c5e41", "#8c5e42", "#8c5e43", "#8c5e44", "#8c5e45", "#8c5e46", "#8c5e47", "#8c5e48", "#8c5e49", "#8c5e4a", "#8c5e4b", "#8c5e4c", "#8c5e4d", "#8c5e4e", "#8c5e4f", "#8c5e50", "#8c5f3d", "#8c5f3e", "#8c5f3f", "#8c5f40", "#8c5f41", "#8c5f42", "#8c5f43", "#8c5f44", "#8c5f45", "#8c5f46", "#8c5f47", "#8c5f48", "#8c5f49", "#8c5f4a", "#8c5f4b", "#8c5f4c", "#8c5f4d", "#8c5f4e", "#8c5f4f", "#8c5f50", "#8c603d", "#8c603e", "#8c603f", "#8c6040", "#8c6041", "#8c6042", "#8c6043", "#8c6044", "#8c6045", "#8c6046", "#8c6047", "#8c6048", "#8c6049", "#8c604a", "#8c604b", "#8c604c", "#8c604d", "#8c604e", "#8c604f", "#8c6050", "#8c613d", "#8c613e", "#8c613f", "#8c6140", "#8c6141", "#8c6142", "#8c6143", "#8c6144", "#8c6145", "#8c6146", "#8c6147", "#8c6148", "#8c6149", "#8c614a", "#8c614b", "#8c614c", "#8c614d", "#8c614e", "#8c614f", "#8c6150", "#8c623d", "#8c623e", "#8c623f", "#8c6240", "#8c6241", "#8c6242", "#8c6243", "#8c6244", "#8c6245", "#8c6246", "#8c6247", "#8c6248", "#8c6249", "#8c624a", "#8c624b", "#8c624c", "#8c624d", "#8c624e", "#8c624f", "#8c6250", "#8c633d", "#8c633e", "#8c633f", "#8c6340", "#8c6341", "#8c6342", "#8c6343", "#8c6344", "#8c6345", "#8c6346", "#8c6347", "#8c6348", "#8c6349", "#8c634a", "#8c634b", "#8c634c", "#8c634d", "#8c634e", "#8c634f", "#8c6350", "#8c643d", "#8c643e", "#8c643f", "#8c6440", "#8c6441", "#8c6442", "#8c6443", "#8c6444", "#8c6445", "#8c6446", "#8c6447", "#8c6448", "#8c6449", "#8c644a", "#8c644b", "#8c644c", "#8c644d", "#8c644e", "#8c644f", "#8c6450", "#8c653d", "#8c653e", "#8c653f", "#8c6540", "#8c6541", "#8c6542", "#8c6543", "#8c6544", "#8c6545", "#8c6546", "#8c6547", "#8c6548", "#8c6549", "#8c654a", "#8c654b", "#8c654c", "#8c654d", "#8c654e", "#8c654f", "#8c6550", "#8c663d", "#8c663e", "#8c663f", "#8c6640", "#8c6641", "#8c6642", "#8c6643", "#8c6644", "#8c6645", "#8c6646", "#8c6647", "#8c6648", "#8c6649", "#8c664a", "#8c664b", "#8c664c", "#8c664d", "#8c664e", "#8c664f", "#8c6650", "#8d533d", "#8d533e", "#8d533f", "#8d5340", "#8d5341", "#8d5342", "#8d5343", "#8d5344", "#8d5345", "#8d5346", "#8d5347", "#8d5348", "#8d5349", "#8d534a", "#8d534b", "#8d534c", "#8d534d", "#8d534e", "#8d534f", "#8d5350", "#8d543d", "#8d543e", "#8d543f", "#8d5440", "#8d5441", "#8d5442", "#8d5443", "#8d5444", "#8d5445", "#8d5446", "#8d5447", "#8d5448", "#8d5449", "#8d544a", "#8d544b", "#8d544c", "#8d544d", "#8d544e", "#8d544f", "#8d5450", "#8d553d", "#8d553e", "#8d553f", "#8d5540", "#8d5541", "#8d5542", "#8d5543", "#8d5544", "#8d5545", "#8d5546", "#8d5547", "#8d5548", "#8d5549", "#8d554a", "#8d554b", "#8d554c", "#8d554d", "#8d554e", "#8d554f", "#8d5550", "#8d563d", "#8d563e", "#8d563f", "#8d5640", "#8d5641", "#8d5642", "#8d5643", "#8d5644", "#8d5645", "#8d5646", "#8d5647", "#8d5648", "#8d5649", "#8d564a", "#8d564b", "#8d564c", "#8d564d", "#8d564e", "#8d564f", "#8d5650", "#8d573d", "#8d573e", "#8d573f", "#8d5740", "#8d5741", "#8d5742", "#8d5743", "#8d5744", "#8d5745", "#8d5746", "#8d5747", "#8d5748", "#8d5749", "#8d574a", "#8d574b", "#8d574c", "#8d574d", "#8d574e", "#8d574f", "#8d5750", "#8d583d", "#8d583e", "#8d583f", "#8d5840", "#8d5841", "#8d5842", "#8d5843", "#8d5844", "#8d5845", "#8d5846", "#8d5847", "#8d5848", "#8d5849", "#8d584a", "#8d584b", "#8d584c", "#8d584d", "#8d584e", "#8d584f", "#8d5850", "#8d593d", "#8d593e", "#8d593f", "#8d5940", "#8d5941", "#8d5942", "#8d5943", "#8d5944", "#8d5945", "#8d5946", "#8d5947", "#8d5948", "#8d5949", "#8d594a", "#8d594b", "#8d594c", "#8d594d", "#8d594e", "#8d594f", "#8d5950", "#8d5a3d", "#8d5a3e", "#8d5a3f", "#8d5a40", "#8d5a41", "#8d5a42", "#8d5a43", "#8d5a44", "#8d5a45", "#8d5a46", "#8d5a47", "#8d5a48", "#8d5a49", "#8d5a4a", "#8d5a4b", "#8d5a4c", "#8d5a4d", "#8d5a4e", "#8d5a4f", "#8d5a50", "#8d5b3d", "#8d5b3e", "#8d5b3f", "#8d5b40", "#8d5b41", "#8d5b42", "#8d5b43", "#8d5b44", "#8d5b45", "#8d5b46", "#8d5b47", "#8d5b48", "#8d5b49", "#8d5b4a", "#8d5b4b", "#8d5b4c", "#8d5b4d", "#8d5b4e", "#8d5b4f", "#8d5b50", "#8d5c3d", "#8d5c3e", "#8d5c3f", "#8d5c40", "#8d5c41", "#8d5c42", "#8d5c43", "#8d5c44", "#8d5c45", "#8d5c46", "#8d5c47", "#8d5c48", "#8d5c49", "#8d5c4a", "#8d5c4b", "#8d5c4c", "#8d5c4d", "#8d5c4e", "#8d5c4f", "#8d5c50", "#8d5d3d", "#8d5d3e", "#8d5d3f", "#8d5d40", "#8d5d41", "#8d5d42", "#8d5d43", "#8d5d44", "#8d5d45", "#8d5d46", "#8d5d47", "#8d5d48", "#8d5d49", "#8d5d4a", "#8d5d4b", "#8d5d4c", "#8d5d4d", "#8d5d4e", "#8d5d4f", "#8d5d50", "#8d5e3d", "#8d5e3e", "#8d5e3f", "#8d5e40", "#8d5e41", "#8d5e42", "#8d5e43", "#8d5e44", "#8d5e45", "#8d5e46", "#8d5e47", "#8d5e48", "#8d5e49", "#8d5e4a", "#8d5e4b", "#8d5e4c", "#8d5e4d", "#8d5e4e", "#8d5e4f", "#8d5e50", "#8d5f3d", "#8d5f3e", "#8d5f3f", "#8d5f40", "#8d5f41", "#8d5f42", "#8d5f43", "#8d5f44", "#8d5f45", "#8d5f46", "#8d5f47", "#8d5f48", "#8d5f49", "#8d5f4a", "#8d5f4b", "#8d5f4c", "#8d5f4d", "#8d5f4e", "#8d5f4f", "#8d5f50", "#8d603d", "#8d603e", "#8d603f", "#8d6040", "#8d6041", "#8d6042", "#8d6043", "#8d6044", "#8d6045", "#8d6046", "#8d6047", "#8d6048", "#8d6049", "#8d604a", "#8d604b", "#8d604c", "#8d604d", "#8d604e", "#8d604f", "#8d6050", "#8d613d", "#8d613e", "#8d613f", "#8d6140", "#8d6141", "#8d6142", "#8d6143", "#8d6144", "#8d6145", "#8d6146", "#8d6147", "#8d6148", "#8d6149", "#8d614a", "#8d614b", "#8d614c", "#8d614d", "#8d614e", "#8d614f", "#8d6150", "#8d623d", "#8d623e", "#8d623f", "#8d6240", "#8d6241", "#8d6242", "#8d6243", "#8d6244", "#8d6245", "#8d6246", "#8d6247", "#8d6248", "#8d6249", "#8d624a", "#8d624b", "#8d624c", "#8d624d", "#8d624e", "#8d624f", "#8d6250", "#8d633d", "#8d633e", "#8d633f", 
        "#8d6340", "#8d6341", "#8d6342", "#8d6343", "#8d6344", "#8d6345", "#8d6346", "#8d6347", "#8d6348", "#8d6349", "#8d634a", "#8d634b", "#8d634c", "#8d634d", "#8d634e", "#8d634f", "#8d6350", "#8d643d", "#8d643e", "#8d643f", "#8d6440", "#8d6441", "#8d6442", "#8d6443", "#8d6444", "#8d6445", "#8d6446", "#8d6447", "#8d6448", "#8d6449", "#8d644a", "#8d644b", "#8d644c", "#8d644d", "#8d644e", "#8d644f", "#8d6450", "#8d653d", "#8d653e", "#8d653f", "#8d6540", "#8d6541", "#8d6542", "#8d6543", "#8d6544", "#8d6545", "#8d6546", "#8d6547", "#8d6548", "#8d6549", "#8d654a", "#8d654b", "#8d654c", "#8d654d", "#8d654e", "#8d654f", "#8d6550", "#8d663d", "#8d663e", "#8d663f", "#8d6640", "#8d6641", "#8d6642", "#8d6643", "#8d6644", "#8d6645", "#8d6646", "#8d6647", "#8d6648", "#8d6649", "#8d664a", "#8d664b", "#8d664c", "#8d664d", "#8d664e", "#8d664f", "#8d6650", "#8e533d", "#8e533e", "#8e533f", "#8e5340", "#8e5341", "#8e5342", "#8e5343", "#8e5344", "#8e5345", "#8e5346", "#8e5347", "#8e5348", "#8e5349", "#8e534a", "#8e534b", "#8e534c", "#8e534d", "#8e534e", "#8e534f", "#8e5350", "#8e543d", "#8e543e", "#8e543f", "#8e5440", "#8e5441", "#8e5442", "#8e5443", "#8e5444", "#8e5445", "#8e5446", "#8e5447", "#8e5448", "#8e5449", "#8e544a", "#8e544b", "#8e544c", "#8e544d", "#8e544e", "#8e544f", "#8e5450", "#8e553d", "#8e553e", "#8e553f", "#8e5540", "#8e5541", "#8e5542", "#8e5543", "#8e5544", "#8e5545", "#8e5546", "#8e5547", "#8e5548", "#8e5549", "#8e554a", "#8e554b", "#8e554c", "#8e554d", "#8e554e", "#8e554f", "#8e5550", "#8e563d", "#8e563e", "#8e563f", "#8e5640", "#8e5641", "#8e5642", "#8e5643", "#8e5644", "#8e5645", "#8e5646", "#8e5647", "#8e5648", "#8e5649", "#8e564a", "#8e564b", "#8e564c", "#8e564d", "#8e564e", "#8e564f", "#8e5650", "#8e573d", "#8e573e", "#8e573f", "#8e5740", "#8e5741", "#8e5742", "#8e5743", "#8e5744", "#8e5745", "#8e5746", "#8e5747", "#8e5748", "#8e5749", "#8e574a", "#8e574b", "#8e574c", "#8e574d", "#8e574e", "#8e574f", "#8e5750", "#8e583d", "#8e583e", "#8e583f", "#8e5840", "#8e5841", "#8e5842", "#8e5843", "#8e5844", "#8e5845", "#8e5846", "#8e5847", "#8e5848", "#8e5849", "#8e584a", "#8e584b", "#8e584c", "#8e584d", "#8e584e", "#8e584f", "#8e5850", "#8e593d", "#8e593e", "#8e593f", "#8e5940", "#8e5941", "#8e5942", "#8e5943", "#8e5944", "#8e5945", "#8e5946", "#8e5947", "#8e5948", "#8e5949", "#8e594a", "#8e594b", "#8e594c", "#8e594d", "#8e594e", "#8e594f", "#8e5950", "#8e5a3d", "#8e5a3e", "#8e5a3f", "#8e5a40", "#8e5a41", "#8e5a42", "#8e5a43", "#8e5a44", "#8e5a45", "#8e5a46", "#8e5a47", "#8e5a48", "#8e5a49", "#8e5a4a", "#8e5a4b", "#8e5a4c", "#8e5a4d", "#8e5a4e", "#8e5a4f", "#8e5a50", "#8e5b3d", "#8e5b3e", "#8e5b3f", "#8e5b40", "#8e5b41", "#8e5b42", "#8e5b43", "#8e5b44", "#8e5b45", "#8e5b46", "#8e5b47", "#8e5b48", "#8e5b49", "#8e5b4a", "#8e5b4b", "#8e5b4c", "#8e5b4d", "#8e5b4e", "#8e5b4f", "#8e5b50", "#8e5c3d", "#8e5c3e", "#8e5c3f", "#8e5c40", "#8e5c41", "#8e5c42", "#8e5c43", "#8e5c44", "#8e5c45", "#8e5c46", "#8e5c47", "#8e5c48", "#8e5c49", "#8e5c4a", "#8e5c4b", "#8e5c4c", "#8e5c4d", "#8e5c4e", "#8e5c4f", "#8e5c50", "#8e5d3d", "#8e5d3e", "#8e5d3f", "#8e5d40", "#8e5d41", "#8e5d42", "#8e5d43", "#8e5d44", "#8e5d45", "#8e5d46", "#8e5d47", "#8e5d48", "#8e5d49", "#8e5d4a", "#8e5d4b", "#8e5d4c", "#8e5d4d", "#8e5d4e", "#8e5d4f", "#8e5d50", "#8e5e3d", "#8e5e3e", "#8e5e3f", "#8e5e40", "#8e5e41", "#8e5e42", "#8e5e43", "#8e5e44", "#8e5e45", "#8e5e46", "#8e5e47", "#8e5e48", "#8e5e49", "#8e5e4a", "#8e5e4b", "#8e5e4c", "#8e5e4d", "#8e5e4e", "#8e5e4f", "#8e5e50", "#8e5f3d", "#8e5f3e", "#8e5f3f", "#8e5f40", "#8e5f41", "#8e5f42", "#8e5f43", "#8e5f44", "#8e5f45", "#8e5f46", "#8e5f47", "#8e5f48", "#8e5f49", "#8e5f4a", "#8e5f4b", "#8e5f4c", "#8e5f4d", "#8e5f4e", "#8e5f4f", "#8e5f50", "#8e603d", "#8e603e", "#8e603f", "#8e6040", "#8e6041", "#8e6042", "#8e6043", "#8e6044", "#8e6045", "#8e6046", "#8e6047", "#8e6048", "#8e6049", "#8e604a", "#8e604b", "#8e604c", "#8e604d", "#8e604e", "#8e604f", "#8e6050", "#8e613d", "#8e613e", "#8e613f", "#8e6140", "#8e6141", "#8e6142", "#8e6143", "#8e6144", "#8e6145", "#8e6146", "#8e6147", "#8e6148", "#8e6149", "#8e614a", "#8e614b", "#8e614c", "#8e614d", "#8e614e", "#8e614f", "#8e6150", "#8e623d", "#8e623e", "#8e623f", "#8e6240", "#8e6241", "#8e6242", "#8e6243", "#8e6244", "#8e6245", "#8e6246", "#8e6247", "#8e6248", "#8e6249", "#8e624a", "#8e624b", "#8e624c", "#8e624d", "#8e624e", "#8e624f", "#8e6250", "#8e633d", "#8e633e", "#8e633f", "#8e6340", "#8e6341", "#8e6342", "#8e6343", "#8e6344", "#8e6345", "#8e6346", "#8e6347", "#8e6348", "#8e6349", "#8e634a", "#8e634b", "#8e634c", "#8e634d", "#8e634e", "#8e634f", "#8e6350", "#8e643d", "#8e643e", "#8e643f", "#8e6440", "#8e6441", "#8e6442", "#8e6443", "#8e6444", "#8e6445", "#8e6446", "#8e6447", "#8e6448", "#8e6449", "#8e644a", "#8e644b", "#8e644c", "#8e644d", "#8e644e", "#8e644f", "#8e6450", "#8e653d", "#8e653e", "#8e653f", "#8e6540", "#8e6541", "#8e6542", "#8e6543", "#8e6544", "#8e6545", "#8e6546", "#8e6547", "#8e6548", "#8e6549", "#8e654a", "#8e654b", "#8e654c", "#8e654d", "#8e654e", "#8e654f", "#8e6550", "#8e663d", "#8e663e", "#8e663f", "#8e6640", "#8e6641", "#8e6642", "#8e6643", "#8e6644", "#8e6645", "#8e6646", "#8e6647", "#8e6648", "#8e6649", "#8e664a", "#8e664b", "#8e664c", "#8e664d", "#8e664e", "#8e664f", "#8e6650", "#8f533d", "#8f533e", "#8f533f", "#8f5340", "#8f5341", "#8f5342", "#8f5343", "#8f5344", "#8f5345", "#8f5346", "#8f5347", "#8f5348", "#8f5349", "#8f534a", "#8f534b", "#8f534c", "#8f534d", "#8f534e", "#8f534f", "#8f5350", "#8f543d", "#8f543e", "#8f543f", "#8f5440", "#8f5441", "#8f5442", "#8f5443", "#8f5444", "#8f5445", "#8f5446", "#8f5447", "#8f5448", "#8f5449", "#8f544a", "#8f544b", "#8f544c", "#8f544d", "#8f544e", "#8f544f", "#8f5450", "#8f553d", "#8f553e", "#8f553f", "#8f5540", "#8f5541", "#8f5542", "#8f5543", "#8f5544", "#8f5545", "#8f5546", "#8f5547", "#8f5548", "#8f5549", "#8f554a", "#8f554b", "#8f554c", "#8f554d", "#8f554e", "#8f554f", "#8f5550", "#8f563d", "#8f563e", "#8f563f", "#8f5640", "#8f5641", "#8f5642", "#8f5643", "#8f5644", "#8f5645", "#8f5646", "#8f5647", "#8f5648", "#8f5649", "#8f564a", "#8f564b", "#8f564c", "#8f564d", "#8f564e", "#8f564f", "#8f5650", "#8f573d", "#8f573e", "#8f573f", "#8f5740", "#8f5741", "#8f5742", "#8f5743", "#8f5744", "#8f5745", "#8f5746", "#8f5747", "#8f5748", "#8f5749", "#8f574a", "#8f574b", "#8f574c", "#8f574d", "#8f574e", "#8f574f", "#8f5750", "#8f583d", "#8f583e", "#8f583f", "#8f5840", "#8f5841", "#8f5842", "#8f5843", "#8f5844", "#8f5845", "#8f5846", "#8f5847", "#8f5848", "#8f5849", "#8f584a", "#8f584b", "#8f584c", "#8f584d", "#8f584e", "#8f584f", "#8f5850", "#8f593d", "#8f593e", "#8f593f", "#8f5940", "#8f5941", "#8f5942", "#8f5943", "#8f5944", "#8f5945", "#8f5946", "#8f5947", "#8f5948", "#8f5949", "#8f594a", "#8f594b", "#8f594c", "#8f594d", "#8f594e", "#8f594f", "#8f5950", "#8f5a3d", "#8f5a3e", "#8f5a3f", "#8f5a40", "#8f5a41", "#8f5a42", "#8f5a43", "#8f5a44", "#8f5a45", "#8f5a46", "#8f5a47", "#8f5a48", "#8f5a49", "#8f5a4a", "#8f5a4b", "#8f5a4c", "#8f5a4d", "#8f5a4e", "#8f5a4f", "#8f5a50", "#8f5b3d", "#8f5b3e", "#8f5b3f", "#8f5b40", "#8f5b41", "#8f5b42", "#8f5b43", "#8f5b44", "#8f5b45", "#8f5b46", "#8f5b47", "#8f5b48", "#8f5b49", "#8f5b4a", "#8f5b4b", "#8f5b4c", "#8f5b4d", "#8f5b4e", "#8f5b4f", "#8f5b50", "#8f5c3d", "#8f5c3e", "#8f5c3f", "#8f5c40", "#8f5c41", "#8f5c42", "#8f5c43", "#8f5c44", "#8f5c45", "#8f5c46", "#8f5c47", "#8f5c48", "#8f5c49", "#8f5c4a", "#8f5c4b", "#8f5c4c", "#8f5c4d", "#8f5c4e", "#8f5c4f", "#8f5c50", "#8f5d3d", "#8f5d3e", "#8f5d3f", "#8f5d40", "#8f5d41", "#8f5d42", "#8f5d43", "#8f5d44", "#8f5d45", "#8f5d46", "#8f5d47", "#8f5d48", "#8f5d49", "#8f5d4a", "#8f5d4b", "#8f5d4c", "#8f5d4d", "#8f5d4e", "#8f5d4f", "#8f5d50", "#8f5e3d", "#8f5e3e", "#8f5e3f", "#8f5e40", "#8f5e41", "#8f5e42", "#8f5e43", "#8f5e44", "#8f5e45", "#8f5e46", "#8f5e47", "#8f5e48", "#8f5e49", "#8f5e4a", "#8f5e4b", "#8f5e4c", "#8f5e4d", "#8f5e4e", "#8f5e4f", "#8f5e50", "#8f5f3d", "#8f5f3e", "#8f5f3f", "#8f5f40", "#8f5f41", "#8f5f42", "#8f5f43", "#8f5f44", "#8f5f45", "#8f5f46", "#8f5f47", "#8f5f48", "#8f5f49", "#8f5f4a", "#8f5f4b", "#8f5f4c", "#8f5f4d", "#8f5f4e", "#8f5f4f", "#8f5f50", "#8f603d", "#8f603e", "#8f603f", "#8f6040", "#8f6041", "#8f6042", "#8f6043", "#8f6044", "#8f6045", "#8f6046", "#8f6047", "#8f6048", "#8f6049", "#8f604a", "#8f604b", "#8f604c", "#8f604d", "#8f604e", "#8f604f", "#8f6050", "#8f613d", "#8f613e", "#8f613f", "#8f6140", "#8f6141", "#8f6142", "#8f6143", "#8f6144", "#8f6145", "#8f6146", "#8f6147", "#8f6148", "#8f6149", "#8f614a", "#8f614b", "#8f614c", "#8f614d", "#8f614e", "#8f614f", "#8f6150", "#8f623d", "#8f623e", "#8f623f", "#8f6240", "#8f6241", "#8f6242", "#8f6243", "#8f6244", "#8f6245", "#8f6246", "#8f6247", "#8f6248", "#8f6249", "#8f624a", "#8f624b", "#8f624c", "#8f624d", "#8f624e", "#8f624f", "#8f6250", "#8f633d", "#8f633e", "#8f633f", "#8f6340", "#8f6341", "#8f6342", "#8f6343", "#8f6344", "#8f6345", "#8f6346", "#8f6347", "#8f6348", "#8f6349", "#8f634a", "#8f634b", "#8f634c", "#8f634d", "#8f634e", "#8f634f", "#8f6350", "#8f643d", "#8f643e", "#8f643f", "#8f6440", "#8f6441", "#8f6442", "#8f6443", "#8f6444", "#8f6445", "#8f6446", "#8f6447", "#8f6448", "#8f6449", "#8f644a", "#8f644b", "#8f644c", "#8f644d", "#8f644e", "#8f644f", "#8f6450", "#8f653d", "#8f653e", "#8f653f", "#8f6540", "#8f6541", "#8f6542", "#8f6543", "#8f6544", "#8f6545", "#8f6546", "#8f6547", "#8f6548", "#8f6549", "#8f654a", "#8f654b", "#8f654c", "#8f654d", "#8f654e", "#8f654f", "#8f6550", "#8f663d", "#8f663e", "#8f663f", "#8f6640", "#8f6641", "#8f6642", "#8f6643", "#8f6644", "#8f6645", "#8f6646", "#8f6647", "#8f6648", "#8f6649", "#8f664a", "#8f664b", "#8f664c", "#8f664d", "#8f664e", "#8f664f", "#8f6650", "#90533d", "#90533e", "#90533f", "#905340", "#905341", "#905342", "#905343", "#905344", "#905345", "#905346", "#905347", "#905348", "#905349", "#90534a", "#90534b", "#90534c", "#90534d", "#90534e", "#90534f", "#905350", "#90543d", "#90543e", "#90543f", "#905440", "#905441", "#905442", "#905443", "#905444", "#905445", "#905446", "#905447", 
        "#905448", "#905449", "#90544a", "#90544b", "#90544c", "#90544d", "#90544e", "#90544f", "#905450", "#90553d", "#90553e", "#90553f", "#905540", "#905541", "#905542", "#905543", "#905544", "#905545", "#905546", "#905547", "#905548", "#905549", "#90554a", "#90554b", "#90554c", "#90554d", "#90554e", "#90554f", "#905550", "#90563d", "#90563e", "#90563f", "#905640", "#905641", "#905642", "#905643", "#905644", "#905645", "#905646", "#905647", "#905648", "#905649", "#90564a", "#90564b", "#90564c", "#90564d", "#90564e", "#90564f", "#905650", "#90573d", "#90573e", "#90573f", "#905740", "#905741", "#905742", "#905743", "#905744", "#905745", "#905746", "#905747", "#905748", "#905749", "#90574a", "#90574b", "#90574c", "#90574d", "#90574e", "#90574f", "#905750", "#90583d", "#90583e", "#90583f", "#905840", "#905841", "#905842", "#905843", "#905844", "#905845", "#905846", "#905847", "#905848", "#905849", "#90584a", "#90584b", "#90584c", "#90584d", "#90584e", "#90584f", "#905850", "#90593d", "#90593e", "#90593f", "#905940", "#905941", "#905942", "#905943", "#905944", "#905945", "#905946", "#905947", "#905948", "#905949", "#90594a", "#90594b", "#90594c", "#90594d", "#90594e", "#90594f", "#905950", "#905a3d", "#905a3e", "#905a3f", "#905a40", "#905a41", "#905a42", "#905a43", "#905a44", "#905a45", "#905a46", "#905a47", "#905a48", "#905a49", "#905a4a", "#905a4b", "#905a4c", "#905a4d", "#905a4e", "#905a4f", "#905a50", "#905b3d", "#905b3e", "#905b3f", "#905b40", "#905b41", "#905b42", "#905b43", "#905b44", "#905b45", "#905b46", "#905b47", "#905b48", "#905b49", "#905b4a", "#905b4b", "#905b4c", "#905b4d", "#905b4e", "#905b4f", "#905b50", "#905c3d", "#905c3e", "#905c3f", "#905c40", "#905c41", "#905c42", "#905c43", "#905c44", "#905c45", "#905c46", "#905c47", "#905c48", "#905c49", "#905c4a", "#905c4b", "#905c4c", "#905c4d", "#905c4e", "#905c4f", "#905c50", "#905d3d", "#905d3e", "#905d3f", "#905d40", "#905d41", "#905d42", "#905d43", "#905d44", "#905d45", "#905d46", "#905d47", "#905d48", "#905d49", "#905d4a", "#905d4b", "#905d4c", "#905d4d", "#905d4e", "#905d4f", "#905d50", "#905e3d", "#905e3e", "#905e3f", "#905e40", "#905e41", "#905e42", "#905e43", "#905e44", "#905e45", "#905e46", "#905e47", "#905e48", "#905e49", "#905e4a", "#905e4b", "#905e4c", "#905e4d", "#905e4e", "#905e4f", "#905e50", "#905f3d", "#905f3e", "#905f3f", "#905f40", "#905f41", "#905f42", "#905f43", "#905f44", "#905f45", "#905f46", "#905f47", "#905f48", "#905f49", "#905f4a", "#905f4b", "#905f4c", "#905f4d", "#905f4e", "#905f4f", "#905f50", "#90603d", "#90603e", "#90603f", "#906040", "#906041", "#906042", "#906043", "#906044", "#906045", "#906046", "#906047", "#906048", "#906049", "#90604a", "#90604b", "#90604c", "#90604d", "#90604e", "#90604f", "#906050", "#90613d", "#90613e", "#90613f", "#906140", "#906141", "#906142", "#906143", "#906144", "#906145", "#906146", "#906147", "#906148", "#906149", "#90614a", "#90614b", "#90614c", "#90614d", "#90614e", "#90614f", "#906150", "#90623d", "#90623e", "#90623f", "#906240", "#906241", "#906242", "#906243", "#906244", "#906245", "#906246", "#906247", "#906248", "#906249", "#90624a", "#90624b", "#90624c", "#90624d", "#90624e", "#90624f", "#906250", "#90633d", "#90633e", "#90633f", "#906340", "#906341", "#906342", "#906343", "#906344", "#906345", "#906346", "#906347", "#906348", "#906349", "#90634a", "#90634b", "#90634c", "#90634d", "#90634e", "#90634f", "#906350", "#90643d", "#90643e", "#90643f", "#906440", "#906441", "#906442", "#906443", "#906444", "#906445", "#906446", "#906447", "#906448", "#906449", "#90644a", "#90644b", "#90644c", "#90644d", "#90644e", "#90644f", "#906450", "#90653d", "#90653e", "#90653f", "#906540", "#906541", "#906542", "#906543", "#906544", "#906545", "#906546", "#906547", "#906548", "#906549", "#90654a", "#90654b", "#90654c", "#90654d", "#90654e", "#90654f", "#906550", "#90663d", "#90663e", "#90663f", "#906640", "#906641", "#906642", "#906643", "#906644", "#906645", "#906646", "#906647", "#906648", "#906649", "#90664a", "#90664b", "#90664c", "#90664d", "#90664e", "#90664f", "#906650", "#91533d", "#91533e", "#91533f", "#915340", "#915341", "#915342", "#915343", "#915344", "#915345", "#915346", "#915347", "#915348", "#915349", "#91534a", "#91534b", "#91534c", "#91534d", "#91534e", "#91534f", "#915350", "#91543d", "#91543e", "#91543f", "#915440", "#915441", "#915442", "#915443", "#915444", "#915445", "#915446", "#915447", "#915448", "#915449", "#91544a", "#91544b", "#91544c", "#91544d", "#91544e", "#91544f", "#915450", "#91553d", "#91553e", "#91553f", "#915540", "#915541", "#915542", "#915543", "#915544", "#915545", "#915546", "#915547", "#915548", "#915549", "#91554a", "#91554b", "#91554c", "#91554d", "#91554e", "#91554f", "#915550", "#91563d", "#91563e", "#91563f", "#915640", "#915641", "#915642", "#915643", "#915644", "#915645", "#915646", "#915647", "#915648", "#915649", "#91564a", "#91564b", "#91564c", "#91564d", "#91564e", "#91564f", "#915650", "#91573d", "#91573e", "#91573f", "#915740", "#915741", "#915742", "#915743", "#915744", "#915745", "#915746", "#915747", "#915748", "#915749", "#91574a", "#91574b", "#91574c", "#91574d", "#91574e", "#91574f", "#915750", "#91583d", "#91583e", "#91583f", "#915840", "#915841", "#915842", "#915843", "#915844", "#915845", "#915846", "#915847", "#915848", "#915849", "#91584a", "#91584b", "#91584c", "#91584d", "#91584e", "#91584f", "#915850", "#91593d", "#91593e", "#91593f", "#915940", "#915941", "#915942", "#915943", "#915944", "#915945", "#915946", "#915947", "#915948", "#915949", "#91594a", "#91594b", "#91594c", "#91594d", "#91594e", "#91594f", "#915950", "#915a3d", "#915a3e", "#915a3f", "#915a40", "#915a41", "#915a42", "#915a43", "#915a44", "#915a45", "#915a46", "#915a47", "#915a48", "#915a49", "#915a4a", "#915a4b", "#915a4c", "#915a4d", "#915a4e", "#915a4f", "#915a50", "#915b3d", "#915b3e", "#915b3f", "#915b40", "#915b41", "#915b42", "#915b43", "#915b44", "#915b45", "#915b46", "#915b47", "#915b48", "#915b49", "#915b4a", "#915b4b", "#915b4c", "#915b4d", "#915b4e", "#915b4f", "#915b50", "#915c3d", "#915c3e", "#915c3f", "#915c40", "#915c41", "#915c42", "#915c43", "#915c44", "#915c45", "#915c46", "#915c47", "#915c48", "#915c49", "#915c4a", "#915c4b", "#915c4c", "#915c4d", "#915c4e", "#915c4f", "#915c50", "#915d3d", "#915d3e", "#915d3f", "#915d40", "#915d41", "#915d42", "#915d43", "#915d44", "#915d45", "#915d46", "#915d47", "#915d48", "#915d49", "#915d4a", "#915d4b", "#915d4c", "#915d4d", "#915d4e", "#915d4f", "#915d50", "#915e3d", "#915e3e", "#915e3f", "#915e40", "#915e41", "#915e42", "#915e43", "#915e44", "#915e45", "#915e46", "#915e47", "#915e48", "#915e49", "#915e4a", "#915e4b", "#915e4c", "#915e4d", "#915e4e", "#915e4f", "#915e50", "#915f3d", "#915f3e", "#915f3f", "#915f40", "#915f41", "#915f42", "#915f43", "#915f44", "#915f45", "#915f46", "#915f47", "#915f48", "#915f49", "#915f4a", "#915f4b", "#915f4c", "#915f4d", "#915f4e", "#915f4f", "#915f50", "#91603d", "#91603e", "#91603f", "#916040", "#916041", "#916042", "#916043", "#916044", "#916045", "#916046", "#916047", "#916048", "#916049", "#91604a", "#91604b", "#91604c", "#91604d", "#91604e", "#91604f", "#916050", "#91613d", "#91613e", "#91613f", "#916140", "#916141", "#916142", "#916143", "#916144", "#916145", "#916146", "#916147", "#916148", "#916149", "#91614a", "#91614b", "#91614c", "#91614d", "#91614e", "#91614f", "#916150", "#91623d", "#91623e", "#91623f", "#916240", "#916241", "#916242", "#916243", "#916244", "#916245", "#916246", "#916247", "#916248", "#916249", "#91624a", "#91624b", "#91624c", "#91624d", "#91624e", "#91624f", "#916250", "#91633d", "#91633e", "#91633f", "#916340", "#916341", "#916342", "#916343", "#916344", "#916345", "#916346", "#916347", "#916348", "#916349", "#91634a", "#91634b", "#91634c", "#91634d", "#91634e", "#91634f", "#916350", "#91643d", "#91643e", "#91643f", "#916440", "#916441", "#916442", "#916443", "#916444", "#916445", "#916446", "#916447", "#916448", "#916449", "#91644a", "#91644b", "#91644c", "#91644d", "#91644e", "#91644f", "#916450", "#91653d", "#91653e", "#91653f", "#916540", "#916541", "#916542", "#916543", "#916544", "#916545", "#916546", "#916547", "#916548", "#916549", "#91654a", "#91654b", "#91654c", "#91654d", "#91654e", "#91654f", "#916550", "#91663d", "#91663e", "#91663f", "#916640", "#916641", "#916642", "#916643", "#916644", "#916645", "#916646", "#916647", "#916648", "#916649", "#91664a", "#91664b", "#91664c", "#91664d", "#91664e", "#91664f", "#916650", "#92533d", "#92533e", "#92533f", "#925340", "#925341", "#925342", "#925343", "#925344", "#925345", "#925346", "#925347", "#925348", "#925349", "#92534a", "#92534b", "#92534c", "#92534d", "#92534e", "#92534f", "#925350", "#92543d", "#92543e", "#92543f", "#925440", "#925441", "#925442", "#925443", "#925444", "#925445", "#925446", "#925447", "#925448", "#925449", "#92544a", "#92544b", "#92544c", "#92544d", "#92544e", "#92544f", "#925450", "#92553d", "#92553e", "#92553f", "#925540", "#925541", "#925542", "#925543", "#925544", "#925545", "#925546", "#925547", "#925548", "#925549", "#92554a", "#92554b", "#92554c", "#92554d", "#92554e", "#92554f", "#925550", "#92563d", "#92563e", "#92563f", "#925640", "#925641", "#925642", "#925643", "#925644", "#925645", "#925646", "#925647", "#925648", "#925649", "#92564a", "#92564b", "#92564c", "#92564d", "#92564e", "#92564f", "#925650", "#92573d", "#92573e", "#92573f", "#925740", "#925741", "#925742", "#925743", "#925744", "#925745", "#925746", "#925747", "#925748", "#925749", "#92574a", "#92574b", "#92574c", "#92574d", "#92574e", "#92574f", "#925750", "#92583d", "#92583e", "#92583f", "#925840", "#925841", "#925842", "#925843", "#925844", "#925845", "#925846", "#925847", "#925848", "#925849", "#92584a", "#92584b", "#92584c", "#92584d", "#92584e", "#92584f", "#925850", "#92593d", "#92593e", "#92593f", "#925940", "#925941", "#925942", "#925943", "#925944", "#925945", "#925946", "#925947", "#925948", "#925949", "#92594a", "#92594b", "#92594c", "#92594d", "#92594e", "#92594f", 
        "#925950", "#925a3d", "#925a3e", "#925a3f", "#925a40", "#925a41", "#925a42", "#925a43", "#925a44", "#925a45", "#925a46", "#925a47", "#925a48", "#925a49", "#925a4a", "#925a4b", "#925a4c", "#925a4d", "#925a4e", "#925a4f", "#925a50", "#925b3d", "#925b3e", "#925b3f", "#925b40", "#925b41", "#925b42", "#925b43", "#925b44", "#925b45", "#925b46", "#925b47", "#925b48", "#925b49", "#925b4a", "#925b4b", "#925b4c", "#925b4d", "#925b4e", "#925b4f", "#925b50", "#925c3d", "#925c3e", "#925c3f", "#925c40", "#925c41", "#925c42", "#925c43", "#925c44", "#925c45", "#925c46", "#925c47", "#925c48", "#925c49", "#925c4a", "#925c4b", "#925c4c", "#925c4d", "#925c4e", "#925c4f", "#925c50", "#925d3d", "#925d3e", "#925d3f", "#925d40", "#925d41", "#925d42", "#925d43", "#925d44", "#925d45", "#925d46", "#925d47", "#925d48", "#925d49", "#925d4a", "#925d4b", "#925d4c", "#925d4d", "#925d4e", "#925d4f", "#925d50", "#925e3d", "#925e3e", "#925e3f", "#925e40", "#925e41", "#925e42", "#925e43", "#925e44", "#925e45", "#925e46", "#925e47", "#925e48", "#925e49", "#925e4a", "#925e4b", "#925e4c", "#925e4d", "#925e4e", "#925e4f", "#925e50", "#925f3d", "#925f3e", "#925f3f", "#925f40", "#925f41", "#925f42", "#925f43", "#925f44", "#925f45", "#925f46", "#925f47", "#925f48", "#925f49", "#925f4a", "#925f4b", "#925f4c", "#925f4d", "#925f4e", "#925f4f", "#925f50", "#92603d", "#92603e", "#92603f", "#926040", "#926041", "#926042", "#926043", "#926044", "#926045", "#926046", "#926047", "#926048", "#926049", "#92604a", "#92604b", "#92604c", "#92604d", "#92604e", "#92604f", "#926050", "#92613d", "#92613e", "#92613f", "#926140", "#926141", "#926142", "#926143", "#926144", "#926145", "#926146", "#926147", "#926148", "#926149", "#92614a", "#92614b", "#92614c", "#92614d", "#92614e", "#92614f", "#926150", "#92623d", "#92623e", "#92623f", "#926240", "#926241", "#926242", "#926243", "#926244", "#926245", "#926246", "#926247", "#926248", "#926249", "#92624a", "#92624b", "#92624c", "#92624d", "#92624e", "#92624f", "#926250", "#92633d", "#92633e", "#92633f", "#926340", "#926341", "#926342", "#926343", "#926344", "#926345", "#926346", "#926347", "#926348", "#926349", "#92634a", "#92634b", "#92634c", "#92634d", "#92634e", "#92634f", "#926350", "#92643d", "#92643e", "#92643f", "#926440", "#926441", "#926442", "#926443", "#926444", "#926445", "#926446", "#926447", "#926448", "#926449", "#92644a", "#92644b", "#92644c", "#92644d", "#92644e", "#92644f", "#926450", "#92653d", "#92653e", "#92653f", "#926540", "#926541", "#926542", "#926543", "#926544", "#926545", "#926546", "#926547", "#926548", "#926549", "#92654a", "#92654b", "#92654c", "#92654d", "#92654e", "#92654f", "#926550", "#92663d", "#92663e", "#92663f", "#926640", "#926641", "#926642", "#926643", "#926644", "#926645", "#926646", "#926647", "#926648", "#926649", "#92664a", "#92664b", "#92664c", "#92664d", "#92664e", "#92664f", "#926650", "#93533d", "#93533e", "#93533f", "#935340", "#935341", "#935342", "#935343", "#935344", "#935345", "#935346", "#935347", "#935348", "#935349", "#93534a", "#93534b", "#93534c", "#93534d", "#93534e", "#93534f", "#935350", "#93543d", "#93543e", "#93543f", "#935440", "#935441", "#935442", "#935443", "#935444", "#935445", "#935446", "#935447", "#935448", "#935449", "#93544a", "#93544b", "#93544c", "#93544d", "#93544e", "#93544f", "#935450", "#93553d", "#93553e", "#93553f", "#935540", "#935541", "#935542", "#935543", "#935544", "#935545", "#935546", "#935547", "#935548", "#935549", "#93554a", "#93554b", "#93554c", "#93554d", "#93554e", "#93554f", "#935550", "#93563d", "#93563e", "#93563f", "#935640", "#935641", "#935642", "#935643", "#935644", "#935645", "#935646", "#935647", "#935648", "#935649", "#93564a", "#93564b", "#93564c", "#93564d", "#93564e", "#93564f", "#935650", "#93573d", "#93573e", "#93573f", "#935740", "#935741", "#935742", "#935743", "#935744", "#935745", "#935746", "#935747", "#935748", "#935749", "#93574a", "#93574b", "#93574c", "#93574d", "#93574e", "#93574f", "#935750", "#93583d", "#93583e", "#93583f", "#935840", "#935841", "#935842", "#935843", "#935844", "#935845", "#935846", "#935847", "#935848", "#935849", "#93584a", "#93584b", "#93584c", "#93584d", "#93584e", "#93584f", "#935850", "#93593d", "#93593e", "#93593f", "#935940", "#935941", "#935942", "#935943", "#935944", "#935945", "#935946", "#935947", "#935948", "#935949", "#93594a", "#93594b", "#93594c", "#93594d", "#93594e", "#93594f", "#935950", "#935a3d", "#935a3e", "#935a3f", "#935a40", "#935a41", "#935a42", "#935a43", "#935a44", "#935a45", "#935a46", "#935a47", "#935a48", "#935a49", "#935a4a", "#935a4b", "#935a4c", "#935a4d", "#935a4e", "#935a4f", "#935a50", "#935b3d", "#935b3e", "#935b3f", "#935b40", "#935b41", "#935b42", "#935b43", "#935b44", "#935b45", "#935b46", "#935b47", "#935b48", "#935b49", "#935b4a", "#935b4b", "#935b4c", "#935b4d", "#935b4e", "#935b4f", "#935b50", "#935c3d", "#935c3e", "#935c3f", "#935c40", "#935c41", "#935c42", "#935c43", "#935c44", "#935c45", "#935c46", "#935c47", "#935c48", "#935c49", "#935c4a", "#935c4b", "#935c4c", "#935c4d", "#935c4e", "#935c4f", "#935c50", "#935d3d", "#935d3e", "#935d3f", "#935d40", "#935d41", "#935d42", "#935d43", "#935d44", "#935d45", "#935d46", "#935d47", "#935d48", "#935d49", "#935d4a", "#935d4b", "#935d4c", "#935d4d", "#935d4e", "#935d4f", "#935d50", "#935e3d", "#935e3e", "#935e3f", "#935e40", "#935e41", "#935e42", "#935e43", "#935e44", "#935e45", "#935e46", "#935e47", "#935e48", "#935e49", "#935e4a", "#935e4b", "#935e4c", "#935e4d", "#935e4e", "#935e4f", "#935e50", "#935f3d", "#935f3e", "#935f3f", "#935f40", "#935f41", "#935f42", "#935f43", "#935f44", "#935f45", "#935f46", "#935f47", "#935f48", "#935f49", "#935f4a", "#935f4b", "#935f4c", "#935f4d", "#935f4e", "#935f4f", "#935f50", "#93603d", "#93603e", "#93603f", "#936040", "#936041", "#936042", "#936043", "#936044", "#936045", "#936046", "#936047", "#936048", "#936049", "#93604a", "#93604b", "#93604c", "#93604d", "#93604e", "#93604f", "#936050", "#93613d", "#93613e", "#93613f", "#936140", "#936141", "#936142", "#936143", "#936144", "#936145", "#936146", "#936147", "#936148", "#936149", "#93614a", "#93614b", "#93614c", "#93614d", "#93614e", "#93614f", "#936150", "#93623d", "#93623e", "#93623f", "#936240", "#936241", "#936242", "#936243", "#936244", "#936245", "#936246", "#936247", "#936248", "#936249", "#93624a", "#93624b", "#93624c", "#93624d", "#93624e", "#93624f", "#936250", "#93633d", "#93633e", "#93633f", "#936340", "#936341", "#936342", "#936343", "#936344", "#936345", "#936346", "#936347", "#936348", "#936349", "#93634a", "#93634b", "#93634c", "#93634d", "#93634e", "#93634f", "#936350", "#93643d", "#93643e", "#93643f", "#936440", "#936441", "#936442", "#936443", "#936444", "#936445", "#936446", "#936447", "#936448", "#936449", "#93644a", "#93644b", "#93644c", "#93644d", "#93644e", "#93644f", "#936450", "#93653d", "#93653e", "#93653f", "#936540", "#936541", "#936542", "#936543", "#936544", "#936545", "#936546", "#936547", "#936548", "#936549", "#93654a", "#93654b", "#93654c", "#93654d", "#93654e", "#93654f", "#936550", "#93663d", "#93663e", "#93663f", "#936640", "#936641", "#936642", "#936643", "#936644", "#936645", "#936646", "#936647", "#936648", "#936649", "#93664a", "#93664b", "#93664c", "#93664d", "#93664e", "#93664f", "#936650", "#94533d", "#94533e", "#94533f", "#945340", "#945341", "#945342", "#945343", "#945344", "#945345", "#945346", "#945347", "#945348", "#945349", "#94534a", "#94534b", "#94534c", "#94534d", "#94534e", "#94534f", "#945350", "#94543d", "#94543e", "#94543f", "#945440", "#945441", "#945442", "#945443", "#945444", "#945445", "#945446", "#945447", "#945448", "#945449", "#94544a", "#94544b", "#94544c", "#94544d", "#94544e", "#94544f", "#945450", "#94553d", "#94553e", "#94553f", "#945540", "#945541", "#945542", "#945543", "#945544", "#945545", "#945546", "#945547", "#945548", "#945549", "#94554a", "#94554b", "#94554c", "#94554d", "#94554e", "#94554f", "#945550", "#94563d", "#94563e", "#94563f", "#945640", "#945641", "#945642", "#945643", "#945644", "#945645", "#945646", "#945647", "#945648", "#945649", "#94564a", "#94564b", "#94564c", "#94564d", "#94564e", "#94564f", "#945650", "#94573d", "#94573e", "#94573f", "#945740", "#945741", "#945742", "#945743", "#945744", "#945745", "#945746", "#945747", "#945748", "#945749", "#94574a", "#94574b", "#94574c", "#94574d", "#94574e", "#94574f", "#945750", "#94583d", "#94583e", "#94583f", "#945840", "#945841", "#945842", "#945843", "#945844", "#945845", "#945846", "#945847", "#945848", "#945849", "#94584a", "#94584b", "#94584c", "#94584d", "#94584e", "#94584f", "#945850", "#94593d", "#94593e", "#94593f", "#945940", "#945941", "#945942", "#945943", "#945944", "#945945", "#945946", "#945947", "#945948", "#945949", "#94594a", "#94594b", "#94594c", "#94594d", "#94594e", "#94594f", "#945950", "#945a3d", "#945a3e", "#945a3f", "#945a40", "#945a41", "#945a42", "#945a43", "#945a44", "#945a45", "#945a46", "#945a47", "#945a48", "#945a49", "#945a4a", "#945a4b", "#945a4c", "#945a4d", "#945a4e", "#945a4f", "#945a50", "#945b3d", "#945b3e", "#945b3f", "#945b40", "#945b41", "#945b42", "#945b43", "#945b44", "#945b45", "#945b46", "#945b47", "#945b48", "#945b49", "#945b4a", "#945b4b", "#945b4c", "#945b4d", "#945b4e", "#945b4f", "#945b50", "#945c3d", "#945c3e", "#945c3f", "#945c40", "#945c41", "#945c42", "#945c43", "#945c44", "#945c45", "#945c46", "#945c47", "#945c48", "#945c49", "#945c4a", "#945c4b", "#945c4c", "#945c4d", "#945c4e", "#945c4f", "#945c50", "#945d3d", "#945d3e", "#945d3f", "#945d40", "#945d41", "#945d42", "#945d43", "#945d44", "#945d45", "#945d46", "#945d47", "#945d48", "#945d49", "#945d4a", "#945d4b", "#945d4c", "#945d4d", "#945d4e", "#945d4f", "#945d50", "#945e3d", "#945e3e", "#945e3f", "#945e40", "#945e41", "#945e42", "#945e43", "#945e44", "#945e45", "#945e46", "#945e47", "#945e48", "#945e49", "#945e4a", "#945e4b", "#945e4c", "#945e4d", "#945e4e", "#945e4f", "#945e50", "#945f3d", "#945f3e", "#945f3f", "#945f40", "#945f41", "#945f42", "#945f43", 
        "#945f44", "#945f45", "#945f46", "#945f47", "#945f48", "#945f49", "#945f4a", "#945f4b", "#945f4c", "#945f4d", "#945f4e", "#945f4f", "#945f50", "#94603d", "#94603e", "#94603f", "#946040", "#946041", "#946042", "#946043", "#946044", "#946045", "#946046", "#946047", "#946048", "#946049", "#94604a", "#94604b", "#94604c", "#94604d", "#94604e", "#94604f", "#946050", "#94613d", "#94613e", "#94613f", "#946140", "#946141", "#946142", "#946143", "#946144", "#946145", "#946146", "#946147", "#946148", "#946149", "#94614a", "#94614b", "#94614c", "#94614d", "#94614e", "#94614f", "#946150", "#94623d", "#94623e", "#94623f", "#946240", "#946241", "#946242", "#946243", "#946244", "#946245", "#946246", "#946247", "#946248", "#946249", "#94624a", "#94624b", "#94624c", "#94624d", "#94624e", "#94624f", "#946250", "#94633d", "#94633e", "#94633f", "#946340", "#946341", "#946342", "#946343", "#946344", "#946345", "#946346", "#946347", "#946348", "#946349", "#94634a", "#94634b", "#94634c", "#94634d", "#94634e", "#94634f", "#946350", "#94643d", "#94643e", "#94643f", "#946440", "#946441", "#946442", "#946443", "#946444", "#946445", "#946446", "#946447", "#946448", "#946449", "#94644a", "#94644b", "#94644c", "#94644d", "#94644e", "#94644f", "#946450", "#94653d", "#94653e", "#94653f", "#946540", "#946541", "#946542", "#946543", "#946544", "#946545", "#946546", "#946547", "#946548", "#946549", "#94654a", "#94654b", "#94654c", "#94654d", "#94654e", "#94654f", "#946550", "#94663d", "#94663e", "#94663f", "#946640", "#946641", "#946642", "#946643", "#946644", "#946645", "#946646", "#946647", "#946648", "#946649", "#94664a", "#94664b", "#94664c", "#94664d", "#94664e", "#94664f", "#946650", "#95533d", "#95533e", "#95533f", "#955340", "#955341", "#955342", "#955343", "#955344", "#955345", "#955346", "#955347", "#955348", "#955349", "#95534a", "#95534b", "#95534c", "#95534d", "#95534e", "#95534f", "#955350", "#95543d", "#95543e", "#95543f", "#955440", "#955441", "#955442", "#955443", "#955444", "#955445", "#955446", "#955447", "#955448", "#955449", "#95544a", "#95544b", "#95544c", "#95544d", "#95544e", "#95544f", "#955450", "#95553d", "#95553e", "#95553f", "#955540", "#955541", "#955542", "#955543", "#955544", "#955545", "#955546", "#955547", "#955548", "#955549", "#95554a", "#95554b", "#95554c", "#95554d", "#95554e", "#95554f", "#955550", "#95563d", "#95563e", "#95563f", "#955640", "#955641", "#955642", "#955643", "#955644", "#955645", "#955646", "#955647", "#955648", "#955649", "#95564a", "#95564b", "#95564c", "#95564d", "#95564e", "#95564f", "#955650", "#95573d", "#95573e", "#95573f", "#955740", "#955741", "#955742", "#955743", "#955744", "#955745", "#955746", "#955747", "#955748", "#955749", "#95574a", "#95574b", "#95574c", "#95574d", "#95574e", "#95574f", "#955750", "#95583d", "#95583e", "#95583f", "#955840", "#955841", "#955842", "#955843", "#955844", "#955845", "#955846", "#955847", "#955848", "#955849", "#95584a", "#95584b", "#95584c", "#95584d", "#95584e", "#95584f", "#955850", "#95593d", "#95593e", "#95593f", "#955940", "#955941", "#955942", "#955943", "#955944", "#955945", "#955946", "#955947", "#955948", "#955949", "#95594a", "#95594b", "#95594c", "#95594d", "#95594e", "#95594f", "#955950", "#955a3d", "#955a3e", "#955a3f", "#955a40", "#955a41", "#955a42", "#955a43", "#955a44", "#955a45", "#955a46", "#955a47", "#955a48", "#955a49", "#955a4a", "#955a4b", "#955a4c", "#955a4d", "#955a4e", "#955a4f", "#955a50", "#955b3d", "#955b3e", "#955b3f", "#955b40", "#955b41", "#955b42", "#955b43", "#955b44", "#955b45", "#955b46", "#955b47", "#955b48", "#955b49", "#955b4a", "#955b4b", "#955b4c", "#955b4d", "#955b4e", "#955b4f", "#955b50", "#955c3d", "#955c3e", "#955c3f", "#955c40", "#955c41", "#955c42", "#955c43", "#955c44", "#955c45", "#955c46", "#955c47", "#955c48", "#955c49", "#955c4a", "#955c4b", "#955c4c", "#955c4d", "#955c4e", "#955c4f", "#955c50", "#955d3d", "#955d3e", "#955d3f", "#955d40", "#955d41", "#955d42", "#955d43", "#955d44", "#955d45", "#955d46", "#955d47", "#955d48", "#955d49", "#955d4a", "#955d4b", "#955d4c", "#955d4d", "#955d4e", "#955d4f", "#955d50", "#955e3d", "#955e3e", "#955e3f", "#955e40", "#955e41", "#955e42", "#955e43", "#955e44", "#955e45", "#955e46", "#955e47", "#955e48", "#955e49", "#955e4a", "#955e4b", "#955e4c", "#955e4d", "#955e4e", "#955e4f", "#955e50", "#955f3d", "#955f3e", "#955f3f", "#955f40", "#955f41", "#955f42", "#955f43", "#955f44", "#955f45", "#955f46", "#955f47", "#955f48", "#955f49", "#955f4a", "#955f4b", "#955f4c", "#955f4d", "#955f4e", "#955f4f", "#955f50", "#95603d", "#95603e", "#95603f", "#956040", "#956041", "#956042", "#956043", "#956044", "#956045", "#956046", "#956047", "#956048", "#956049", "#95604a", "#95604b", "#95604c", "#95604d", "#95604e", "#95604f", "#956050", "#95613d", "#95613e", "#95613f", "#956140", "#956141", "#956142", "#956143", "#956144", "#956145", "#956146", "#956147", "#956148", "#956149", "#95614a", "#95614b", "#95614c", "#95614d", "#95614e", "#95614f", "#956150", "#95623d", "#95623e", "#95623f", "#956240", "#956241", "#956242", "#956243", "#956244", "#956245", "#956246", "#956247", "#956248", "#956249", "#95624a", "#95624b", "#95624c", "#95624d", "#95624e", "#95624f", "#956250", "#95633d", "#95633e", "#95633f", "#956340", "#956341", "#956342", "#956343", "#956344", "#956345", "#956346", "#956347", "#956348", "#956349", "#95634a", "#95634b", "#95634c", "#95634d", "#95634e", "#95634f", "#956350", "#95643d", "#95643e", "#95643f", "#956440", "#956441", "#956442", "#956443", "#956444", "#956445", "#956446", "#956447", "#956448", "#956449", "#95644a", "#95644b", "#95644c", "#95644d", "#95644e", "#95644f", "#956450", "#95653d", "#95653e", "#95653f", "#956540", "#956541", "#956542", "#956543", "#956544", "#956545", "#956546", "#956547", "#956548", "#956549", "#95654a", "#95654b", "#95654c", "#95654d", "#95654e", "#95654f", "#956550", "#95663d", "#95663e", "#95663f", "#956640", "#956641", "#956642", "#956643", "#956644", "#956645", "#956646", "#956647", "#956648", "#956649", "#95664a", "#95664b", "#95664c", "#95664d", "#95664e", "#95664f", "#956650", "#96533d", "#96533e", "#96533f", "#965340", "#965341", "#965342", "#965343", "#965344", "#965345", "#965346", "#965347", "#965348", "#965349", "#96534a", "#96534b", "#96534c", "#96534d", "#96534e", "#96534f", "#965350", "#96543d", "#96543e", "#96543f", "#965440", "#965441", "#965442", "#965443", "#965444", "#965445", "#965446", "#965447", "#965448", "#965449", "#96544a", "#96544b", "#96544c", "#96544d", "#96544e", "#96544f", "#965450", "#96553d", "#96553e", "#96553f", "#965540", "#965541", "#965542", "#965543", "#965544", "#965545", "#965546", "#965547", "#965548", "#965549", "#96554a", "#96554b", "#96554c", "#96554d", "#96554e", "#96554f", "#965550", "#96563d", "#96563e", "#96563f", "#965640", "#965641", "#965642", "#965643", "#965644", "#965645", "#965646", "#965647", "#965648", "#965649", "#96564a", "#96564b", "#96564c", "#96564d", "#96564e", "#96564f", "#965650", "#96573d", "#96573e", "#96573f", "#965740", "#965741", "#965742", "#965743", "#965744", "#965745", "#965746", "#965747", "#965748", "#965749", "#96574a", "#96574b", "#96574c", "#96574d", "#96574e", "#96574f", "#965750", "#96583d", "#96583e", "#96583f", "#965840", "#965841", "#965842", "#965843", "#965844", "#965845", "#965846", "#965847", "#965848", "#965849", "#96584a", "#96584b", "#96584c", "#96584d", "#96584e", "#96584f", "#965850", "#96593d", "#96593e", "#96593f", "#965940", "#965941", "#965942", "#965943", "#965944", "#965945", "#965946", "#965947", "#965948", "#965949", "#96594a", "#96594b", "#96594c", "#96594d", "#96594e", "#96594f", "#965950", "#965a3d", "#965a3e", "#965a3f", "#965a40", "#965a41", "#965a42", "#965a43", "#965a44", "#965a45", "#965a46", "#965a47", "#965a48", "#965a49", "#965a4a", "#965a4b", "#965a4c", "#965a4d", "#965a4e", "#965a4f", "#965a50", "#965b3d", "#965b3e", "#965b3f", "#965b40", "#965b41", "#965b42", "#965b43", "#965b44", "#965b45", "#965b46", "#965b47", "#965b48", "#965b49", "#965b4a", "#965b4b", "#965b4c", "#965b4d", "#965b4e", "#965b4f", "#965b50", "#965c3d", "#965c3e", "#965c3f", "#965c40", "#965c41", "#965c42", "#965c43", "#965c44", "#965c45", "#965c46", "#965c47", "#965c48", "#965c49", "#965c4a", "#965c4b", "#965c4c", "#965c4d", "#965c4e", "#965c4f", "#965c50", "#965d3d", "#965d3e", "#965d3f", "#965d40", "#965d41", "#965d42", "#965d43", "#965d44", "#965d45", "#965d46", "#965d47", "#965d48", "#965d49", "#965d4a", "#965d4b", "#965d4c", "#965d4d", "#965d4e", "#965d4f", "#965d50", "#965e3d", "#965e3e", "#965e3f", "#965e40", "#965e41", "#965e42", "#965e43", "#965e44", "#965e45", "#965e46", "#965e47", "#965e48", "#965e49", "#965e4a", "#965e4b", "#965e4c", "#965e4d", "#965e4e", "#965e4f", "#965e50", "#965f3d", "#965f3e", "#965f3f", "#965f40", "#965f41", "#965f42", "#965f43", "#965f44", "#965f45", "#965f46", "#965f47", "#965f48", "#965f49", "#965f4a", "#965f4b", "#965f4c", "#965f4d", "#965f4e", "#965f4f", "#965f50", "#96603d", "#96603e", "#96603f", "#966040", "#966041", "#966042", "#966043", "#966044", "#966045", "#966046", "#966047", "#966048", "#966049", "#96604a", "#96604b", "#96604c", "#96604d", "#96604e", "#96604f", "#966050", "#96613d", "#96613e", "#96613f", "#966140", "#966141", "#966142", "#966143", "#966144", "#966145", "#966146", "#966147", "#966148", "#966149", "#96614a", "#96614b", "#96614c", "#96614d", "#96614e", "#96614f", "#966150", "#96623d", "#96623e", "#96623f", "#966240", "#966241", "#966242", "#966243", "#966244", "#966245", "#966246", "#966247", "#966248", "#966249", "#96624a", "#96624b", "#96624c", "#96624d", "#96624e", "#96624f", "#966250", "#96633d", "#96633e", "#96633f", "#966340", "#966341", "#966342", "#966343", "#966344", "#966345", "#966346", "#966347", "#966348", "#966349", "#96634a", "#96634b", "#96634c", "#96634d", "#96634e", "#96634f", "#966350", "#96643d", "#96643e", "#96643f", "#966440", "#966441", "#966442", "#966443", "#966444", "#966445", "#966446", "#966447", "#966448", "#966449", "#96644a", "#96644b", 
        "#96644c", "#96644d", "#96644e", "#96644f", "#966450", "#96653d", "#96653e", "#96653f", "#966540", "#966541", "#966542", "#966543", "#966544", "#966545", "#966546", "#966547", "#966548", "#966549", "#96654a", "#96654b", "#96654c", "#96654d", "#96654e", "#96654f", "#966550", "#96663d", "#96663e", "#96663f", "#966640", "#966641", "#966642", "#966643", "#966644", "#966645", "#966646", "#966647", "#966648", "#966649", "#96664a", "#96664b", "#96664c", "#96664d", "#96664e", "#96664f", "#966650", "#97533d", "#97533e", "#97533f", "#975340", "#975341", "#975342", "#975343", "#975344", "#975345", "#975346", "#975347", "#975348", "#975349", "#97534a", "#97534b", "#97534c", "#97534d", "#97534e", "#97534f", "#975350", "#97543d", "#97543e", "#97543f", "#975440", "#975441", "#975442", "#975443", "#975444", "#975445", "#975446", "#975447", "#975448", "#975449", "#97544a", "#97544b", "#97544c", "#97544d", "#97544e", "#97544f", "#975450", "#97553d", "#97553e", "#97553f", "#975540", "#975541", "#975542", "#975543", "#975544", "#975545", "#975546", "#975547", "#975548", "#975549", "#97554a", "#97554b", "#97554c", "#97554d", "#97554e", "#97554f", "#975550", "#97563d", "#97563e", "#97563f", "#975640", "#975641", "#975642", "#975643", "#975644", "#975645", "#975646", "#975647", "#975648", "#975649", "#97564a", "#97564b", "#97564c", "#97564d", "#97564e", "#97564f", "#975650", "#97573d", "#97573e", "#97573f", "#975740", "#975741", "#975742", "#975743", "#975744", "#975745", "#975746", "#975747", "#975748", "#975749", "#97574a", "#97574b", "#97574c", "#97574d", "#97574e", "#97574f", "#975750", "#97583d", "#97583e", "#97583f", "#975840", "#975841", "#975842", "#975843", "#975844", "#975845", "#975846", "#975847", "#975848", "#975849", "#97584a", "#97584b", "#97584c", "#97584d", "#97584e", "#97584f", "#975850", "#97593d", "#97593e", "#97593f", "#975940", "#975941", "#975942", "#975943", "#975944", "#975945", "#975946", "#975947", "#975948", "#975949", "#97594a", "#97594b", "#97594c", "#97594d", "#97594e", "#97594f", "#975950", "#975a3d", "#975a3e", "#975a3f", "#975a40", "#975a41", "#975a42", "#975a43", "#975a44", "#975a45", "#975a46", "#975a47", "#975a48", "#975a49", "#975a4a", "#975a4b", "#975a4c", "#975a4d", "#975a4e", "#975a4f", "#975a50", "#975b3d", "#975b3e", "#975b3f", "#975b40", "#975b41", "#975b42", "#975b43", "#975b44", "#975b45", "#975b46", "#975b47", "#975b48", "#975b49", "#975b4a", "#975b4b", "#975b4c", "#975b4d", "#975b4e", "#975b4f", "#975b50", "#975c3d", "#975c3e", "#975c3f", "#975c40", "#975c41", "#975c42", "#975c43", "#975c44", "#975c45", "#975c46", "#975c47", "#975c48", "#975c49", "#975c4a", "#975c4b", "#975c4c", "#975c4d", "#975c4e", "#975c4f", "#975c50", "#975d3d", "#975d3e", "#975d3f", "#975d40", "#975d41", "#975d42", "#975d43", "#975d44", "#975d45", "#975d46", "#975d47", "#975d48", "#975d49", "#975d4a", "#975d4b", "#975d4c", "#975d4d", "#975d4e", "#975d4f", "#975d50", "#975e3d", "#975e3e", "#975e3f", "#975e40", "#975e41", "#975e42", "#975e43", "#975e44", "#975e45", "#975e46", "#975e47", "#975e48", "#975e49", "#975e4a", "#975e4b", "#975e4c", "#975e4d", "#975e4e", "#975e4f", "#975e50", "#975f3d", "#975f3e", "#975f3f", "#975f40", "#975f41", "#975f42", "#975f43", "#975f44", "#975f45", "#975f46", "#975f47", "#975f48", "#975f49", "#975f4a", "#975f4b", "#975f4c", "#975f4d", "#975f4e", "#975f4f", "#975f50", "#97603d", "#97603e", "#97603f", "#976040", "#976041", "#976042", "#976043", "#976044", "#976045", "#976046", "#976047", "#976048", "#976049", "#97604a", "#97604b", "#97604c", "#97604d", "#97604e", "#97604f", "#976050", "#97613d", "#97613e", "#97613f", "#976140", "#976141", "#976142", "#976143", "#976144", "#976145", "#976146", "#976147", "#976148", "#976149", "#97614a", "#97614b", "#97614c", "#97614d", "#97614e", "#97614f", "#976150", "#97623d", "#97623e", "#97623f", "#976240", "#976241", "#976242", "#976243", "#976244", "#976245", "#976246", "#976247", "#976248", "#976249", "#97624a", "#97624b", "#97624c", "#97624d", "#97624e", "#97624f", "#976250", "#97633d", "#97633e", "#97633f", "#976340", "#976341", "#976342", "#976343", "#976344", "#976345", "#976346", "#976347", "#976348", "#976349", "#97634a", "#97634b", "#97634c", "#97634d", "#97634e", "#97634f", "#976350", "#97643d", "#97643e", "#97643f", "#976440", "#976441", "#976442", "#976443", "#976444", "#976445", "#976446", "#976447", "#976448", "#976449", "#97644a", "#97644b", "#97644c", "#97644d", "#97644e", "#97644f", "#976450", "#97653d", "#97653e", "#97653f", "#976540", "#976541", "#976542", "#976543", "#976544", "#976545", "#976546", "#976547", "#976548", "#976549", "#97654a", "#97654b", "#97654c", "#97654d", "#97654e", "#97654f", "#976550", "#97663d", "#97663e", "#97663f", "#976640", "#976641", "#976642", "#976643", "#976644", "#976645", "#976646", "#976647", "#976648", "#976649", "#97664a", "#97664b", "#97664c", "#97664d", "#97664e", "#97664f", "#976650", "#98533d", "#98533e", "#98533f", "#985340", "#985341", "#985342", "#985343", "#985344", "#985345", "#985346", "#985347", "#985348", "#985349", "#98534a", "#98534b", "#98534c", "#98534d", "#98534e", "#98534f", "#985350", "#98543d", "#98543e", "#98543f", "#985440", "#985441", "#985442", "#985443", "#985444", "#985445", "#985446", "#985447", "#985448", "#985449", "#98544a", "#98544b", "#98544c", "#98544d", "#98544e", "#98544f", "#985450", "#98553d", "#98553e", "#98553f", "#985540", "#985541", "#985542", "#985543", "#985544", "#985545", "#985546", "#985547", "#985548", "#985549", "#98554a", "#98554b", "#98554c", "#98554d", "#98554e", "#98554f", "#985550", "#98563d", "#98563e", "#98563f", "#985640", "#985641", "#985642", "#985643", "#985644", "#985645", "#985646", "#985647", "#985648", "#985649", "#98564a", "#98564b", "#98564c", "#98564d", "#98564e", "#98564f", "#985650", "#98573d", "#98573e", "#98573f", "#985740", "#985741", "#985742", "#985743", "#985744", "#985745", "#985746", "#985747", "#985748", "#985749", "#98574a", "#98574b", "#98574c", "#98574d", "#98574e", "#98574f", "#985750", "#98583d", "#98583e", "#98583f", "#985840", "#985841", "#985842", "#985843", "#985844", "#985845", "#985846", "#985847", "#985848", "#985849", "#98584a", "#98584b", "#98584c", "#98584d", "#98584e", "#98584f", "#985850", "#98593d", "#98593e", "#98593f", "#985940", "#985941", "#985942", "#985943", "#985944", "#985945", "#985946", "#985947", "#985948", "#985949", "#98594a", "#98594b", "#98594c", "#98594d", "#98594e", "#98594f", "#985950", "#985a3d", "#985a3e", "#985a3f", "#985a40", "#985a41", "#985a42", "#985a43", "#985a44", "#985a45", "#985a46", "#985a47", "#985a48", "#985a49", "#985a4a", "#985a4b", "#985a4c", "#985a4d", "#985a4e", "#985a4f", "#985a50", "#985b3d", "#985b3e", "#985b3f", "#985b40", "#985b41", "#985b42", "#985b43", "#985b44", "#985b45", "#985b46", "#985b47", "#985b48", "#985b49", "#985b4a", "#985b4b", "#985b4c", "#985b4d", "#985b4e", "#985b4f", "#985b50", "#985c3d", "#985c3e", "#985c3f", "#985c40", "#985c41", "#985c42", "#985c43", "#985c44", "#985c45", "#985c46", "#985c47", "#985c48", "#985c49", "#985c4a", "#985c4b", "#985c4c", "#985c4d", "#985c4e", "#985c4f", "#985c50", "#985d3d", "#985d3e", "#985d3f", "#985d40", "#985d41", "#985d42", "#985d43", "#985d44", "#985d45", "#985d46", "#985d47", "#985d48", "#985d49", "#985d4a", "#985d4b", "#985d4c", "#985d4d", "#985d4e", "#985d4f", "#985d50", "#985e3d", "#985e3e", "#985e3f", "#985e40", "#985e41", "#985e42", "#985e43", "#985e44", "#985e45", "#985e46", "#985e47", "#985e48", "#985e49", "#985e4a", "#985e4b", "#985e4c", "#985e4d", "#985e4e", "#985e4f", "#985e50", "#985f3d", "#985f3e", "#985f3f", "#985f40", "#985f41", "#985f42", "#985f43", "#985f44", "#985f45", "#985f46", "#985f47", "#985f48", "#985f49", "#985f4a", "#985f4b", "#985f4c", "#985f4d", "#985f4e", "#985f4f", "#985f50", "#98603d", "#98603e", "#98603f", "#986040", "#986041", "#986042", "#986043", "#986044", "#986045", "#986046", "#986047", "#986048", "#986049", "#98604a", "#98604b", "#98604c", "#98604d", "#98604e", "#98604f", "#986050", "#98613d", "#98613e", "#98613f", "#986140", "#986141", "#986142", "#986143", "#986144", "#986145", "#986146", "#986147", "#986148", "#986149", "#98614a", "#98614b", "#98614c", "#98614d", "#98614e", "#98614f", "#986150", "#98623d", "#98623e", "#98623f", "#986240", "#986241", "#986242", "#986243", "#986244", "#986245", "#986246", "#986247", "#986248", "#986249", "#98624a", "#98624b", "#98624c", "#98624d", "#98624e", "#98624f", "#986250", "#98633d", "#98633e", "#98633f", "#986340", "#986341", "#986342", "#986343", "#986344", "#986345", "#986346", "#986347", "#986348", "#986349", "#98634a", "#98634b", "#98634c", "#98634d", "#98634e", "#98634f", "#986350", "#98643d", "#98643e", "#98643f", "#986440", "#986441", "#986442", "#986443", "#986444", "#986445", "#986446", "#986447", "#986448", "#986449", "#98644a", "#98644b", "#98644c", "#98644d", "#98644e", "#98644f", "#986450", "#98653d", "#98653e", "#98653f", "#986540", "#986541", "#986542", "#986543", "#986544", "#986545", "#986546", "#986547", "#986548", "#986549", "#98654a", "#98654b", "#98654c", "#98654d", "#98654e", "#98654f", "#986550", "#98663d", "#98663e", "#98663f", "#986640", "#986641", "#986642", "#986643", "#986644", "#986645", "#986646", "#986647", "#986648", "#986649", "#98664a", "#98664b", "#98664c", "#98664d", "#98664e", "#98664f", "#986650", "#99533d", "#99533e", "#99533f", "#995340", "#995341", "#995342", "#995343", "#995344", "#995345", "#995346", "#995347", "#995348", "#995349", "#99534a", "#99534b", "#99534c", "#99534d", "#99534e", "#99534f", "#995350", "#99543d", "#99543e", "#99543f", "#995440", "#995441", "#995442", "#995443", "#995444", "#995445", "#995446", "#995447", "#995448", "#995449", "#99544a", "#99544b", "#99544c", "#99544d", "#99544e", "#99544f", "#995450", "#99553d", "#99553e", "#99553f", "#995540", "#995541", "#995542", "#995543", "#995544", "#995545", "#995546", "#995547", "#995548", "#995549", "#99554a", "#99554b", "#99554c", "#99554d", "#99554e", "#99554f", "#995550", "#99563d", "#99563e", "#99563f", 
        "#995640", "#995641", "#995642", "#995643", "#995644", "#995645", "#995646", "#995647", "#995648", "#995649", "#99564a", "#99564b", "#99564c", "#99564d", "#99564e", "#99564f", "#995650", "#99573d", "#99573e", "#99573f", "#995740", "#995741", "#995742", "#995743", "#995744", "#995745", "#995746", "#995747", "#995748", "#995749", "#99574a", "#99574b", "#99574c", "#99574d", "#99574e", "#99574f", "#995750", "#99583d", "#99583e", "#99583f", "#995840", "#995841", "#995842", "#995843", "#995844", "#995845", "#995846", "#995847", "#995848", "#995849", "#99584a", "#99584b", "#99584c", "#99584d", "#99584e", "#99584f", "#995850", "#99593d", "#99593e", "#99593f", "#995940", "#995941", "#995942", "#995943", "#995944", "#995945", "#995946", "#995947", "#995948", "#995949", "#99594a", "#99594b", "#99594c", "#99594d", "#99594e", "#99594f", "#995950", "#995a3d", "#995a3e", "#995a3f", "#995a40", "#995a41", "#995a42", "#995a43", "#995a44", "#995a45", "#995a46", "#995a47", "#995a48", "#995a49", "#995a4a", "#995a4b", "#995a4c", "#995a4d", "#995a4e", "#995a4f", "#995a50", "#995b3d", "#995b3e", "#995b3f", "#995b40", "#995b41", "#995b42", "#995b43", "#995b44", "#995b45", "#995b46", "#995b47", "#995b48", "#995b49", "#995b4a", "#995b4b", "#995b4c", "#995b4d", "#995b4e", "#995b4f", "#995b50", "#995c3d", "#995c3e", "#995c3f", "#995c40", "#995c41", "#995c42", "#995c43", "#995c44", "#995c45", "#995c46", "#995c47", "#995c48", "#995c49", "#995c4a", "#995c4b", "#995c4c", "#995c4d", "#995c4e", "#995c4f", "#995c50", "#995d3d", "#995d3e", "#995d3f", "#995d40", "#995d41", "#995d42", "#995d43", "#995d44", "#995d45", "#995d46", "#995d47", "#995d48", "#995d49", "#995d4a", "#995d4b", "#995d4c", "#995d4d", "#995d4e", "#995d4f", "#995d50", "#995e3d", "#995e3e", "#995e3f", "#995e40", "#995e41", "#995e42", "#995e43", "#995e44", "#995e45", "#995e46", "#995e47", "#995e48", "#995e49", "#995e4a", "#995e4b", "#995e4c", "#995e4d", "#995e4e", "#995e4f", "#995e50", "#995f3d", "#995f3e", "#995f3f", "#995f40", "#995f41", "#995f42", "#995f43", "#995f44", "#995f45", "#995f46", "#995f47", "#995f48", "#995f49", "#995f4a", "#995f4b", "#995f4c", "#995f4d", "#995f4e", "#995f4f", "#995f50", "#99603d", "#99603e", "#99603f", "#996040", "#996041", "#996042", "#996043", "#996044", "#996045", "#996046", "#996047", "#996048", "#996049", "#99604a", "#99604b", "#99604c", "#99604d", "#99604e", "#99604f", "#996050", "#99613d", "#99613e", "#99613f", "#996140", "#996141", "#996142", "#996143", "#996144", "#996145", "#996146", "#996147", "#996148", "#996149", "#99614a", "#99614b", "#99614c", "#99614d", "#99614e", "#99614f", "#996150", "#99623d", "#99623e", "#99623f", "#996240", "#996241", "#996242", "#996243", "#996244", "#996245", "#996246", "#996247", "#996248", "#996249", "#99624a", "#99624b", "#99624c", "#99624d", "#99624e", "#99624f", "#996250", "#99633d", "#99633e", "#99633f", "#996340", "#996341", "#996342", "#996343", "#996344", "#996345", "#996346", "#996347", "#996348", "#996349", "#99634a", "#99634b", "#99634c", "#99634d", "#99634e", "#99634f", "#996350", "#99643d", "#99643e", "#99643f", "#996440", "#996441", "#996442", "#996443", "#996444", "#996445", "#996446", "#996447", "#996448", "#996449", "#99644a", "#99644b", "#99644c", "#99644d", "#99644e", "#99644f", "#996450", "#99653d", "#99653e", "#99653f", "#996540", "#996541", "#996542", "#996543", "#996544", "#996545", "#996546", "#996547", "#996548", "#996549", "#99654a", "#99654b", "#99654c", "#99654d", "#99654e", "#99654f", "#996550", "#99663d", "#99663e", "#99663f", "#996640", "#996641", "#996642", "#996643", "#996644", "#996645", "#996646", "#996647", "#996648", "#996649", "#99664a", "#99664b", "#99664c", "#99664d", "#99664e", "#99664f", "#996650", "#9a533d", "#9a533e", "#9a533f", "#9a5340", "#9a5341", "#9a5342", "#9a5343", "#9a5344", "#9a5345", "#9a5346", "#9a5347", "#9a5348", "#9a5349", "#9a534a", "#9a534b", "#9a534c", "#9a534d", "#9a534e", "#9a534f", "#9a5350", "#9a543d", "#9a543e", "#9a543f", "#9a5440", "#9a5441", "#9a5442", "#9a5443", "#9a5444", "#9a5445", "#9a5446", "#9a5447", "#9a5448", "#9a5449", "#9a544a", "#9a544b", "#9a544c", "#9a544d", "#9a544e", "#9a544f", "#9a5450", "#9a553d", "#9a553e", "#9a553f", "#9a5540", "#9a5541", "#9a5542", "#9a5543", "#9a5544", "#9a5545", "#9a5546", "#9a5547", "#9a5548", "#9a5549", "#9a554a", "#9a554b", "#9a554c", "#9a554d", "#9a554e", "#9a554f", "#9a5550", "#9a563d", "#9a563e", "#9a563f", "#9a5640", "#9a5641", "#9a5642", "#9a5643", "#9a5644", "#9a5645", "#9a5646", "#9a5647", "#9a5648", "#9a5649", "#9a564a", "#9a564b", "#9a564c", "#9a564d", "#9a564e", "#9a564f", "#9a5650", "#9a573d", "#9a573e", "#9a573f", "#9a5740", "#9a5741", "#9a5742", "#9a5743", "#9a5744", "#9a5745", "#9a5746", "#9a5747", "#9a5748", "#9a5749", "#9a574a", "#9a574b", "#9a574c", "#9a574d", "#9a574e", "#9a574f", "#9a5750", "#9a583d", "#9a583e", "#9a583f", "#9a5840", "#9a5841", "#9a5842", "#9a5843", "#9a5844", "#9a5845", "#9a5846", "#9a5847", "#9a5848", "#9a5849", "#9a584a", "#9a584b", "#9a584c", "#9a584d", "#9a584e", "#9a584f", "#9a5850", "#9a593d", "#9a593e", "#9a593f", "#9a5940", "#9a5941", "#9a5942", "#9a5943", "#9a5944", "#9a5945", "#9a5946", "#9a5947", "#9a5948", "#9a5949", "#9a594a", "#9a594b", "#9a594c", "#9a594d", "#9a594e", "#9a594f", "#9a5950", "#9a5a3d", "#9a5a3e", "#9a5a3f", "#9a5a40", "#9a5a41", "#9a5a42", "#9a5a43", "#9a5a44", "#9a5a45", "#9a5a46", "#9a5a47", "#9a5a48", "#9a5a49", "#9a5a4a", "#9a5a4b", "#9a5a4c", "#9a5a4d", "#9a5a4e", "#9a5a4f", "#9a5a50", "#9a5b3d", "#9a5b3e", "#9a5b3f", "#9a5b40", "#9a5b41", "#9a5b42", "#9a5b43", "#9a5b44", "#9a5b45", "#9a5b46", "#9a5b47", "#9a5b48", "#9a5b49", "#9a5b4a", "#9a5b4b", "#9a5b4c", "#9a5b4d", "#9a5b4e", "#9a5b4f", "#9a5b50", "#9a5c3d", "#9a5c3e", "#9a5c3f", "#9a5c40", "#9a5c41", "#9a5c42", "#9a5c43", "#9a5c44", "#9a5c45", "#9a5c46", "#9a5c47", "#9a5c48", "#9a5c49", "#9a5c4a", "#9a5c4b", "#9a5c4c", "#9a5c4d", "#9a5c4e", "#9a5c4f", "#9a5c50", "#9a5d3d", "#9a5d3e", "#9a5d3f", "#9a5d40", "#9a5d41", "#9a5d42", "#9a5d43", "#9a5d44", "#9a5d45", "#9a5d46", "#9a5d47", "#9a5d48", "#9a5d49", "#9a5d4a", "#9a5d4b", "#9a5d4c", "#9a5d4d", "#9a5d4e", "#9a5d4f", "#9a5d50", "#9a5e3d", "#9a5e3e", "#9a5e3f", "#9a5e40", "#9a5e41", "#9a5e42", "#9a5e43", "#9a5e44", "#9a5e45", "#9a5e46", "#9a5e47", "#9a5e48", "#9a5e49", "#9a5e4a", "#9a5e4b", "#9a5e4c", "#9a5e4d", "#9a5e4e", "#9a5e4f", "#9a5e50", "#9a5f3d", "#9a5f3e", "#9a5f3f", "#9a5f40", "#9a5f41", "#9a5f42", "#9a5f43", "#9a5f44", "#9a5f45", "#9a5f46", "#9a5f47", "#9a5f48", "#9a5f49", "#9a5f4a", "#9a5f4b", "#9a5f4c", "#9a5f4d", "#9a5f4e", "#9a5f4f", "#9a5f50", "#9a603d", "#9a603e", "#9a603f", "#9a6040", "#9a6041", "#9a6042", "#9a6043", "#9a6044", "#9a6045", "#9a6046", "#9a6047", "#9a6048", "#9a6049", "#9a604a", "#9a604b", "#9a604c", "#9a604d", "#9a604e", "#9a604f", "#9a6050", "#9a613d", "#9a613e", "#9a613f", "#9a6140", "#9a6141", "#9a6142", "#9a6143", "#9a6144", "#9a6145", "#9a6146", "#9a6147", "#9a6148", "#9a6149", "#9a614a", "#9a614b", "#9a614c", "#9a614d", "#9a614e", "#9a614f", "#9a6150", "#9a623d", "#9a623e", "#9a623f", "#9a6240", "#9a6241", "#9a6242", "#9a6243", "#9a6244", "#9a6245", "#9a6246", "#9a6247", "#9a6248", "#9a6249", "#9a624a", "#9a624b", "#9a624c", "#9a624d", "#9a624e", "#9a624f", "#9a6250", "#9a633d", "#9a633e", "#9a633f", "#9a6340", "#9a6341", "#9a6342", "#9a6343", "#9a6344", "#9a6345", "#9a6346", "#9a6347", "#9a6348", "#9a6349", "#9a634a", "#9a634b", "#9a634c", "#9a634d", "#9a634e", "#9a634f", "#9a6350", "#9a643d", "#9a643e", "#9a643f", "#9a6440", "#9a6441", "#9a6442", "#9a6443", "#9a6444", "#9a6445", "#9a6446", "#9a6447", "#9a6448", "#9a6449", "#9a644a", "#9a644b", "#9a644c", "#9a644d", "#9a644e", "#9a644f", "#9a6450", "#9a653d", "#9a653e", "#9a653f", "#9a6540", "#9a6541", "#9a6542", "#9a6543", "#9a6544", "#9a6545", "#9a6546", "#9a6547", "#9a6548", "#9a6549", "#9a654a", "#9a654b", "#9a654c", "#9a654d", "#9a654e", "#9a654f", "#9a6550", "#9a663d", "#9a663e", "#9a663f", "#9a6640", "#9a6641", "#9a6642", "#9a6643", "#9a6644", "#9a6645", "#9a6646", "#9a6647", "#9a6648", "#9a6649", "#9a664a", "#9a664b", "#9a664c", "#9a664d", "#9a664e", "#9a664f", "#9a6650"]

        self.race_names = ['CAUC', 'ARAB', 'INDIAN', 'AFRICAN', 'ASIAN', 'NATIVE-AMERICAN', 'HISPANIC','MELANESIAN']
        self.surnames = {'ASIAN': ['Wang', 'Li', 'Zhang', 'Chen', 'Yang', 'Huang', 'Zhao', 'Wu', 'Zhou', 'Xu', 'Sun', 'Ma', 'Hu', 'He', 'Gao', 'Lin', 'Wong', 'Lu', 'Chu', 'Chan', 'Sato', 'Suzuki', 'Takahashi', 'Tanaka', 'Watanabe', 'Ito', 'Yamamoto', 'Nakamura', 'Yamamoto', 'Nakamura', 'Kobayashi', 'Kato', 'Yoshida', 'Yamada', 'Sasaki', 'Yamaguchi', 'Saito', 'Kimura', 'Hayashi', 'Shimizu', 'Yamazaki', 'Ishikawa', 'Nakajima', 'Maeda', 'Fujita', 'Ogawa', 'Goto', 'Okada', 'Hasegawa', 'Sakamoto', 'Endo', 'Fuji', 'Fukuda', 'Ota', 'Harada', 'Ono', 'Shibata', 'Tainaka', 'Akiyama', 'Hiiragi', 'Hirasawa', 'Izumi', 'Kotobuki', 'Nakano', 'Toshinou', 'Takanashi', 'Marui', 'Alana', 'Hale', 'Hekekia', 'Iona', 'Kahale', 'Kahele', 'Kaue', 'Kaiwi', "Kalawai'a", 'Kalili', 'Kalua', 'Kama', "Ka'aukai", "Ka'uhane", 'Kelekolio', "Keli'i", 'Mahelona', "Mahi'ai", 'Palakiko', "'Akamu", "'Aukai", 'Opunui', 'Santos', 'Reyes', 'Cruz', 'Bautista', 'Ocampo', 'delRosario', 'Gonzales', 'Aquino', 'Ramos', 'Garcia', 'Lopez', 'delaCruz', 'Mendoza', 'Pascual', 'Castillo', 'Villanueva', 'Diaz', 'Rivera', 'Navarro', 'Mercado', 'Morales', 'Fernandez', 'Marquez', 'Rodriguez', 'Sanchez', 'DeLeon'],
                'CAUC': ['Bakker', 'Boer', 'Bos', 'Bosch', 'Bosman', 'Brouwer', 'Dekker', 'Dijkstra', 'Driessen', 'Gerritsen', 'Groen', 'Hendriks', 'Hermans', 'Hoekstra', 'Huisman', 'Jacobs', 'Jansen', 'Janssen', 'Kok', 'Kramer', 'Kuipers', 'Meijer', 'Mol', 'Mulder', 'Peters', 'Post', 'Postma', 'Prins', 'Sanders', 'Schipper', 'Scholten', 'Schouten', 'Smeets', 'Smits', 'Timmermans', 'Veenstra', 'Verbeek', 'Verhoeven', 'Vermeulen', 'Vink', 'Visser', 'Vos', 'Willemsen', 'deJonge', 'deLange', 'deVos', 'deLeeuw', 'deRuiter', 'deKoning', 'deBruyn', 'deGraaf', 'deHaan', 'deWit', 'deBoer', 'deJong', 'deVries', 'vanDam', 'vanDijk', 'vanWijk', 'vanDoorn', 'vanVeen', 'vanDongen', 'vanLeeuwen', 'vanVliet', 'vanBeek', 'vandeVen', 'vandePol', 'vanderVeen', 'vandenBerg', 'vanderWal', 'vanderLaan', 'vanderVelde', 'vanderMeer', 'vanderLinden', 'vandenBrink', 'Smith', 'Johnson', 'Williams', 'Brown', 'Jones', 'Miller', 'Davis', 'Wilson', 'Moore', 'White', 'Young', 'Allen', 'Hall', 'Walker', 'Wright', 'Green', 'Adams', 'Hill', 'King', 'Lee', 'Campbell', 'Turner', 'Parker', 'Murphy', 'Bell', 'Brooks', 'Fisher', 'Gray', 'Muller', 'Schmidt', 'Schneider', 'Fischer', 'Meyer', 'Weber', 'Schulz', 'Wagner', 'Becker', 'Hoffmann', 'Nowak', 'Kowalski', 'Wisniewski', 'Wojcik', 'Kowalczyk', 'Kaminski', 'Lewandowski', 'Zielinski', 'Szymanski', 'Wozniak', 'Dabrowski', 'Kozlowski', 'Jankowski', 'Mazur', 'Kwiatkowski', 'Wojciechowski', 'Krawczyk', 'Kaczmarek', 'Piotrowski', 'Gabrowski', 'Melnyk', 'Shevchenko', 'Boyko', 'Kovalenko', 'Bondarenko', 'Tkachenko', 'Kovalchuk', 'Kravchenko', 'Oliynyk', 'Shevchuk', 'Koval', 'Polishchuk', 'Bondar', 'Tkachuk', 'Moroz', 'Marchenko', 'Lysenko', 'Rudenko', 'Savchenko', 'Petrenko', 'Papadopoulos', 'Vlahos', 'Angelopoulos', 'Nikolaidis', 'Georgiou', 'Petridis', 'Athanasiadis', 'Dimitriadis', 'Papadakis', 'Panagiotopoulos', 'Papantoniou', 'Antoniou', 'Gianopoulos', 'Afxentiou', 'Xarhakos', 'Koteas', 'Charisteas', 'Chiotis', 'Kouyiomtzis', 'Dimas', 'Cosmatos', 'Kanellis', 'Korhonen', 'Virtanen', 'Makinen', 'Nieminen', 'Makela', 'Hamalainen', 'Laine', 'Heikkinen', 'Koshinen', 'Jarvinen', 'Lehtonen', 'Saarinen', 'Salminen', 'Heinonen', 'Niemi', 'heikkila', 'Salonen', 'Kinnunen', 'Turenen', 'Salo', 'Laitinen', 'Naslund', 'Sedin', 'Jokinen', 'Kattila', 'Savolainen', 'Lahtinen', 'Ahonen', 'Hansen', 'Johansen', 'Larsen', 'Andersen', 'Pedersen', 'Nilsen', 'Kristiansen', 'Jensen', 'Karlsen', 'Johnsen', 'Pettersen', 'Eriksen', 'Berg', 'Haugen', 'Hagen', 'Johannesen', 'Andreassen', 'Jacobsen', 'Halvorsen', 'Andersson', 'Johansson', 'Karlsson', 'Nilsson', 'Eriksson', 'Larsson', 'Olsson', 'Persson', 'Svensson', 'Gustafsson', 'Pettersson', 'Jonsson', 'Jansson', 'Hansson', 'Bengtsson', 'Lindberg', 'Jakobsson', 'Magnusson', 'Olofsson'],
                'HISPANIC': ['Rodriguez', 'Gómez', 'Gonzalez', 'Martinez', 'Garcia', 'Lopez', 'Hernandez', 'Sanchez', 'Ramirez', 'Perez', 'Diaz', 'Munoz', 'Rojas', 'Moreno', 'Jimenez', 'Garcia', 'Fernandez', 'Gonzalez', 'Rodriguez', 'Lopez', 'Martinez', 'Sanchez', 'Perez', 'Martin', 'Gomez', 'Ruiz', 'Hernandez', 'Diaz', 'Alvarez', 'Navarro', 'Romero', 'Serrano', 'Delgado', 'Castro', 'Ortega', 'Iglesias', 'Ramirez', 'Ferrari', 'Esposito', 'Rossi', 'Russo', 'Bianchi', 'Colombo', 'Romano', 'Marino', 'Gallo', 'Conti', 'DeLuca', 'Mancini', 'Costa', 'Giordano', 'Rizzo', 'Lombardi', 'Moretti', 'Garcia', 'Fernandez', 'Gonzalez', 'Rodriguez', 'Lopez', 'Martinez', 'Sanchez', 'Perez', 'Martin', 'Gomez', 'Ruiz', 'Hernandez', 'Diaz', 'Alvarez', 'Navarro', 'Romero', 'Serrano', 'Delgado', 'Castro', 'Ortega', 'Iglesias', 'Ramirez'],
                'INDIAN': ['Kumar', 'Moorthi', 'Shastri', 'Acharya', 'Prasad', 'Pillai', 'Gowda', 'Nayak', 'Radhakrishnan', 'Desmukh', 'Kulkarni', 'Bhat', 'Kamat', 'Bhandary', 'Srivastava', 'Padmanabhan', 'Krishnamurthy', 'Balasubramanian', 'Upadhyay', 'Poojary', 'Ballal', 'Chowta', 'Naik', 'Kadamba', 'Hoysala', 'Maurya', 'Poonja', 'Namboodiri', 'Pannikar', 'Varma', 'Sooriyaprakash', 'Subrahmyan', 'Vijay', 'Sudhakar', 'Srinivasan', 'Jeyaramachandharan', 'Murthiyrakkaventharan', 'Ikkuzhan', 'Patel'],
                'AFRICAN': ['Addisu', 'Abel', 'Adunga', 'Aman', 'Afewerek', 'Bayissa', 'Biniam', 'Berhanu', 'Bruk', 'Dagim', 'Dawit', 'Dula', 'Ejigu', 'Eleazar', 'Etefu', 'Fashika', 'Frew', 'Gabra', 'Gedeyon', 'Getachew', 'Habtamu', 'Hassan', 'Jember', 'Kabede', 'Mekonnen', 'Mulu', 'Neguse', 'Rada', 'Solomon', 'Taddese', 'Tamrat', 'Temesgen', 'Tesfaye', 'Workneh', 'Yeakob', 'Yohannes', 'Zewedu', 'Adebayo', 'Oni', 'Eze', 'Mohammed', 'Matrins', 'Adeyemi', 'Okafor', 'Chukwu', 'Okonkwo', 'Balogun', 'Savage', 'Lawal', 'Attah', 'Chukwumereije', 'Fofana', 'Iweala', 'Okonjo', 'Ezekwesili', 'Keita', 'Soyinka', 'Solarin', 'Gbadamosi', 'Olanrewaju', 'Magoro', 'Madaki', 'Jang', 'Oyinlola', 'Onwudiwe', 'Jakande', 'Igwe', 'Obi', 'Orji', 'Ohakim', 'Alakija', 'Akenzua'],
                'ARAB': ['Al-Qahtani', 'Al-Shammari', 'Al-Onazy', 'Al-Dosary', 'Al-Subaiee', 'Al-Yami', 'Al-harbi', 'Al-Ghamdi', 'Al-Zahrani', 'Al-Reshedi', 'Al-Blawi', 'Al-Juhani', 'Al-Ahmari', 'Yilmaz', 'Kaya', 'Demir', 'Sahin', 'Celik', 'Yildiz', 'Yildirim', 'Ozturk', 'Aydin', 'Ozdemir'],
                'NATIVE-AMERICAN':['Wang', 'Li', 'Zhang', 'Chen', 'Yang', 'Huang', 'Zhao', 'Wu', 'Zhou', 'Xu', 'Sun', 'Ma', 'Hu', 'He', 'Gao', 'Lin', 'Wong', 'Lu', 'Chu', 'Chan', 'Sato', 'Suzuki', 'Takahashi', 'Tanaka', 'Watanabe', 'Ito', 'Yamamoto', 'Nakamura', 'Yamamoto', 'Nakamura', 'Kobayashi', 'Kato', 'Yoshida', 'Yamada', 'Sasaki', 'Yamaguchi', 'Saito', 'Kimura', 'Hayashi', 'Shimizu', 'Yamazaki', 'Ishikawa', 'Nakajima', 'Maeda', 'Fujita', 'Ogawa', 'Goto', 'Okada', 'Hasegawa', 'Sakamoto', 'Endo', 'Fuji', 'Fukuda', 'Ota', 'Harada', 'Ono', 'Shibata', 'Tainaka', 'Akiyama', 'Hiiragi', 'Hirasawa', 'Izumi', 'Kotobuki', 'Nakano', 'Toshinou', 'Takanashi', 'Marui', 'Alana', 'Hale', 'Hekekia', 'Iona', 'Kahale', 'Kahele', 'Kaue', 'Kaiwi', "Kalawai'a", 'Kalili', 'Kalua', 'Kama', "Ka'aukai", "Ka'uhane", 'Kelekolio', "Keli'i", 'Mahelona', "Mahi'ai", 'Palakiko', "'Akamu", "'Aukai", 'Opunui', 'Santos', 'Reyes', 'Cruz', 'Bautista', 'Ocampo', 'delRosario', 'Gonzales', 'Aquino', 'Ramos', 'Garcia', 'Lopez', 'delaCruz', 'Mendoza', 'Pascual', 'Castillo', 'Villanueva', 'Diaz', 'Rivera', 'Navarro', 'Mercado', 'Morales', 'Fernandez', 'Marquez', 'Rodriguez', 'Sanchez', 'DeLeon','Bakker', 'Boer', 'Bos', 'Bosch', 'Bosman', 'Brouwer', 'Dekker', 'Dijkstra', 'Driessen', 'Gerritsen', 'Groen', 'Hendriks', 'Hermans', 'Hoekstra', 'Huisman', 'Jacobs', 'Jansen', 'Janssen', 'Kok', 'Kramer', 'Kuipers', 'Meijer', 'Mol', 'Mulder', 'Peters', 'Post', 'Postma', 'Prins', 'Sanders', 'Schipper', 'Scholten', 'Schouten', 'Smeets', 'Smits', 'Timmermans', 'Veenstra', 'Verbeek', 'Verhoeven', 'Vermeulen', 'Vink', 'Visser', 'Vos', 'Willemsen', 'deJonge', 'deLange', 'deVos', 'deLeeuw', 'deRuiter', 'deKoning', 'deBruyn', 'deGraaf', 'deHaan', 'deWit', 'deBoer', 'deJong', 'deVries', 'vanDam', 'vanDijk', 'vanWijk', 'vanDoorn', 'vanVeen', 'vanDongen', 'vanLeeuwen', 'vanVliet', 'vanBeek', 'vandeVen', 'vandePol', 'vanderVeen', 'vandenBerg', 'vanderWal', 'vanderLaan', 'vanderVelde', 'vanderMeer', 'vanderLinden', 'vandenBrink', 'Smith', 'Johnson', 'Williams', 'Brown', 'Jones', 'Miller', 'Davis', 'Wilson', 'Moore', 'White', 'Young', 'Allen', 'Hall', 'Walker', 'Wright', 'Green', 'Adams', 'Hill', 'King', 'Lee', 'Campbell', 'Turner', 'Parker', 'Murphy', 'Bell', 'Brooks', 'Fisher', 'Gray', 'Muller', 'Schmidt', 'Schneider', 'Fischer', 'Meyer', 'Weber', 'Schulz', 'Wagner', 'Becker', 'Hoffmann', 'Nowak', 'Kowalski', 'Wisniewski', 'Wojcik', 'Kowalczyk', 'Kaminski', 'Lewandowski', 'Zielinski', 'Szymanski', 'Wozniak', 'Dabrowski', 'Kozlowski', 'Jankowski', 'Mazur', 'Kwiatkowski', 'Wojciechowski', 'Krawczyk', 'Kaczmarek', 'Piotrowski', 'Gabrowski', 'Melnyk', 'Shevchenko', 'Boyko', 'Kovalenko', 'Bondarenko', 'Tkachenko', 'Kovalchuk', 'Kravchenko', 'Oliynyk', 'Shevchuk', 'Koval', 'Polishchuk', 'Bondar', 'Tkachuk', 'Moroz', 'Marchenko', 'Lysenko', 'Rudenko', 'Savchenko', 'Petrenko', 'Papadopoulos', 'Vlahos', 'Angelopoulos', 'Nikolaidis', 'Georgiou', 'Petridis', 'Athanasiadis', 'Dimitriadis', 'Papadakis', 'Panagiotopoulos', 'Papantoniou', 'Antoniou', 'Gianopoulos', 'Afxentiou', 'Xarhakos', 'Koteas', 'Charisteas', 'Chiotis', 'Kouyiomtzis', 'Dimas', 'Cosmatos', 'Kanellis', 'Korhonen', 'Virtanen', 'Makinen', 'Nieminen', 'Makela', 'Hamalainen', 'Laine', 'Heikkinen', 'Koshinen', 'Jarvinen', 'Lehtonen', 'Saarinen', 'Salminen', 'Heinonen', 'Niemi', 'heikkila', 'Salonen', 'Kinnunen', 'Turenen', 'Salo', 'Laitinen', 'Naslund', 'Sedin', 'Jokinen', 'Kattila', 'Savolainen', 'Lahtinen', 'Ahonen', 'Hansen', 'Johansen', 'Larsen', 'Andersen', 'Pedersen', 'Nilsen', 'Kristiansen', 'Jensen', 'Karlsen', 'Johnsen', 'Pettersen', 'Eriksen', 'Berg', 'Haugen', 'Hagen', 'Johannesen', 'Andreassen', 'Jacobsen', 'Halvorsen', 'Andersson', 'Johansson', 'Karlsson', 'Nilsson', 'Eriksson', 'Larsson', 'Olsson', 'Persson', 'Svensson', 'Gustafsson', 'Pettersson', 'Jonsson', 'Jansson', 'Hansson', 'Bengtsson', 'Lindberg', 'Jakobsson', 'Magnusson', 'Olofsson','Rodriguez', 'Gómez', 'Gonzalez', 'Martinez', 'Garcia', 'Lopez', 'Hernandez', 'Sanchez', 'Ramirez', 'Perez', 'Diaz', 'Munoz', 'Rojas', 'Moreno', 'Jimenez', 'Garcia', 'Fernandez', 'Gonzalez', 'Rodriguez', 'Lopez', 'Martinez', 'Sanchez', 'Perez', 'Martin', 'Gomez', 'Ruiz', 'Hernandez', 'Diaz', 'Alvarez', 'Navarro', 'Romero', 'Serrano', 'Delgado', 'Castro', 'Ortega', 'Iglesias', 'Ramirez', 'Ferrari', 'Esposito', 'Rossi', 'Russo', 'Bianchi', 'Colombo', 'Romano', 'Marino', 'Gallo', 'Conti', 'DeLuca', 'Mancini', 'Costa', 'Giordano', 'Rizzo', 'Lombardi', 'Moretti', 'Garcia', 'Fernandez', 'Gonzalez', 'Rodriguez', 'Lopez', 'Martinez', 'Sanchez', 'Perez', 'Martin', 'Gomez', 'Ruiz', 'Hernandez', 'Diaz', 'Alvarez', 'Navarro', 'Romero', 'Serrano', 'Delgado', 'Castro', 'Ortega', 'Iglesias', 'Ramirez','Kumar', 'Moorthi', 'Shastri', 'Acharya', 'Prasad', 'Pillai', 'Gowda', 'Nayak', 'Radhakrishnan', 'Desmukh', 'Kulkarni', 'Bhat', 'Kamat', 'Bhandary', 'Srivastava', 'Padmanabhan', 'Krishnamurthy', 'Balasubramanian', 'Upadhyay', 'Poojary', 'Ballal', 'Chowta', 'Naik', 'Kadamba', 'Hoysala', 'Maurya', 'Poonja', 'Namboodiri', 'Pannikar', 'Varma', 'Sooriyaprakash', 'Subrahmyan', 'Vijay', 'Sudhakar', 'Srinivasan', 'Jeyaramachandharan', 'Murthiyrakkaventharan', 'Ikkuzhan', 'Patel','Addisu', 'Abel', 'Adunga', 'Aman', 'Afewerek', 'Bayissa', 'Biniam', 'Berhanu', 'Bruk', 'Dagim', 'Dawit', 'Dula', 'Ejigu', 'Eleazar', 'Etefu', 'Fashika', 'Frew', 'Gabra', 'Gedeyon', 'Getachew', 'Habtamu', 'Hassan', 'Jember', 'Kabede', 'Mekonnen', 'Mulu', 'Neguse', 'Rada', 'Solomon', 'Taddese', 'Tamrat', 'Temesgen', 'Tesfaye', 'Workneh', 'Yeakob', 'Yohannes', 'Zewedu', 'Adebayo', 'Oni', 'Eze', 'Mohammed', 'Matrins', 'Adeyemi', 'Okafor', 'Chukwu', 'Okonkwo', 'Balogun', 'Savage', 'Lawal', 'Attah', 'Chukwumereije', 'Fofana', 'Iweala', 'Okonjo', 'Ezekwesili', 'Keita', 'Soyinka', 'Solarin', 'Gbadamosi', 'Olanrewaju', 'Magoro', 'Madaki', 'Jang', 'Oyinlola', 'Onwudiwe', 'Jakande', 'Igwe', 'Obi', 'Orji', 'Ohakim', 'Alakija', 'Akenzua','Al-Qahtani', 'Al-Shammari', 'Al-Onazy', 'Al-Dosary', 'Al-Subaiee', 'Al-Yami', 'Al-harbi', 'Al-Ghamdi', 'Al-Zahrani', 'Al-Reshedi', 'Al-Blawi', 'Al-Juhani', 'Al-Ahmari', 'Yilmaz', 'Kaya', 'Demir', 'Sahin', 'Celik', 'Yildiz', 'Yildirim', 'Ozturk', 'Aydin', 'Ozdemir']
                }

        self.misc_colors=[(98, 68, 93),(99, 98, 87),]
     
        self.race_skin_colors = {
            'CAUCASIAN':{'light':[], 'normal':[], 'dark':[],'outline':[]}, 
            'ARAB':{'light':[], 'normal':[(98, 63, 38),], 'dark':[(84, 50, 34),(84, 51, 33),(87, 44, 32),(87, 45, 32),(87, 53, 37),(90, 40, 21),(90, 53, 33),(90, 56, 40),(93, 55, 41),(93, 67, 55),(96, 58, 40),(96, 58, 43),(98, 54, 38),(98, 58, 44),(98, 61, 38),(99, 60, 47),(104, 58, 37),(107, 58, 44), (107, 58, 44),(107, 58, 46),(107, 60, 37),],'outline':[]}, 
            'INDIAN':{'light':[], 'normal':[(84, 50, 34),(84, 51, 33),(87, 45, 32),(87, 53, 37),(90, 40, 21),(90, 53, 33), (90, 56, 40),(93, 55, 41),(96, 54, 38),(96, 58, 40),(96, 58, 43),(98, 54, 38),(98, 58, 44),(98, 61, 38),(98, 63, 38),(99, 60, 47),(104, 58, 37),(107, 58, 44), (107, 58, 44),(107, 58, 46),(107, 60, 37),], 'dark':[],'outline':[]}, 
            'AFRICAN':{'light':[(71, 61, 52),(82, 53, 41),(84, 51, 33),(86, 58, 43), (90, 40, 21),(90, 53, 33), (90, 56, 40),(93, 55, 41),(96, 58, 40),(96, 58, 43),(98, 54, 38),(98, 58, 44),(98, 61, 38),(98, 63, 38),(99, 60, 47),(107, 58, 46),], 'normal':[(53, 32, 21),(58, 37, 27),(60, 33, 19),(60, 33, 21),(62, 33, 23),(66, 36, 25),(66, 37, 19),(66, 41, 26),(68, 37, 24), (71, 35, 23),(71, 37, 24),(71, 43, 24),(76, 42, 26),(79, 45, 33),(79, 49, 32),(79, 49, 35),(79, 50, 27),(87, 44, 32), (87, 45, 32),(87, 53, 37),(96, 54, 38),(98, 63, 38),(104, 58, 37),(107, 58, 44),(50, 26, 17), (107, 58, 44),(107, 60, 37),], 'dark':[(42, 19, 12), (43, 21, 13), (43, 25, 13),(44, 21, 13),(44, 22, 13), (48, 25, 12), (49, 28, 16),(49, 28, 19),(49, 28, 25), ],'outline':[]}, 
            'ASIAN':{'light':[], 'normal':[], 'dark':[(84, 50, 34),],'outline':[]}, 
            'NATIVE-AMERICAN':{'light':[], 'normal':[(87, 44, 32), (96, 54, 38),(96, 58, 43),(98, 61, 38),(99, 60, 47), (107, 58, 44),], 'dark':[(54, 28, 17),(55, 29, 19),(60, 31, 21),(76, 42, 26),(79, 45, 33),(82, 53, 41),(84, 50, 34),(87, 45, 32),(87, 53, 37),(90, 40, 21),(93, 67, 55),(96, 58, 40),(98, 54, 38),(98, 58, 44),(104, 58, 37),(107, 58, 44),],'outline':[]}, 
            'HISPANIC':{'light':[], 'normal':[(90, 40, 21),(93, 67, 55),(96, 54, 38),(96, 58, 43),(98, 58, 44),(98, 61, 38),(99, 60, 47),(104, 58, 37),(107, 58, 44), (107, 58, 44),(107, 58, 46),(107, 60, 37),], 'dark':[(84, 50, 34),(84, 51, 33),(87, 44, 32), (87, 45, 32),(87, 53, 37),(90, 53, 33), (90, 56, 40),(96, 58, 40),(98, 54, 38),],'outline':[]},
            'BLUE_ALIEN':{'light':[(87, 190, 162),(99, 166, 140),], 'normal':[(38, 146, 159),], 'dark':[(27, 45, 60),(46, 67, 76),(66, 69, 74),(98, 63, 38),],'outline':[]},
            'GREEN_ALIEN':{'light':[(68, 86, 57),], 'normal':[(82, 132, 0),], 'dark':[],'outline':[]}
        }         
        
        self.rgb_skin_colors = [(107, 69, 41), (107, 69, 55), (107, 73, 52), (107, 190, 170), (109, 69, 43), (109, 69, 55), (112, 58, 29), (112, 58, 37), 
         (112, 58, 38), (112, 60, 46), (112, 61, 46), (112, 108, 109), (112, 139, 165), (112, 156, 200), (112, 172, 132), (112, 213, 159), (115, 75, 60), (115, 77, 49), (115, 243, 197), (117, 65, 45), (117, 78, 53), (117, 78, 55), (119, 83, 62), (120, 69, 40), (120, 83, 60), (120, 91, 79), (120, 121, 134), (120, 145, 93), (123, 65, 43), (123, 71, 48), (123, 136, 164),
         (123, 140, 142), (123, 154, 197), (123, 180, 145), (124, 82, 67), (125, 125, 69), (126, 71, 44), (126, 81, 66), (126, 82, 58), (126, 86, 68), (126, 91, 60), (126, 94, 104), (129, 73, 37), (129, 82, 58), (129, 83, 60), (129, 89, 62), (129, 89, 65), (129, 125, 101), (131, 84, 60), (131, 110, 104), (131, 175, 140), (132, 85, 49), (132, 85, 66), (132, 90, 66), (133, 85, 67),
         (134, 87, 90), (134, 98, 70), (134, 122, 66), (134, 128, 148), (134, 179, 142), (137, 82, 52), (137, 89, 60), (137, 93, 76), (137, 95, 75), (137, 98, 75), (137, 99, 76), (137, 110, 87), (140, 93, 74), (140, 125, 125), (140, 130, 126), (140, 170, 123), (142, 91, 66), (142, 105, 74), (142, 152, 71), (142, 168, 189), (144, 72, 34), (145, 93, 66), (145, 93, 71), (145, 105, 71),
         (145, 105, 82), (145, 138, 112), (147, 105, 65), (148, 87, 53), (148, 94, 68), (148, 101, 66), (150, 107, 82), (150, 110, 90), (150, 134, 82), (153, 101, 71), (153, 110, 84), (153, 134, 170), (153, 183, 134), (156, 109, 99), (156, 117, 93), (156, 118, 87), (156, 132, 84), (156, 134, 82), (156, 134, 115), (156, 150, 66), (156, 170, 120), (156, 174, 115), (159, 106, 84), 
         (159, 109, 90), (159, 110, 90), (159, 117, 90), (159, 117, 96), (162, 171, 167), (164, 113, 90), (164, 115, 84), (164, 122, 101), (164, 124, 105), (165, 117, 99), (165, 135, 214), (165, 139, 120), (165, 147, 142), (165, 164, 162), (167, 117, 90), (167, 118, 82), (167, 126, 109), (167, 129, 112), (167, 155, 164), (167, 180, 129), (170, 118, 96), (170, 129, 104), (170, 131, 101), 
         (170, 148, 137), (170, 159, 140), (173, 115, 96), (173, 117, 107), (173, 121, 82), (173, 121, 99), (173, 125, 107), (173, 129, 104), (173, 131, 109), (173, 134, 99), (173, 134, 117), (173, 160, 162), (173, 186, 140), (175, 110, 74), (175, 135, 112), (175, 159, 145), (175, 171, 183), (178, 125, 99), (178, 131, 93), (181, 128, 101), (181, 130, 115), (181, 138, 107), (181, 143, 126), 
         (181, 147, 131), (181, 148, 214), (181, 154, 90), (181, 167, 159), (183, 134, 112), (183, 142, 120), (183, 144, 115), (183, 144, 120), (183, 146, 112), (183, 148, 123), (183, 151, 137), (183, 154, 134), (183, 175, 140), (186, 121, 93), (186, 126, 90), (186, 138, 112), (186, 148, 126), (186, 166, 71), (186, 179, 191), (186, 187, 186), (189, 134, 106), (189, 134, 123), (189, 142, 99), 
         (189, 143, 115), (189, 144, 126), (189, 146, 120), (189, 148, 134), (189, 154, 115), (189, 163, 148), (189, 166, 156), (189, 174, 123), (191, 105, 153), (191, 142, 109), (191, 142, 115), (191, 146, 115), (191, 155, 126), (191, 163, 145), (191, 168, 162), (191, 196, 159), (194, 144, 123), (194, 144, 126), (194, 156, 137), (194, 158, 134), (194, 159, 129), (194, 167, 153), (194, 168, 145), 
         (194, 183, 131), (196, 173, 162), (197, 155, 126), (197, 161, 149), (197, 163, 140), (197, 172, 161), (197, 174, 162), (197, 174, 173), (197, 174, 181), (198, 168, 162), (200, 158, 126), (200, 183, 167), (200, 185, 180), (203, 152, 129), (203, 159, 137), (203, 159, 142), (203, 167, 153), (203, 170, 145), (203, 170, 153), (203, 170, 156), (203, 170, 157), (203, 192, 132), (206, 158, 148),
         (206, 162, 140), (206, 171, 153), (206, 174, 156), (206, 187, 183), (208, 148, 126), (208, 163, 142), (208, 194, 183), (211, 167, 145), (211, 168, 145), (211, 176, 159), (211, 179, 167), (211, 187, 164), (214, 65, 63), (214, 129, 41), (214, 167, 153), (214, 174, 156), (214, 179, 164), (214, 186, 165), (214, 188, 178), (214, 190, 153), (214, 203, 203), (216, 176, 150), (216, 183, 150), 
         (219, 180, 156), (219, 196, 175), (222, 186, 165), (222, 202, 197), (224, 158, 38), (224, 159, 68), (230, 196, 173), (230, 197, 90), (233, 132, 76), (236, 185, 136), (247, 174, 101)]

 #       self.rgb_skin_colors = [(27, 45, 60), (38, 146, 159), (42, 19, 12), (43, 21, 13), (43, 25, 13), (44, 21, 13), (44, 22, 13), (46, 67, 76), (48, 25, 12), (49, 28, 16), (49, 28, 19), (49, 28, 25), (50, 26, 17), (53, 32, 21), (54, 28, 17), (55, 29, 19), (58, 37, 27), (60, 31, 21), (60, 33, 19), (60, 33, 21), (62, 33, 23), (66, 36, 25), (66, 37, 19), (66, 41, 26),
 #        (66, 69, 74), (68, 37, 24), (68, 86, 57), (71, 35, 23), (71, 37, 24), (71, 41, 24), (71, 43, 24), (71, 61, 52), (76, 42, 26), (79, 42, 26), (79, 45, 33), (79, 49, 32), (79, 49, 35), (79, 50, 27), (82, 53, 41), (82, 132, 0), (84, 50, 34), (84, 51, 33), (86, 58, 43), (87, 44, 32), (87, 45, 32), (87, 53, 37), (87, 190, 162), (90, 40, 21), (90, 53, 33), (90, 56, 40), 
 #        (93, 55, 41), (93, 67, 55), (96, 54, 38), (96, 58, 40), (96, 58, 43), (98, 54, 38), (98, 58, 44), (98, 61, 38), (98, 63, 38), (98, 68, 93), (99, 60, 47), (99, 98, 87), (99, 166, 140), (104, 58, 37), (107, 58, 44), (107, 58, 46), (107, 60, 37), (107, 69, 41), (107, 69, 55), (107, 73, 52), (107, 190, 170), (109, 69, 43), (109, 69, 55), (112, 58, 29), (112, 58, 37), 
 #        (112, 58, 38), (112, 60, 46), (112, 61, 46), (112, 108, 109), (112, 139, 165), (112, 156, 200), (112, 172, 132), (112, 213, 159), (115, 75, 60), (115, 77, 49), (115, 243, 197), (117, 65, 45), (117, 78, 53), (117, 78, 55), (119, 83, 62), (120, 69, 40), (120, 83, 60), (120, 91, 79), (120, 121, 134), (120, 145, 93), (123, 65, 43), (123, 71, 48), (123, 136, 164),
 #        (123, 140, 142), (123, 154, 197), (123, 180, 145), (124, 82, 67), (125, 125, 69), (126, 71, 44), (126, 81, 66), (126, 82, 58), (126, 86, 68), (126, 91, 60), (126, 94, 104), (129, 73, 37), (129, 82, 58), (129, 83, 60), (129, 89, 62), (129, 89, 65), (129, 125, 101), (131, 84, 60), (131, 110, 104), (131, 175, 140), (132, 85, 49), (132, 85, 66), (132, 90, 66), (133, 85, 67),
 #        (134, 87, 90), (134, 98, 70), (134, 122, 66), (134, 128, 148), (134, 179, 142), (137, 82, 52), (137, 89, 60), (137, 93, 76), (137, 95, 75), (137, 98, 75), (137, 99, 76), (137, 110, 87), (140, 93, 74), (140, 125, 125), (140, 130, 126), (140, 170, 123), (142, 91, 66), (142, 105, 74), (142, 152, 71), (142, 168, 189), (144, 72, 34), (145, 93, 66), (145, 93, 71), (145, 105, 71),
 #        (145, 105, 82), (145, 138, 112), (147, 105, 65), (148, 87, 53), (148, 94, 68), (148, 101, 66), (150, 107, 82), (150, 110, 90), (150, 134, 82), (153, 101, 71), (153, 110, 84), (153, 134, 170), (153, 183, 134), (156, 109, 99), (156, 117, 93), (156, 118, 87), (156, 132, 84), (156, 134, 82), (156, 134, 115), (156, 150, 66), (156, 170, 120), (156, 174, 115), (159, 106, 84), 
 #        (159, 109, 90), (159, 110, 90), (159, 117, 90), (159, 117, 96), (162, 171, 167), (164, 113, 90), (164, 115, 84), (164, 122, 101), (164, 124, 105), (165, 117, 99), (165, 135, 214), (165, 139, 120), (165, 147, 142), (165, 164, 162), (167, 117, 90), (167, 118, 82), (167, 126, 109), (167, 129, 112), (167, 155, 164), (167, 180, 129), (170, 118, 96), (170, 129, 104), (170, 131, 101), 
 #        (170, 148, 137), (170, 159, 140), (173, 115, 96), (173, 117, 107), (173, 121, 82), (173, 121, 99), (173, 125, 107), (173, 129, 104), (173, 131, 109), (173, 134, 99), (173, 134, 117), (173, 160, 162), (173, 186, 140), (175, 110, 74), (175, 135, 112), (175, 159, 145), (175, 171, 183), (178, 125, 99), (178, 131, 93), (181, 128, 101), (181, 130, 115), (181, 138, 107), (181, 143, 126), 
 #        (181, 147, 131), (181, 148, 214), (181, 154, 90), (181, 167, 159), (183, 134, 112), (183, 142, 120), (183, 144, 115), (183, 144, 120), (183, 146, 112), (183, 148, 123), (183, 151, 137), (183, 154, 134), (183, 175, 140), (186, 121, 93), (186, 126, 90), (186, 138, 112), (186, 148, 126), (186, 166, 71), (186, 179, 191), (186, 187, 186), (189, 134, 106), (189, 134, 123), (189, 142, 99), 
 #        (189, 143, 115), (189, 144, 126), (189, 146, 120), (189, 148, 134), (189, 154, 115), (189, 163, 148), (189, 166, 156), (189, 174, 123), (191, 105, 153), (191, 142, 109), (191, 142, 115), (191, 146, 115), (191, 155, 126), (191, 163, 145), (191, 168, 162), (191, 196, 159), (194, 144, 123), (194, 144, 126), (194, 156, 137), (194, 158, 134), (194, 159, 129), (194, 167, 153), (194, 168, 145), 
 #        (194, 183, 131), (196, 173, 162), (197, 155, 126), (197, 161, 149), (197, 163, 140), (197, 172, 161), (197, 174, 162), (197, 174, 173), (197, 174, 181), (198, 168, 162), (200, 158, 126), (200, 183, 167), (200, 185, 180), (203, 152, 129), (203, 159, 137), (203, 159, 142), (203, 167, 153), (203, 170, 145), (203, 170, 153), (203, 170, 156), (203, 170, 157), (203, 192, 132), (206, 158, 148),
 #        (206, 162, 140), (206, 171, 153), (206, 174, 156), (206, 187, 183), (208, 148, 126), (208, 163, 142), (208, 194, 183), (211, 167, 145), (211, 168, 145), (211, 176, 159), (211, 179, 167), (211, 187, 164), (214, 65, 63), (214, 129, 41), (214, 167, 153), (214, 174, 156), (214, 179, 164), (214, 186, 165), (214, 188, 178), (214, 190, 153), (214, 203, 203), (216, 176, 150), (216, 183, 150), 
 #        (219, 180, 156), (219, 196, 175), (222, 186, 165), (222, 202, 197), (224, 158, 38), (224, 159, 68), (230, 196, 173), (230, 197, 90), (233, 132, 76), (236, 185, 136), (247, 174, 101)]
               
        self.skin_colors = ['#dbb49c', '#d8b096', '#c5aca1', '#bfa391', '#d3b09f', '#c2a799', '#cbaa91', '#ceae9c', '#c5a195', '#cbaa9d', '#cbaa9c', '#d3a891', '#cbaa99', '#d3a791', '#ba947e', '#cb9f8e', '#cb9881', '#b79a86', '#c59b7e', '#c89e7e', '#bd9486', '#c29c89', '#d0a38e', '#bf9273', '#bd8f73', '#d0947e', '#af8770', '#bf9b7e', '#c2907e', '#b78e78', '#ad8675', '#bf8e6d', '#ad7963', '#ba8a70',
                    '#bf8e73', '#bd9278', '#b78670', '#a47a65', '#b27d63', '#a47c69', '#ad836d', '#ba7e5a', '#b2835d', '#b58065', '#aa8168', '#9f6e5a', '#a4715a', '#a7755a', '#996e54', '#9f7560', '#af6e4a', '#9f755a', '#966e5a', '#a47354', '#9f6d5a', '#916952', '#89624b', '#945e44', '#895234', '#a77652', '#996547', '#895f4b', '#945735', '#78533c', '#81593e', '#915d47', '#77533e', '#7b4730',
                    '#815941', '#83543c', '#6b4934', '#814925', '#7e472c', '#703a26', '#89593c', '#754e37', '#81533c', '#703a1d', '#754e35', '#7b412b', '#784528', '#5a2815', '#603a28', '#633c2f', '#703c2e', '#75412d', '#603a2b', '#6b3a2e', '#573525', '#683a25', '#5a3828', '#572d20', '#6b3c25', '#703d2e', '#4f3120', '#543222', '#703a25', '#4f2a1a', '#4c2a1a', '#572c20', '#472518', '#3e2117',
                    '#352015', '#3c1f15', '#472317', '#30190c', '#371d13', '#361c11', '#2b150d', '#2a130c', '#c8b7a7', '#dbc4af', '#d3bba4', '#c2a891', '#d6baa5', '#bda394', '#c5aea2', '#c29f81', '#ceab99', '#cb9f89', '#c5a38c', '#b79270', '#a58b78', '#c29e86', '#aa9489', '#c2907b', '#ad8663', '#b7947b', '#bd907e', '#b79078', '#b79073', '#ad7952', '#aa8365', '#a57563', '#ad8168', '#aa7660', 
                    '#896e57', '#ad7360', '#916947', '#9f6a54', '#9c7657', '#895d4c', '#966b52', '#8e5b42', '#845a42', '#915d42', '#7e523a', '#8e694a', '#81523a', '#7e5b3c', '#734d31', '#5d4337', '#6b3a2c', '#6d4537', '#623f26', '#6d452b', '#603626', '#623626', '#623a2c', '#623d26', '#4f2d21', '#543321', '#4f321b', '#472b18', '#472918', '#442518', '#3c2115', '#422513', '#3c2113', '#2b190d', 
                    '#311c10', '#321a11', '#2c150d', '#2c160d', '#d0c2b7', '#d3b3a7', '#e6c4ad', '#d6bcb2', '#debaa5', '#d6b3a4', '#d6ae9c', '#c5aead', '#cba799', '#ce9e94', '#b58f7e', '#bda69c', '#cea28c', '#bd9a73', '#b79789', '#bd8e63', '#b59383', '#a78170', '#bd866a', '#bd867b', '#b58a6b', '#b58273', '#9c755d', '#ba795d', '#ad756b', '#9c6d63', '#a77e6d', '#ad7d6b', '#946542', '#845531', 
                    '#8c5d4a', '#89634c', '#6b4529', '#845542', '#7e5644', '#855543', '#7e5142', '#734b3c', '#5a3521', '#563a2b', '#6b4537', '#5d3729', '#523529', '#4f3123', '#422419', '#3a251b', '#311c19', '#311c13', '#a2aba7', '#d6cbcb', '#b5a79f', '#c4ada2', '#706c6d', '#cebbb7', '#bf6999', '#d6413f', '#ecb988', '#e9844c', '#d68129', '#e09e26', '#e09f44', '#baa647', '#b59a5a', '#70ac84', 
                    '#8caa7b', '#7d7d45', '#9caa78', '#528400', '#a7b481', '#8e9847', '#70d59f', '#445639', '#86b38e', '#57bea2', '#83af8c', '#709cc8', '#26929f', '#7b8c8e', '#708ba5', '#a587d6', '#2e434c', '#8c7d7d', '#b594d6', '#7b88a4', '#868094', '#866246', '#9986aa', '#7c5243', '#ada0a2', '#c8b9b4', '#af9f91', '#a5938e', '#836e68', '#785b4f', '#f7ae65', '#904822', '#936941', '#e6c55a', 
                    '#99b786', '#9c9642', '#bdae7b', '#adba8c', '#918a70', '#78915d', '#73f3c5', '#8ea8bd', '#787986', '#42454a', '#62445d', '#636257', '#473d34', '#decac5', '#9c8673', '#babbba', '#d6a799', '#d8b796', '#d6be99', '#63a68c', '#6bbeaa', '#9cae73', '#bfc49f', '#b7af8c', '#c2b783', '#7bb491', '#7b9ac5', '#bab3bf', '#a5a4a2', '#1b2d3c', '#afabb7', '#c5aeb5', '#a79ba4', '#aa9f8c', 
                    '#bfa8a2', '#8c827e', '#c6a8a2', '#cbc084', '#867a42', '#9c8652', '#817d65', '#968652', '#9c8454', '#86575a', '#7e5e68', '#42291a']
    
          #self.rgb_skin_colors = [(219, 180, 156), (216, 176, 150), (197, 172, 161), (191, 163, 145), (211, 176, 159), (194, 167, 153), (203, 170, 145), (206, 174, 156), (197, 161, 149), (203, 170, 157), (203, 170, 156), (211, 168, 145), (203, 170, 153), (211, 167, 145), (186, 148, 126), (203, 159, 142), (203, 152, 129), (183, 154, 134), (197, 155, 126), (200, 158, 126), (189, 148, 134), (194, 156, 137),
        #                        (208, 163, 142), (191, 146, 115), (189, 143, 115), (208, 148, 126), (175, 135, 112), (191, 155, 126), (194, 144, 126), (183, 142, 120), (173, 134, 117), (191, 142, 109), (173, 121, 99), (186, 138, 112), (191, 142, 115), (189, 146, 120), (183, 134, 112), (164, 122, 101), (178, 125, 99), (164, 124, 105), (173, 131, 109), (186, 126, 90), (178, 131, 93), (181, 128, 101), 
        #                        (170, 129, 104), (159, 110, 90), (164, 113, 90), (167, 117, 90), (153, 110, 84), (159, 117, 96), (175, 110, 74), (159, 117, 90), (150, 110, 90), (164, 115, 84), (159, 109, 90), (145, 105, 82), (137, 98, 75), (148, 94, 68), (137, 82, 52), (167, 118, 82), (153, 101, 71), (137, 95, 75), (148, 87, 53), (120, 83, 60), (129, 89, 62), (145, 93, 71), (119, 83, 62), (123, 71, 48), 
        #                        (129, 89, 65), (131, 84, 60), (107, 73, 52), (129, 73, 37), (126, 71, 44), (112, 58, 38), (137, 89, 60), (117, 78, 55), (129, 83, 60), (112, 58, 29), (117, 78, 53), (123, 65, 43), (120, 69, 40), (90, 40, 21), (96, 58, 40), (99, 60, 47), (112, 60, 46), (117, 65, 45), (96, 58, 43), (107, 58, 46), (87, 53, 37), (104, 58, 37), (90, 56, 40), (87, 45, 32), (107, 60, 37), 
        #                        (112, 61, 46), (79, 49, 32), (84, 50, 34), (112, 58, 37), (79, 42, 26), (76, 42, 26), (87, 44, 32), (71, 37, 24), (62, 33, 23), (53, 32, 21), (60, 31, 21), (71, 35, 23), (48, 25, 12), (55, 29, 19), (54, 28, 17), (43, 21, 13), (42, 19, 12), (200, 183, 167), (219, 196, 175), (211, 187, 164), (194, 168, 145), (214, 186, 165), (189, 163, 148), (197, 174, 162), (194, 159, 129), 
        #                        (206, 171, 153), (203, 159, 137), (197, 163, 140), (183, 146, 112), (165, 139, 120), (194, 158, 134), (170, 148, 137), (194, 144, 123), (173, 134, 99), (183, 148, 123), (189, 144, 126), (183, 144, 120), (183, 144, 115), (173, 121, 82), (170, 131, 101), (165, 117, 99), (173, 129, 104), (170, 118, 96), (137, 110, 87), (173, 115, 96), (145, 105, 71), (159, 106, 84), (156, 118, 87),
        #                        (137, 93, 76), (150, 107, 82), (142, 91, 66), (132, 90, 66), (145, 93, 66), (126, 82, 58), (142, 105, 74), (129, 82, 58), (126, 91, 60), (115, 77, 49), (93, 67, 55), (107, 58, 44), (109, 69, 55), (98, 63, 38), (109, 69, 43), (96, 54, 38), (98, 54, 38), (98, 58, 44), (98, 61, 38), (79, 45, 33), (84, 51, 33), (79, 50, 27), (71, 43, 24), (71, 41, 24), (68, 37, 24), (60, 33, 21), 
        #                        (66, 37, 19), (60, 33, 19), (43, 25, 13), (49, 28, 16), (50, 26, 17), (44, 21, 13), (44, 22, 13), (208, 194, 183), (211, 179, 167), (230, 196, 173), (214, 188, 178), (222, 186, 165), (214, 179, 164), (214, 174, 156), (197, 174, 173), (203, 167, 153), (206, 158, 148), (181, 143, 126), (189, 166, 156), (206, 162, 140), (189, 154, 115), (183, 151, 137), (189, 142, 99), (181, 147, 131), 
        #                        (167, 129, 112), (189, 134, 106), (189, 134, 123), (181, 138, 107), (181, 130, 115), (156, 117, 93), (186, 121, 93), (173, 117, 107), (156, 109, 99), (167, 126, 109), (173, 125, 107), (148, 101, 66), (132, 85, 49), (140, 93, 74), (137, 99, 76), (107, 69, 41), (132, 85, 66), (126, 86, 68), (133, 85, 67), (126, 81, 66), (115, 75, 60), (90, 53, 33), (86, 58, 43), (107, 69, 55), 
        #                        (93, 55, 41), (82, 53, 41), (79, 49, 35), (66, 36, 25), (58, 37, 27), (49, 28, 25), (49, 28, 19), (162, 171, 167), (214, 203, 203), (181, 167, 159), (196, 173, 162), (112, 108, 109), (206, 187, 183), (191, 105, 153), (214, 65, 63), (236, 185, 136), (233, 132, 76), (214, 129, 41), (224, 158, 38), (224, 159, 68), (186, 166, 71), (181, 154, 90), (112, 172, 132), (140, 170, 123), 
        #                        (125, 125, 69), (156, 170, 120), (82, 132, 0), (167, 180, 129), (142, 152, 71), (112, 213, 159), (68, 86, 57), (134, 179, 142), (87, 190, 162), (131, 175, 140), (112, 156, 200), (38, 146, 159), (123, 140, 142), (112, 139, 165), (165, 135, 214), (46, 67, 76), (140, 125, 125), (181, 148, 214), (123, 136, 164), (134, 128, 148), (134, 98, 70), (153, 134, 170), (124, 82, 67), (173, 160, 162), 
        #                        (200, 185, 180), (175, 159, 145), (165, 147, 142), (131, 110, 104), (120, 91, 79), (247, 174, 101), (144, 72, 34), (147, 105, 65), (230, 197, 90), (153, 183, 134), (156, 150, 66), (189, 174, 123), (173, 186, 140), (145, 138, 112), (120, 145, 93), (115, 243, 197), (142, 168, 189), (120, 121, 134), (66, 69, 74), (98, 68, 93), (99, 98, 87), (71, 61, 52), (222, 202, 197), (156, 134, 115), 
        #                        (186, 187, 186), (214, 167, 153), (216, 183, 150), (214, 190, 153), (99, 166, 140), (107, 190, 170), (156, 174, 115), (191, 196, 159), (183, 175, 140), (194, 183, 131), (123, 180, 145), (123, 154, 197), (186, 179, 191), (165, 164, 162), (27, 45, 60), (175, 171, 183), (197, 174, 181), (167, 155, 164), (170, 159, 140), (191, 168, 162), (140, 130, 126), (198, 168, 162), (203, 192, 132),
        #                        (134, 122, 66), (156, 134, 82), (129, 125, 101), (150, 134, 82), (156, 132, 84), (134, 87, 90), (126, 94, 104), (66, 41, 26)]
        self.noref_hair_colors=['Silver/White', 'Blonde','Brown','Orange/Ginger', 'Black', 'Strawberry blonde','E Red','Violet','E Blue', 'E Pink','E Green', 'Rose Gold']
        self.hair_color_names = ['base', 'PlatinumBlonde', 'GoldenBlonde', 'Blonde', 'LightBrownGreyish', 'LightBrown', 'LightBlonde', 'Red', 'LightRed', 'Black']
        self.rgb_hair_colors = {
            'base': {'light': (101, 80, 58), 'normal': (89, 59, 51), 'dark': (41, 31, 20), 'outline': (75, 21, 7)},
            'PlatinumBlonde': {'light': (240, 222, 202), 'normal': (233, 202, 167), 'dark': (217, 160, 98), 'outline': (143, 94, 44)}, 
            'GoldenBlonde': {'light': (233, 185, 120), 'normal': (241, 158, 67), 'dark': (221, 116, 1), 'outline': (141, 80, 3)}, 
            'Blonde': {'light': (250, 193, 110), 'normal': (238, 169, 96), 'dark': (200, 135, 72), 'outline': (167, 112, 55)}, 
            'LightBrownGreyish': {'light': (179, 152, 113), 'normal': (149, 121, 81), 'dark': (112, 96, 52), 'outline': (102, 73, 45)}, 
            'LightBrown': {'light': (163, 108, 48), 'normal': (141, 96, 46), 'dark': (115, 71, 32), 'outline': (82, 47, 12)}, 
            'LightBlonde': {'light': (238, 218, 196), 'normal': (230, 201, 170), 'dark': (222, 176, 126), 'outline': (189, 148, 102)}, 
            'Red': {'light': (251, 88, 40), 'normal': (249, 72, 21), 'dark': (184, 58, 22), 'outline': (114, 43, 22)}, 
            'LightRed': {'light': (220, 148, 82), 'normal': (219, 121, 62), 'dark': (199, 77, 18), 'outline': (105, 22, 20)}, 
            'Black': {'light': (62, 53, 40), 'normal': (45, 37, 30), 'dark': (30, 26, 21), 'outline': (1, 1, 0)}
            }
        self.hex_hair_colors = {
            'base': {'light': '#65503a', 'normal': '#593b33', 'dark': '#291f14', 'outline': '#4b1507'},  
            'PlatinumBlonde': {'light': '#f0deca', 'normal': '#e9caa7', 'dark': '#d9a062', 'outline': '#8f5e2c'}, 
            'GoldenBlonde': {'light': '#e9b978', 'normal': '#f19e43', 'dark': '#dd7401', 'outline': '#8d5003'}, 
            'Blonde': {'light': '#fac16e', 'normal': '#eea960', 'dark': '#c88748', 'outline': '#a77037'}, 
            'LightBrownGreyish': {'light': '#b39871', 'normal': '#957951', 'dark': '#706034', 'outline': '#66492d'}, 
            'LightBrown': {'light': '#a36c30', 'normal': '#8d602e', 'dark': '#734720', 'outline': '#522f0c'}, 
            'LightBlonde': {'light': '#eedac4', 'normal': '#e6c9aa', 'dark': '#deb07e', 'outline': '#bd9466'}, 
            'Red': {'light': '#fb5828', 'normal': '#f94815', 'dark': '#b83a16', 'outline': '#722b16'}, 
            'LightRed': {'light': '#dc9452', 'normal': '#db793e', 'dark': '#c74d12', 'outline': '#691614'}, 
            'Black': {'light': '#3e3528', 'normal': '#2d251e', 'dark': '#1e1a15', 'outline': '#010100'}
            }

   # def _is_born(self):
   #     current_time = time.time()
   #     self.age = current_time - self.birthday[0]
   #     return self.age 
    def steps(self,n,size):
        return (int((n)/size))*size

    def random_number(self,cap=10,iterations:int=10):
        step_history = []
        for x in range(iterations):
            step_history.append(self.random_range(0,cap))
        return sum(step_history)/len(step_history)
    def get_edges(self,x,y,w,h,lr_mod=0.5,tb_mod=0.5):
        left,right = x - w*lr_mod, x + w*lr_mod
        top,bottom = y + h*tb_mod, y - h*tb_mod
        return [left,top,right,bottom]
    def get_dim(self,left,top,right,bottom):
        w = abs(left - right)
        h = abs(bottom - top) 
        x = left + w/2
        y = bottom + h/2
        return [x,y,w,h]
    def get_corners(self,x,y,w,h,x_buffer:float=1,y_buffer:float=1):
        left,top,right,bottom = self.get_edges(x,y,w,h,x_buffer,y_buffer)
        return [[left,top],[right,top],[right,bottom],[left,bottom]]

    def get_sides(self,x,y,w,h,x_buffer:float=1,y_buffer:float=1):
        left, top, right, bottom = self.get_edges(x,y,w,h,x_buffer,y_buffer)
        return [[left,y],[x,top],[right,y],[x,bottom]]

    def frange(self,start,end,step):
        import numpy as np
        return list(np.arange(start,end,step,dtype=float))

    def random_shuffle(self,List:list=[]):
        rlist=[]
        for item in range(len(List)):
            selection = self.random_selection(List)
            rlist.append(selection)
            List.remove(selection)
        return rlist

    def distance_between_points(self,x1, x2, y1, y2):
        from math import sqrt
        return sqrt((pow((x2 - x1), 2) + pow((y2 - y1), 2)))

    def sub_percent(self,base, percent):
        '''Subtract a number by a certain percentage'''
        return base-(base*(percent/100))

    def total_volume(self, circumfrence, length):
        from math import pi
        return ((pi*(circumfrence))*length)

    def random_shade(self):
        return self.random_range(0,255)
    def random_rgb_color(self):
        return (self.random_shade(),self.random_shade(),self.random_shade())

    def random_float(self, places=None):
        rand = random.random()
        return rand

    def random_range(self,low,high,step=1):
        return self.random_selection(list(range(low,high,step)))

    def calcforce(self, mass, acc):
        '''Calcuates the force of an object moving.'''
        force = mass * acc
        return force

    def get_point_matrix(self, x, y, spread=1):
        '''Returns a matrix based on an origin and a spread.'''
        matrix = [(x-spread,y+spread),  (x, y+spread), (x+spread, y+spread),
                  (x-spread, y),        (x, y),        (x+spread, y),
                  (x-spread, y-spread), (x, y-spread), (x+spread, y-spread)]
        return matrix

    def ft_in_to_meters(self, a):
        '''a = (feet, inches) to meters'''
        feet_to_inches = a[0] * 12
        total_inches = feet_to_inches + a[1]
        meters = total_inches * 0.0254
        return meters

    def ft_to_meters(self,feet):
        '''Feet only to meters'''
        meters = feet / 3.28
        return meters

    def meters_to_ft(self,meters):
        '''Meters to feet only'''
        total_inches = meters  * 39.37
        feet = total_inches / 12
        inches = total_inches 
        return feet
    
    def meters_to_inches(self, meters):
        '''Meters to inches only'''
        total_inches = meters  * 39.37
        return total_inches

    def inches_to_meters(self, inches):
        '''Inches to meters'''
        meters = inches / 39.37
        return meters

    def ft_to_inches(self, feet):
        '''Feet to inches only'''
        inches = feet*12
        return inches

    def hex_to_rgb(self, value):
        '''Converts hex color code into RGB'''
        value = value.lstrip('#')
        lv = len(value)
        return tuple(int(value[i:i+lv//3], 16) for i in range(0, lv, lv//3))
    
    def rgb_to_hex(self, rgb):
        '''Converts rgb color code into hex'''
        if len(rgb)==3:
            return '%02x%02x%02x' % rgb
        elif len(rgb)==4:
            return '%02x%02x%02x%02x' % rgb

    def random_skin_color(self, mode='hex'):
        '''Generates and returns a random skin color'''
        if mode == 'hex':
            length = len(self.skin_colors)
            index = random.randint(0, length-1)
            e = self.skin_colors[index]
            return e
        elif mode == 'tuple':
            length = len(self.rgb_skin_colors)
            index = random.randint(0, length-1)
            e = self.rgb_skin_colors[index]
            return e

    def lbs_to_kilos(self, weight_lbs):
        '''Converts weight in pounds to kilos.'''
        kilos = weight_lbs / 2.205
        return kilos

    def kilos_to_lbs(self, kilos):
        '''Converts kilos to pounds.'''
        lbs = kilos * 2.205
        return lbs

    def fib(self, n):
        '''Returns the nth fibonacci sequence of a number'''
        a = 0
        b = 1
        if n<0:
            print('INCORRECT INPUT')
        elif n==0:
            return 0
        elif n==1:
            return 1
        else:
            for i in range(2, n):
                c = a + b
                a = b
                b = c
            return  b

    def random_selection(self, list_or_tuple=[None, None, None]):
        '''Returns a random selection from a specified list'''
        selection = None
        while selection is None:
            try:
                length = len(list(list_or_tuple))
                l = random.randint(0, int((length-1)))
                selection = list_or_tuple[int(l)]
                return selection
                break
            except ValueError:
                pass

    def inv_op(self, num=1, modifier=0, dimensions=1):
        '''Returns the inverse opposite of a number within a space'''
        inv_op = ((-num)+modifier)/dimensions
        return inv_op

    def generate_person_fast(self):
        '''Generates primitive person attributes placed within a dictionary'''
        NAME_ARRAYS = (self.surnames['ASIAN'], self.surnames['CAUC'],self.surnames['HISPANIC'],self.surnames['INDIAN'],self.surnames['AFRICAN'],self.surnames['ARAB'],self.surnames['NATIVE-AMERICAN'])
        cards = [self.EYE_COLOR_NAMES, self.CHEST_SIZE, self.HEIGHT_METERS, self.WEIGHT_KG, self.MUSCULARITY, self.FATTINESS, self.SKILL_INTELLIGENCE, self.SHOULDER_WIDTH, self.SHOULDER_DEPTH, self.WAIST_WIDTH, self.WAIST_DEPTH, self.HIP_WIDTH, self.HIP_DEPTH, self.SEX, self.GENDER_IDENTITY, self.GENDER_EXPRESSION, self.SEXUAL_ORIENTATION, self.alive, self.birthday, self.race_names ]
        person = ['eye color', 'chest', 'height', 'weight', 'muscularity', 'fattiness', 'intelligence', 'shoulder width', 'shoulder depth', 'waist width', 'waist depth', 'hip width', 'hip depth', 'sex', 'gender identity', 'gender expressed', 'sex orientation', 'living', 'birthday', 'race', 'last name']
        merged = {}       
        for a, b in zip(person, cards):
            e = self.random_selection(b)
            merged[a] =e
            #print(a, e)
        while 'first name' not in merged:
            if merged['sex'] in ('MALE', 'BOY', 'TRANSMALE'):
                e = self.random_selection(self.male_first_names)
                merged['first name'] = e
            elif merged['sex'] in ('FEMALE', 'GIRL', 'TRANSFEMALE'):
                e = self.random_selection(self.female_first_names)
                merged['first name'] = e
            elif merged['sex'] in ('INTERSEX/FUTA/HERMAPHRODITE','NULL'):
                num = random.randint(0, 100)
                if num > 50:
                    e = self.random_selection(self.male_first_names)
                    merged['first name'] = e
                elif num < 50:
                    e = self.random_selection(self.female_first_names)
                    merged['first name'] = e
                merged['sex orientation']=self.random_selection([self.SEXUAL_ORIENTATION[1],self.SEXUAL_ORIENTATION[-2],self.SEXUAL_ORIENTATION[-1]])

        while 'last name' not in merged:
            if merged['race'] in 'CAUC':
                e = self.random_selection(self.surnames['CAUC'])
                merged['last name']=e   
            if merged['race'] in 'ASIAN':
                e = self.random_selection(self.surnames['ASIAN'])
                merged['last name']=e
            if merged['race'] in 'INDIAN':
                e = self.random_selection(self.surnames['INDIAN'])
                merged['last name']=e
            if merged['race'] in 'HISPANIC':
                e = self.random_selection(self.surnames['HISPANIC'])
                merged['last name']=e
            if merged['race'] in 'AFRICAN':
                e = self.random_selection(self.surnames['AFRICAN'])
                merged['last name']=e
            if merged['race'] in 'ARAB':
                e = self.random_selection(self.surnames['ARAB'])
                merged['last name']=e
            if merged['race'] in 'NATIVE-AMERICAN':
                e = self.random_selection(self.surnames['NATIVE-AMERICAN'])
                merged['last name']=e

        merged['birthday']=self.birthday
        #print(merged['birthday'])
       # for o,v in merged.items():
       #     print(o, v)
        #ff = time.time() - 1000000000
        #print(datetime.datetime.fromtimestamp(ff))
        #print(merged)
        if 'first name' and 'last name' in merged and 'sex orientation' in merged and 'birthday' in merged:
            return merged

    def test(self):
        for a in self.skin_colors:
            e = self.hex_to_rgb(a)
            print(e)

    def is_odd(self,num):
        if (num % 2) == 0:
            return False
        else:
            return True

    


class Animator:
    def __init__(self, max_width=500, max_height=500):
        self.max_width = list(range(max_width+1))
        self.max_height = list(range(max_height+1))
        self.all_frames = {'total_frames':[],
                            'start':[],
                            'end':[]}
        self.occupied_points = {}
        self.future_frame = []
        self.current_frame = []

    def all_point_sums(self):
        all_points = []
        max_width = self.max_width
        max_height = self.max_height
        for a,b in zip(max_width, max_height):
            c = [a+b]
            all_points.append(c)
        if len(all_points) > 0:
            return all_points

    def all_points(self):
        template = []
        all_height_points = []
        all_width_points = []
        max_width = self.max_width
        max_height = self.max_height
        return (max_width, max_height)

import json
import os

# class dt():

#     def datetime():
#         '''Returns the current date and time in a string format'''
#         a = datetime.datetime.now()
#         return a
    
#     def timestamp():
#         '''Returns the current unix time, specify c = True to recieve only integar value'''
#         a = time.time()
#         return a
    
#     def fromtimestamp(timestamp):
#         '''Converts a timestamp to a date and time'''
#         if timestamp:
#             a = datetime.datetime.fromtimestamp(timestamp)
#             return a
    
#     def date():
#         '''Returns only the current date as a string '''
#         a = dt.datetime()
#         s = str(a).split(' ')
#         return s[0]
    
#     def sdate():
#         '''Returns only the current date as a list [YYYY,MM,DD]'''
#         date = dt.date()
#         date = date.split('-')
#         return date

#     def time():
#         '''Returns only the current time in HH:MM:SS'''
#         a = dt.datetime()
#         s = str(a).split(' ')
#         return s[1]


class DoShit:
    def wrap(self, string, wrap='\"'):
        return '{}{}{}'.format(wrap,string,wrap)
    def make_dirs(self,directory:str,folder_names:list=[]):
        if directory == None:
            directory = ''
            t = 1
        else:
            t = 0
        for d in folder_names:
            url = '{}{}\\'.format(directory,d)
            self.make_dir(url,c=t)

    def fill_list(self,amount,data=None):
        '''Prepares a blank list of a certain length'''
        return [data for r in range(amount)]

    def sort_list_by_most_common_elements(self,List):
        from collections import Counter
        return [item for items,c in Counter(List).most_common() for item in [items]*c]

    def alt(self,io):
        if io == 0:
            return 1
        elif io == 1:
            return 0

    def peeldir(self,directory,amount):
        broken = list(self.break_fileurl(directory))
        if len(broken)>amount:
            for i in range(amount):
                broken.pop(-1)
            new = '\\'.join(broken)
            return new

    def sortkeys(self,unsorted_dict:dict, reverse=0):
        '''Sort dictoinary by keys.'''
        original=unsorted_dict.copy()
        keys=unsorted_dict.keys()
        values=unsorted_dict.values()
        ret = {}
        sortedkeys = sorted(keys,reverse=reverse)
        for x in sortedkeys:
            ret[x]=original[x]
        return ret
        
    def sortvalues(self,keyvalue,reverse=False):
        '''Sort dictionary by values. Default is descending order.'''
        sortedkeys= sorted(keyvalue.items(), key = lambda kv:(kv[1], kv[0]))  
        ret={}
        if not reverse:
            for x in sortedkeys:
                ret[x[0]]=x[1]
            return ret
        else:
            for x in sortedkeys[::-1]:
                ret[x[0]]=x[1]
            return ret

    def readfromjson(self, filename,parse=1):
        '''Reads from a json encoded file.'''
        cont=self.readfrom(filename)
        if parse:
            if cont:
                cont=json.loads(cont)
                return cont
            
    def writetojson(self, filename, contents):
        '''Writes contents to a file using jason encoding.'''
        cont=json.dumps(contents,indent=4)
        self.writeto(filename,cont,m=1)

    def is_file(path):
        '''Test whether a path location is a file or not.'''
        if os.path.isdir(path):
            return False
        elif os.path.isfile(path):  
            return True

    def is_folder(path):
        '''Test whether a path location is a folder or not.'''
        if os.path.isdir(path):  
            return True 
        elif os.path.isfile(path):
            return False

    def find_sequences(self, random_range):
        '''Finds sets of sequential numbers and places them in a list in an order in which they were found.'''
        seq = [[]]
        for item1, item2 in zip(random_range, random_range[1:]):  # pairwise iteration
            if item2 - item1 == 1:
                # The difference is 1, if we're at the beginning of a sequence add both
                # to the result, otherwise just the second one (the first one is already
                # included because of the previous iteration).
                if not seq[-1]:  # index -1 means "last element".
                    seq[-1].extend((item1, item2))
                else:
                    seq[-1].append(item2)
            elif seq[-1]: 
                # The difference isn't 1 so add a new empty list in case it just ended a sequence.
                seq.append([])
        # In case "l" doesn't end with a "sequence" one needs to remove the trailing empty list.
        if not seq[-1]:
            del seq[-1]
        return seq

    def rename_file(self,url, new_filename, filetype=None):
        '''Rename a file internally without changing the actual filename.'''
        i = list(self.break_fileurl(url))
        if len(i) == 3:
            i[1]=new_filename
            if filetype:
                i[-1]='.'+str(filetype)
            i=''.join(i)
            return i
        else:
            print('Url specified is a folder not a file. Please specify a specific file url.')

    def merge_dict(self, *dict_args):
        '''Given any number of dictionaries, shallow copy and merge into a new dict,
        precedence goes to key value pairs in latter dictionaries.'''
        result = {}
        for dictionary in dict_args:
            result.update(dictionary)
        return result

    def dup_del_dict(self, dict_list=[]):
        '''Delete duplicate dictionaries from a list.'''
        d = []
        l = len(dict_list)
        if l > 0:
            temp = list(self._chunking(dict_list, int(l/2)))
            if len(temp) == 2:
                if len(temp[0]) == len(temp[1]):
                    n = 0
                    for x,y in zip(temp[0], temp[1]):
                        if x in d:
                            n+=1
                        elif x not in d:
                            d.append(x)

                        if y in d:
                            n+=1
                        elif y not in d:
                            d.append(y)
                        if n:
                            print('dups found:',str(n)+'/'+str(l))
                    if len(d) > 0:
                        return d
                else:
                    n = 0
                    for x in dict_list:
                        if x in d:
                            n+=1
                        elif x not in d:
                            d.append(x)
                        if n:
                            print('dups found:',str(n)+'/'+str(l))
    
    def _count_list_items(self, li = []):
        '''Count the number of items in a each list within a list'''
        length = [] 
        for x in li:
            length.append(len(x))
        return length

    def _reverse_list(self, li):
        '''Reverse a list replacing index 0 with -1 and vice versa'''
        if li:
            if len(li) > 1:
                new_list = []
                for x in list(li):
                    v = li.pop(-1)
                    new_list.append(v)
                return new_list
            elif len(li) <= 1:
                return li

    def custom_writer(self, lines=[], filename=None):
        '''Used with custom string and custom coder to write content to the output file'''
        for content in lines:
            W = self.writeto('custom.txt',content=content, mode='a')
    
    def clear_file(self, filename):
        '''Clears a file of any content'''
        W = self.writeto(filename, '', 'w')

    def custom_string(self, num):
        '''Custom string should returna list of lines to be written in custom coder.'''
        if num:
            m = Metatron()
        #    h = m.s_ultra(length=3, orbital_amount=2, BLOCK_VALUES_ONE=m.ALPHA, BLOCK_VALUES_TWO=m.ALPHA, BLOCK_VALUES_THREE=m.ALPHA)
#            l = ['            if lenccc > '+str(num)+':',       
#                '                if len(ccc['+str(num)+'])>0:',
#                '                    '+h[0]+' = ccc['+str(num)+'][l]',
#                '                    self._r('+h[0]+')']

#            l = ['                if myll > '+str(num)+':',
#                 '                    if len(my_l['+str(num)+'])>0:',
#                 '                        '+h[0]+' = my_l['+str(num)+'][counter]',
#                 '                        '+h[1]+' = self.hash_pool['+h[0]+'][\'percent\']',
#                 '                        if '+h[1]+' < percent:',
#                 '                            items.append('+h[0]+')']
            
            l = ['    if ll > '+str(num-1)+':',
                '        collision = check_for_collision_with_list(PLAYER_SPRITE, NPC_SPRITE_LIST['+str(num)+'].sprite)',
                '        if collision:',
                '            if NPC_SPRITE_LIST['+str(num)+'] not in nearby_objects:',
                '                nearby_objects.append(NPC_SPRITE_LIST['+str(num)+'])',
                '            else:',
                '                if NPC_SPRITE_LIST['+str(num)+'] in nearby_objects:',
                '                    nearby_objects.remove(NPC_SPRITE_LIST['+str(num)+'])']                      
            return l

    def custom_coder(self,start=0, _range=100,n=0):
        '''Write custom code to file'''
        print('writing code')
        if n == 1:
            self.clear_file('custom.txt')
        for x in range(start, int(_range)):
            l = self.custom_string(int(x))
            if l and len(l) > 0:
                self.custom_writer(l)
        
    def eff_list(self, slist=[], div=2):
        '''Divides a list and checks each list length for congruency.'''
        ll = len(list(slist))
        print(ll,'/',div,'=',str(int(ll/div)))
        ch = list(self._chunking(list(slist), int(ll/div)))
        o_l = len(ch)
        #list_1 = len(ch[0])
        #list_2 = len(ch[1])
        #list_3 = len(ch[2])
        #list_4 = len(ch[3])
    #    output = str(o_l)+' '+str(list_1)+' '+str(list_2)+' '+str(list_3)+' '+str(list_4)+' '+str(len(ch[4]))+' '+str(len(ch[5]))+' '+str(len(ch[6]))+' '+str(len(ch[7]))+' '+str(len(ch[8]))+' '+str(len(ch[9]))+' '+str(len(ch[10]))+' '+str(len(ch[11]))+' '+str(len(ch[12]))+' '+str(len(ch[13]))
    #    print(output)
        th = []
        if len(ch[0]) != len(ch[1]):
            a = len(ch[0])
            b = len(ch[1])
            _rt = []
            if a > b:
                for j in range(0, a):
                    g = ch[0].pop(-1)
                    if g:
                        th.append(g)
                for xx in ch:
                    _rt.append(xx)
                _rt.append(th)
                return _rt
            elif b > a: 
                for j in  range(0, b):
                    g = ch[1].pop(-1)
                    if g:
                        th.append(g)
                for xx in ch:
                    _rt.append(xx)
                _rt.append(th)
                return _rt

        if len(ch[0]) == len(ch[1]):
            _rt = []
            for xx in ch:
                _rt.append(xx)
            return _rt

    def get_dict(self, keys, values):
        '''Internal - Returns a dicitonary from a list of keys and a list of values.'''
        mydict = dict(zip(keys, values))
        return mydict

    def break_fileurl(self,url):
        if '\\' in str(url):
            url = str(url).replace('\\', '/')
        if '/' in str(url):
            f = str(url).split('/')
            filename = f.pop(-1)
        items = []
        string = ''
        ext_found = 0
        for char in filename[::-1]:
            if char == '.' and not ext_found:
                items.append(string[::-1])
                ext_found = 1
                string = ''
            else:
                string+=char
        items.append(string[::-1])
        items=items[::-1]
        for i in items:
            if i:
                f.append(i)
        if len(items)==1:
            foldername = f.pop(-1)
            directory = '/'.join(f)+'/'
            return (directory,foldername,'/')
        elif len(items)==2:
            ext = f.pop(-1)
            filename = f.pop(-1)
            directory = '/'.join(f)+'/'
            return (directory,filename,'.'+ext)

    def chunks(self,List,amount):
        return list(DoShit()._chunking(List,amount))

    def _chunking(d_list, chunks_amount):
        '''External - Groups a list into chunks'''
        for i in range(0, len(d_list), chunks_amount):
            yield d_list[i:i + chunks_amount]
        return d_list

    def _chunking(self, d_list, chunks_amount):
        '''Internal - Groups a list into chunks'''
        if len(d_list) > 0 and chunks_amount != 0:
            for i in range(0, len(d_list), chunks_amount):
                yield d_list[i:i + chunks_amount]
            return d_list

    def join_chunks(self, chunks=[], join=' '):
        '''Joins each list within a list of chunks.'''
        if len(chunks) > 0:
            sl = []
            for x in chunks:
                H = str(join).join(x)
                sl.append(H)
            if len(sl) > 0:
                return sl

    def clean_files(self,file_list:list):
        filenames=[]
        for url in file_list:
            brokenurl=self.break_fileurl(url)
            if '.' in brokenurl[-1]:
                filename=''.join(brokenurl[-2:])
            else:
                filename='/'.join(brokenurl[-2:])
            filenames.append(filename)
        return filenames

    def merge_lists(self, li=[[]]):
        '''Merges a list of lists or a list of chunks into a single list'''
        if len(li) > 0:
            nl = []
            for x in li:
                nl.extend(x)
            if len(nl) > 0:
                return nl

    def prod(iterable):
        '''Returns the product of the items in a given list.'''
        from functools import reduce
        import operator
        return reduce(operator.mul, iterable, 1)

    def file_exist(filename):
        '''Check if a file exists'''
        if filename:
            if os.path.exists(filename):
                return True
            else:
                return False

    def array_count(self, searchfor, array):
        '''Searches a list for an element and counts how many times it occurs within the list.'''
        count = 0
        for e in array:
            if e == searchfor:
                count = count +1
        return count

    def readfrom(self, filename, mode=0, no_n=False,encoding='utf-8',readmode='r',cont=1):
        '''Either reads the entire file or returns a list of lines,
            mode = 1: returns a list of lines 
            mode = 0: returns the entire file
            no_n = 1: removes the "\n" from the end of all lines
            encoding: declare which encoding to use when reading a file
            readmode: can either be r for normal reading or rb reading a file bitwise
            cont = whether the file should ignore the end of a file based on a null or keep reading after the file has ended'''
        if os.path.exists(filename):
            if readmode=='rb':
                encoding=None
            f = open(filename, readmode,encoding=encoding)
            if mode == 1:
                R = []
                size = os.path.getsize(filename)
                for a in range(size):
                    e = f.readline()
#                    print(e)
                    if no_n:
                        e = e.replace('\n', '')
                    if not cont:
                        if e:
                            R.append(e)
                        else:
                            break
                    else:
                        R.append(e)
                return R
            elif mode == 0:
                e = f.read()
                return e

    def multi_replace(self, string, remove_items=(',', ' ', '\n'), replacement='', str_break='\n'):
        '''Replaces all instances of of a single or list of items with a single replacement.
        Also breaks up a string if it is being held together by the '\n' charachter.'''
        #try:
        string = str(string)
        if str_break in string:
            string = string.split(str_break)
            f = ''.join(str(x) for x in string)
       #         pass
        elif str_break not in string:
            f = string
      #          pass
     #   except TypeError:
     #       pass
        if type(remove_items) is list or type(remove_items) is tuple and len(remove_items) > 1:
            for mark in remove_items:
                if mark in f:
                    f = f.replace(mark, replacement)
        elif type(remove_items) is list or type(remove_items) is tuple and len(remove_items) == 1:
            if remove_items[0] in f:
                f = f.replace(remove_items[0], replacement)
        elif type(remove_items) is str:
            if remove_items in f:
                f = f.replace(remove_items, replacement)
        return f

    def dup_del(self, array):
        '''Delete all duplicates of from a list'''
        myarray = list(dict.fromkeys(array))
        return myarray

    def make_dir(self, dir_name=None, c=True):
        '''Creates a new folder within the current directory or a specified directory'''
        if c == False and dir_name:
            os.mkdir(dir_name)
        elif c and dir_name:
            if DoShit.current_dir() not in str(dir_name):
                dd=str(DoShit.current_dir())+'/'+str(dir_name)
            else:
                dd=str(dir_name)
            print('Are you sure you want to create a directory in {}?'.format(dd))
            input('Continue?')
            os.mkdir(dd)
        
    def current_dir(self):
        '''Returns the current working directory of the current file'''
        filename = str(os.path.dirname(os.path.realpath(__file__)))
        return filename
    
    def current_dir():
        '''Returns the current working directory of the current file'''
        filename = str(os.path.dirname(os.path.realpath(__file__)))
        return filename

    def writeto(self, filename, content, mode='w', m=0, mute=False):
        '''Writes content to a file, 
            m defines whether to use the current folder, 
            and mute defines whether to return confirmation.'''
        if mute == False:
            print('writing to file...')
        if m == 0:
            if os.path.dirname(os.path.realpath(__file__)) not in filename:
                filename = str(os.path.dirname(os.path.realpath(__file__))) + str('\\' + filename)
        elif m == 1:
            filename = filename
        if os.path.exists(filename):
            f = open(filename, mode)
            f.write(content)
            if mode != 'wb':
                if mode == 'a' or 'a+' and content not in (None, ''):
                    f.write('\n')
            f.close()
        elif os.path.exists(filename) == False:
            f = open(filename, mode)
            f.write(content)
            if mode == 'a':
                f.write('\n')
            f.close()

    def random_selection(self, list_or_tuple=[None, None, None]):
        '''Returns a random selection from a specified list'''
        length = len(list_or_tuple)
        l = random.randint(0, (length-1))
        selection = list_or_tuple[l]
        return selection

    def array_search(self, search, array):
        '''Counts the instances within an array.'''
        count = 0
        for e in array:
            if e == search:
                count = count +1
        return count
    
    def cat(self,values):
        return DoShit.get_dict(list(range(len(values))),values)

    def hashbrick_SHA3512(self, string):
        '''Take a string and hash each letter.'''
        met = Metatron()
        return ''.join([met.hash3_512(x) for x in str(string)])
    
    def hash1(self, string):
        '''Basic sha256 hash algorithm'''
        try:
            string = str(string)
            post = hashlib.sha1(string.encode())
            pos = post.hexdigest()
            return pos
        except ValueError:
            print('Argument Must Be A String!')

    def mismatch(self,ListA, ListB):
        '''Take listA and listB and find the differences between both.
            returns (items in A missing from B, items in B missing from A)'''
        u=(set(ListA).difference(ListB))    #missing
        v=(set(ListB).difference(ListA))    #additional
        return (u,v)



class Timer:
    def __init__(self, default_time=0, time_increment=1, reset_interval=None):
        self.time_increment=time_increment
        self.default_time=int(default_time)
        self.save_timer = self.default_time
        self.reset_interval = reset_interval
       # if type(self.reset_interval) in (int, float) and self.save_timer == self.reset_interval:
       #     self.reset_timer()

    def count_up(self):
        if type(self.time_increment) in (int, float):
            self.save_timer+=self.time_increment

    def count_down(self):
        if type(self.time_increment) in (int, float):
            self.save_timer-=self.time_increment
            
    def timed_execution(self, function, execution_interval=None):
        if execution_interval == None:
            execution_interval = self.reset_interval
        if self.save_timer == execution_interval:
            print(self.save_timer)
            f = function()
            self.reset_timer()
        else:
            pass

    def reset_timer(self):
        self.save_timer=self.default_time
        return self.save_timer

import keyboard
import hashlib

class Metatron:
    '''A multitool for generating and building random bits'''
    def __init__(self):
        self.outer_states = []
        self.rail_entity = []
        self.orbitals = []
        self.is_active = 0
        self.DELTA_ORBITAL = []
        self.MEMORY_BLOCK = []
        self.MEMORY_BLOCK_DB = {}
        self.randomize = False
        self.NUMERIC = '0123456789'
        self.ALPHANUMERIC = list('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
        self.ALPHA = list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
        self.BASE58 = list('123456789abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ')

    def hashbrick_SHA3512(string):
        '''Take a string and hash each letter.'''
        return ''.join([met.hash3_512(x) for x in str(string)])

        #specify a list(range()) to create custom id to identify custom objects
    def generate_weights(self, min=-1000000, max=1000000):
        '''Generate random weights between a min and max, return a tuple(low, mean, high)'''
        ints = [random.randint(min, max), random.randint(min, max)]
        ints.sort()
        weight_low = ints[0]
        weight_high = ints[1]
        weight_average = (ints[0]+ints[1])/2
        weights = [weight_low, weight_average, weight_high]
        return weights

    def true_random(self,alist):
        '''Returns a truely random number loosely based on chaos theory.'''
        E = None
        pg = p_geometry()
        while not E:
            E = self.entity(pg.random_selection(alist), pg.random_selection(alist), pg.random_selection(alist), f_one_frequency=range(0, 100), f_two_frequency=range(200,400), f_three_frequency=range(600, 800))
        return E
    
    def entity(self, function_one=None, function_two=None, function_three=None, f_one_frequency=range(0, 25), f_two_frequency=range(26, 51), f_three_frequency=range(52, 76), convert_iterable_to_string=True):
        '''Returns a function result based on a frequency of availability'''
        weight_range = []
        if f_one_frequency:
            weight_range.extend(list(f_one_frequency))
        if f_two_frequency:
            weight_range.extend(list(f_two_frequency))
        if f_three_frequency:
            weight_range.extend(list(f_three_frequency))
        weight_range.sort()
        maximum = len(weight_range)
        minimum = weight_range[0]
        entity_id = self.generate_weights((-maximum), maximum)
        found = False

        if entity_id[1] in list(f_one_frequency) and function_one:
#            print('F1:')
            a = function_one
            #print(a)
            found= True
            if convert_iterable_to_string == True:
                a = self.iter_to_string(a)
            return (a, entity_id)
        if entity_id[1] in list(f_two_frequency) and function_two:
#            print('F2:')
            b = function_two
            #print(b)
            found= True
            if convert_iterable_to_string == True:
                b = self.iter_to_string(b)
            return (b, entity_id) 
        if entity_id[1] in list(f_three_frequency) and function_three:
#            print('F3:')
            c = function_three
            #print(c)
            found= True
            if convert_iterable_to_string == True:
                c = self.iter_to_string(c)
            return (c, entity_id)

    def _randomized_targeting(self):
        '''Returns/Updates random target index in which to add data'''
        loaf_size = len(self.orbitals)
        #print(loaf_size)
        if loaf_size:
            active = random.randint(0, (loaf_size-1))
            self.is_active = int(active)
            return self.is_active
   
    def hash76(self, length=36, orbital_amount=1, CIPHER=None):
        '''Simple way of returning hashes/data blocks based on a cipher'''
        if CIPHER == None:
            CIPHER = self.ALPHANUMERIC
      
        if length <= 62:
            m = Metatron()
            s = m.s_ultra(length=length, orbital_amount=orbital_amount, BLOCK_VALUES_ONE=CIPHER, BLOCK_VALUES_TWO=CIPHER, BLOCK_VALUES_THREE=CIPHER)
        elif length > 62:
            m = Metatron()
            #orbital amount = how many times does 64 go into the desired length
            ext_need = length % 62
            extra_strings = int(ext_need / 62)+1
            #print(ext_need)
            #print(extra_strings)
           # if ext_need > 64:
           #     string
          #  for x in range(0, strings):
            temp = m.s_ultra(length=int(length/2), orbital_amount=(orbital_amount*2+extra_strings*2))
            tool = temp.pop(-1)
            tool = tool+temp.pop(-1)+temp.pop(-1)
            #print(tool)
            q = []
            for x in temp:
                e = temp.pop(-1)
                q.append((x+e+tool[:ext_need])[:length-1])
                tool = tool[::-1][1:]
            #    print(tool)
        
    #        for b in q:
   #             #print(len(b))
  #              print(b)
 #           print(len(q[-1]), len(q))
            s = q
            #temp = m.s_ultra(length=64, )
        return s 

    def s_ultra(self, length=36, orbital_amount=1, BLOCK_VALUES_ONE=['1', '2', '3', '4' '5', '6','7','8','9','0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'], BLOCK_VALUES_TWO=['1','2','3','4','5','6','7','8','9','0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'], BLOCK_VALUES_THREE=['1','2','3','4','5','6','7','8','9','0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']):
        '''Slim automated version of generate memory block'''
        u = p_geometry()
        self.new_orbit(orbital_amount)
        f = None
        exclude_from_results=['[',']','\'','\"',',',' ']
        memorize_interval = length
        forget_interval = length+1
        while len(self.MEMORY_BLOCK) < orbital_amount:
            self._randomized_targeting()
            if f == None:
                f = self.entity(u.random_selection(BLOCK_VALUES_ONE), u.random_selection(BLOCK_VALUES_TWO), u.random_selection(BLOCK_VALUES_THREE), range(0, 100), range(101, 140), range(141, 160))
            if f:
               # print('Orbital', self.is_active, 'is currently active')
                if f[0] and len(self.orbitals) > 0:
                    if f[0] not in self.orbitals[self.is_active]:
                        self.new_orbit_item(orbital_item=f[0])
                        self.manifest_item_in_orbital(orbital_item=f[0], manifestion=f[1])
                        f = None
                elif len(self.orbitals) == 0:
                    return
                
                self.manage_orbit_memory(memorize_interval, forget_interval, memory_polish=exclude_from_results)
               # print({'premature':self.orbitals, 'matured':self.MEMORY_BLOCK, 'archived':self.MEMORY_BLOCK_DB})
                f = None
               # print(self.orbitals)
        if len(self.MEMORY_BLOCK)==orbital_amount:
            return self.MEMORY_BLOCK
        #return f

    def iter_to_string(self, iterable):
        '''Removes [] or () from a list or tuple. - Slightly redundant probably should be deleted but also why not keep it lol'''
        if type(iterable) in (list, tuple, dict):
            print(iterable)
            if type(iterable) == list: 
                a = str(iterable).strip('[]')
            elif type(iterable) == tuple:
                a = str(iterable).strip('()')
            return a
        else:
            return iterable
    
    def _new_orbit(self):
        '''Adds a new orbit/dictionary to the class orbital list'''
        O = {}
        self.orbitals.append(O)
       # print('new orbit added')

    def new_orbit(self, oa=None):
        '''Used in generate_new_block() to ask the user how many orbitals/dictionaries should be added to the orbitals list'''
        try:
            if oa == None:
                oa = int(input('Add orbit amount:'))
            for a in range(0, oa):
                self._new_orbit()
        except ValueError:
            print('Amount must be an integer')
            input()
            pass

    def new_orbit_item(self, orbital_position=None, orbital_item=None, orbit_entity=[]):
        '''Adds an orbit_entity/item to an orbital_item at a certain orbital_postion'''
        if orbital_position == None:
            orbital_position = self.is_active
        if orbital_item !=None and orbital_position !=None:
            self.orbitals[orbital_position][orbital_item]=orbit_entity

    def set_active(self):
        '''Ask the user which orbital should be set as active.'''
        try:
            oa = int(input('Select active orbit: '))
            omax = len(self.orbitals)
            if oa in list(range(0, omax)):
                self.is_active = oa
            else:
                print('Not a valid orbital')
                pass
        except ValueError:
            print('Orbital ID must be an integer')
            input()
            pass

    def hash256(self, string):
        '''Basic sha256 hash algorithm'''
        try:
            string = str(string)
            post = hashlib.sha256(string.encode())
            pos = post.hexdigest()
            return pos
        except ValueError:
            print('Argument Must Be A String!')

    def hash1(self, string):
        '''Basic sha256 hash algorithm'''
        try:
            string = str(string)
            post = hashlib.sha1(string.encode())
            pos = post.hexdigest()
            return pos
        except ValueError:
            print('Argument Must Be A String!')

    def seed_hash(self, seed, amount=16):
        ds = DoShit()
        leng = len(seed)
        u = list(ds._chunking(seed, int(leng/amount)))
        li = []
        for x in u:
            s = self.hash3_512(x)
            li.append(s)
        return li

    def hash3_512(self, string):
        '''Basic sha3_512 hash algorithm'''
        try:
            string = str(string)
            post = hashlib.sha3_512(string.encode())
            pos = post.hexdigest()
            return pos
        except ValueError:
            print('Argument Must Be A String!')

    def set_orbital_target_mode(self):
        '''Switches the orbital trargening mode to random or chronological'''
        if keyboard.is_pressed('R'):
            self.randomize = True
            return self.randomize
        if keyboard.is_pressed('shift+R'):
            self.randomize = False
            return self.randomize
        if self.randomize == True:
            #print('randomize active')
            loaf_size = len(self.orbitals)
            #print(loaf_size)
            if loaf_size:
                active = random.randint(0, (loaf_size-1))
                self.is_active = int(active)
                return self.is_active
        elif self.randomize == False:
            if keyboard.is_pressed('shift+Q'):
                self.set_active()
                return self.set_active()

    def move_memory_block_to_db(self):
        '''Moves the memory block to the memory block db/dictionary'''
        name = self.generate_weights()
        name = str(name[1])
        if name not in self.MEMORY_BLOCK_DB and len(self.MEMORY_BLOCK) > 0:
            self.MEMORY_BLOCK_DB[name]=self.MEMORY_BLOCK
            self.MEMORY_BLOCK = []    
            print('MEMORY BLOCK RESERVED')
            pass
        else:
            pass

    def write_array_to_file(self, array, save_interval_in_seconds):
        '''Writes an array to file'''
        ds = DoShit()
        u = p_geometry()
        filenames = list(range(0, save_interval_in_seconds))
        filename = u.random_selection(filenames)
        #filename = self.hash256(hashable_name)
        writable = json.dumps(array)
        ds.writeto(str(str(filename)+'.txt'), writable)

    def loop_switch(self, true_false_variable, key_switch='space', break_switch='esc'):
        '''Handles generate_block() function on off switch'''
        if keyboard.is_pressed(key_switch):
            true_false_variable = True
            return True
        elif keyboard.is_pressed(str('shift+'+key_switch)):
            true_false_variable = False
            return False
        #else:
        #    return True
        #if keyboard.is_pressed(break_switch):
         #   break
        #    return break

    def generate_memory_block(self, memorize_interval=52, forget_interval=53, save_interval_in_seconds=100000, BLOCK_VALUES_ONE=['1', '2', '3', '4' '5', '6','7','8','9','0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'], BLOCK_VALUES_TWO=['1','2','3','4','5','6','7','8','9','0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'], BLOCK_VALUES_THREE=['1','2','3','4','5','6','7','8','9','0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'],exclude_from_results=['[',']','\'','\"',',',' ']):
        '''Mechanical interface version for generating and building blocks of data'''
        self._new_orbit()
        u = p_geometry()
        running = True
        RUNNING = True
        if forget_interval in ('0',None):
            forget_interval = memory_interval+1
        timer = Timer(reset_interval=10000)            
        while RUNNING:
            running = self.loop_switch(running)
            if keyboard.is_pressed('esc'):
                RUNNING = False
            if running:
                timer.count_up()
                if len(self.MEMORY_BLOCK) > 0 and timer.save_timer == timer.reset_interval:
                    self.write_array_to_file(self.MEMORY_BLOCK, save_interval_in_seconds)
                    timer.reset_timer()
                self.set_orbital_target_mode()

                if keyboard.is_pressed('N'):
                    self.new_orbit()
                if keyboard.is_pressed('S'):
                    self.move_memory_block_to_db()
                else: pass

                f = self.entity(u.random_selection(BLOCK_VALUES_ONE), u.random_selection(BLOCK_VALUES_TWO), u.random_selection(BLOCK_VALUES_THREE), range(0, 100), range(101, 140), range(141, 160))
                if f:
            #        HASHED_NAME = self.hash256(f[0])
                    print('Orbital', self.is_active, 'is currently active')
                    if f[0] and len(self.orbitals) > 0:
                        if f[0] not in self.orbitals[self.is_active]:
                            self.new_orbit_item(orbital_item=f[0])
                            self.manifest_item_in_orbital(orbital_item=f[0], manifestion=f[1])
                    elif len(self.orbitals) == 0:
                        print('NO ORBITALS FOUND TRY ADDING AN ORBITAL BY PRESSING "N"')
                        print('Current block in focus:', self.MEMORY_BLOCK) 
                        print('Press "S" to save the current memory block')
                    
                    self.manage_orbit_memory(memorize_interval, forget_interval, memory_polish=exclude_from_results)
                self.generate_pipe()
        return {'premature':self.orbitals, 'matured':self.MEMORY_BLOCK, 'archived':self.MEMORY_BLOCK_DB}

    def manifest_item_in_orbital(self, orbital_position=None, orbital_item=None, manifestion=None):
        '''Adds an item to a positino/orbit with a corresponding orbital key'''
        if orbital_position == None:
            orbital_position = self.is_active
        if orbital_item !=None and manifestion != None:
            self.orbitals[orbital_position][orbital_item].append(manifestion)
        
    def manage_orbit_memory(self, memorize_interval=None, forget_interval=None, orbitals=None, memory_polish=[]):
        '''Moves the filled orbital to the memory block, once if the memorize interval is reached. Also removes any specified chars from memory_polish
            from the result.'''
        if memorize_interval != None:
            self.memorize_interval = memorize_interval
        if forget_interval != None:
            self.forget_interval = forget_interval
        if orbitals != None and type(orbitals) == list:
            self.orbitals = orbitals
        for orbit in self.orbitals:
            count = len(orbit)
            if count == self.memorize_interval:
               # print('Orbit memorized')
                self._orbit_to_memory(orbit, memory_polish)
                count = 0
                #print(self.MEMORY_BLOCK)
            if count == self.forget_interval:
                self.orbitals.remove(orbit)
                count = 0

    def _orbit_to_memory(self, orbit, exclude_from_results):
        '''Removes chars from results and moves them to the memory block db'''
        ds = DoShit()
        blank = []
        mem_block = blank.copy()
        mem_block.extend(orbit) 
        test = str(mem_block)
        ftest = ds.multi_replace(test, exclude_from_results)
        if ftest not in self.MEMORY_BLOCK:
            self.MEMORY_BLOCK.append(str(ftest))                            
            #print(self.MEMORY_BLOCK)

    def retrieve_block(self, block_type):
        '''Return the list of blocks base on specified block type. '''
        if block_type in ('orbitals', 'ORBITALS', 'ORB', 'orb'):
            return self.orbitals
        elif block_type in ('memory_block', 'MEMORY_BLOCK', 'MEMORYBLOCK'):
            return self.MEMORY_BLOCK
        elif block_type in ('mem_db', 'db', 'memory_block_db', 'MEMORY_BLOCK_DB', 'MEM_BLOCK_DB'):
            return self.MEMORY_BLOCK_DB
        else:
            pass

    def clear_block(self, block_type):
        '''Resets or deletes the contents of a specified block type'''
        if block_type in ('orbitals', 'ORBITALS','ORB', 'orb','orbital', self.orbitals):
            self.orbitals=[]
        elif block_type in ('mem_block','memory_block', 'MEMORY_BLOCK','MEMORYBLOCK', self.MEMORY_BLOCK):
            self.MEMORY_BLOCK=[]
        elif block_type in ('mem_db', 'db', 'memory_block_db','MEMORY_BLOCK_DB','MEM_BLOCK_DB', self.MEMORY_BLOCK_DB):
            self.MEMORY_BLOCK_DB={}
        else:
            pass

    def generate_zip_key(self,string='3001'):
        pass

    def rename_dict_item(self,original_dict, original_dict_key, new_key=None):
        '''Replaces a dictionary key and value with a new key and value.'''
        if type(new_key) == str and type(original_dict_key)==str:
            original_dict[str(new_key)] = original_dict[str(original_dict_key)]
            original_dict.remove(original_dict_key)
        return original_dict

    def generate_pipe(self):
        '''Takes a char from Memory block and adds the key to a list/rail.'''
        db = self.retrieve_block('mem_db')
        if db:
            if len(db) >= 2:
                rail_info = []
                print('Valid rail')
                for k,v in db.items():
                    rail_info.append(k)
                    #print(k,v)
                return rail_info

            
class Rail_Entity(Metatron):
    '''Spawns traversable rail that can be used to store data and connect dictionary keys/nodes.'''
    def __init__(self):
        Metatron.__init__(self)
        self.generate_memory_block()
        self.MEMORY_BLOCK_DB = self.MEMORY_BLOCK_DB
        self.rail = self.generate_pipe()
        self._entity_location = 0
        self.previous_entity_location=self._entity_location-1
        self.future_entity_location=self._entity_location+1
        self._rail_orientation=0
        
        if self.rail:
            if len(self.rail) >= 2:
                self.rail_sides = self.retrieve_block('mem_db')
                self.rail_length = len(self.rail)
        self.front = [';']
        self.back = [':']
        self.rail_selector='1003'
        self.rail_entity = None
        self.elect_rail_entity(self.rail_selector)
        self.rail_view = []

    def elect_rail_entity(self, char=None, front_selector=':', rear_selector=';'):
        '''Assigns a char as the selector rider'''
        if char == None:
            ch = input('Enter a character or string to use as a rail entity')
            char = str(ch)
            self.rail_selector = char
        self.rail_selector = char
        self.front[0] = front_selector
        self.back[0] = rear_selector
        self.rail_entity = str(rear_selector+char+front_selector)
        return self.rail_entity
    
    def rail_go_forward(self):
        '''Progress the rail entity forward'''
        if self.rail_length >= 2 and self.rail_entity != None:
            f = self.rail.copy()
            self._entity_location= self._entity_location +1

            if self._entity_location >= self.rail_length:
                self._entity_location = self.rail_length
            if self._entity_location <= 0:
                self._entity_location = 0
           
            if self.rail_entity in self.rail_view and self._entity_location >= 0:
           #    print(self._entity_location)
                if type(self._entity_location) == int:
                    f.insert(self._entity_location, self.rail_entity)
                    self.rail_view=f
       
            if self.rail_entity not in self.rail_view:
                f.insert(0, self.rail_entity)
                self.rail_view=f

    def rail_go_backwards(self):
        '''Regress the rail entity backwards.'''
        f = self.rail.copy()
        self._entity_location= self._entity_location -1

        if self._entity_location >= self.rail_length:
            self._entity_location = self.rail_length
        if self._entity_location <= 0:
            self._entity_location = 0
           
        if self.rail_entity in self.rail_view and self._entity_location >= 0:
            #    print(self._entity_location)
            if type(self._entity_location) == int:
                f.insert(self._entity_location, self.rail_entity)
                self.rail_view=f
            #NEED TO FIX MOVING FORWARD AND BACKWARD
             
            if self.rail_entity not in self.rail_view:
                f.insert(0, self.rail_entity)
                self.rail_view=f

    def _update_rail_view(self):
        '''Used to control rail entity movement by using input()'''
        if self.rail_length >= 2 and self.rail_entity != None:
            print(self.rail_view)
            movement = input('Type "D" to move forward or "A" to move backwards')
            if movement in ('D', 'd'):
                self.rail_go_forward()
            if movement in ('A','a'):
                self.rail_go_backwards()
    
    def _update_rail_entity(self):
        pass

    def get_front_neighbor(self):
        '''Print the front neighbor to the rail entity'''
        if self.rail_entity in self.rail_view:
            front_neighbor_index = int(self.rail_view.index(self.rail_entity))+1
            print(front_neighbor_index)

    def get_rear_neighbor(self):
        '''Print the rear neighbor to the rail entity.'''
        if self.rail_entity in self.rail_view:
            front_neighbor_index = int(self.rail_view.index(self.rail_entity))-1
            print(front_neighbor_index)

class Data_Weight:
    '''Class for converting string data into binary, and finding similarly wieghted strings.'''
    def __init__(self):
        self.mouth = []

    def intake(self, data):
        '''Returns the sum of all binaries  of the data string added together 
        - used to flatten a string into a an integer - wrapper for internal definition from_value(data)'''
        e = self.from_value(data)
        return e

    def inv_bin(self, string):
        '''Takes a binary string and inverts the bits. 1s become 0s and 0s become 1s'''
        string = str(string)
        new_string = ''
        for x in string:
            if x == 1 or x == '1':
                new_string = new_string+'0'
            elif x == 0 or x == '0':
                new_string = new_string+'1'
            elif x == ' ':
                new_string = new_string+x
        return new_string

    def from_value(self, data):
        '''Takes a string of data and converts it to binary,
        puts each char binary in a list then adds them all together.
        sum of all binaries added together - used to flatten a string into a an integer'''
        q = self.strtobin(data)
        q = q.split()
        s = 0 
        for x in q:
            for y in x:
                s = s+int(y)        
        return s

    def strtobin(self, string, is_list=False):
        '''Converts a string to binary.'''
        if type(string) == str:
            e = ' '.join(format(ord(x), 'b') for x in string)
            return e
        elif type(string) in (int, float):
            string = str(int(string))
            e = ' '.join(format(ord(x), 'b') for x in string)
            return e
        elif type(string) in (list, tuple):
            ds = DoShit()
            try:
                string = str(sum(string))
            except TypeError:
                string = str(string)
            string = ds.multi_replace(string, remove_items=[',','[',']','(',')', '\'', ' '])
            print(string)
            e = ' '.join(format(ord(x), 'b') for x in string)
            return e

    def d_intake_proto(self, string):
        '''Takes the sum of every 2 chars in a string'''
        if type(string) == (str):       #if the input string is a string
            string = list(string)       #converts into a list
            copy = string.copy()        #copys the list
            copy.pop(0)                 #removes the first item from the copy
            print(string)               #prints the original list
            c = len(string)             #gets the length of the original list minus the first item
            n = []                      #declare a new empty list
            for x in string:            #for item(x) in the list
                for y in copy:          #and for item(y) in the list
                    n.append(x+y)       #append item x and item y together and append the list
                    copy.pop(0)         #removes the first item from the copy
                    break               #break each time only iterat teh first object then remove 
            #for x, y in zip(string, )
            p = []                      #declare new list
            for f in n:                 #for item(f) in n get the weghted value of each pair in n
                f = self.intake(f)      
                p.append(f)             #append the 
            print(p)
            return p
    
    def hash76(self, length=52, sample_size=1):
        '''Simple way of returning hashes/data blocks based on a cipher'''
        m = Metatron()
        r = m.s_ultra(length=length, orbital_amount=sample_size)
        return r

    def hash1(self, string):
        '''Basic sha256 hash algorithm'''
        try:
            string = str(string)
            post = hashlib.sha1(string.encode())
            pos = post.hexdigest()
            return pos
        except ValueError:
            print('Argument Must Be A String!')


    def like(self, my_weight=None, max_length=60, sample_size=10, min_length=0, s=False):
        '''Find a like charachter or string based on the binary sum of a number'''
        j=max_length
        T = []
        e = min_length
        if my_weight:
            while e != (my_weight):
                ff = self.hash76(length=j, sample_size=sample_size)
                if ff:
                    for m in ff:
                        e = self.intake(m)
                        if e == (my_weight):
                            T.append(m)
                        elif s and e <= my_weight:
                            T.append(m)
                    #print(ff)
                if j==0 or j == min_length:
                    break
                if s and len(T) >= sample_size:
                    break
                j-=1
            return T  

    def like_strings(self, string, length=None, sample_size=25, s=False, mute=False):
        '''Find similar strings from another string or char'''
        if length == None:
            length = len(string)
        my_weight = self.intake(str(string))
        if mute == False:
            print('We want to find this wieght: ', my_weight)
        T = []
        while len(T) < sample_size:
            ff = self.like(my_weight, max_length=length, sample_size=sample_size, s=s)
            #print(ff)
            T.extend(ff)
        if mute == False:
            print('done')
        if len(T)>0:
            return T
        else:
            print('A like string could not be found, try breaking your input up into parts. Strings less than 64 bits tend to work most efficiently')
            return 

class CheckSum():
    def __init__(self):
        self.layer=[]
        self.checksum=None
        self.errors={}
    def index(self, i):
        '''Return a recorded value using index.'''
        return self.layer[i]

    def cpass(self):
        '''Check True'''
        self.layer.append(1)
    def check(self):
        self.cpass()
    def cfail(self):
        '''Check False'''
        self.layer.append(0)
    def fail(self):
        self.cfail()
    def result(self):
        return self.cum()
    def cum(self):
        '''Give the average checksum, less accurate 0-1 most accurate'''
        t=len(self.layer)
        o=sum(self.layer)
        self.checksum=float(o/t)
        return self.checksum

    def check_errors(self):
        c=0
        errors=[]
        for x in self.layer:
            if x ==0:
                errors.append(c)
            c+=1
        self.errors=errors
        return self.errors
        
    def check_param_equalto(self, param1,param2):
        if param1 == param2:
            self.cpass()
        else:
            self.cfail()

#m = Metatron()
class Abalam:
    def listen():
        f = input()
        print('Input:', f)
        return f
    
    def import_memory():
        pass

class Cabris:
    def __init__(self, word_memory=None):
        if word_memory ==  None:
            self.phrases = {'subjects':{},
                            'predicates':{},
                            'greetings':{'coming':{},'going':{}},
                            'objects':{},
                            'verbs':{},
                            'who':{},
                            'what':{},
                            'when':{},
                            'where':{},
                            'how':{},
                            'pronouns':{},
                            'approval':{},
                            'disapporval':{},
                            'adjectives':{}
                            }
            self._all = (self.phrases['subjects'],self.phrases['predicates'],
                        self.phrases['greetings']['coming'],self.phrases['greetings']['going'],
                        self.phrases['objects'],self.phrases['verbs'],self.phrases['who'],
                        self.phrases['what'],self.phrases['when'],self.phrases['where'],
                        self.phrases['how'],self.phrases['pronouns'],self.phrases['adjectives'])
           
            self.common_pairs = []
        self.alive = True
        while self.alive:
            e = Abalam.listen()
            if e:
                self.comprehend(e)
            if keyboard.is_pressed('Q'):
                break

    def comprehend(self, phrase):
        s = str(phrase).split()
        print(self._all)
        print(s)
        if s not in [e for e in self._all]:
            print('Im not understanding.')
            define = input('What does '+phrase+' mean? Reply: ')
            print()
            if define:
                f = input('So '+phrase+', '+define+' ')
                if f in ('YES','yes') or define in 'YES' or define in 'yes':
                    for x in self.phrases:
                        print(x)
                    phrase_type = input('What type of phrase is this? ')
                    if self._valid_phrase_type(phrase_type):
                        if self._is_greeting(phrase_type, phrase, define):
                            return
                        elif self._is_greeting(phrase_type, phrase, define) == False:
                            self.add_phrase(phrase_type, str(phrase), define)
                            print('continue to learning phase')
                            return

    def _valid_phrase_type(self, phrase_type):
        if phrase_type in self.phrases:
            return True
        else:
            return False

    def add_phrase(self, phrase_type, phrase, definition):
        if phrase not in self.phrases[phrase_type]:
            self.phrases[phrase_type][str(phrase)]=[]
            self.phrases[phrase_type][str(phrase)].append(str(definition))
            print('New phrase and definition added')
            print(self.phrases)
            return
        if phrase in self.phrases[phrase_type]:
            if self._is_greeting(phrase_type, phrase,definition):
                return
            else:
                self.phrases[phrase_type][str(phrase)].append(str(definition))
                print('New definition added')
                return

    def _is_greeting(self, phrase_type, phrase, definition):
        if phrase_type in ('greetings', 'greeting'):
            print('Greeting Types:')
            for x in self.phrases['greetings']:
                print(x)
            e = input('Is the greeting coming or going? ')
            if e in ('coming', 'COMING', 'C', 'come', 'COME', 'c'):
                if phrase not in self.phrases['greetings']['coming']:
                    self.phrases['greetings']['coming'][phrase] =[]
                    self.phrases['greetings']['coming'][phrase].append(definition)
                elif phrase in self.phrases['greetings']['coming']:
                    self.phrases['greetings']['coming'][phrase].append(definition)
            elif e in ('going', 'GOING', 'go', 'GO', 'G', 'g'):
                if phrase not in self.phrases['greetings']['going']:
                    self.phrases['greetings']['going'][phrase] =[]
                    self.phrases['greetings']['going'][phrase].append(definition)
                elif phrase in self.phrases['greetings']['going']:
                    self.phrases['greetings']['going'][phrase].append(definition)
            print(self.phrases)
            return True
        return False

#fff = c.generate_memory_block(memorize_interval=3, forget_interval=4, BLOCK_VALUES_ONE=[it[0]], BLOCK_VALUES_TWO=[it], BLOCK_VALUES_THREE=[it])
#print(fff)
#for x in it:
    
 #   print(x)
 #   combined["#"+color_code]=(rgb)
 #   x+=1

#print(pg.rgb_skin_colors)

#for colorname in pg.rgb_hair_colors:
##    print(colorname)
#   for color in pg.rgb_hair_colors[colorname]:
#        print(color)
#        print(pg.rgb_hair_colors[colorname][color])
        #print(color)
        #print(shade)

#colors = {}
#bases = ['CAUCASIAN', 'ARAB', 'INDIAN', 'AFRICAN', 'ASIAN', 'NATIVE-AMERICAN', 'HISPANIC']
#bases.sort()
#for n in bases:
#    colors[n]=[]
#for x in eee:
#    yy = ds.multi_replace(x,remove_items=['.'], replacement=' ')
#    colors[yy[:-1]]=[]
#rint(colors)

#olors = json.dumps(new)
#ds.writeto('eye_color_test.json', colors, 'w')

#fff = c.generate_memory_block(memorize_interval=3, forget_interval=4, BLOCK_VALUES_ONE=['CAUCASIAN.', 'ARAB.','INDIAN.', 'AFRICA.', 'NATIVE-AMERICAN.', 'HISPANIC.'], BLOCK_VALUES_TWO=['CAUCASIAN.', 'ARAB.','INDIAN.', 'AFRICA.', 'NATIVE-AMERICAN.', 'HISPANIC.'], BLOCK_VALUES_THREE=['CAUCASIAN.', 'ARAB.','INDIAN.', 'AFRICA.', 'NATIVE-AMERICAN.', 'HISPANIC.'])
#print(fff)

#ffu = Rail_Entity()
#ffu.elect_rail_entity('HARRY')
#print('Raw Rail',ffu.rail)
#print('Rail Sides',ffu.rail_sides)
#print('Rail Entity',ffu.rail_entity)
#hh = input()
#if hh in 'go':
#    while True:
#        if keyboard.is_pressed('A'):
#            ffu.get_front_neighbor()
#            ffu._update_rail_view()

#        if keyboard.is_pressed('D'):
#            ffu.get_rear_neighbor()
#            ffu._update_rail_view()
        
#        if keyboard.is_pressed('esc'):
#            break
#print('This is me retrieving the block', ffu.retrieve_block('mem_db'))
#input()
def custom(outputs):
    fun_output = []
    for f in outputs:
        s = f.split('#')
        for p in s:
            p = '#'+str(p)
            fun_output.append(p)
    return fun_output

def complete():
    files = ['C:\\Users\\Caelum\\Downloads\\Zabadoo_SkintonesandSpeadsheet\\Hex_Pamphlet_0.csv','C:\\Users\\Caelum\\Downloads\\Zabadoo_SkintonesandSpeadsheet\\Hex_Pamphlet_1.csv','C:\\Users\\Caelum\\Downloads\\Zabadoo_SkintonesandSpeadsheet\\Hex_Pamphlet_2.csv','C:\\Users\\Caelum\\Downloads\\Zabadoo_SkintonesandSpeadsheet\\Hex_Pamphlet_3.csv','C:\\Users\\Caelum\\Downloads\\Zabadoo_SkintonesandSpeadsheet\\Hex_Pamphlet_4.csv','C:\\Users\\Caelum\\Downloads\\Zabadoo_SkintonesandSpeadsheet\\Hex_Pamphlet_5.csv','C:\\Users\\Caelum\\Downloads\\Zabadoo_SkintonesandSpeadsheet\\Hex_Pamphlet_6.csv']
    e = DoShit()
    outputs = []
    for fi in files:
        u = e.readfrom(fi, 0)
        if u:
            d = e.multi_replace(u)
            outputs.append(d)
    ou = e.dup_del(custom(outputs)) 
    ou.pop(0)
    mew = json.dumps(ou)
    mew2 = e.writeto('skin_color_palette.json',mew)
