from NN import p_geometry, Metatron
from arcade import color
from arcade import SpriteList, Sprite
import sys

sys.path.append(sys.path[0]+'\\Custom Releases')
from inventory import Inventory
"""
Description:
Classes: Skin() - Generates a skin object
         Tooth() - Generates a tooth object
         Organs() - Generates templates/dictionaries for organ infromation. 
                    Also contains possible organ disease names
         GENERATE_MAN() - Contains all organ information and internal health monitor.
                    Subclass for Private_Human()
         ALPHA_MAN() - Generates a detailed person template with organ, body, face, mood, health info, interactions, inventory
                            No sprite information however

PAGE FOR CREATING OBJECTS AND GENERATING NPC AND PLAYER PROPERTIES
AND ASSIGNING THEM TO THE GAME WORLD
"""
r = p_geometry()
data = Metatron()

psychological_disorders = {'Neurodevelopmental Disorders':['intellectual disability'],
                        'Communication Disorders':['suttering',
                                                  'communicatoin disorder'],
                        'Autism Spectrum Disorder':[1,2,3,4,5,6,7,8,9,10],
                        'ADHD':['impulsive',
                                'inattention'],
                        'Mania':['overconfidence','irritible', 'increase energy'],
                        'Depression':['fatigue','irritible','suicidal thoughts','guilt','sleepless','loss of interest'],
                        'Anxiety':['generalize anxiety','phobia','social anxiety','panic attacks','separation anxiety'],
                        'PTSD':['depression','anxiety'],
                        'Dissociative Disorders':['Amnesia', 'mulitple personality','depersonalization'],
                        'Eating Disorders':['Anorexia','Bulimia','Pica','Binge-Eater'],
                        'Sleep Disorders':['Narcolepsy', 'Insomnia', 'parasomnia'],
                        'Impulsive Disorders':['Kleptomania', 'Pyromania', 'Explosive Anger', 'Conduct Disorder'],
                        'Substance Disorders':['Alcohol Addiction','Cannabis Addiction', 'Inhalant Addiction','Meth addiction', 'Cocaine Addiction', 'Opiod Addiction'],
                        'Delirium':['Abnormal attention', 'Abnormal awareness','confusion'],
                        'Psychosis':['Delusions','Hallucinations','Improper Speech','Lack of motivation','Senseless behavior'],
                        'OCD':['Unusual behaviors','Unusual habits'],
                        'Personality Disorders':['Antisocial','Personality Disorder','Emotionally Unstable','dependant', 'independant','narcissistic', 'paranoia','shizoid']}

class Skin_Scar:
    def __init__(self):
        pass

class Skin:
    '''Used to decribe and interact with a skin objects - unfinished'''
    def __init__(self):
        self.average_sensitivity
        self.hyper_sensitive_areas
        self.average_tightness
        self.hydration_level
        self.average_smoothness
        self.scarring
        self.diseases
        self.open_wounds
        self.current_interaction
        pass

class Tooth:
    '''Used to decribe and interact with teeth objects - unfinished'''    
    def _init_(self):
        pass

class Organs:
    '''Holds organs and organ disease templates, details and sprites'''
    def __init__(self):
        self.brain_diseases = {'headache','alzheimers', 'huntingtons', 'epilepsy', 'parkinsons', 'stroke', 'dementia', 'cancer', 'concussion', 'high stress', 'insomnia','polio','meningitis','als','euphoria','dizziness','tumors','damage','neuropathy','polio','hemorrhage'}
        self.heart_diseases = {'arrythmia', 'failure', 'cardiac arrest', 'high blood pressure', 'low blood pressure', 'tachychardia', 'bradychardia'}
        self.lung_diseases = {'asthma', 'copd', 'cancer' ,'fibrosis', 'pneumonia', 'smokers lung', 'pulmonary embolism', 'difficulty breathing'}
        self.liver_diseases = {'fatty liver', 'hepatitis c', 'cirrhosis', 'cancer', 'failure'}
        self.stomach_diseases = {'ulcers', 'lactose intolerant', 'food poisoning', 'upset stomach', 'starvation'}
        self.intenstinal_diseases = {'uclers', 'ibs', 'hemorrhoids', 'crohns disease', 'hernia'}
        self.genital_diseases = {'hiv/aids', 'chlamydia', 'herpes simplex', 'cancer', 'gonorrhea', 'syphilis'}
        self.blood_diseases = {'aenemia', 'leukemia', 'anyuerism'}
        self.possible_organ_issues={'brain':self.brain_diseases,
                                'heart':self.heart_diseases,
                                'left_lung':self.lung_diseases,
                                'right_lung':self.lung_diseases, 
                                'liver':self.liver_diseases, 
                                'stomach':self.stomach_diseases, 
                                'intestines':self.intenstinal_diseases}

        self.possible_health_issues = {'hair':['dandruff', 'alopecia', 'mild hair loss', 'severe hair loss', 'graying', 'greyed','psoriasis'],
                                    'nerve':self.brain_diseases,
                                    'psychological':psychological_disorders,
                                    'skin':['gangrene','staphylacocus','acne','psorisis','herpes','blistering','hives','eczema','measles','melanoma','vitiligio','warts'],
                                    'teeth':[],
                                    'hearing':[],
                                    'eye':[],
                                    'muscle tissue':[],
                                    'genitals':[],
                                    'organ':self.possible_organ_issues}

        self.more_organs_blank={'brain':[],
                           'heart':[],
                           'left_lung':[],
                           'right_lung':[], 
                           'liver':[], 
                           'stomach':[], 
                           'intestines':[]}

        self.organs_blank = {'hair':[],
                            'nerve':[],
                            'psychological':[],
                            'skin':[],
                            'teeth':[],
                            'hearing':[],
                            'eye':[],
                            'muscle tissue':[],
                            'genitals':[],
                            'organ':self.more_organs_blank}

    def draw_eyes(self, iris_color, pupil_type, pupil_size, pupil_color, whites_color, origin, radius):
        whites_color= color.WHITE
        color = None
        person_template[1]['head_properties']['face']['eyes']
        pass

    def draw_hair(self):
        pass

    def draw_forehead(self):
        pass
    
    def draw_nose(self):
        pass

    def draw_mouth(self):
        pass
    
    def draw_ears(self):
        pass
    
    def draw_cheeks(self):
        pass

    def draw_jawline(self):
        canvas = soc_sim_house((256,256),8,8,None)
        jaw = canvas.canvas.custom_block((5,1),1)
        print(jaw)

    def draw_teeth(self):
        pass
    
    def draw_upperhead_canvas(self):
        pass

    def draw_lowerhead_canvas(self):
        pass
   
    def draw_body_skin(self):
        pass

    def draw_body_hair(self):
        pass

    def draw_veins_and_arteries(self):
        pass

    def draw_muscles(self):
        pass

    def draw_genitals(self):
        pass

    def draw_brain(self):
        pass

    def draw_heart(self):
        pass
    
    def draw_lungs(self):
        pass

    def draw_liver(self):
        pass

    def draw_stomach_and_intestines(self):
        pass
    
    def draw_clothes(self):
        pass
    
class GENERATE_MAN(Organs):
    '''Holds templates and functions for generating a basic human outline'''
    def __init__(self, family_hash=None, parent_hash=None):
        Organs.__init__(self)
        self.health_summary = {}
        self.hair_color = None
        self.facial_hair_possible = False
        self.facial_hair_length = None
        self.facial_hair_areas = []
        self.face_template={'facial_hair_possible':self.facial_hair_possible,
                              'facial_hair_length':self.facial_hair_length, 
                              'facial_hair_areas':self.facial_hair_areas,
                              'forehead':{}, 'eyes':{}, 'nose':{}, 'mouth':{},
                              'ears':{}, 'cheeks':{}, 'jawline':{}, 'teeth':{}}
        self.head = {'hair':{'head_hair_color':None, 'facial_hair_color':None},
                     'face':self.face_template,
                     'upper_head':{},
                     'lower_head':{}}

        self.organ_status={'brain':{'center':['x', 'y','z'],
                                    'dimensions':['x', 'y', 'z'],
                                    'sprite':Sprite(),
                                    'child_sprite':None,
                                    'ailments':[]},
                            'heart':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]},
                           'left_lung':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]},
                           'right_lung':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]}, 
                           'liver':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':['x', 'y', 'z'],
                                     'child_sprite':None, 
                                     'ailments':[]}, 
                           'stomach':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]}, 
                           'intestines':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]}}

        self.body_status = {'hair':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]},
                            'nerve':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]},
                            'psychological':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]},
                            'skin':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]},
                            'teeth':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]},
                            'hearing':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]},
                            'eye':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]},
                            'muscles':{'center':['x', 'y','z'],
                                     'dimensions':['x', 'y', 'z'],
                                     'sprite':Sprite(),
                                     'child_sprite':None, 
                                     'ailments':[]},
                            'genitals':[],
                            'organ':self.organ_status,
                            'summary':self.health_summary}
        self.body = self.body_status.copy()
        self.body_dimensions={'head':self.head,
                                'body':self.body}
        self.relation_template = {'person':None, 'trust':0, 'fear':0, 'reputation':0, 'overrall':0}
        self.relationships = {}
        self.memory_bank_one = {'languages_spoken':[],
                                'likes':[],
                                'dislikes':[],
                                'skills_learned':{},
                                'regressive':{
                                    'traits_owned':[],
                                    'relationships':self.relationships}}
        self.memory_bank_two = {}
        self.memory_bank_three = {}
        self.mind = {'mem_bank_one':self.memory_bank_one,'mem_bank_two':self.memory_bank_two, 'mem_bank_three':self.memory_bank_three}
        self.overall_health_status = {}

        self.possible_languages = {'UNIVERSAL':['ENGLISH'], 
                                   'CAUC':['RUSSIAN','GERMAN', 'FRENCH','ITALIAN','HEBREW','POLISH', 'GREEK'], 
                                   'HISPANIC':['SPANISH','ITALIAN'], 
                                   'ASIAN':['JAPANESE', 'CHINESE'],
                                   'INDIAN':['HINDI', 'ARABIC', 'PERSIAN'], 
                                   'ARAB':['RUSSIAN','HEBREW', 'PERSIAN']}
        self.possible_skill_trees = {'cooking':0, 'humor':0, 'dancing':0, 'assembly':0, 'fitness':0, 'gardening':0, 'logic':0, 'art':0, 'mischief':0, 'parenting':0, 'wellness':0, 'fighting':0, 'sexuality':0}
        self.unique_hash = None
        self.possible_traits={'mood_disorders':['depression', 'schizo', 'bipolar'], 
                              'anxiety_disorders':['phobia', 'panic attacks', 'OCD', 'PTSD'],
                              'personality_disorders':['paranoia', 'schizoid', 'emotionally unstable', 'anxiety', 'OCD', 'antisocial', 'ADHD', 'psychopath'],
                              'psychotic_disorders':{'permanent psychosis':['schizo', 'depression', 'paranoia', 'anxiety', 'bipolar','PTSD'],
                                                    'temporary psychosis':['schizo', 'paranoia', 'anxiety', 'panic attacks', 'euphoria']}, 
                               'eating_disorders':['anorexia', 'bulimia', 'binge eater', 'pica fetish', 'restricitive diet', 'diuretic addiction'],
                               'substance_abuse_disorders':{'seritonin boosters':[], 'seritonin blockers':[], 
                                                            'dopamine boosters':[], 'dopamine blockers':[],
                                                            'adrenaline boosters':[], 'adrenaline blockers':[],
                                                            'histamine boosters':[], 'histamine blockers':[], 
                                                            'acetylcholine boosters':[], 'acetylcholine blockers':[]
                                                            },
                                'genetic_disorders':{'downs syndrome', 'allergies', 'midget syndrome'},
                                'misc_disorders':{'blind', 'mute', 'deaf', 'brain dea', 'hypoxia'}}
                                            
        self.state_template = {'cause':None, 'effect':[]}
        self.lingering_states = {}
        self.possible_needs = {'hunger':100, 'hygiene':100, 'bladder':1, 'bowels':1, 'energy':100, 'social':50, 'sexual':1, 'confidence':50, 'pain':1, 'pleasure':1}
        self.possible_general_mood = {'happy':1, 'sad':1, 'anxiety':1, 'irritible':1, 'hypomanic':1, 'hypermanic':1, 'fear':1, 'lonely':1, 'jealous':1, 'disgust':1, 'suprised':1, 'playful':1}
     
        self.grade={'A':[list(range(75, 100)),'All is well'],
                    'B':[list(range(50, 74)),'This organ is suffering from a minor ailment'],
                    'C':[list(range(25, 49)),'Something is seriously affecting this organ'],
                    'D':[list(range(1, 24)),'This organ is dangerously close to failing'],
                    'F':[[0],'This organ has failed']}

        self.generals = {'skills':self.possible_skill_trees, 'traits':{}, 'lingering states':{}, 'needs':self.possible_needs.copy(), 'mood':self.possible_general_mood.copy(), 'hash':self.unique_hash}
        self.coords = {'center':['x', 'y','z'],
                        'dimensions':['x', 'y', 'z'],
                        'sprite':Sprite(),
                        'child_sprite':None}
        #HEAD #MIND
        #BODY #GENERALS

    def generate_body_template(self):
        '''Returns a blank template to place body variables'''
        outline = {'head_properties':self.head,
                    'body_properties':self.body_status,
                    'body_state':self.more_organs_blank,
                    'mind':self.mind,
                    'general':self.generals,
                    }
        return outline
    
    def generate_nose_template(self):
        '''Returns blank nose template'''
        nose = {'tip_length':0, 
                'tip_width':0,
                'tip_point':('round', 'pointy'),
                'tip_height':0,
                'bridge_length':0,
                'bridge_offset':(None, 'left','right'),
                'bridge_width':0,
                'bridge_height':0,
                'nostril_shape':('melon shape', 'pear shape'),
                'nostril_size':0,
                'center':['x', 'y','z'],
                'dimensions':['x', 'y', 'z'],
                'sprite':['x', 'y', 'z'],
                'child_sprite':None
                }
        return nose

    def age_based_height(self, age):
        '''Calcualates and returns height in (feet, inches) based on age'''
        if age >= 22:
            feet = r.random_selection(list(range(4,8)))
            l = {'4':list(range(7, 11)),
                '5':r.HEIGHT_INCHES,
                '6':r.HEIGHT_INCHES,
                '7':r.HEIGHT_INCHES}
            iinches = l[str(feet)]
            inches = r.random_selection(iinches)
            return (feet, inches)
        elif age in list(range(17, 23)):
            feet = r.random_selection(list(range(4,7)))
            l = {'4':list(range(7, 11)),
                '5':r.HEIGHT_INCHES,
                '6':r.HEIGHT_INCHES}
            iinches = l[str(feet)]
            inches = r.random_selection(iinches)
            return (feet, inches)
        elif age in list(range(11, 17)):
            feet = r.random_selection(list(range(4,6)))
            l = {'4':list(range(7, 11)),
                '5':r.HEIGHT_INCHES}
            iinches = l[str(feet)]
            inches = r.random_selection(iinches)
            return (feet, inches)
        elif age in list(range(6, 12)):
            feet = r.random_selection(list(range(4,6)))
            l = {'4':list(range(7, 11)),
                '5':list(range(0, 4))}
            iinches = l[str(feet)]
            inches = r.random_selection(iinches)
            return (feet, inches)
        elif age in list(range(0, 6)):
            feet = r.random_selection(list(range(2, 4)))
            l = {'2':list(range(0, 11)),
                '3':list(range(0, 6))}
            iinches = l[str(feet)]
            inches = r.random_selection(iinches)
            return (feet, inches)

    def calculate_bmi(self,weight, height):
        '''Calcualte proper body mass index(fat level) based on weight and height'''
        height = float(height)
        bmi = float(weight)/(height*height)
        bmi = str(bmi)[:7]
        return float(bmi)

    def height_based_weight(self, a):
        '''Calcualates a weight based on height. Based on proper bmi chart.'''
        weight_lbs = False
        while weight_lbs == False: 
           # print('feet:', a[0], 'inches:',a[1])
            if a[0] == 0 and a[1] in list(range(0, 11)):
                weight_lbs = r.random_selection(list(range(2, 9)))
            if a[0] == 1 and a[1] in list(range(0, 11)):
                weight_lbs = r.random_selection(list(range(6, 11)))
            if a[0] == 2 and a[1] in list(range(0, 11)):
                weight_lbs = r.random_selection(list(range(30, 81)))
            if a[0] == 3 and a[1] in list(range(0, 11)):
                weight_lbs = r.random_selection(list(range(60, 101)))
            if a[0] == 4 and a[1] in list(range(0, 12)):
                weight_lbs = r.random_selection(list(range(91, 277)))#= int(weight_lbs)
            if a[0] == 5 and a[1] in list(range(0, 6)):
                weight_lbs = r.random_selection(list(range(97, 325)))
                weight_lbs = int(weight_lbs)
            if a[0] == 5 and a[1] in list(range(6, 12)):
                weight_lbs = r.random_selection(list(range(118, 387)))
                weight_lbs = int(weight_lbs)
            if a[0] == 6 and a[1] in list(range(0, 6)):
                weight_lbs = r.random_selection(list(range(140, 444)))
                weight_lbs = int(weight_lbs)
            if a[0] == 6 and a[1] in list(range(6, 12)):
                weight_lbs = r.random_selection(list(range(180, 541)))
                weight_lbs = int(weight_lbs)
            if a[0] == 7 and a[1] in list(range(0, 12)):
                weight_lbs = r.random_selection(list(range(220, 621)))
                weight_lbs = int(weight_lbs)

            if weight_lbs:
                return weight_lbs

    def normalize_bmi(self, muscle, fat):
        '''Adjusts bmi to account for muscle level'''
        both = muscle + fat
        strength_percent = muscle/both
        fat_percent = fat/both
        if both > 100:
            ass = (both % 100)
            b = (both - ass)
            strength = round(strength_percent * b)
            bmi = round(fat_percent * b)
            both = strength + bmi
        if both < 100:
            strength = round(100 * strength_percent)
            bmi = round(100 * fat_percent)
            both = strength+bmi
        if both in (100, 99):
            return (strength, bmi)
        
    def generate_person_template(self):
        '''Generates a blank body template and fills it with values.
        Subprocess for Private_Human class.'''
        HEALTH_REPORT = {}
        outline = self.generate_body_template()
        BASE = r.generate_person_fast()
        if BASE['sex'] in ('MALE', 'TRANSMALE'):
            outline['head_properties']['face']['facial_hair_possible'] = True
            outline['head_properties']['face']['facial_hair_areas']=self.coords.copy()
            outline['head_properties']['face']['facial_hair_length'] = 0
            BASE['chest'] = r.random_selection([r.CHEST_SIZE[0], r.CHEST_SIZE[1]])
        elif BASE['sex'] in ('INTERSEX/FUTA/HERMAPHRODITE'):
            outline['head_properties']['face']['facial_hair_possible'] = True
            outline['head_properties']['face']['facial_hair_areas']=self.coords.copy()
            outline['head_properties']['face']['facial_hair_length'] = 0
        if BASE['sex'] in ('TRANSFEMALE', 'TRANSMALE'):
            BASE['gender identity']='TRANS'
            BASE['intelligence'] = BASE['intelligence']-10
        elif BASE['sex'] in ('INTERSEX/FUTA/HERMAPHRODITE', 'NULL'):
            BASE['intelligence'] = BASE['intelligence']-15
        if BASE['intelligence']<= 0:
            BASE['intelligence'] = 8
        if BASE['eye color']:
            eye_temp = r.eyeball_template.copy()
            eye_temp['iris_color']=BASE['eye color']
            eye_temp['pupil_color']='BLACK'
            eye_temp['whites_color']='WHITE'
            eye_temp['center']=['x', 'y', 'z']
            eye_temp['dimensions']:['x', 'y', 'z']
            eye_temp['sprite']:['x', 'y', 'z']
            eye_temp['child_sprite']:None
            eye_temp['radius']=None
#        print('bday',BASE['birthday'][0][:4])
        age = 2020-int(BASE['birthday'][0][:4])
     #   print('Age: ',age)
        a=None
        while not a:
            a = self.age_based_height(age)
        #feet and inches to meters
        if a:
            meters = r.ft_in_to_meters(a)
     #       print(meters)
            weight_lbs = self.height_based_weight(a)
            if weight_lbs and meters:
          #      print('weight in lbs: ', weight_lbs)
                kilos = r.lbs_to_kilos(weight_lbs)
                kilos = str(kilos)[:6]
                BASE['weight'] = float(kilos)
        #        print('weight in kilos: ', kilos)
                bmi = self.calculate_bmi(kilos, meters)
        #        print('your body is', bmi,'percent body fat')
                BASE['fattiness']= int(bmi)
                meters = str(meters)[:6]
                BASE['height'] = float(meters)
                m, f = (BASE['muscularity'], BASE['fattiness'])
                while (m+f) != 100: 
                    m, f = self.normalize_bmi(m, f)
                    if (m+f) == 100:
                        break
                BASE['muscularity'], BASE['fattiness'] = (m, f)
                #print('muscle',BASE['muscularity'], 'fattiness',BASE['fattiness'])
    #            BASE['muscularity']=
    #            BASE['fattiness'] = 

            q = r.random_selection(r.hair_color_names)
            outline['head_properties']['face']['eyes']=eye_temp
            outline['head_properties']['face']['nose']=self.generate_nose_template()
            outline['head_properties']['face']['forehead']=self.coords.copy()
            outline['head_properties']['hair']['head_hair_color'] = [q, r.hex_hair_colors[q]]
            outline['head_properties']['hair']['facial_hair_color'] = [q, r.hex_hair_colors[q]]
            properties = [BASE, outline]
            return properties
        else:
            print('Generate person template failed!')

    def _get_all_diseases(self):
        H={'brain':self.brain_diseases,
             'heart':self.heart_diseases, 
             'lung':self.lung_diseases,
             'liver':self.liver_diseases,
             'stomach':self.stomach_diseases,
             'intestines':self.intenstinal_diseases,
             'genital':self.genital_diseases,
             'blood':self.blood_diseases}
        return H

#import RELATIONSHIPS
    
class RAW_PREALPHA_MAN(GENERATE_MAN):
    '''Generates a random person or loads one from a template, also holds a lot of the functions, variables and templates to be
    used as a sub class for NPCs, charachters, or bots.'''
    def __init__(self, person_template=None, person_hash=None, randomize_skill_levels=False):
        GENERATE_MAN.__init__(self)
        self._INVENTORY_ = Inventory()
        self.person_template=person_template
        if self.person_template == None:
            #self.person_template = 'oh'
            while self.person_template == None:
                self.person_template = self.generate_person_template()

        if self.person_template:
            self.breast_size = self.person_template[0]['chest']     #classify
            self.height_meters = self.person_template[0]['height']  #no class needed
            self.weight_kilos = self.person_template[0]['weight']  #no class needed
            self.strength_level = self.person_template[0]['muscularity']
            self.bmi = self.person_template[0]['fattiness']
            self.base_iq = self.person_template[0]['intelligence'] 
            self.iq = self.person_template[0]['intelligence']
            self.shoulder_width = self.person_template[0]['shoulder width']
            self.shoulder_depth = self.person_template[0]['shoulder depth']
            self.waist_width = self.person_template[0]['waist width']
            self.waist_depth = self.person_template[0]['waist depth']

            self.hip_width = self.person_template[0]['hip width']
            self.hip_depth = self.person_template[0]['hip depth']
            self.sex = self.person_template[0]['sex']
            self.gender_identity = self.person_template[0]['gender identity']
            self.gender_expression = self.person_template[0]['gender expressed']
            self.orientation = self.person_template[0]['sex orientation']
            self.alive = True
            self.birthday = self.person_template[0]['birthday']
            self.race = self.person_template[0]['race']
            self.first_name = self.person_template[0]['first name']
            self.last_name = self.person_template[0]['last name']        
            self.head_hair_color = self.person_template[1]['head_properties']['hair']['head_hair_color']
            self.facial_hair_color = self.person_template[1]['head_properties']['hair']['facial_hair_color']
            self.facial_hair_possible = self.person_template[1]['head_properties']['face']['facial_hair_possible']
            self.facial_hair_length = self.person_template[1]['head_properties']['face']['facial_hair_length']
            self.forehead_detail = self.person_template[1]['head_properties']['face']['forehead']
            self.eyes = self.person_template[1]['head_properties']['face']['eyes']
            self.nose = self.person_template[1]['head_properties']['face']['nose']
            self.mouth = self.person_template[1]['head_properties']['face']['mouth']
            self.ears = self.person_template[1]['head_properties']['face']['ears']
            self.cheeks = self.person_template[1]['head_properties']['face']['cheeks']
            self.jawline = self.person_template[1]['head_properties']['face']['jawline']
            self.teeth = self.person_template[1]['head_properties']['face']['teeth']
            self.upper_head = self.person_template[1]['head_properties']['upper_head']
            self.lower_head = self.person_template[1]['head_properties']['lower_head']
            self.hair_health = self.person_template[1]['head_properties']['hair']
            self.nerve_health = self.person_template[1]['body_properties']['nerve']
            self.psychological_health = self.person_template[1]['body_properties']['psychological']
            self.skin_health = self.person_template[1]['body_properties']['skin']
            self.teeth_health = self.person_template[1]['body_properties']['teeth']
            self.hearing_health = self.person_template[1]['body_properties']['hearing']
            self.eye_health = self.person_template[1]['body_properties']['eye']
            self.muscle_health = self.person_template[1]['body_properties']['muscles']
            self.genitals_detail = self.person_template[1]['body_properties']['genitals']
            self.organs_brain = self.person_template[1]['body_properties']['organ']['brain']
            self.organs_heart = self.person_template[1]['body_properties']['organ']['heart']
            self.organs_left_lung = self.person_template[1]['body_properties']['organ']['left_lung']
            self.organs_right_lung = self.person_template[1]['body_properties']['organ']['right_lung']
            self.organs_liver = self.person_template[1]['body_properties']['organ']['liver']
            self.organs_stomach = self.person_template[1]['body_properties']['organ']['stomach']
            self.organs_intestines = self.person_template[1]['body_properties']['organ']['intestines']
            self.mind = self.person_template[1]['mind']
            self.skills = self.person_template[1]['general']['skills']
            self.skill_aqua_diff = float(0.5) + self._skill_factor()
            self.reputation = -1
            self.FAMILY = {}
            self.relationships = []
            self.unique_hash = data.hash3_512(self.first_name+self.last_name)
         #   self.maturity_level = 
       #     self.genitals_detail = self.person_template[1]['body_properties']['genitals']
         #   self.brain_health = self.person_template[1]['head_properties']['nerve']
            self.inventory = self._INVENTORY_.inventory
                                #if fighting skill then add stealth takedowns
            self.directions = ['down', 'left','right','up']
            self.direction_facing = self.directions[0]
            self.incoming_interactions = []
            self.outgoing_interactions = []
            self.body_stance = ['standing', 'crouched', 'laid back', 'on knees', 'laid forward', 'limping', 'stunned', 'knocked out', 'all fours', 'laying ls','laying rs']
            self.stats = {}
            self.needs = self.person_template[1]['general']['needs']
            self.moodlets = self.person_template[1]['general']['mood']
            self.mood_state = self.person_template[1]['general']['lingering states']
            self.traits = {}
            self.home = {}
            self._areas_discovered = []
            self.current_area = {}
            self.vicinity = {'origin':(0,0),'height':16*6, 'width':16*6}
            self.objects_in_vicinity = []
            self.action_queue = []
            self.current_action = []
            self.fetishes = {}
            self.role=str(None)
            self.collider = {'height':None,'width':None, 'origin':None}
            self.sprite=SpriteList()
            self.available_interactions = {}
            self.all_social_interations = {'friendly':['Give item', 'Tell joke', 'Compliment', 'Small Talk', 'Be flirty'],
                                    'rude':['Insult', 'Push', 'Tease', 'Punch'],
                                    'demanding':['Forced Hscene', 'Carry person', 'Strip person naked', 'Rob item', 'Forced administration'],
                                    'sneaky':['Pickpocket', 'Use Weapon'],
                                    'neutral':['Talk about family', 'talk about friends', 'talk about likes', 'talk about dislikes', 'Smell person', 'Sell item', 'Ask to follow', 'Ask about sexuality', 'Ask about health']}
    
    # def new_relationship(self, other_person, relation_type=RELATIONSHIPS.STRANGER):
    #     if other_person:
    #         REL = RELATIONSHIPS._NEW_RELATIONSHIP_(self, other_person, relation_type)
    #         return REL

    def put_in_inventory(self, item=None, index=None):
        '''Place an item in the persons inventory'''
        self._INVENTORY_._put_item(item, index)

    def get_from_inventory(self, item=None, index=None):
        '''Get an item from the persons inventory'''
        i = self._INVENTORY_._get_item(item, index)
        return i

    def get_id_card(self):
        pass

    def _needs_callback(self):
        '''Print and return a dictionary of the persons need levels'''
        print(self.needs)
        return self.needs

    def _is_hungry(self):
        '''Bool for asking if the person is hungry. - could be changed based on personality'''
        if self.needs['hunger']<20:
            return True
        elif self.needs['hunger']>=100:
            self.needs['hunger']=100
            return False
        
    def _set_hunger(self, n):
        '''Set/Updates the persons hunger value'''
        self.needs['hunger'] = n

    def _is_dirty(self):
        '''Bool for asking if the person needs a shower. - could be changed based on personality'''
        if self.needs['hygiene']<50:
            return True
        elif self.needs['hygiene']>=100:
            self.needs['hunger']=100
            return False

    def _set_hygiene(self, n):
        '''Set/Updates the persons hygiene value'''
        self.needs['hygiene'] = n
    
    def _must_pee(self):
        '''Bool for asking if the person needs to pee. could be changed based on personality'''
        s = self.needs['bladder']
        if s < 15:
            return True
        elif s >= 100:
            self.needs['bladder']=100
            return False
        
    def _set_bladder(self, n):
        '''Set/Updates the persons bladder value.'''
        self.needs['bladder'] = n
         
    def _must_poop(self):
        '''Bool for asking if the person needs to poop.'''
        s = self.needs['bowels']
        if s < 15:
            return True
        elif s >= 100:
            self.needs['bowels']=100
            return False

    def _set_bowels(self, n):
        '''Set/Updates the persons bowel value.'''
        self.needs['bowels'] = n

    def _is_tired(self):
        '''Bool for asking if the person is tired.'''
        s = self.needs['energy']
        if s < 30:
            return True
        elif s >= 100:
            self.needs['energy']=100
            return False

    def _set_energy(self, n):
        '''Set/Updates the persons energy value.'''
        self.needs['energy'] = n

    def _is_lonely(self):
        '''Bool for asking if the person is lonely.'''
        s = self.needs['social']
        if s < 54:
            return True
        elif s >= 100:
            self.needs['social']=100
            return False

    def _set_social(self, n):
        '''Set/Updates the persons social value.'''
        self.needs['social'] = n

    def _is_randy(self):
        '''Bool for asking if the person is randy.'''
        s = self.needs['sexual']
        if s < 30:
            return False
        elif s >= 54:
            return True

    def _set_sexual(self, n):
        '''Set/Updates the persons sexual value.'''
        self.needs['sexual'] = n

    def _is_confident(self):
        '''Bool for asking if the person is confident.'''
        s = self.needs['confidence']
        if s < 10:
            return False
        elif s >= 50:
            return True

    def _set_confidence(self, n):
        '''Set/Updates the persons confidence value.'''
        self.needs['confidence'] = n

    def _in_pain(self):
        '''Bool for asking if the person is in pain.'''
        s = self.needs['pain']
        if s <= 1:
            return False
        elif s >= 2:
            return False

    def _set_pain(self, n):
        '''Set/Updates the persons confidence value.'''
        self.needs['pain'] = n

    def _exp_pleasure(self):
        '''Bool for asking if the person is experiencing pleasure.'''
        s = self.needs['pleasure']
        if s <= 1:
            return False
        elif s >= 2:
            return True

    def _set_pleasure(self, n):
        '''Set/Updates the persons pleasure value.'''
        self.needs['pleasure'] = n

    def _add_new_skill(self, _new_skill_name=None, _skill_value=0):
        '''Set/Updates the persons confidence value.'''
        if _new_skill_name != None:
            self.mind['mem_bank_one']['skills_learned'][str(_new_skill_name)]=_skill_value
            self.skills[str(_new_skill_name)]=_skill_value
            print('new skill added')
            self._skill_factor()
        else:
            pass
    
    def _skills_match(self):
        '''Updates the skills value, moves all skills learned to the skills variable'''
        if len(self.mind['mem_bank_one']['skills_learned']) > 0:
            for x in self.mind['mem_bank_one']['skills_learned']:
                if x not in self.skills:
                    self.skills[x]=self.mind['mem_bank_one']['skills_learned'][x]
            print('skill matching complete')

    def _skill_factor(self):
        '''Matches person skills with class skills, uses skills to factor iq level and skill aquirance difficulty.
        Also updates the '''
        self._skills_match()
        num = []
        for x in self.skills:
            num.append(self.skills[x])
        factors = abs(sum(num)*0.001)
        self._exp_sex_based_iq_refactoring()
        self._exp_age_based_iq_refactoring()
        iq = abs(self.iq * 0.001)
        self.iq = self.base_iq+(factors * 10)
        self.skill_aqua_diff = 0.5 - factors - iq
        return factors

    def _exp_sex_based_iq_refactoring(self):
        '''Refactors the iq to account for sex.'''
        if self.sex in ('TRANSFEMALE', 'TRANSMALE'):
            self.gender_identity = 'TRANS'
            self.iq = self.iq-10
        elif self.sex in ('INTERSEX/FUTA/HERMAPHRODITE', 'NULL'):
            self.iq = self.iq-15
        if self.iq <= 0:
            self.iq = 8

    def _latest_gamedate(self):
        '''Returns the latest game date.'''
        latest_d = r.time_d[-1]
        latest_m = r.time_m[-1]
        latest_y = r.time_y[-1]
        return (latest_y, latest_m, latest_d)

    def _date_split(self, date):
        '''Splits the date at the "-" value.'''
        print(date)
        if type(date) == list:
            date = date[0]
        d =date.split('-')
        return d

    def _exp_age_based_iq_refactoring(self):
        '''Refactors the iq value to account for age.'''
        latest = self._latest_gamedate()
        bd = self._date_split(self.birthday)
        print(bd)
        ya = int(latest[0]) - int(bd[0])
        ma = int(latest[1]) - int(bd[1])
        da = int(latest[2]) - int(bd[2])
        if ya <= 13 or ya >= 65:
            self.iq = self.iq - 25
        if self.iq <= 0:
            self.iq = 8

    def _exp_brain_health_iq_refactoring(self):
        pass

    def get_internal_organ_health_report(self):
        organ_dict = self.more_organs_blank


class t_volume():
    def __init__(self, current_fill_level, total_volume):
        self.current_fill_level=current_fill_level
        self.total_volume=total_volume
        self.full=self.is_full()

    def is_full(self):
        if self.current_fill_level<self.total_volume:
            self.full=False
            return False
        elif self.current_fill_level>=self.total_volume:
            self.full=True
            return True

    def fill(self, amount=1):
        if self.current_fill_level<self.total_volume:
            self.current_fill_level+=amount
        else:
            if self.drain():
                return True
        
    def drain(self, amount=1):
        if self.current_fill_level>self.total_volume and self.current_fill_level>0:
            self.current_fill_level-=amount
            return True
        elif self.current_fill_level == self.total_volume:
            return True
