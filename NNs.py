import NN
import numpy as np
import arcade

#TODO:
##create exit and entrance nodes/portals
#ABLE TO CREATE GAME OBJECT BASED ON SPRITES LOADED IF NEEDED
#move objects using controls hold any item if item is larger then drag or push
#customizable house
#world objects/interactable objects/items
#npcs
#npc types
# --baby
    # --eye types
    # --eyebrow types
    # --mouth types
    # --ear types
    # --hair types
    # --hat types
    # --glasses types
    # --nose types
    # --mouth types
    # --jaw types
    # --chin types
    # --facial hair types
    # --neck types
    # --chest types
    # --shoulder types
    # --bicep types
    # --forearm types
    # --hand types
    # --finger types
    # --abdomen types
    # --internals animations eventually
    # --weapons and weapon animations
    # --genitalia
    # --theigh types
    # --shin types
    # --foot types
    # --
# --toddler
# --child
# --teen
# --adult
# --oldy
# --nursing home nurses
# --pharmacist/doctor
# --shopkeeper
# --teacher
# --waiter
# --cop
# --factory worker
# --mafia
# --fireman
# --
# --

#neighborhood/world layout
#stores
#pharmacy
#daycare
#old folks home
#school
#bank
#fast food/diner
#firehouse
#police station
#tenement-mafia controlable building
#factory
#atm

met = NN.Metatron()
ds = NN.DoShit()
start_population = 50

obj_code = {0:'dirt','dirt':0,
            1:'grass', 'grass':1, 
            2:'wall', 'wall':2,
            3:'fence', 'fence':3,
            4:'object','object':4,
            5:'storage_object', 'storage_object':5,
            6:'carpet', 'carpet':6, 
            7:'stone floor','stone floor':7,
            8:'tile floor','tile floor':8,
            9:'metal floor','metal floor':9,
            10:'concrete floor', 'concrete floor':10,
            11:'wallpaper','wallpaper':11,
            12:'stone wall','stone wall':12,
            13:'tile wall','tile wall':13,
            14:'metal wall','metal wall':14,
            15:'dry wall','dry wall':15, 
            16:'painted','painted':16,
            18:'brick wall','brick wall':18,
            19:'doors','doors':19, 
            20:'glass door','glass door':20, 
            21:'wooden door','wooden door':21,
            22:'secret door','secret door':22,
            23:'secure door','secure door':23}
    
#print(obj_code.keys())
def _obj_type(code):
    if type(code) == str:
        if code in obj_code:
            return obj_code[code]
    elif type(code) == int:
        if code in obj_code:
            return obj_code[code]

class OPCODES():
    def __init__(self, code):
        self.isint = False
        self.isstr = False
        try:
            code = int(code)
        except:
            pass
        if type(code) == str:
            self.isstr = True 
            c = _obj_type(code)
            if c in obj_code:
                self.tags = obj_code(c)
        elif type(code) == int:
            self.isint = True
            #c = _obj_type(code)
            if c in obj_code:
                self.tags = obj_code()
        self.names = []
        self.code = None


def _generate_population_dictionary(pop_amount):
    '''Populates a dictionary with random 6 bit hashes 
        with a blank list as the key '''
    hash_pool = {}
    temp = []
    #l = temp * pop_amount
    _rhashes = met.hash76(length=6, orbital_amount=pop_amount)
    for i in _rhashes:
        hash_pool[i] = temp
    return hash_pool

#POPULATION = generate_population()
from body import RAW_PREALPHA_MAN
from player import Load_Player

#GENERATE A PERSONALITY FOR EACH OF THE POPULOUS
def generate_npc_personas(population_dictionary):
    _NPC_POPULOUS_LIST_ = []
    for pop in population_dictionary:
        population_dictionary[pop] = RAW_PREALPHA_MAN()
        _NPC_POPULOUS_LIST_.append(pop)
    return {'list':_NPC_POPULOUS_LIST_, 'dict':population_dictionary}

DESIRED_POPULATION = 1000
def GENERATE_RANDOM_NPC_DATA_FOR(DESIRED_POPULATION):
    npc_dict = _generate_population_dictionary(DESIRED_POPULATION)
    NPC_POP_INFO = generate_npc_personas(npc_dict)
    print(NPC_POP_INFO)

def GENERATE_RANDOM_CHARACTER_SPRITE(age='adult', gender='male', occupation=None):
#CURRENTLY IN MOST BASIC STATE NEED TO ADD ITEMS IN COMMNETS ABOVE
    pgeo = NN.p_geometry()
    from NNe import Assets
    VALID_FACES = {'adult':[1, 3, 4, 5, 6, 7, 8, 9, 10],'child':[3, 5, 6,7,9],'teen':[3,4,5,6,7],'toddler':[2,3,4,5], 'baby':[], 'elder':[]}
    VALID_CLOTHES = {'male':[1, 5,8],'female':[3,4,6,7,9,10]}
    VALID_HAIR = {'male':[1,5,8],'female':[3,4,6,7,9,10]}
    if occupation:
        age = occupation
    FACE = str(pgeo.random_selection(VALID_FACES[str(age)]))
    CLOTHES = str(pgeo.random_selection(VALID_CLOTHES[str(gender)]))
    HAIR = str(pgeo.random_selection(VALID_HAIR[str(gender)]))
    pFace = Assets.game_directory+'usethese\\A\\'+str(age)+'\\must_be_refined\\face'+FACE+'.png'
    pClothes = Assets.game_directory+'usethese\\A\\'+str(age)+'\\must_be_refined\\clothes'+CLOTHES+'.png'
    pHair = Assets.game_directory+'usethese\\A\\'+str(age)+'\\must_be_refined\\hair'+HAIR+'.png'
    return (pFace, pClothes, pHair)

def GENERATE_PLAYER_DATA(RANDOM=1):
    player_p = _generate_population_dictionary(1)
    player_hash = met.hash76(length=6)[0]
    if RANDOM == 1:
        player_persona = RAW_PREALPHA_MAN()#if random == True then generate a random persona else generate a person throught a character creator interface 
    else:
        player_persona = None           #NEED TO CREATE A WAY TO CREATE A CUSTOM CHARACTER THAT USES GUI
    #EDIT THE FACE COLOR BASED ON THE PLAYER PERSONA RACE
    #FIND HAIR AND CLOTHES BASED ON GENDER AND AGE

def GET_RANDOM_CHARACHTER(age='adult',gender='male',occupation=None, bot=False, x=0, y=0):
    SPRITES = GENERATE_RANDOM_CHARACTER_SPRITE(age,gender,occupation)
    print(SPRITES)
    if SPRITES:
        LOADED = Load_Player(SPRITES[0], SPRITES[1], SPRITES[2],x=x, y=y, ai=bot)
        return LOADED

def __get_age__(_birthday):
    currentDay = NN.dt.sdate()
    s = _birthday.split('-')
    age = []
    for cDate, bDate in zip(currentDay, s):
        x = abs(int(bDate) - int(cDate))
        age.append(x)
    return age

def GET_RANDOM_CHARACHTER_WITH_PERSONA(occupation=None, bot=False):
    PERSONA = RAW_PREALPHA_MAN()
    print('GENDER',PERSONA.gender_expression,'birth',PERSONA.birthday)
    age = __get_age__(PERSONA.birthday)
    if PERSONA.gender_expression == 'MASCULINE':
        gender = 'male'
    elif PERSONA.gender_expression == 'FEMININE':
        gender = 'female'
    elif PERSONA.gender_expression == 'ANDROGYNOUS':
        pgeo = NN.p_geometry()
        index = list(range(0, 10))
        A = str(pgeo.random_selection(index))
        B = str(pgeo.random_selection(index))
        AB = int(A+B)
        if AB >= 50:
            gender = 'male'
        elif AB < 50:
            gender = 'female'
        print(gender)

    if occupation == None:
#        if age[0] >= 0:
#            age_cat = 'baby'
        if age[0] >= 0:
            age_cat = 'toddler'
        elif age[0] >= 4:
            age_cat = 'child'
        elif age[0] >= 12:
            age_cat = 'teen'
        elif age[0] >= 18:
            age_cat = 'adult'
#        elif age[0] >= 60:
#            age_cat = 'elder'
    elif occupation:
        age_cat = occupation
    print(age_cat)
    if age_cat and gender:
        CHAR = GET_RANDOM_CHARACHTER(age=age_cat, gender=gender, bot=bot)
        return CHAR, PERSONA

#T = GET_RANDOM_CHARACHTER_WITH_PERSONA()

#import PyInstaller
#PyInstaller.
#player_sprite_live = Load_Player(pFace, pClothes, pHair)

#test = GENERATE_RANDOM_CHARACTER_SPRITE()
#print(test)

#print(player_hash)

max_num = 800
rand_len = 900
m = (max_num % rand_len) - (rand_len % max_num)
##print(m)
#########

class block_builder():
    '''Raw engine for building 1D numpy array objects'''
    def np_canvas(self, width, height, scale=1):
        '''Decalare a blank canvas of zero
        **scale is the pixel size of each value in the canvas
        example: if scale=16: (1 point == 16 pixels) '''
        base = np.zeros((int(height/scale), int(width/scale)))
        return base

    def np_obj(self, width,height, enum=1):
        '''Declare a 1D numpy array to be placed within the canvas
            enum is the object id code and can be any valid integer'''
        n = np.ones((height, width))
        n = np.multiply(n, (int(enum)))
        return n

    def _place_object(self, base_np_array, np_array, origin=(0,0)):
        '''Internal class for adding objects to a canvas'''
        if type(base_np_array) != None and type(np_array) != None:
            shape = np_array.shape
            canvas_shape = base_np_array.shape
            y = origin[0]
            x = origin[1]
            a = (x, x+shape[0])
            b = (y, y+shape[1])
        #    #print('array to add shape',shape)
        #    #print('a,',a)
        #    #print('b,',b)
        #    #print('canvas shape',canvas_shape)
        #try:
        ##print(b
            if a[1] <= shape[1] and b[1] <= canvas_shape[1] or shape[1] < a[1] and b[1] <= canvas_shape[1] or a[0] <= canvas_shape[1] and a[1] <= canvas_shape[0] and b[0] <= canvas_shape[1] and b[1] <= canvas_shape[0]:#b[0] <= canvas_shape[1] and b[1] <= canvas_shape[0] and a[0] <= canvas_shape[1] and a[1] <= canvas_shape[0] :
        #    if a[0] <= canvas_shape[1] and a[1] <= canvas_shape[0] and b[0] <= canvas_shape[1] and b[1] <= canvas_shape[0]:
#                #print('o')
#                #print('base/canvas',base_np_array[a[0]:a[1],b[0]:b[1]])
#                #print('nparray to add',np_array)
#                #print('a',a)
#                #print('b',b)
                base_np_array[a[0]:a[1],b[0]:b[1]] = np_array
                #return
                return base_np_array
            else:
               # #print('a[1]',a[1])
    #        if base_np_array.shape == a[1]:
             #   base_np_array[a[0]:a[1],b[0]:b[1]] = np_array
                return base_np_array
        #except ValueError:
        #    return base_np_array[a[0]:a[1],b[0]:b[1]]

    def _upscale(self, new_size=tuple(), downscaled_array=None, tile_size=16, tiles=None):
        '''Upscales a np array when given a new_size, a downscaled_array, and a tilesize
            TO BE ADDED: TILE CODE HANDLING'''
        can = self.np_canvas(new_size[0], new_size[1])
        old_size = downscaled_array.size
        if tiles == None:
            tile = np.ones((tile_size,tile_size))
        elif tiles:
            tile = tiles
        blank = np.zeros((tile_size, tile_size))
        xxx = []
        yyy = []
        xx = 0
        yy = 0
        yyy = 0
        n = []
        interval =tile_size
        for y in downscaled_array:  
            for x in y:
                #try:
                if int(x) == 1:
                    ##print(x)
                    can = self._place_object(can,tile,(xx,yy))
                   # #print(can)
                elif int(x) == 0:
                    ##print(x)
                    can = self._place_object(can,blank, (xx,yy))
                elif int(x) == 2:
                    can = self._place_object(can,np.multiply(tile, (2)), (xx,yy))
                elif int(x) == 3:
                    can = self._place_object(can,np.multiply(tile, (3)), (xx,yy))
                elif int(x) == 4:
                    can = self._place_object(can,np.multiply(tile, (4)), (xx,yy))
                elif int(x) == 5:
                    can = self._place_object(can,np.multiply(tile, (5)), (xx,yy))
               # except ValueError:
            #    #print('prblem')
                xx+=interval
            xx = 0
            yy+=interval
        return can

    def view_block(self, np_array, origin=(), tile_size=16):
        '''Allows one to peek into the numpy array by specifying a viewport'''
        y = origin[0]
        x = origin[1]
        a = (x, x+tile_size)
        b = (y, y+tile_size)
        base = np_array[a[0]:a[1],b[0]:b[1]]
        ##print(base)
        return base


    def get_column(self, np_array, column_x):
        '''Returns a specified column from a specified numpy array'''
        n = np_array[:, column_x]
        size= n.shape
        print(size,'shape')
        return n

    def get_row(self, np_array, column_y):
        '''Returns a specified row from a specified numpy array'''
        n = np_array[column_y, :]
        return n

    def _bbwall_block(self, size=(1,1), enum=2):
        '''Returns basic wall block object to be placed within a canvas'''
        if type(size) in (list, tuple):
            _wall = self.np_obj(size[0], size[1], enum)
            return _wall

    def _bbdoor_block(self, size=(1,1), enum=21):
        '''Returns basic door block object to be placed within a canvas'''
        if type(size) in (list, tuple):
            door = self.np_obj(size[0], size[1], enum)
            return door
    
    def _bbfloor_block(self, size=(1,1), enum=10):
        '''Returns basic floor block object to be placed within a canvas'''
        if type(size) in (list, tuple):
            floor = self.np_obj(size[0], size[1], enum)
            return floor

    def _bbwallpaper(self, size=(1, 1), enum=11):
        '''Returns basic wallpaper block object to be placed within a canvas'''
        if type(size) in (list, tuple):
            wallpaper = self.np_obj(size[0], size[1], enum)
            return wallpaper


class bb_canvas(block_builder):
    def __init__(self, screen_size, bb_scale):
        '''Extended class for block builder that takes into account screen size and scale
            and makes creating, adding and returning canvas items eazier along 
            with easy upscaling and sprite list generation'''
        block_builder.__init__(self)
        self.screen_size = screen_size
        self.scale = bb_scale
        self.canvas = self.np_canvas(self.screen_size[0], self.screen_size[1], self.scale)
        self.max_height = len(self.get_column(self.canvas, 0))
        self.max_width = len(self.get_row(self.canvas, 0))
        self.canvas_size = (self.max_width, self.max_height)
        self.build_buffer = 10
        self.wall_size = (self.canvas_size[0]-self.build_buffer, self.canvas_size[1]-self.build_buffer)
        self.wall_mid_point = (int(self.wall_size[0]/2), int(self.wall_size[1]/2))
        self.midx = int(self.wall_mid_point[0]) #wall_size[0]/2) #wall x length half measure
        self.midy = int(self.wall_mid_point[1]) #wall_size[1]/2)    #wall y height half measure
        self.frame_dimensions = ()

    def max_wall(self, t='v'):
        '''Specify which type of wall to build:
            'v' = vertical wall, 'h' = horizontal wall'''
        if t == 'h':
            wall = self._bbwall_block((self.wall_size[0]+1, 1))
            return wall
        elif t == 'v':
            wall = self._bbwall_block((1, self.wall_size[1]+1))
            return wall

    def place(self, origin=(0,0), np_object=None):
        '''Place and object onto the current canvas at a specified origin'''
        #if np_object:
        self.canvas = self._place_object(self.canvas, np_object, origin)
        return self.canvas

    def get_floor(self, size=(1,1)):
        '''Return a common floor object used in bit games
        id:10'''
        floor = self._bbfloor_block(size)
        return floor
    
    def get_door(self, size=(1,1)):
        '''Return a common door object used in bit games
        id:21'''
        door = self._bbdoor_block(size)
        return door

    def get_wall(self, size=(1,1)):
        '''Return a common wall object used in bit games
        id:2'''
        wall = self._bbwall_block(size)
        return wall

    def get_wallpaper(self, size=(1,1)):
        '''Return a common wallpaper object used in bit games
        id:11'''
        wallpaper = self._bbwallpaper(size)
        return wallpaper

    def _bbhouse_frame(self, padding=5):
        '''Places a box frame within the canvas with the specified padding'''
        v_wall = self.max_wall('v') 
        h_wall = self.max_wall('h')
        #_frame = self.canvas
        self.canvas = self._place_object(self.canvas, h_wall, (padding,padding))    #top
        self.canvas = self._place_object(self.canvas, v_wall, (padding,padding))    #left
        self.canvas = self._place_object(self.canvas, h_wall, (padding,self.wall_size[1]+padding))  #bottom
        self.canvas = self._place_object(self.canvas, v_wall, (self.wall_size[0]+padding,padding))  #right
        self.frame_dimensions = ((padding,padding), (padding,padding),(padding,self.wall_size[1]+padding),(self.wall_size[0]+padding,padding))
        return self.canvas

    def custom_block(self, size=(1,1), id=9):
        '''Generate a custom type block with custom id'''
        block = self._bbwall_block(size, id)
        return block

    def frame(self, padding=5):
        '''Places a box frame within the canvas with the specified padding and returns (canvas, frame_dimensions)'''
        if len(self.frame_dimensions) > 0:
            self.canvas = self._bbhouse_frame(padding)
            return (self.canvas, self.frame_dimensions)
        else:
            self.canvas = self._bbhouse_frame(padding)
            return (self.canvas, self.frame_dimensions)

    def to_vector(canvas, screen_size=(100,100),scale=16):
        '''Transforms a canvas object into a vector dictionary and places it in a dictionary based on its corresponding id
         {...,keyid:vector dictionary,...}'''
        ix = 0
        iy = 0
        U = {1:[],2:[],3:[],4:[],0:[]}
    #    xscale = list(range(0, screen_size[0], int(scale)))    
    #    yscale = list(range(0, screen_size[1], int(scale)))

        scaler = []
        for yyy in canvas:
            for xxx in yyy:
                if int(xxx) == 0:
                    U[0].append((int(ix*scale), int(iy*scale)))
                if int(xxx) == 1:
                    U[1].append((int(ix*scale), int(iy*scale)))
                if int(xxx) == 2:
                    U[2].append((int(ix*scale), int(iy*scale)))
                if int(xxx) == 3:
                    U[3].append((int(ix*scale), int(iy*scale)))
                if int(xxx) == 4:
                    U[4].append((int(ix*scale), int(iy*scale)))
                if int(xxx) > 4:
                    if int(xxx) not in U:
                        U[int(xxx)] = []
                        U[int(xxx)].append((int(ix*scale), int(iy*scale)))
                    elif int(xxx) in U:
                        U[int(xxx)].append((int(ix*scale), int(iy*scale)))
                ix+=1
            ix = 0
            iy+=1
        ##print(U)
        return U

    def _get_last_key(DICT):
        lk = list(DICT.keys())
        lk.sort()
        lk = lk[-1]
        return lk+1

    def _get_key_list(DICT):
        l = list(DICT.keys())
        return l

    def _get_sprite(_VECTOR_DICT, scale=16, color=arcade.color.HARVARD_CRIMSON, sprite=None):
        '''Returns a sprite list using the a vectorized id code dictionary'''
        print('getting sprite')
        sprite_list = arcade.SpriteList(is_static=True)
        slist = {}
        lk = bb_canvas._get_key_list(_VECTOR_DICT)
        for x in lk:
            if x in _VECTOR_DICT:
                for coord in _VECTOR_DICT[x]:
                    if sprite == None:
                        point = arcade.SpriteSolidColor(scale, scale, color)
                    elif sprite != None:
                        try:
                            point = arcade.Sprite(sprite[x])
                            point.height = scale
                            point.width = scale
                        except KeyError:
                            point=arcade.SpriteSolidColor(scale, scale, color)   
                    point.center_x = coord[0]
                    point.center_y = coord[1]
                    sprite_list.append(point)    
                slist[x] = sprite_list
                sprite_list = arcade.SpriteList(is_static=True)    
        return slist

    def view(self, viewport_origin=(0,0), viewport_size=24):
        '''Allows one to peek into the numpy array by specifying a viewport'''
        c = self.view_block(self.canvas, viewport_origin, tile_size=viewport_size)
        return c
 

from NNe import Images, Image
#i = Images.d_load_image('E:\\Essentials\\myfuckinggame\\countertopsroll.png')
#im = Images.sprite_buster64('E:\\Essentials\\myfuckinggame\\countertops256.png')
#imset = [im[0], im[1],im[2], im[5],im[8]]
#i = Images.roll_imgs(imset, 'E:\\Essentials\\myfuckinggame\\countertopsroll.png')
#i.show()
#f = Images.slice_grid(i[0], i[1])
def sprite_countertop():
    _class = 300
#lane = len(f)+_class
    t = {}
    ss = list(range(5,10))
    for bbb in ss:
        AAA = 'E:\\Essentials\\myfuckinggame\\usethese\\buildings\\furnature\\countertop\\'+str(_class+bbb)+'.png'
        #print(AAA)
        if AAA:
            t[_class+bbb] = AAA
    return t
t = sprite_countertop()

#DECLARE CANVAS AND GET MEASUREMENTS OF SCREEN AND SCALED DOWN CANVAS
brick = 'E:\\CODE\\PYTHON\\PythonScripts\\CustomPrereleases\\usethese\\top.png' #'E:\\SimLoli-master\\SimLoli-master\\resources\\img\\walls\\brick\\top.png'
grass = 'E:\\CODE\\PYTHON\\PythonScripts\\CustomPrereleases\\usethese\\grass.png'#'#'E:\\SimLoli-master\\SimLoli-master\\resources\\img\\tiles\\grass.png'
wood = 'E:\\CODE\\PYTHON\\PythonScripts\\CustomPrereleases\\usethese\\hardwood.png'#'E:\\SimLoli-master\\SimLoli-master\\resources\\img\\tiles\\hardwood.png'
yellwall = 'E:\\CODE\\PYTHON\\PythonScripts\\CustomPrereleases\\usethese\\wall_yellow.png'#'E:\\Essentials\\myfuckinggame\\wall_yellow.png'
fridge0 = 'E:\\CODE\\PYTHON\\PythonScripts\\CustomPrereleases\\usethese\\fridge0.png'#'E:\\Essentials\\myfuckinggame\\fridge0.png'
fridge1 = 'E:\\CODE\\PYTHON\\PythonScripts\\CustomPrereleases\\usethese\\fridge1.png'#'E:\\Essentials\\myfuckinggame\\fridge1.png'
buidling_material = {2:brick, 0:grass, 10:wood, 11:yellwall}

furniture = {98:fridge0, 99:fridge1}
furniture.update(t)

class soc_sim_house():
    '''Used to build the interior of houses and buildings within society simulator'''
    def __init__(self, screen_size=(100,100), scale=16, tile_scale=16, tile_dictionary=None):
        self.tiles = tile_dictionary
        self.scale = scale
        self.tile_scale = tile_scale
        self.r = 3
        self.screen_size = screen_size
        self.canvas = bb_canvas(self.screen_size, self.scale)
        self.house_frame = self.build()
        self.DICT = {}
        self.walls = None
        self.s = arcade.SpriteList()

    def build(self):
        '''Generates house frame template based on the screen size'''
        c = self.canvas.frame(5)
        self.canvas.place((0,0), c[0])
        midx = self.canvas.midx #int(self.canvas.wall_mid_point[0]) #wall_size[0]/2) #wall x length half measure
        midy = self.canvas.midy #int(self.canvas.wall_mid_point[1]) #wall_size[1]/2)    #wall y height half measure
        room_one = self.canvas.get_floor((midx-1,midy-1)) #bl
        room_two = self.canvas.get_floor((midx-1,midy))   #tl
        room_three = self.canvas.get_floor((midx,midy-1)) 
        room_four = self.canvas.get_floor((midx,midy))
        midv = self.canvas.max_wall('v')
        midh = self.canvas.max_wall('h')
        door = self.canvas.get_door()
        hdoor = self.canvas.get_door((4,1))
        vdoor = self.canvas.get_door((1,4))
        wp = self.canvas.get_wallpaper((midx-1,4))
        wpr = self.canvas.get_wallpaper((midx,4))
        
        porter = {0:self.canvas.custom_block((1,4), 100),   #vertical door
                1:self.canvas.custom_block((4,1), 100)}     #horizontal door
        pgeo = NN.p_geometry()
        r = pgeo.random_selection((0,1))
        #print(r)

    #    r1x= int((int((6*16)/self.screen_size[0]))*16)+6
    #    r1y = int((int((6*16)/self.screen_size[1]))*16)+6
#        r1x = int(6/16) * int(self.screen_size[0]/16)
#        r1y = int(6/16) * int(self.screen_size[1]/16)
        blc = c[1][0]
        brc = c[1][3]
       # #print('tid',c)
        tlc = c[1][2]
        trc = (blc[0]+midx+1,tlc[1])#-+midy+1)

        ##print(tlc)

        #PLACE FLOORS
        self.canvas.place(((blc[0]+1,blc[1]+1)),room_one) # bl
        self.canvas.place((tlc[0]+1,tlc[1]-midy),room_two) #tl
        self.canvas.place((brc[0]-midx,brc[1]+1),room_three) #br
        self.canvas.place((trc[0],trc[1]-midy),room_four) #tr

        #PLACE CENTER WALLS
        self.canvas.place((blc[0]+midx,blc[1]), midv)
        self.canvas.place((blc[0],blc[1]+midy), midh)

        #PLACE WALLPAPER
        self.canvas.place((trc[0],trc[1]-4), wpr) # TOP RIGHT
        self.canvas.place((tlc[0]+1,trc[1]-4), wp) # TOP LEFT
        self.canvas.place((blc[0]+1,tlc[1]-midy-5), wp) # bottom LEFT
        self.canvas.place((blc[0]+midx+1,tlc[1]-midy-5), wpr) # bottom right

        #PLACE DOORWAYS
        ex = self.canvas.get_floor((4,6))
        ey = self.canvas.get_floor((1,4))
        self.canvas.place((blc[0]+midx,blc[1]+int(midy*1.5)-2), ey)  #top center    
        self.canvas.place((blc[0]+midx,blc[1]+int(midy/2)-2), ey)  #bottom center
        self.canvas.place((blc[0]+int(midx/2),blc[1]+midy), ex) #left center
        
        self.canvas.place((blc[0]+int(midx/2),blc[1]+midy-5), ex) #left center

        Grass = np.random.randint(0,3, size=(10,10))
        self.canvas.place((trc[0]+10,trc[1]-5),Grass) #tr
        
        #DECLARE TELEPORTER
        if self.r == 3:
            if r == 0:
                self.r = r
                self.canvas.place((brc[0], brc[1]+2), porter[r])
            if r == 1:
                self.r = r
                self.canvas.place((blc[0]+midx+10,blc[1]), porter[r])
        
        if self.r == 1:
            self.canvas.place((blc[0]+midx+10,blc[1]), porter[self.r])
        elif self.r == 0:
            self.canvas.place((brc[0], brc[1]+2), porter[self.r])

        return self.canvas

    def vectorize(self):
        '''Turns canvas into vector dictionary'''
        self.DICT = bb_canvas.to_vector(self.canvas.canvas, self.screen_size, self.scale)
        return self.DICT

    def get_sprite(self):
        '''Returns a sprite based on the current canvas'''
        self.DICT = self.vectorize()
        sprite = bb_canvas._get_sprite(self.DICT, self.tile_scale, sprite=self.tiles, color=arcade.color.BLACK)
        self.walls = sprite[2]
        for x in sprite[11]:
            self.walls.append(x)
        return sprite

#    def update(self, screen_size, draw=False):
#        '''Adjusts the screen size and returns the updated canvas based sprite accordingly'''
#        if screen_size != self.screen_size:
#            self.screen_size = screen_size
#            self.canvas = bb_canvas(self.screen_size, self.scale)
#            self.canvas = self.build()
#            self.DICT = self.vectorize()
#            s = self.get_sprite()
#            if draw == True:
#                lk = bb_canvas._get_last_key(s)
#                for x in range(0, lk):
#                    s[x].draw()
#            return s

    def _update(self, screen_size, draw=False):
        '''Adjusts the screen size and returns the updated canvas based sprite accordingly'''
        if screen_size != self.screen_size:
            self.screen_size = screen_size
            self._gen()
            if draw == True:
                lk = bb_canvas._get_key_list(self.s)
                for x in lk:
                    self.s[x].draw()
            return self.s

    def _gen(self):
        if len(self.s)<=0 or self.canvas == None:
            self.canvas = bb_canvas(self.screen_size, self.scale)
            self.canvas = self.build()
            #self.DICT = self.vectorize()
            self.s = self.get_sprite()

    def update(self, screen_size):
        update = self._update(screen_size)
        if update:
            self.s = update

    def __draw__(self,keylist,length,index):
        if length > index:
            self.s[keylist[index]].draw()
            return
        else:
            return
    
    def _draw(self):
        self._gen()
        if len(self.s) > 0:
            lk = list(self.s.keys())#bb_canvas._get_last_key(self.s)
            lklen=len(lk)
            self.__draw__(lk,lklen,1)
            self.__draw__(lk,lklen,2)
            self.__draw__(lk,lklen,3)
            self.__draw__(lk,lklen,4)
            self.__draw__(lk,lklen,5)
            self.__draw__(lk,lklen,6)
            self.__draw__(lk,lklen,7)
            self.__draw__(lk,lklen,8)
            self.__draw__(lk,lklen,9)

            self.__draw__(lk,lklen,11)
            self.__draw__(lk,lklen,12)
            self.__draw__(lk,lklen,13)
            self.__draw__(lk,lklen,14)
            self.__draw__(lk,lklen,15)
            self.__draw__(lk,lklen,16)
            self.__draw__(lk,lklen,17)
            self.__draw__(lk,lklen,18)
            self.__draw__(lk,lklen,19)

            self.__draw__(lk,lklen,21)
            self.__draw__(lk,lklen,22)
            self.__draw__(lk,lklen,23)
            self.__draw__(lk,lklen,24)
            self.__draw__(lk,lklen,25)
            self.__draw__(lk,lklen,26)
            self.__draw__(lk,lklen,27)
            self.__draw__(lk,lklen,28)
            self.__draw__(lk,lklen,29)

            self.__draw__(lk,lklen,31)
            self.__draw__(lk,lklen,32)
            self.__draw__(lk,lklen,33)
            self.__draw__(lk,lklen,34)
            self.__draw__(lk,lklen,35)
            self.__draw__(lk,lklen,36)
            self.__draw__(lk,lklen,37)
            self.__draw__(lk,lklen,38)
            self.__draw__(lk,lklen,39)

            self.__draw__(lk,lklen,41)
            self.__draw__(lk,lklen,42)
            self.__draw__(lk,lklen,43)
            self.__draw__(lk,lklen,44)
            self.__draw__(lk,lklen,45)
            self.__draw__(lk,lklen,46)
            self.__draw__(lk,lklen,47)
            self.__draw__(lk,lklen,48)
            self.__draw__(lk,lklen,49)








class soc_sim_house_furnishings(soc_sim_house):
    '''Used to build the interior of houses and buildings within society simulator'''
    def __init__(self, screen_size=(100,100), scale=16, tile_scale=32, tile_dictionary=None):
        soc_sim_house.__init__(self, screen_size=screen_size, scale=scale, tile_scale=tile_scale, tile_dictionary=tile_dictionary)
        self.house_frame = self.build()
        self.tile_scale = scale*2
        self.tiles = tile_dictionary
        self.DICT = {}
        self.s = arcade.SpriteList()
        self.walls = None
        self.init = False
        self.counters = None

    def build(self):
        '''Generates house frame template based on the screen size'''
        c = self.canvas.frame(5)
        self.canvas.place((0,0), c[0])
        midx = self.canvas.midx #int(self.canvas.wall_mid_point[0]) #wall_size[0]/2) #wall x length half measure
        midy = self.canvas.midy #int(self.canvas.wall_mid_point[1]) #wall_size[1]/2)    #wall y height half measure
        room_one = self.canvas.get_floor((midx-1,midy-1)) #bl
        room_two = self.canvas.get_floor((midx-1,midy))   #tl
        room_three = self.canvas.get_floor((midx-1,midy-1)) 
        room_four = self.canvas.get_floor((midx-1,midy))
        midv = self.canvas.max_wall('v')
        midh = self.canvas.max_wall('h')
        door = self.canvas.get_door()
        hdoor = self.canvas.get_door((4,1))
        vdoor = self.canvas.get_door((1,4))
        wp = self.canvas.get_wallpaper((midx-1,4))
        porter = {0:self.canvas.custom_block((1,4), 100),   #vertical door
                1:self.canvas.custom_block((4,1), 100)}     #horizontal door
        pgeo = NN.p_geometry()
        r = pgeo.random_selection((0,1))
    #    #print(r)

    #    r1x= int((int((6*16)/self.screen_size[0]))*16)+6
    #    r1y = int((int((6*16)/self.screen_size[1]))*16)+6
#        r1x = int(6/16) * int(self.screen_size[0]/16)
#        r1y = int(6/16) * int(self.screen_size[1]/16)
        blc = c[1][0]
        brc = c[1][3]
       # #print('tid',c)
        tlc = c[1][2]
        trc = (blc[0]+midx+1,tlc[1])#-+midy+1)
        #print(tlc)
        fridge0 = self.canvas.custom_block((1,1), 98)
        fridge1 = self.canvas.custom_block((1,1), 99)
        counter_A = self.canvas.custom_block((1,1), 305)
        counter_B = self.canvas.custom_block((1,1), 306)
        counter_C = self.canvas.custom_block((1,1), 306)
        counter_D = self.canvas.custom_block((1,1), 306)
        counter_E = self.canvas.custom_block((10,1), 306)
        
        self.canvas.place((trc[0]+1,trc[1]-4),fridge0) #tr
        self.canvas.place((trc[0]+1,trc[1]-6),fridge1) #tr

        self.canvas.place((trc[0]+6,trc[1]-5),counter_A) #tr
        self.canvas.place((trc[0]+7,trc[1]-5),counter_B) #tr

        self.canvas.place((trc[0]+8,trc[1]-5),counter_C) #tr
        self.canvas.place((trc[0]+9,trc[1]-5),counter_D) #tr

        self.canvas.place((trc[0]+10,trc[1]-5),counter_E) #tr

        return self.canvas

    def vectorize(self):
        '''Turns canvas into vector dictionary'''
        self.DICT = bb_canvas.to_vector(self.canvas.canvas, self.screen_size, self.scale)
        return self.DICT

    def get_sprite(self):
        '''Returns a sprite based on the current canvas'''
        self.DICT = self.vectorize()
        sprite = bb_canvas._get_sprite(self.DICT, self.tile_scale, sprite=self.tiles, color=(0,0,0,0))
        self.s =sprite
        counters = [305, 306, 307, 309]
        counter_base =arcade.SpriteList()
        for x in counters:
            if x in sprite:
                for a in sprite[x]:
                    counter_base.append(a)
        self.counters = counter_base
        return sprite

    def _update(self, screen_size, draw=False):
        '''Adjusts the screen size and returns the updated canvas based sprite accordingly'''
        if screen_size != self.screen_size:
            print('updating screen')
            self.screen_size = screen_size
            self._gen()
        #    self.canvas = bb_canvas(self.screen_size, self.scale)
        #    self.canvas = self.build()
            #self.DICT = self.vectorize()
        #    self.s = self.get_sprite()
        #    self.walls = self.s[2]
            if draw == True:
                lk = bb_canvas._get_key_list(self.s)
                for x in lk:
                    self.s[x].draw()
            return self.s

    def update(self, screen_size):
        update = self._update(screen_size)
        if update:
            self.s = update
    #    else:
    #        lk = bb_canvas._get_last_key(self.s)
    #        for x in range(0, lk):
    #            if x in self.s:
    #                self.s[x].update()
        
    def _draw(self):
        self._gen()
        if len(self.s) > 0:
            #print(self.s)
            lk = bb_canvas._get_key_list(self.s)
            for x in lk:
                if x in self.s:
                    self.s[x].draw()
            return self.s


#from NNe import Images

#f = 'E:\\Essentials\\myfuckinggame\\countertops.png'
#s = Images.d_load_image(f)
#s =s[0].resize((16,16))
#s.show()
#Images._save_image(s, 'E:\\Essentials\\myfuckinggame\\countertops2.png')

class player_scroll():
    def __init__(self, player_sprite=arcade.Sprite(),screen_size=(800, 600)):
            self.changed = False
            self.view_left = 0
            self.view_bottom = 0
            self.player = player_sprite
            self.VIEWPORT_MARGIN = 100
            self.SCREEN_WIDTH = screen_size[0]
            self.SCREEN_HEIGHT = screen_size[1] 
                    
    def player_update_viewport(self, player=None):
        changed = False
        # Scroll left
        if player:
            self.player = player
        left_boundary = self.view_left + self.VIEWPORT_MARGIN
        if self.player.left < left_boundary:
            self.view_left -= left_boundary - self.player.left
            changed = True

        # Scroll right
        right_boundary = self.view_left + self.SCREEN_WIDTH - self.VIEWPORT_MARGIN
        if self.player.right > right_boundary:
            self.view_left += self.player.right - right_boundary
            changed = True
        # Scroll up
        top_boundary = self.view_bottom + self.SCREEN_HEIGHT - self.VIEWPORT_MARGIN
        if self.player.top > top_boundary:
            self.view_bottom += self.player.top - top_boundary
            changed = True

        # Scroll down
        bottom_boundary = self.view_bottom + self.VIEWPORT_MARGIN
        if self.player.bottom < bottom_boundary:
            self.view_bottom -= bottom_boundary - self.player.bottom
            changed = True

            # Make sure our boundaries are integer values. While the view port does
            # support floating point numbers, for this application we want every pixel
            # in the view port to map directly onto a pixel on the screen. We don't want
            # any rounding errors.
        self.view_left = int(self.view_left)
        self.view_bottom = int(self.view_bottom)

            # If we changed the boundary values, update the view port to match
        if changed:
            arcade.set_viewport(self.view_left, self.SCREEN_WIDTH + self.view_left,
                                self.view_bottom, self.SCREEN_HEIGHT + self.view_bottom)
            return (self.view_left, self.SCREEN_WIDTH + self.view_left,
                                self.view_bottom, self.SCREEN_HEIGHT + self.view_bottom)
#canvas.view((4,0))
##print(canvas)
#r = bb._upscale(screen_size, canvas.canvas)

def _upscale(screen_size, canvas):
    pass

#VECTOR_DICT = to_vector(canvas.canvas, screen_size)

#def draw_coord_dict(_VECTOR_DICT, scale=16, color=arcade.color.HARVARD_CRIMSON):
#    sprite_list = arcade.SpriteList()
#    for x in range(0,10):
#        for coord in _VECTOR_DICT[x]:
#            point = arcade.SpriteSolidColor(scale, scale, color)
#            point.center_x = coord[0]
#            point.center_y = coord[1]
#            sprite_list.append(point)            
#    return sprite_list

def pixel(size=(1,1), color=(255,255,255,255)):
    pixel = Image.new('RGBA',size,color)
    return pixel

def put_pixel(origin=(0,0),im1=None,im2=None):
    im1.paste(im2,origin)
    return im1

class new_image():
    def __init__(self,size=(1,1),bg_color=(0,0,0,0),type='RGBA'):
        self.height=size[1]
        self.width=size[0]
        self.size= size
        self.color = bg_color
        self.type = type
        self.image=Image.new(type,size,bg_color)
    
    def insert(self, size=(1,1), origin=(0,0), color=(0,0,0,0)):
        px = pixel(size, color)
        self.image.paste(px, origin)
        return self.image

    def draw_pixel(self, size=(1,1), origin=(0,0), color=(0,0,0,0)):
        e= self.insert(size, origin, color)
        return e

    def show(self):
        self.image.show()

from NN import p_geometry
import math
pgeo = p_geometry()
m = 19
#while m != 0:
#    print(round(m,400))
#    m = m/2
#x,y = 3,4
#a = np.arange(x*y).reshape(x,y)
#canvas = bb_canvas((256,256),1)
#canvas.canvas.dtype=np.float
#block = canvas.custom_block((10,10),9.5)
#canvas.place((1,1), block)
#print(a)
#print(canvas.canvas)
def get_diagonals_from_np_array(a):
    diags = [a[::-1,:].diagonal(i) for i in range(-a.shape[0]+1,a.shape[1])]
    diags.extend(a.diagonal(i) for i in range(a.shape[1]-1,-a.shape[0],-1))
    return [n.tolist() for n in diags]   

#c= get_diagonals_from_np_array(canvas.canvas)
#print(c[:20])
def create_a_new_image_with_verticle_fibonnaci_spaced_rectangle_pattern():
    im = new_image((256,256), (255,255,255,255))
    for x in range(21):                     #DECLARE AREAS OF WHERE TO PLACE BODY ITEMS
        i = pgeo.fib(x)
        im.draw_pixel(size=(100,10), origin=(int(im.width/3), i))

#ELECTRICIANS
#COMPUTER SCIENCE PHD
#ENGINEERS
#PR HR 

#COMPUTER BASED ON LIGHT PROJECTION
#TESLA CIRCUIT EXPLORATION
#WEB ARCHIVING
#P2P INTERNET PROTOCOL FOR ANONYMISATION
#LOCALIZED P2P INTERNET SHARING
#
#    67              95              90              64                94               94            45
#588252 580605  1610207 1599122 2386153 2350057 2965636 290624  2732104 2655392 2380946 2278123 108231  56849
x = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

px = []
#for x in range(1):
#    base = pixel((20,20), base_color)
#    px.append(base)
#center = (int(width/2), int(height/2))
#im = put_pixel(center, im, base)

#c = 0
#for x in px:
#    print(c)
#    center=(c+c+1,center[1]+c)
#    im = put_pixel(center,im1=im, im2=x)
#    c+=1

#print(canvas.canvas)
#im=Images.image_from_array(canvas.canvas,'RGBA')
def face():
    pass

def house_get(screen_size):
    VECTOR_DICT = to_vector(canvas.canvas, screen_size)
    SPRITES = draw_coord_dict(VECTOR_DICT)
    return (SPRITES, screen_size)
