import datetime
import time

def find_age(c_day,c_month,c_year,b_day,b_month,b_year):
    if b_day>c_day:
        c_month = c_month-1
        c_day = c_day+31
    if b_month>c_month:
        c_year = c_year - 1
        c_month = c_month+12
    calc_day  = c_day - b_day
    calc_month= c_month - b_month
    calc_year = c_year - b_year
    print('{} Year(s) {} Month(s) {} Day(s)'.format(calc_year,calc_month,calc_day))
    return calc_year,calc_month,calc_day

def _split_date(date:str):
    ndate = date.split('-')
    return int(ndate[0]),int(ndate[1]),int(ndate[2])

class dt():
    def age(dateA,dateB):
        '''Dates should be in a list [YYYY,MM,DD]'''
        return find_age(dateA[2],dateA[1],dateA[0],dateB[2],dateB[1],dateB[0])
    def datetime():
        '''Returns the current date and time in a string format'''
        return datetime.datetime.now()
    def timestamp():
        '''Returns the current unix time'''
        return time.time()
    def fromtimestamp(timestamp):
        '''Converts a timestamp to a date and time'''
        if timestamp:
            return datetime.datetime.fromtimestamp(timestamp)
    def date():
        '''Returns only the current date as a string '''
        return str(dt.datetime()).split(' ')[0]    
    def sdate():
        '''Returns only the current date as a list [YYYY,MM,DD]'''
        return str(dt.date()).split('-')
    def time():
        '''Returns only the current time in HH:MM:SS'''
        return str(dt.datetime()).split(' ')[1]

