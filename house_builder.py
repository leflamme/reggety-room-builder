from NNs import bb_canvas, block_builder,np
from NN import p_geometry

'''TODO:CREATE A DRAWABLE CANVAS
        CREATE NODES THAT CAN BE DISTRIBUTED AND PROGRAMMED
        TRY TO CREATE GEOMENTRY
        TRY TO FIND OTHER IDEAS TO EXPERIEMENT WITH NUMPY AND BLOCK BUILDER''' 
def _cog(pid=23):
        arr = np.array([[1,2,3],
                        [4,5,6],
                        [7,8,9]])
        arr = np.multiply(arr,pid)
        return arr

print(_cog())
def tile(arr,size:tuple):
        foreign = np.tile(arr,size)
        return foreign

#pid=23
def wall_dict(pid,objectlist):
        r = list(range(1,9))
        d={}
        for x in r:
                d[x*pid]=None

wall = {1:None,2:None,3:None,
        4:None,5:None,6:None,
        7:None,8:None,9:None}

#m=tile(_cog(),(2,2))
#canvas_object.place(np_object=m)

# g=canvas_object.get_column(_cog(),0)
# g=tile(g,(1,3))

#canvas_object.place(np_object=g)

#canvas_object.place((1,0),np_object=g)
#canvas_object.place((2,0),np_object=g)
#canvas_object.place((3,0),np_object=g)
#canvas_object.place((4,0),np_object=g)

#canvas_object.canvas[:]=g
# canvas_object.place((2,2),foreign)

# print(canvas_object.halfy)
#canvas_object.view()
def room_frame(canvas_object,x,y,width,height,wall_base_dict,wallpaper_dict):
        wpcodes=wallpaper_dict
        codes=wall_base_dict
        vwall=canvas_object.custom_block((1,height-2),codes['v'])
        hwall = canvas_object.custom_block((width-2,1),codes['h'])
        topwp = canvas_object.custom_block((width,1),wpcodes['wptop'])
        botwp = canvas_object.custom_block((width,1),wpcodes['wpbottom'])
        extwptop = canvas_object.custom_block((width,1),wpcodes['wpexttop'])
        extwpbot = canvas_object.custom_block((width,1),wpcodes['wpextbot'])
        halfh = canvas_object.custom_block((int(width-2/2),1),codes['h'])
        halfv = canvas_object.custom_block((1,int(height-2/2)),codes['v'])
        tlc = canvas_object.custom_block((1,1),codes['tl'])
        trc = canvas_object.custom_block((1,1),codes['tr'])
        blc = canvas_object.custom_block((1,1),codes['bl'])
        brc = canvas_object.custom_block((1,1),codes['br'])

        canvas_object.place((x,y),tlc)
        canvas_object.place((x+1,y),hwall)
        canvas_object.place((x,y+1),topwp)
        canvas_object.place((x,y+2),botwp)
        canvas_object.place((x+width-1,y),trc)
        canvas_object.place((x,y+1),vwall)
        canvas_object.place((x,y+height-1),blc)
        canvas_object.place((x+1,y+height-1),hwall)
        canvas_object.place((x,y+height),extwptop)
        canvas_object.place((x,y+height+1),extwpbot)
        canvas_object.place((x+width-1,y+height-1),brc)
        canvas_object.place((x+width-1,y+1),vwall)
        return canvas_object

#from NNe import Images
#wbases=Images.sprite_buster64('E:\\CODE\\PYTHON\\PythonScripts\\CustomPrereleases\\usethese\\buildings\\walls\\wall_base.png')

#tl=Images._flip_horizontal(Images._flip_vertical(wbases[9]))
#br=wbases[9]
#tr = Images._flip_vertical(wbases[9])
#bl = Images._flip_horizontal(wbases[9])
#h = wbases[2]
#v = wbases[5]

#p={'fourway':wbases[0],'threeway':wbases[1],'h':wbases[2],'rightend':wbases[3],'vstraight':wbases[5],'brcorner':wbases[9]}
base_codes={10000:'fourway',11000:'threeway',111000:'h',111100:'rightend',111110:'v',111111:'brcorner'}
codes={'v':10000,'h':10001,'tl':10002,'tr':10003,'bl':10004,'br':10005,'tri':30,'quad':40}
wpcodes={'wptop':90000,'wpbottom':90001,'wpexttop':90002,'wpextbot':90003}

def random_top_door(canvas_object,x,y,width,dwidth=2,pid=11):
        top_doorx = p_geometry().random_selection(list(range(x+1,width-2)))
        top_doory = [y,y+1,y+2]
        door_item=canvas_object.custom_block((dwidth,3),pid)
        canvas_object.place((top_doorx,top_doory[0]),door_item)
      #  canvas_object.place((top_doorx,top_doory[1]),door_item)
      #  canvas_object.place((top_doorx,top_doory[2]),door_item)
        return canvas_object

def random_bottom_door(canvas_object,x,y,width,height,dwidth=2,pid=11):
        top_doorx = p_geometry().random_selection(list(range(x+1,width-2)))
       # top_doory = [height-1,height+1,height+2]
        door_item=canvas_object.custom_block((dwidth,3),pid)
        canvas_object.place((top_doorx,height-1),door_item)
        #canvas_object.place((top_doorx,top_doory[1]),door_item)
       # canvas_object.place((top_doorx,top_doory[2]),door_item)
        return canvas_object

def random_left_door(canvas_object,x,innery,innerh,pid=11):
        top_doorx = x   #p_geometry().random_selection(list(range(x+1,width-2)))
        top_doory = p_geometry().random_selection(list(range(innery,innerh+1))) #[innery,innery+1,innery+2]
        door_item=canvas_object.custom_block((1,3),pid)
        canvas_object.place((top_doorx,top_doory),door_item)
        return canvas_object

def random_right_door(canvas_object,width,innery,pid=11):
        top_doorx = width-1   #p_geometry().random_selection(list(range(x+1,width-2)))
        top_doory = p_geometry().random_selection(list(range(innery,innerh+1))) #[innery,innery+1,innery+2]
        door_item=canvas_object.custom_block((1,3),pid)
        canvas_object.place((top_doorx,top_doory),door_item)
        return canvas_object

def get_point_value(canvas_object,x,y):
        return canvas_object.view((x,y),viewport_size=1,show=False)


#door=canvas_object.get_door()
class room():
        def __init__(self,x,y, width, height,scale=1,codebook={},wallpapers={}):
                self.x=x
                self.y=y
                self.width=width
                self.height=height
                self.scale=scale
                self.canvas=bb_canvas((self.width,self.height+2),self.scale)
                self.wpcodes=wallpapers
                self.inner_x = self.x+1
                self.inner_y = self.y+3
                self.inner_height = self.height-4
                self.inner_width = self.width-2
                self.codebook=codebook

        def set_wallpaper(self,wallpaper_dict:dict):
                self.wpcodes=wallpaper_dict
                return

        def auto_place_door(self,left=False,top=False,right=False,bottom=False):
                if left:
                        return random_left_door(self.canvas,self.x,self.inner_y,self.inner_height,11)
                if top:
                        return random_top_door(self.canvas,self.x,self.y,self.width,2)
                if right:
                        return random_right_door(self.canvas,self.width,self.inner_y)
                if bottom:
                        return random_bottom_door(self.canvas,self.x,self.y,self.width,self.height)
                        
        def manual_place_door(self,xy):
                self.door_location=xy
                door=self.canvas.custom_block((1,1),11)
                self.canvas.place((xy[0],xy[1]),door)
                return
        
        def update_floor(self):
                pass

        def _generate_room(self):
                room_frame(self.canvas,self.x,self.y,self.width,self.height,self.codebook,self.wpcodes)
                floorfill = self.canvas.get_floor((self.inner_width,self.inner_height))
                self.canvas.place((self.inner_x,self.inner_y),floorfill)
                return self.canvas

        def quick_room(self):
                self._generate_room()
                d= p_geometry().random_selection(['left','right','top','bottom'])
                if d=='left':
                        self.auto_place_door(left=True)
                if d=='right':
                        self.auto_place_door(right=True)
                if d=='top':
                        self.auto_place_door(top=True)
                if d=='bottom':
                        self.auto_place_door(bottom=True)
                return self.canvas

        def view(self):
                self.canvas.view()
        def show(self):
                self.view()
        def extend_left(self):
                pass
        def extend_right(self):
                pass
        def extend_top(self):
                pass
        def extend_bottom(self):
                pass
        def resize(self):
                pass
        def rotate(self):
                pass

x=0
y=0
height=12
width=5
innerx=x+1
innery=y+3
innerh=height-4
innerw=width-2
r1=room(x,y,width,height,1,codes,wpcodes)
r1.quick_room()
r2=room(x,y,width,height,1,codes,wpcodes)
r2.quick_room()

canvas_object = bb_canvas((width,height+2), 1)
canvas_object = room_frame(canvas_object,x,y,width,height,codes,wpcodes)
m = canvas_object.get_floor((innerw,innerh))
canvas_object.place((innerx,innery),m)

random_top_door(canvas_object,x,y,width)
random_bottom_door(canvas_object,x,y,width,height)
random_left_door(canvas_object,x,innery,innerh,11)
random_right_door(canvas_object,width,innery,11)

'''IF TRYING TO JOIN 2 ROOMS:
        SELECT 2 ROOMS TO JOIN,
        SELECT WHICH SIDE TO JOIN
        GET THE CORNERS OF THE ROOM
        A SINGLE ROOM HAS TWO SIDE CORNERS, THESE NEED TO BE CHANGED TO
        A THREE WAY
        TOP LEFT = (0,0) OR (X,Y)
        TOP RIGHT = (WIDTH,Y)
        BOTTOM LEFT = (X,HEIGHT)
        BOTTOM RIGHT = (WIDTH,HEIGHT)
        '''
def point_test(canvas_object,x,y,query):
        '''if point is 10004 or bottom right
           then point should be 10000
           if point'''
        if get_point_value(canvas_object,x,y)[0][0] == query:
                return True
        else:
                return False
def get_corners(room):
        aA = (room.x,room.y)
        aB = (room.width-1,room.y)
        aC = (room.width-1,room.height-1)
        aD = (room.x,room.height-1)
        return aA,aB,aC,aD

def test_points(room,points,codebook):
        tl = point_test(room.canvas,points[0][0],points[0][1],codebook['tl'])
        tr = point_test(room.canvas,points[1][0],points[1][1],codebook['tr'])
        br = point_test(room.canvas,points[2][0],points[2][1],codebook['br'])
        bl = point_test(room.canvas,points[3][0],points[3][1],codebook['bl'])
        return tl, tr,br,bl

def find_greatest(A,B):
        if A > B:
                return A
        elif B > A:
                return B
        elif A == B:
                return A



def merge_room(side='left',roomA:room=None,roomB:room=None,codebook:dict={}):
        aA, aB, aC, aD = get_corners(roomA)
        bA, bB, bC, bD = get_corners(roomB)
        #if all corners are in there orignal state all tests will be true
        #meaning it is available for 3 way only, additional testing needs to be done
        #in order to declare a 4way
        atl,atr,abr,abl = test_points(roomA,[aA,aB,aC,aD],codebook)
        btl,btr,bbr,bbl = test_points(roomB,[bA,bB,bC,bD],codebook)
        tri = roomA.canvas.custom_block((1,1),codebook['tri'])
        quad= roomA.canvas.custom_block((1,1),codebook['quad'])
        if atl and abl and btr and bbr and side=='left':
                leftside=True
                roomA.canvas.place((aA[0],aA[1]),tri)
                roomA.canvas.place((aA[0],aC[1]),tri)
                roomB.canvas.place((bB[0],bB[1]),tri)
                roomB.canvas.place((bB[0],bC[1]),tri)
                ht = find_greatest(roomA.canvas.max_height, roomB.canvas.max_height) #here we simply find the tallest room and use it as our height of the combined room
                ex = roomA.canvas.max_width + roomB.canvas.max_width - 1        #since we are adding to the left side we need to get the combined width of the rooms
                                                                                #we would use the same method if we were to add to the right side except we would replace room B with room A coordinates and room A with room B coordinates
                new_canvas = bb_canvas((ex,ht),1)                               
                new_canvas.place((roomA.canvas.max_width-1,0),roomA.canvas.canvas)
                new_canvas.place((0,0),roomB.canvas.canvas)
                return new_canvas
             #   new_canvas.view()
        if atl and atr and bbl and bbr and side == 'top':
                topside=True
                #test for 3 way or 4 way
                tln = point_test(room.canvas,points[0][0],points[0][1],codebook['tl'])
                trn = point_test(room.canvas,points[1][0],points[1][1],codebook['tr'])
                tlt = point_test(room.canvas,points[0][0],points[0][1],codebook['tri'])
                trt = point_test(room.canvas,points[1][0],points[1][1],codebook['tri'])
                tlq = point_test(room.canvas,points[0][0],points[0][1],codebook['quad'])
                trq = point_test(room.canvas,points[1][0],points[1][1],codebook['quad'])

        if abl and abr and btl and btr and side == 'bottom':
                bottomside=True
                #test for 3 way or 4 way
                bln = point_test(room.canvas,points[0][0],points[0][1],codebook['tl'])
                brn = point_test(room.canvas,points[1][0],points[1][1],codebook['tr'])
                blt = point_test(room.canvas,points[0][0],points[0][1],codebook['tri'])
                brt = point_test(room.canvas,points[1][0],points[1][1],codebook['tri'])
                blq = point_test(room.canvas,points[0][0],points[0][1],codebook['quad'])
                brq = point_test(room.canvas,points[1][0],points[1][1],codebook['quad'])

        if atr and abr and btl and bbl and side == 'right':
                rightside = True
                roomB.canvas.place((bA[0],bA[1]),tri)
                roomB.canvas.place((bA[0],bC[1]),tri)
                roomA.canvas.place((aB[0],aB[1]),tri)
                roomA.canvas.place((aB[0],aC[1]),tri)
                ht = find_greatest(roomA.canvas.max_height, roomB.canvas.max_height) #here we simply find the tallest room and use it as our height of the combined room
                ex = roomA.canvas.max_width + roomB.canvas.max_width - 1        #since we are adding to the left side we need to get the combined width of the rooms
                new_canvas = bb_canvas((ex,ht),1)                               
                new_canvas.place((roomB.canvas.max_width-1,0),roomB.canvas.canvas)
                new_canvas.place((0,0),roomA.canvas.canvas)
                return new_canvas
        
        pass

n=merge_room('left',r1,r2,codes)
n.view()
#SET EACH POINT OF THE SQUARE TO A VARIABLE
cA = (0,0)
cB = (width-1,0)
cC = (width-1,height-1)
cD = (0,height-1)

#TEST WHETHER EACH POINT IS IN ITS DEFUALT STATE
#WE CAN ALSO TEST OTHER CODES
tl=point_test(canvas_object,cA[0],cA[1],codes['tl'])
tr=point_test(canvas_object,cB[0],cB[1],codes['tr'])
br=point_test(canvas_object,cC[0],cC[1],codes['br'])
bl=point_test(canvas_object,cD[0],cD[1],codes['bl'])
leftside,topside,rightside,bottomside=False,False,False,False
tri = canvas_object.custom_block((1,1),codes['tri'])
quad = canvas_object.custom_block((1,1),codes['quad'])
#IF THE SIDE YUOU WANT TO ADD A ROOM TO IS IN ITS DEFAULT STATE
#THEN A NEW ROOM MAY BE JOINED TO THE EXISTING ROOM
#IF NOT IN DEFAULT STATE THEN IT IMPLIES THERE IS ALREADY A ROOM THERE AND 
#HENCE UNABLE TO ADD THE ROOM ERROR OCCURS

if tl and tr:
        topside=True
if br and bl:
        bottomside=True

if tl and bl:
        leftside=True

if tr and br:
        rightside=True
#IF THE LEFT SIDE IS AVAILABLE FOR AN ADDITIONAL WALL
#WE NEED TO TAKE THE WIDTH OF THE EXISIING WALL AND THE NEW WALL
#THEN CREATE A NEW CANVAS OBJECT USING THE WIDTH OF BOTH (IF IT WERE THE TOP OR BOTTOM WALL WE WOULD NEED THE HEIGHT)
if leftside:
        canvas_object.place((0,0),tri)
        canvas_object.place((0,height-1),tri)

        widthA = canvas_object.max_width
        widthB = canvas_object.max_width
        heightA = canvas_object.max_height
        heightB = canvas_object.max_height
        if heightA > heightB:
                heit = heightA
        elif heightB > heightA:
                heit = heightB
        elif heightA == heightB:
                heit = heightA
        g = widthA + widthB
        new_canvas = bb_canvas((g,heit),1)
        new_canvas.place((0,0),canvas_object.canvas)
        new_canvas.place((widthA,0),canvas_object.canvas)
  #      new_canvas.view()
if topside:
        canvas_object.place((0,0),tri)
        canvas_object.place((width-1,0),tri)

if rightside:
        canvas_object.place((width-1,0),tri)
        canvas_object.place((width-1,height-1),tri)
if bottomside:
        canvas_object.place((width-1,height-1),tri)
        canvas_object.place((0,height-1),tri)

if topside and rightside:
        canvas_object.place(cB,quad)
#         #pladce quad in top right corner
if topside and leftside:
#         #place quad in top left corner
        canvas_object.place(cA,quad)
if leftside and bottomside:
        canvas_object.place(cD,quad)
#         #place quad in bottom left corner
if rightside and bottomside:
        canvas_object.place(cC,quad)
#         #place quad in bottom right corner

#e=canvas_object.view()

class wall_base():
        def __init__(self):
                self.url='E:\\CODE\\PYTHON\\PythonScripts\\CustomPrereleases\\usethese\\buildings\\walls\\wall_base.png'
                self.x=0
                self.y=0
                