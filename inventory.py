'''release v0.01'''

class Inventory:
    '''Allows the creation of a multi purpose inventory, when an object is added it gets stacked into a the corresponding slot
    This is does not use any dictionaries. Rather a list of lists or items.'''
    def __init__(self, size=33):
        self.inventory = []
        for x in range(0, int(size)):
            self.inventory.append(None)

    def _put_item(self, item=None, index=None):
        '''Place an item into the inventory'''
        if index and item:
            s = self.inventory[int(index)]
            if type(s) == list:
                self.inventory[int(index)].append(item)
            if type(s) in (str, int) or not s:
                old = self.inventory[index]
                self.inventory[index] = []
                self.inventory[index].append(old)
                self.inventory[index].append(item)
                self.inventory[index].remove(None)
        elif item and index == None:
            if item in self.inventory or type(item) in self.inventory:
                try:
                    index = self.inventory.index(type(item))
                    print('index found at'+str(index))
                except ValueError:
                    index = self.inventory.index(item)

                old = self.inventory[index]
                self.inventory[index]= []
                self.inventory[index].append(old)
                self.inventory[index].append(item)
            elif item not in self.inventory:
                found = False
                for i in self.inventory:
                    if type(i) == list:
                        if item in i or type(item) in i:
                            i.append(item)
                            found = True
                            return
                if found == False:
                    if None in self.inventory:
                        n = self.inventory.index(None)
                        self.inventory[n] = item
                    else:
                        pass
            print(item,'added to inventory')
            return

    def put(self, item=None, index=None):
        self._put_item(item, index)
    
    def drop_from_inventory(self, item=None, index=None):
        '''Removes item from inventory without returning the item. The item is lost'''
        if (item, index) != (None, None):
            if index:
                self.inventory[index]=None
            else:
                index = self.inventory.index(item)
                self.inventory[index]=None
            print(item, 'has been dropped')
        elif (item, index) == (None, None):
            pass
      
    def _get_item(self, item=None, index=None):
        '''Removes and returns an item from the inventory.'''
        use = None
        if (item, index) != (None, None):
            if index:
                use = self.inventory[index]
                print(use, 'taken from inventory')
                return use
            else:
                for i in self.inventory:
                    g = self.inventory.index(i)
                    if type(i) in (list, tuple):
                        if item in i or type(item) in i:
                            index = i.index(item)
                            use = i.pop(index)
                            if len(i) == 0 or i == []:
                                i = None
                                self.inventory[g] = i
                print(use, 'taken from inventory')
                return use
        elif (item, index) == (None, None):
            pass
        

    def examine_inventory_item(self):
        pass


class ONE():
    pass

class TWO():
    pass

test = Inventory()
F = ONE()
C = ONE()
g = TWO()
#if type(F) == ONE:
#    F = type(F)

test.put(F)
test.put(C)
test.put(g)

print(test.inventory,type(F).__name__)